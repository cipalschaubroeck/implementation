import { Component, OnInit } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { ConfirmationService } from 'primeng/api';
import { ThemeSwitcher } from './shared/services/theme-switcher.service';
import { UserInfoService } from './user/service/user-info.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  topBarMenuActive: boolean;
  topBarItemClick: boolean;
  activeTopBarItem: any;

  constructor (private translateService: TranslateService,
               private confirmationService: ConfirmationService,
               private userInfoService: UserInfoService) {
  }

  onTopBarMenuButtonClick (event) {
    this.topBarItemClick = true;
    this.topBarMenuActive = !this.topBarMenuActive;

    event.preventDefault();
  }

  onTopBarItemClick (event, item) {
    this.topBarItemClick = true;
    this.activeTopBarItem = this.activeTopBarItem === item ? undefined : item;

    event.preventDefault();
  }

  changeLanguage (lang: string) {
    if (this.translateService.currentLang !== lang) {
      this.confirmationService.confirm({
        message: this.translateService.instant('CHANGE_LANGUAGE.AreYouSure'),
        header: this.translateService.instant('CHANGE_LANGUAGE.Header'),
        acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
        rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          localStorage.setItem('lang', lang);
          this.translateService.use(lang);
          location.reload();
        }
      });
    }
  }

  isBrowserOwner (): boolean {
    return window.self === window.top;
  }

  ngOnInit (): void {
    this.userInfoService.getUserTheme().subscribe(ThemeSwitcher.changeTheme);
  }
}
