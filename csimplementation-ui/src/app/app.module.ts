import { APP_BASE_HREF, PlatformLocation } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, Injector, LOCALE_ID, NgModule, Provider } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { TranslateCompiler, TranslateLoader, TranslateModule, TranslateService } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateMessageFormatCompiler } from 'ngx-translate-messageformat-compiler';
import { DialogService, MessageService } from 'primeng/api';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AdministrativeModule } from './contracts/management/administration/administrative.module';
// tslint:disable-next-line:max-line-length
import { AddItemComponent } from './contracts/management/administration/components/document-manager/components/document-process/dialogs/add-item/add-item.component';
// tslint:disable-next-line:max-line-length
import { DuplicateItemComponent } from './contracts/management/administration/components/document-manager/components/document-process/dialogs/duplicate-item/duplicate-item.component';
import { ContractManagementComponent } from './contracts/management/components/contract-management.component';
import { BudgetsComponent } from './contracts/management/initiate-contract/budgets/components/budgets/budgets.component';
import { ContractorComponent } from './contracts/management/initiate-contract/contractor/components/contractor/contractor.component';
// tslint:disable-next-line:max-line-length
import { ExecutionPeriodComponent } from './contracts/management/initiate-contract/execution-period/components/execution-period/execution-period.component';
import { InventoryComponent } from './contracts/management/initiate-contract/inventory/components/inventory/inventory.component';
// tslint:disable-next-line:max-line-length
import { PricesReviewComponent } from './contracts/management/initiate-contract/prices-review/components/prices-review/prices-review.component';
// tslint:disable-next-line:max-line-length
import { ValidateInitiationComponent } from './contracts/management/initiate-contract/validate-initiation/components/validate-initiation.component';
import { AccessOverviewComponent } from './contracts/overview/components/access-overview.component';
import { AddContractComponent } from './contracts/overview/dialogs/add-contract/add-contract.component';
import { SetStatusComponent } from './contracts/overview/dialogs/set-status/set-status.component';
import { RequestInterceptService } from './contracts/overview/services/request-intercept.service';
// tslint:disable-next-line:max-line-length
import { CollaboratorsDetailComponent } from './core/components/top-menu/collaborators/dialogs/collaborators-detail/collaborators-detail.component';
// tslint:disable-next-line:max-line-length
import { MenuContractingAuthorityComponent } from './core/components/top-menu/contracting-authority/components/menu-contracting-authority/menu-contracting-authority.component';
// tslint:disable-next-line:max-line-length
import { ContractingAuthorityDetailComponent } from './core/components/top-menu/contracting-authority/dialogs/contracting-authority-detail/contracting-authority-detail.component';
// tslint:disable-next-line:max-line-length
import { MenuOrganizationContactsComponent } from './core/components/top-menu/organization-contacts/components/menu-organization-contacts/menu-organization-contacts.component';
// tslint:disable-next-line:max-line-length
import { OrganizationContactsDetailComponent } from './core/components/top-menu/organization-contacts/dialogs/organization-contacts-detail/organization-contacts-detail.component';
import { CoreModule } from './core/core.module';
import { ConfigurationService } from './core/services/configuration.service';
import { appInitializer } from './core/utils/app-initializer';
import { DashboardComponent } from './dashboard/dashboard.component';
import { importKeycloakIfActive, provideKeycloakIfActive } from './keycloak/keycloak-initializer';
// tslint:disable-next-line:max-line-length
import { JointVentureDetailsComponent } from './main/core/components/top-menu/organization-contacts/components/joint-venture-details/joint-venture-details.component';
// tslint:disable-next-line:max-line-length
import { JointVentureMembersComponent } from './main/core/components/top-menu/organization-contacts/components/joint-venture-details/joint-venture-members/joint-venture-members.component';
import { GenericTreeService } from './shared/components/generic-tree/services/generic-tree.service';
import { DeleteConfirmationComponent } from './shared/dialogs/delete-confirmation/delete-confirmation.component';
import { WarningComponent } from './shared/dialogs/warning/warning.component';
import { AppInjector } from './shared/services/app-injector.service';
import { GenericMessageService } from './shared/services/generic-message.service';
import { SharedModule } from './shared/shared.module';
import { UserInfoService } from './user/service/user-info.service';

const providers: Provider[] = [
  {provide: LOCALE_ID, useValue: 'nl-BE'},
  ConfigurationService,
  TranslateService,
  DialogService,
  MessageService,
  GenericTreeService,
  UserInfoService,
  GenericMessageService,
  {
    provide: APP_BASE_HREF,
    useValue: window['base-href']
  },
  {
    provide: APP_INITIALIZER,
    useFactory: appInitializer,
    multi: true,
    deps: [
      ConfigurationService,
      HttpClient,
      PlatformLocation
    ]
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: RequestInterceptService,
    multi: true
  }
];
provideKeycloakIfActive(providers);

export function createTranslateLoader (http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const imports = [
  BrowserModule,
  HttpClientModule,
  SharedModule,
  CoreModule,
  AppRoutingModule,
  AdministrativeModule,
  TranslateModule.forRoot({
    loader: {
      provide: TranslateLoader,
      useFactory: createTranslateLoader,
      deps: [HttpClient]
    },
    compiler: {
      provide: TranslateCompiler,
      useClass: TranslateMessageFormatCompiler
    }
  })
];
const entryComponents = [
  SetStatusComponent,
  AddContractComponent,
  DeleteConfirmationComponent,
  WarningComponent,
  MenuContractingAuthorityComponent,
  ContractingAuthorityDetailComponent,
  CollaboratorsDetailComponent,
  MenuOrganizationContactsComponent,
  OrganizationContactsDetailComponent,
  AddItemComponent,
  DuplicateItemComponent,
  JointVentureDetailsComponent
];
importKeycloakIfActive(imports);

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    AccessOverviewComponent,
    SetStatusComponent,
    AddContractComponent,
    DeleteConfirmationComponent,
    WarningComponent,
    ContractManagementComponent,
    ValidateInitiationComponent,
    ContractorComponent,
    PricesReviewComponent,
    InventoryComponent,
    BudgetsComponent,
    ExecutionPeriodComponent,
    JointVentureDetailsComponent,
    JointVentureMembersComponent
  ],
  imports: imports,
  providers: providers,
  bootstrap: [AppComponent],
  entryComponents: entryComponents
})
export class AppModule {

  constructor (injector: Injector) {
    AppInjector.setInjector(injector);
  }
}

