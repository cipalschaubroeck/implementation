export interface QualificationInfoModel {
  id?: string;
  name: string;
  fixed: boolean;
  links?: {
    self: string;
  };
}
