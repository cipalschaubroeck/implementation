import { StatusType } from '../../contracts/common/enums/status-type.enum';
import { CollaboratorModel } from './collaborator-model';
import { InitiateContractAuthorityInfoModel } from './initiate-contract-authority-model';

export interface ContractInfoModel {
  id?: string;
  authority?: InitiateContractAuthorityInfoModel;
  mainContactId?: string;
  collaborators?: CollaboratorModel[];
  status?: StatusType;
  links?: {
    self: string;
  };
}
