export interface CollaboratorModel {
  id?: string;
  qualification?: string;
  lastName?: string;
  firstName?: string;
  department?: string;
  function?: string;
  competence?: string;
  startDate?: string;
  links?: {
    self?: string;
    contract?: string;
    authority?: string;
    qualificationFunction?: string
    collaborator?: string;
  };
}
