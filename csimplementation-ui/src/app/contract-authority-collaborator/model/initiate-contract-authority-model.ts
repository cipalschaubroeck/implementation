import { CollaboratorModel } from './collaborator-model';

export interface InitiateContractAuthorityInfoModel {
  name: string;
  collaborators: CollaboratorModel[];
  links?: {
    self: string;
  };
}
