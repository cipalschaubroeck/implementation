import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
// tslint:disable-next-line:max-line-length
import { AddEditCollaboratorComponent } from './components/add-edit-collaborator/add-edit-collaborator.component';
import { CollaboratorCompetenceComponent } from './components/collaborator-competence-table/collaborator-competence.component';
import { QualifiedPersonSelectionComponent } from './components/qualified-person-selection/qualified-person-selection.component';

const imports = [
  SharedModule
];
const components = [
  CollaboratorCompetenceComponent,
  AddEditCollaboratorComponent,
  QualifiedPersonSelectionComponent
];

const exports = [
  components
];

const entryComponents = [
  QualifiedPersonSelectionComponent,
  AddEditCollaboratorComponent
];

@NgModule({
  declarations: components,
  imports: imports,
  exports: exports,
  entryComponents: entryComponents
})
export class AuthorityCollaboratorSharedModule {
}
