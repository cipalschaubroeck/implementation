import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AuthorityCollaboratorSharedModule } from './authority-collaborator-shared.module';
// tslint:disable-next-line:max-line-length
import { InitiateAuthorityCollaboratorsRoutingModule } from './components/authority-collaborators-table/initiate/initiate-authority-collaborators-routing.module';
// tslint:disable-next-line:max-line-length
import { InitiateAuthorityCollaboratorsComponent } from './components/authority-collaborators-table/initiate/initiate-authority-collaborators.component';

const imports = [
  SharedModule,
  AuthorityCollaboratorSharedModule,
  InitiateAuthorityCollaboratorsRoutingModule
];
const components = [
  InitiateAuthorityCollaboratorsComponent
];

@NgModule({
  declarations: components,
  imports: imports
})
export class InitiateAuthorityCollaboratorModule {
}
