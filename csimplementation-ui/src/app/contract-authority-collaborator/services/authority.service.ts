import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../core/services/base-http.service';
import { ConstantsEnum } from '../../shared/enums/constants.enum';
import { CollaboratorModel } from '../model/collaborator-model';
import { ContractInfoModel } from '../model/contract-model';
import { InitiateContractAuthorityInfoModel } from '../model/initiate-contract-authority-model';
import { QualificationInfoModel } from '../model/qualification-model';

@Injectable({
  providedIn: 'root'
})
export class AuthorityService extends BaseHttpService<any> {

  private static mapContractAuthority (data: any): ContractInfoModel {
    return data ? {
      id: data.id,
      authority: AuthorityService.mapInitiateContractAuthority(data.authority),
      collaborators: AuthorityService.mapContractAuthorityCollaboratorQF(data),
      mainContactId: data.mainContactId,
      status: data.status,
      links: {
        self: AuthorityService.prepareLink(data._links.self.href)
      }
    } : null;
  }

  private static mapInitiateContractAuthority (data: any): InitiateContractAuthorityInfoModel {
    return data ? {
      name: data.name,
      collaborators: AuthorityService.mapAuthorityCollaborators(data),
      links: {
        self: AuthorityService.prepareLink(data._links.self.href)
      }
    } : null;
  }

  private static mapContractAuthorityCollaboratorQF (data: any): CollaboratorModel[] {
    const collaborators: CollaboratorModel[] = [];
    if (data && data.collaborators) {
      data.collaborators.forEach(collaborator => {
        collaborators.push({
          function: collaborator.function,
          department: collaborator.department,
          firstName: collaborator.firstName,
          lastName: collaborator.lastName,
          id: collaborator.id,
          qualification: collaborator.qualification,
          startDate: collaborator.startDate,
          links: {
            self: AuthorityService.prepareLink(collaborator._links.self.href),
            contract: data._links.self.href
          }
        });
      });
    }
    return collaborators;
  }

  private static mapAuthorityCollaborators (data: any): CollaboratorModel[] {

    const collaborators: CollaboratorModel[] = [];
    if (data && data.collaborators) {
      data.collaborators.forEach(collaborator => {
        collaborator.qualificationFunction.forEach(qf => {
          collaborators.push({
            function: qf.function,
            department: collaborator.department,
            firstName: collaborator.firstName,
            lastName: collaborator.lastName,
            id: qf.id,
            qualification: qf.qualification,
            startDate: qf.startDate,
            links: {
              self: AuthorityService.prepareLink(collaborator._links.self.href),
              authority: AuthorityService.prepareLink(data._links.self.href)
            }
          });
        });
      });
    }
    return collaborators;
  }

  private static prepareLink (link: string): string {
    return link.indexOf('{?projection}') !== -1 ?
      link.replace('{?projection}', '') : link;
  }

  private static setMainContact (mainContactId: string, collaboratorId: string): boolean {
    return mainContactId === collaboratorId;
  }

  getContractingAuthorities (): Observable<InitiateContractAuthorityInfoModel[]> {
    const params: HttpParams = new HttpParams()
      .set(ConstantsEnum.PROJECTION.valueOf(), 'authorityInfo');
    return this.httpClient.get<InitiateContractAuthorityInfoModel>(
      this.endpoint + 'contracting-authority', {params})
      .pipe(
        map((data: any) => {
          const authorities: InitiateContractAuthorityInfoModel[] = [];
          data._embedded.contractingAuthorities.forEach(authority => {
            authorities.push(AuthorityService.mapInitiateContractAuthority(authority));
          });
          return authorities;
        })
      );
  }

  createOrUpdateContractAuthorityRelation (
    contractingAuthority: InitiateContractAuthorityInfoModel, contractLink: String): Observable<any> {
    return this.httpClient.put(this.endpoint + 'contract-authority-relation',
      {
        'contract': contractLink,
        'contractingAuthority': contractingAuthority && contractingAuthority.links && contractingAuthority.links.self ?
          contractingAuthority.links.self : null
      });
  }

  getAuthorityCollaboratorsAssignedToContract (contractId: string): Promise<ContractInfoModel> {

    const headers = new HttpHeaders().set('Cache-Control', 'no-cache');
    const params: HttpParams = new HttpParams()
      .set(ConstantsEnum.PROJECTION.valueOf(), 'contractAuthorityCollaboratorsInfo');
    return new Promise(response => {
      this.httpClient.get<any>(this.endpoint + 'contract/' + contractId, {headers: headers, params: params})
        .pipe(map((contract: any) => {
          return AuthorityService.mapContractAuthority(contract);
        })).subscribe(response);
    });
  }

  patchMainContact (collaborator: CollaboratorModel): Observable<any> {
    return this.httpClient.patch<any>(collaborator.links.contract, {'mainAuthorityCollaboratorContact': collaborator.links.self});
  }

  patchDate (collaborator: CollaboratorModel, date: Date): Observable<any> {
    return this.httpClient.patch<any>(collaborator.links.self, {'startDate': date});
  }

  removeCollaborator (collaborator: CollaboratorModel): Observable<any> {
    return this.httpClient.delete(collaborator.links.self);
  }

  getQualifications (): Promise<QualificationInfoModel[]> {
    return new Promise(response => {
      this.httpClient.get(this.endpoint + 'qualification')
        .pipe(map((data: any) => {
          const qualifications: QualificationInfoModel[] = [];
          data._embedded.qualifications.forEach(qualification => {
            qualifications.push({
              name: qualification.name,
              fixed: qualification.fixed,
              links: {
                self: AuthorityService.prepareLink(qualification._links.self.href)
              }
            });
          });
          return qualifications;
        })).subscribe(response);
    });
  }

  getCollaboratorsByQualification (authorityLink: string, qualification: string): Promise<CollaboratorModel[]> {
    return new Promise(response => {

      const params: HttpParams = new HttpParams()
        .set(ConstantsEnum.PROJECTION.valueOf(), 'qualificationFunctionInfo')
        .set('qualification', qualification);

      this.httpClient.get(authorityLink + '/collaborators/filtered', {params}).pipe(
        map((authority: any) => {
          const collaborators: CollaboratorModel[] = [];
          authority._embedded.qualificationFunctions.forEach(col => {
            collaborators.push({
              firstName: col.firstName,
              lastName: col.lastName,
              function: col.function,
              department: col.department,
              qualification: col.qualification,
              links: {
                self: AuthorityService.prepareLink(col._links.self.href),
                collaborator: AuthorityService.prepareLink(col._links.collaborator.href)
              }
            });
          });
          return collaborators;
        })).subscribe(response);
    });
  }

  assignCollaboratorToContract (contractLink: string, qualificationFuncLink: string, competence: string) {
    return this.httpClient.post(contractLink + '/authority-collaborator-q-f/assignment', {
      'qualificationFunction': qualificationFuncLink,
      'competence': competence
    });
  }

  updateCollaboratorFromContract (collaborator: CollaboratorModel, qualificationFunctionLink: string, contractCollaborator: string) {
    return this.httpClient.post(collaborator.links.contract + '/authority-collaborator-q-f/assignment', {
      'qualificationFunction': qualificationFunctionLink,
      'competence': collaborator.competence,
      'collaborator': collaborator.links.collaborator,
      'contractCollaborator': contractCollaborator
    });
  }

  getCollaboratorDetail (collaborator: CollaboratorModel): Observable<CollaboratorModel> {
    const params: HttpParams = new HttpParams().set(ConstantsEnum.PROJECTION.valueOf(), 'contractAuthorityCollaboratorQFInfo');
    return this.httpClient.get(collaborator.links.self, {params}).pipe(
      map((col: any) => {
        return {
          firstName: col.firstName,
          department: col.department,
          lastName: col.lastName,
          function: col.function,
          startDate: col.startDate,
          qualification: col.qualification,
          id: col.id,
          competence: col.competence,
          links: {
            self: AuthorityService.prepareLink(col._links.self.href),
            qualificationFunction: AuthorityService.prepareLink(col._links['qualification-function'].href),
            collaborator: AuthorityService.prepareLink(col._links['collaborator'].href)
          }
        } as CollaboratorModel;
      }));
  }
}
