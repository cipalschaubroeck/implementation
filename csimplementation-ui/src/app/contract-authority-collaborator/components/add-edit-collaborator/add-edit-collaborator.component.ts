import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { Observable } from 'rxjs';
import { GenericMessageService } from '../../../shared/services/generic-message.service';
import { CollaboratorModel } from '../../model/collaborator-model';
import { ContractInfoModel } from '../../model/contract-model';
import { InitiateContractAuthorityInfoModel } from '../../model/initiate-contract-authority-model';
import { AuthorityService } from '../../services/authority.service';

@Component({
  selector: 'app-add-edit-collaborator',
  templateUrl: './add-edit-collaborator.component.html',
  styleUrls: ['./add-edit-collaborator.component.css']
})
export class AddEditCollaboratorComponent implements OnInit {

  constructor (public ref: DynamicDialogRef,
               public config: DynamicDialogConfig,
               private authorityService: AuthorityService,
               private messageService: GenericMessageService) {
  }

  /* true ? edit collaborator : save collaborator */
  private edit: boolean;
  private personForm: FormGroup;
  private qualificationOptions: SelectItem[] = [];
  private lastNameOptions: SelectItem[] = [];
  private formSubmitAttempt: boolean;
  private authority: InitiateContractAuthorityInfoModel;
  private contract: ContractInfoModel;
  private collaborators: CollaboratorModel[] = [];
  private collaborator: CollaboratorModel;
  private initialCollaboratorLink: string;

  private static initForm (): FormGroup {
    return new FormGroup({
      qualification: new FormControl('', Validators.required),
      competence: new FormControl({value: '', disabled: true}),
      lastName: new FormControl({value: '', disabled: true}, Validators.required),
      firstName: new FormControl({value: '', disabled: true}),
      department: new FormControl({value: '', disabled: true}),
      function: new FormControl({value: '', disabled: true})
    });
  }

  ngOnInit () {
    this.edit = this.config.data.edit;
    this.contract = this.config.data.contract;
    this.authority = this.config.data.authority.value;

    this.authorityService.getQualifications().then(data => {
      data.forEach(qualification => {
        this.qualificationOptions.push({
          value: qualification.name,
          label: qualification.name
        });
      });
      if (this.edit) {
        this.collaborator = this.config.data.collaborator;
        this.collaborator.links.contract = this.contract.links.self;
        this.authorityService.getCollaboratorsByQualification(this.authority.links.self, this.collaborator.qualification)
          .then(collaborators => {
            collaborators.forEach(collaborator => {
              this.lastNameOptions.push({
                label: collaborator.lastName + ', ' + collaborator.firstName
                  + ', ' + collaborator.department + ', ' + collaborator.function,
                value: {
                  lastName: collaborator.lastName,
                  firstName: collaborator.firstName,
                  function: collaborator.function,
                  department: collaborator.department,
                  links: {
                    self: collaborator.links.self,
                    collaborator: collaborator.links.collaborator
                  }

                } as CollaboratorModel
              });
            });
            this.initForm(this.collaborator);
          });
      } else {
        this.personForm = AddEditCollaboratorComponent.initForm();
      }
    });
  }

  private initForm (collaborator: CollaboratorModel) {
    this.authorityService.getCollaboratorDetail(collaborator).subscribe(col => {
      this.collaborator = col;
      this.initialCollaboratorLink = col.links.self;
      this.collaborator.links.contract = this.contract.links.self;

      this.personForm = new FormGroup({
        qualification: new FormControl(col.qualification, Validators.required),
        competence: new FormControl(col.competence),
        lastName: new FormControl({
          lastName: col.lastName,
          firstName: col.firstName,
          function: col.function,
          department: col.department,
          links: {
            self: col.links.qualificationFunction,
            collaborator: col.links.collaborator
          }
        } as CollaboratorModel, Validators.required),
        firstName: new FormControl({value: col.firstName, disabled: true}),
        department: new FormControl({value: col.department, disabled: true}),
        function: new FormControl({value: col.function, disabled: true})
      });
    });
  }

  private isFieldValid (field: string) {
    return this.personForm.get(field).invalid && (this.personForm.get(field).touched ||
      (this.personForm.get(field).untouched && this.personForm.get(field).invalid && this.formSubmitAttempt));
  }

  private validate () {
    this.formSubmitAttempt = true;
  }

  private cancel () {
    this.ref.close();
  }

  private save () {
    let ob: Observable<any>;
    if (this.edit) {
      const link = this.collaborator.links.qualificationFunction ?
        this.collaborator.links.qualificationFunction : this.collaborator.links.self;
      this.collaborator.competence = this.personForm.controls.competence.value;
      ob = this.authorityService.updateCollaboratorFromContract(this.collaborator, link, this.initialCollaboratorLink);
    } else {
      ob = this.authorityService.assignCollaboratorToContract(
        this.contract.links.self, this.collaborator.links.self, this.personForm.controls.competence.value);
    }
    ob.subscribe(() => {
      this.config.data.update(this.ref);
      this.messageService.success();
    }, error => {
      this.messageService.error();
    }, () => this.ref.close());

  }

  private loadQualifications () {
    return this.authorityService.getQualifications().then(data => {
      data.forEach(qualification => {
        this.qualificationOptions.push({
          value: qualification,
          label: qualification.name
        });
      });
    });
  }

  private onQualificationSelection () {
    this.personForm.controls.firstName.setValue('');
    this.personForm.controls.department.setValue('');
    this.personForm.controls.function.setValue('');
    this.personForm.controls.competence.setValue('');

    this.lastNameOptions = [];
    this.populateLastNameOptions(this.personForm.controls.qualification.value);
  }

  private populateLastNameOptions (qualification: string) {
    this.authorityService.getCollaboratorsByQualification(this.authority.links.self, qualification)
      .then(collaborators => {
        if (collaborators) {
          this.personForm.controls.competence.enable();
          this.personForm.controls.lastName.enable();
          collaborators.forEach(collaborator => {
            this.lastNameOptions.push({
              label: collaborator.lastName + ', ' + collaborator.firstName + ', ' + collaborator.department + ', ' + collaborator.function,
              value: {
                lastName: collaborator.lastName,
                firstName: collaborator.firstName,
                function: collaborator.function,
                department: collaborator.department,
                links: {
                  self: collaborator.links.self
                }
              } as CollaboratorModel
            });
          });
        }
      });
  }

  private onLastNameSelection () {
    this.collaborator = this.personForm.controls.lastName.value;
    if (this.edit && this.collaborator) {
      this.collaborator.links.contract = this.contract.links.self;
    }
    this.personForm.controls.firstName.setValue(this.collaborator ? this.collaborator.firstName : '');
    this.personForm.controls.department.setValue(this.collaborator ? this.collaborator.department : '');
    this.personForm.controls.function.setValue(this.collaborator ? this.collaborator.function : '');
  }

}
