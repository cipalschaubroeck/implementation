export enum QualifiedPersonTypeEnum {
  INTERNAL = 'INTERNAL',
  EXTERNAL = 'EXTERNAL'
}
