import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DialogService, DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { GenericTranslateService } from '../../../shared/services/generic-translate.service';
import { AddEditCollaboratorComponent } from '../add-edit-collaborator/add-edit-collaborator.component';
import { QualifiedPersonTypeEnum } from './model/qualified-person-type.enum';

@Component({
  selector: 'app-add-qualified-person',
  templateUrl: './qualified-person-selection.component.html',
  styleUrls: ['./qualified-person-selection.component.css']
})
export class QualifiedPersonSelectionComponent implements OnInit {

  constructor (
    public ref: DynamicDialogRef,
    public config: DynamicDialogConfig,
    private dialogService: DialogService,
    private translate: GenericTranslateService) {
  }

  private personType = QualifiedPersonTypeEnum;
  private qualifiedPersonForm: FormGroup;

  private static initForm (): FormGroup {
    return new FormGroup({
      selection: new FormControl('', Validators.required)
    });
  }

  ngOnInit () {
    this.qualifiedPersonForm = QualifiedPersonSelectionComponent.initForm();
  }

  private cancel () {
    this.ref.close();
  }

  private next () {
    this.ref.close();
    this.openCollaboratorDetail();

  }

  private openCollaboratorDetail () {
    const title = this.translate.translate('CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.addQualifiedPerson');
    this.dialogService.open(AddEditCollaboratorComponent, {
      header: title,
      data: {
        'contract': this.config.data.contract,
        'authority': this.config.data.authority,
        'update': this.config.data.update,
        'edit': false
      },
      width: '35%',
      contentStyle: {'min-height': '575px', 'overflow': 'visible'}
    });
  }
}
