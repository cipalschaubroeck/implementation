import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { FormStatusEnum } from '../../../../contracts/common/enums/formStatus.enum';
import { ContractInfoModel } from '../../../model/contract-model';
import { AuthorityCollaboratorsTable } from '../authority-collaborators-table';

@Component({
  selector: 'app-authority-collaborators',
  templateUrl: '../authority-collaborators.component.html',
  styleUrls: ['../authority-collaborators.component.css'],
  providers: [DialogService]
})
export class InitiateAuthorityCollaboratorsComponent extends AuthorityCollaboratorsTable implements OnInit {

  ngOnInit (): void {
    super.ngOnInit();
  }

  protected setFormComponents (contract: ContractInfoModel) {
    this.form = this.initForm(contract);
    this.selectedAuthority.next(this.form.controls.client.value);
    this.contract = contract;
    this.qualifiedPersonData.next(this.contract.collaborators);
    this.setButtonTableStatus();
    this.contractManagementService.emitInitiateContractAuthorityValidation(this.contract.links.self, null)
      .then(isComplete => {
        if (isComplete === FormStatusEnum.READ_ONLY) {
          this.form.disable();
          this.validButtons.next(true);
        }
      });
  }

  private emitInitiateContractAuthorityValidation () {
    this.contractManagementService.emitInitiateContractAuthorityValidation(this.contract.links.self, null)
      .then();
  }

  protected patchSubscription () {
    this.messageService.success();
    this.emitInitiateContractAuthorityValidation();
    this.updateTable();
  }
}
