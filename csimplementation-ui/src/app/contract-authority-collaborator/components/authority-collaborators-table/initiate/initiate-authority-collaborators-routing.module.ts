import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitiateAuthorityCollaboratorsComponent } from './initiate-authority-collaborators.component';

const routes: Routes = [
  {path: 'authority', component: InitiateAuthorityCollaboratorsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateAuthorityCollaboratorsRoutingModule {
}
