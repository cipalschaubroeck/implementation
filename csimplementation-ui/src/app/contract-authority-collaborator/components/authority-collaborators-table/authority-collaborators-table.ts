import { Injectable, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DialogService, DynamicDialogRef, SelectItem } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { ContractManagementService } from '../../../contracts/management/services/contract-management.service';
import { GenericTableComponent } from '../../../shared/components/generic-table/generic-table.component';
import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../shared/components/generic-table/models/generic-table.models';
import { GenericMessageService } from '../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../shared/services/generic-translate.service';
import { CollaboratorModel } from '../../model/collaborator-model';
import { ContractInfoModel } from '../../model/contract-model';
import { InitiateContractAuthorityInfoModel } from '../../model/initiate-contract-authority-model';
import { AuthorityService } from '../../services/authority.service';
import { AddEditCollaboratorComponent } from '../add-edit-collaborator/add-edit-collaborator.component';
import { QualifiedPersonSelectionComponent } from '../qualified-person-selection/qualified-person-selection.component';

@Injectable()
export abstract class AuthorityCollaboratorsTable implements OnInit {

  protected form: FormGroup;
  protected authorityOptions: SelectItem[] = [];
  protected contract: ContractInfoModel;
  protected selectedAuthority: BehaviorSubject<InitiateContractAuthorityInfoModel>
    = new BehaviorSubject(null);
  protected selectedCollaborator: BehaviorSubject<CollaboratorModel>
    = new BehaviorSubject(null);
  protected qualifiedPersonData: BehaviorSubject<CollaboratorModel[]>
    = new BehaviorSubject<CollaboratorModel[]>([]);
  protected cols: GenericTableColumnModel[];
  protected buttons: GenericTableButtonModel[];
  protected tableConfig: GenericTableConfigModel;
  protected formSubmitAttempt: boolean;
  protected validButtons: BehaviorSubject<boolean> = new BehaviorSubject(false);
  protected contractId: string;
  @ViewChild(GenericTableComponent, {static: false}) table: GenericTableComponent;

  protected abstract setFormComponents (contract: ContractInfoModel);

  protected abstract patchSubscription ();

  constructor (protected authorityService: AuthorityService,
               protected route: ActivatedRoute,
               protected messageService: GenericMessageService,
               protected contractManagementService: ContractManagementService,
               protected fb: FormBuilder,
               protected dialogService: DialogService,
               protected translate: GenericTranslateService) {
  }

  ngOnInit (): void {
    this.cols = [
      {
        field: 'qualification',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.qualification',
        hasSort: false
      },
      {
        field: 'lastName',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.lastName',
        hasSort: false
      },
      {
        field: 'firstName',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.firstName',
        hasSort: false
      },
      {
        field: 'department',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.department',
        hasSort: false
      },
      {
        field: 'function',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.function',
        hasSort: false
      },
      {
        field: 'mainContact',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.mainContact',
        hasSort: false
      },
      {
        field: 'startingDate',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.startDate',
        hasSort: false
      }
    ];
    this.buttons = [
      {
        icon: 'ui-icon-add',
        click: this.addQualifiedPerson,
        scope: this,
        disabled: this.validButtons
      },
      {
        icon: 'ui-icon-trash',
        click: this.removeQualifiedPerson,
        scope: this,
        disabled: this.validButtons
      },
      {
        icon: 'ui-icon-edit',
        click: this.editQualifiedPerson,
        scope: this,
        disabled: this.validButtons
      }
    ];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.noResults',
      hasSearchFilter: false,
      paginator: {rows: 5}
    };

    this.contractId = this.route.snapshot.parent.paramMap.get('contractId');
    this.getContractingAuthorities().then(authorities => {
      if (authorities) {
        this.authorityService.getAuthorityCollaboratorsAssignedToContract(this.contractId)
          .then((contract: ContractInfoModel) => {
            this.setFormComponents(contract);
          });
      }
    });
  }

  private updateTableFromDialog (ref: DynamicDialogRef) {
    this.updateTable();
    this.table.setSelection(null);
    this.selectedCollaborator.next(null);
  }

  private adjustForm (contract: ContractInfoModel) {
    const searchPattern = new RegExp('^radio');
    const searchPattern2 = new RegExp('^date');
    const keys = Object.keys(this.form.controls);
    keys.forEach(key => {
      if (searchPattern.test(key) || searchPattern2.test(key)) {
        this.form.removeControl(key);
      }
    });
    contract.collaborators.forEach(key => {
      this.form.addControl('radio' + key.id, new FormControl(contract.mainContactId));
      this.form.addControl('date' + key.id, new FormControl(contract.mainContactId));
    });
  }

  protected patchContractAuthorityRelation (field: string) {
    this.patchRelation(field).subscribe(() => {
      this.patchSubscription();
    }, error => {
      this.messageService.error();
    });
  }

  protected addQualifiedPerson () {
    const title = this.translate.translate('CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.addQualifiedPerson');
    this.dialogService.open(QualifiedPersonSelectionComponent, {
      header: title,
      data: {
        'contract': this.contract,
        'authority': this.selectedAuthority,
        'update': (ref: DynamicDialogRef) => this.updateTableFromDialog(ref)
      },
      width: '25%',
      contentStyle: {'min-height': '160px', 'overflow': 'visible'}
    });
  }

  protected removeQualifiedPerson () {
    if (this.selectedCollaborator.value) {
      this.authorityService.removeCollaborator(this.selectedCollaborator.value).subscribe(
        () => {
          this.updateTable();
          this.messageService.success();
        },
        () => this.messageService.error());
    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }

  protected editQualifiedPerson () {
    if (this.selectedCollaborator.value) {
      const title = this.translate.translate('CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.editQualifiedPerson');
      this.dialogService.open(AddEditCollaboratorComponent, {
        header: title,
        data: {
          'contract': this.contract,
          'authority': this.selectedAuthority,
          'collaborator': this.selectedCollaborator.value,
          'update': (ref: DynamicDialogRef) => this.updateTableFromDialog(ref),
          'edit': true
        },
        width: '35%',
        contentStyle: {'min-height': '575px', 'overflow': 'visible'}
      });

    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }

  protected patchRelation (field: string): Observable<any> {
    this.validButtons.next(!this.form.valid);
    this.selectedAuthority.next(this.form.controls[field].value);
    return this.authorityService.createOrUpdateContractAuthorityRelation(this.selectedAuthority.value, this.contract.links.self);
  }

  protected setMainContact (collaborator: CollaboratorModel) {
    if (!this.validButtons.getValue()) {
      this.authorityService.patchMainContact(collaborator).subscribe(
        () => this.messageService.success(),
        () => this.messageService.error());
    }
  }

  protected setStartDate (collaborator: CollaboratorModel, date: any) {
    this.authorityService.patchDate(collaborator, date).subscribe(
      () => this.messageService.success(), () => this.messageService.error());
  }

  protected isFieldValid (field: string) {
    return this.form.get(field).invalid && (this.form.get(field).touched ||
      (this.form.get(field).untouched && this.form.get(field).invalid && this.formSubmitAttempt));
  }

  protected setButtonTableStatus () {
    this.validButtons.next(!this.form.valid);
  }

  protected validate () {
    this.formSubmitAttempt = true;
    if (this.form.valid) {
      this.messageService.validated();
    } else {
      this.messageService.notValidated();
    }
  }

  protected getContractingAuthorities (): Promise<any> {
    return new Promise(response => {
      this.authorityService.getContractingAuthorities().subscribe(data => {
        data.forEach(authority => {
          this.authorityOptions.push({value: authority, label: authority.name});
        });
        response(this.authorityOptions);
      });
    });
  }

  protected initForm (contract: ContractInfoModel): FormGroup {
    const controls: [][] = [];
    controls['client'] = new FormControl(contract.authority, Validators.required);
    if (contract.collaborators) {
      contract.collaborators.forEach(key => {
        controls['radio' + key.id] = new FormControl(contract.mainContactId);
        controls['date' + key.id] = new FormControl(key.startDate ? new Date(key.startDate.trim()) : '');
      });
    }
    return this.fb.group(controls);
  }

  protected updateTable () {
    this.authorityService.getAuthorityCollaboratorsAssignedToContract(this.contractId)
      .then(contract => {
        this.adjustForm(contract);
        this.qualifiedPersonData.next(contract.collaborators);
      });
  }
}
