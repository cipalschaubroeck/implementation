import { Component, OnInit } from '@angular/core';
import { DialogService } from 'primeng/api';
import { StatusType } from '../../../../contracts/common/enums/status-type.enum';
import { ContractInfoModel } from '../../../model/contract-model';
import { AuthorityCollaboratorsTable } from '../authority-collaborators-table';

@Component({
  selector: 'app-authority-collaborators',
  templateUrl: '../authority-collaborators.component.html',
  styleUrls: ['../authority-collaborators.component.css'],
  providers: [DialogService]
})
export class AdminAuthorityCollaboratorsComponent extends AuthorityCollaboratorsTable implements OnInit {

  ngOnInit (): void {
    super.ngOnInit();
  }

  protected setFormComponents (contract: ContractInfoModel) {
    if (contract.status === StatusType.PREPARATION) {
      this.contract = {};
      this.validButtons.next(true);
    } else {
      this.contract = contract;
    }
    this.form = this.initForm(this.contract);
    this.selectedAuthority.next(this.form.controls.client.value);
    this.qualifiedPersonData.next(this.contract.collaborators);
    this.setButtonTableStatus();
    if (contract.status === StatusType.PREPARATION) {
      this.form.disable();
    }
  }

  protected patchSubscription () {
    this.messageService.success();
    this.updateTable();
  }

}
