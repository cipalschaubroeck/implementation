import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAuthorityCollaboratorsComponent } from './admin-authority-collaborators.component';

const routes: Routes = [
  {path: 'admin-authority', component: AdminAuthorityCollaboratorsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminAuthorityCollaboratorsRoutingModule {
}
