import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';
import { GenericTableComponent } from '../../../shared/components/generic-table/generic-table.component';
import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../shared/components/generic-table/models/generic-table.models';
import { CollaboratorModel } from '../../model/collaborator-model';
import { AuthorityService } from '../../services/authority.service';

@Component({
  selector: 'app-collaborator-competence',
  templateUrl: './collaborator-competence.component.html',
  styleUrls: ['./collaborator-competence.component.css']
})
export class CollaboratorCompetenceComponent implements OnInit, OnDestroy {

  private data: BehaviorSubject<any> = new BehaviorSubject([]);
  private selected$: BehaviorSubject<any>;
  private cols: GenericTableColumnModel[];
  private buttons: GenericTableButtonModel[];
  private tableConfig: GenericTableConfigModel;
  @Input() selectedCollaborator: BehaviorSubject<CollaboratorModel>;
  @ViewChild(GenericTableComponent, { static: true }) table: GenericTableComponent;
  private subs: Subscription[] = [];
  private title: string;

  constructor (private authorityService: AuthorityService) {
  }

  ngOnDestroy (): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  ngOnInit () {
    this.subs.push(this.selectedCollaborator.subscribe(c => {
      if (c) {
        this.authorityService.getCollaboratorDetail(c).subscribe(col => {
          this.data.next([col]);
          this.title = col.qualification;
        });
      } else {
        this.title = '';
        this.data.next([]);
      }
    }));
    this.cols = [
      {
        field: 'competence',
        text: 'CONTRACT.AUTHORITY.QUALIFIED_PERSONS_TABLE.competence',
        hasSort: false,
      }
    ];
    this.buttons = [];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      hasSearchFilter: false,
      selectionMode: 'none'
    };
  }

}
