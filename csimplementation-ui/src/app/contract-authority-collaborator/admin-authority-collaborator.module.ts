import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AuthorityCollaboratorSharedModule } from './authority-collaborator-shared.module';
// tslint:disable-next-line:max-line-length
import { AdminAuthorityCollaboratorsRoutingModule } from './components/authority-collaborators-table/admin/admin-authority-collaborators-routing.module';
// tslint:disable-next-line:max-line-length
import { AdminAuthorityCollaboratorsComponent } from './components/authority-collaborators-table/admin/admin-authority-collaborators.component';

const imports = [
  SharedModule,
  AuthorityCollaboratorSharedModule,
  AdminAuthorityCollaboratorsRoutingModule
];
const components = [
  AdminAuthorityCollaboratorsComponent
];

@NgModule({
  declarations: components,
  imports: imports
})
export class AdminAuthorityCollaboratorModule {
}
