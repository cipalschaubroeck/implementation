import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseHttpService } from '../../../../core/services/base-http.service';
import { Utils } from '../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../shared/models/page-model';
import { DocumentProcessModel } from '../components/document-manager/components/models/document-process-info';
import { DocumentProcessType } from '../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../models/administrative-documents.model';
import { DocumentLinkModel } from '../models/document-link.model';
import { EmailLinkModel } from '../models/email-link.model';
import { UploadLinkModel } from '../models/upload-link.model';

@Injectable()
export class AdministrativeDocumentsService extends BaseHttpService<AdministrativeDocumentsModel> {

  private contractId: string;
  pageModel: PageModel = {data: null, page: {totalNumber: 0}};
  selected$: BehaviorSubject<AdministrativeDocumentsModel> = new BehaviorSubject(null);

  private static generateDocumentLinks (links): DocumentLinkModel {
    return {
      addresseeList: Utils.cleanUrl(links.addresseeList.href),
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      self: Utils.cleanUrl(links.self.href),
      signingOfficerData: Utils.cleanUrl(links.signingOfficerData.href)
    };
  }

  private static generateEmailLinks (links): EmailLinkModel {
    return {
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      self: Utils.cleanUrl(links.self.href)
    };
  }

  private static generateUploadLinks (links): UploadLinkModel {
    return {
      self: Utils.cleanUrl(links.self.href)
    };
  }

  private static generateLinks (item): DocumentLinkModel | EmailLinkModel | UploadLinkModel {
    let links: DocumentLinkModel | EmailLinkModel | UploadLinkModel;
    if (item.type === DocumentProcessType.DOCUMENT) {
      links = AdministrativeDocumentsService.generateDocumentLinks(item._links);
    } else if (item.type === DocumentProcessType.EMAIL) {
      links = AdministrativeDocumentsService.generateEmailLinks(item._links);
    } else if (item.type === DocumentProcessType.UPLOAD) {
      links = AdministrativeDocumentsService.generateUploadLinks(item._links);
    } else {
      console.error('unknown document type ' + item.type);
    }
    return links;
  }

  getAllList (params: HttpParams) {
    params = params.append('projection', 'documentProcessInfo');
    return this.httpClient.get<AdministrativeDocumentsModel[]>(this.endpoint + 'contract/' + this.contractId + '/document-process/', {
      params
    }).pipe(
      map((data: any) => {
        const documentProcesses: AdministrativeDocumentsModel[] = [];
        for (const item of data._embedded.documentProcesses) {
          documentProcesses.push({
            id: item.id,
            name: item.name,
            date: item.date,
            status: item.status,
            type: item.type,
            links: AdministrativeDocumentsService.generateLinks(item)
          });
        }
        this.pageModel.page.totalNumber = data.page.totalElements;
        this.pageModel.data = documentProcesses;
        return this.pageModel;
      })
    );
  }

  duplicateItem (process: AdministrativeDocumentsModel, name: string): Observable<any> {
    return this.httpClient.post<AdministrativeDocumentsModel>(process.links.self, name);
  }

  removeItem (url: string): Observable<any> {
    return this.httpClient.delete<AdministrativeDocumentsModel>(url);
  }

  updateName (link: string, name: string) {
    return this.httpClient.patch<AdministrativeDocumentsModel>(link, {
      name: name
    }).pipe(map((data: any) => {
      return data.name;
    }));
  }

  setContractId (id: string) {
    this.contractId = id;
  }

  createDocumentProcess (documentProcess: DocumentProcessModel): Observable<any> {
    return this.httpClient.post(this.endpoint + 'document-process', {
      'contract': documentProcess.links.contract,
      'type': documentProcess.type,
      'name': documentProcess.name
    });
  }

  getContractLink (contractId: string): Promise<any> {
    const params = new HttpParams().set(ConstantsEnum.PROJECTION, 'contractStatusInfo');
    return new Promise<any>(response => {
      this.httpClient.get(this.endpoint + 'contract/' + contractId, {params: params}).subscribe(response);
    });
  }

}
