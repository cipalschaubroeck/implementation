import { TestBed } from '@angular/core/testing';

import { AdministrativeDocumentsService } from './administrative-documents.service';

describe('AdministrativeDocumentsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdministrativeDocumentsService = TestBed.get(AdministrativeDocumentsService);
    expect(service).toBeTruthy();
  });
});
