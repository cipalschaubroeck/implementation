import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseHttpService } from '../../../../../../../../../../../core/services/base-http.service';
import { Utils } from '../../../../../../../../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../../../../../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../../../../../../../../shared/models/page-model';
import { GenericMessageService } from '../../../../../../../../../../../shared/services/generic-message.service';
import { PostalAddresseeLinkModel, PostalAddresseeModel } from '../models/postal-addressee.model';

@Injectable()
export class PostalAddresseeService extends BaseHttpService<PostalAddresseeModel> {

  private processId: string;
  selected$: BehaviorSubject<PostalAddresseeModel> = new BehaviorSubject(null);
  list$: BehaviorSubject<PostalAddresseeModel[]> = new BehaviorSubject<PostalAddresseeModel[]>([]);
  pageModel: PageModel = {
    data: new BehaviorSubject<PostalAddresseeLinkModel[]>([]),
    page: {
      totalNumber: 0
    }
  };

  private static generateLinks (links): PostalAddresseeLinkModel {
    return {
      addressee: Utils.cleanUrl(links.addressee.href),
      addresseeReplacements: Utils.cleanUrl(links.addresseeReplacements.href),
      addresses: Utils.cleanUrl(links.addresses.href),
      emails: Utils.cleanUrl(links.emails.href),
      generatedDocument: Utils.cleanUrl(links.generatedDocument.href),
      itemInAssociation: Utils.cleanUrl(links.itemInAssociation.href),
      phoneNumbers: Utils.cleanUrl(links.phoneNumbers.href),
      postalAddressee: Utils.cleanUrl(links.postalAddressee.href),
      process: Utils.cleanUrl(links.process.href),
      self: Utils.cleanUrl(links.self.href)
    };
  }

  constructor (private http: HttpClient,
               protected messageService: GenericMessageService) {
    super(http, messageService);
  }

  getAllList (params: HttpParams) {

    params = params.append(ConstantsEnum.PROJECTION.valueOf(), 'postalAddresseeTableItem');

    return this.httpClient.get<PostalAddresseeModel[]>(this.endpoint + 'postal-document-process/' + this.processId + '/addressees', {
      params
    }).pipe(
      map((data: any) => {
        const addresses: PostalAddresseeModel[] = [];
        for (const item of data._embedded.postalAddressees) {
          addresses.push({
            companyName: item.companyName,
            email: item.email,
            personName: item.personName,
            phone: item.phone,
            links: PostalAddresseeService.generateLinks(item._links)
          });
        }
        this.pageModel.page.totalNumber = data.page.totalElements;
        this.pageModel.data = addresses;
        return this.pageModel;
      })
    );
  }

  removeItem () {
    if (this.selected$ !== null) {
      this.httpClient.delete(this.selected$.value.links.self).subscribe(() => {
        const addressList = this.list$.getValue();
        const indexToRemove = addressList.indexOf(this.selected$.value);
        addressList.splice(indexToRemove, 1);
        this.list$.next([...addressList]);
        this.selected$.next(null);
      }, error => {
        console.log(error);
      });
    }
  }

  setProcessId (processId) {
    this.processId = processId;
  }
}
