import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { ConfirmationService, DialogService } from 'primeng/api';

import { BehaviorSubject } from 'rxjs';
import { filter, mergeMap } from 'rxjs/operators';

import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../../../../../../../../shared/components/generic-table/models/generic-table.models';

import { DocumentProcessType } from '../../../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../../../../../../../models/administrative-documents.model';
import { DocumentLinkModel } from '../../../../../../../models/document-link.model';
import { AdministrativeDocumentsService } from '../../../../../../../services/administrative-documents.service';
import { UploadDialogComponent } from './components/upload-dialog/upload-dialog.component';
import { AttachmentModel } from './models/attachment.model';
import { AttachmentsService } from './services/attachments.service';

@Component({
  selector: 'app-attachments',
  templateUrl: './attachments.component.html',
  styleUrls: ['./attachments.component.css']
})
export class AttachmentsComponent implements OnInit {

  private attachments$: BehaviorSubject<AttachmentModel[]>;
  private selected$: BehaviorSubject<AttachmentModel>;
  private selected: AttachmentModel;
  private tableConfig: GenericTableConfigModel;
  private cols: GenericTableColumnModel[];
  private buttons: GenericTableButtonModel[];

  constructor (private attachmentsService: AttachmentsService,
               private confirmationService: ConfirmationService,
               private administrativeDocumentService: AdministrativeDocumentsService,
               private dialogService: DialogService,
               private translateService: TranslateService
  ) {
  }

  static linkFile () {
    // TODO CSPROC-1718 link file
  }

  ngOnInit () {
    this.selected = null;
    this.selected$ = this.attachmentsService.selected$;
    this.selected$.subscribe(value => this.selected = value);
    this.initAttachments();
    this.cols = [
      {
        field: 'name',
        text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.DocumentName',
        class: null,
        hasSort: true
      }
    ];
    this.buttons = [
      {
        icon: 'ui-icon-file-upload',
        title: null,
        click: this.uploadFile,
        scope: this
      },
      {
        icon: 'ui-icon-link',
        title: null,
        click: AttachmentsComponent.linkFile,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        title: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.RemoveItemTitle',
        click: this.confirmRemove,
        scope: this
      }
    ];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.NoResults',
      paginator: {
        rows: 10,
        rowsPerPageOptions: [10, 20, 30]
      }
    };
  }

  private initAttachments () {
    this.attachments$ = this.administrativeDocumentService.selected$
      .pipe(filter(documentSelected => documentSelected && documentSelected.type === DocumentProcessType.DOCUMENT))
      .pipe(mergeMap(documentSelected => this.getAllList(documentSelected))) as BehaviorSubject<AttachmentModel[]>;
  }

  private getAllList (documentSelected: AdministrativeDocumentsModel) {
    return this.attachmentsService.getAllListAttachments((<DocumentLinkModel>documentSelected.links).attachments);
  }

  uploadFile () {
    const title = this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.UPLOAD.Header');
    this.dialogService.open(UploadDialogComponent, {
      header: title,
      width: '35%'
    });
  }

  confirmRemove () {
    this.confirmationService.confirm({
      message: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.REMOVE.Message'),
      header: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ATTACHMENTS.REMOVE.Header'),
      acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
      rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.attachmentsService.removeItem();
      }
    });
  }
}
