import { StatusType } from '../../../../../../common/enums/status-type.enum';

export interface ContractDocumentProcess {
  status: StatusType;
  links?: {
    self?: string;
  };
}
