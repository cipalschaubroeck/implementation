import { DocumentProcessType } from '../../../../enums/document-process-type.enum';

export interface DocumentProcessModel {
  type: DocumentProcessType;
  name: string;
  links?: {
    self?: string;
    contract?: string;
  };
}
