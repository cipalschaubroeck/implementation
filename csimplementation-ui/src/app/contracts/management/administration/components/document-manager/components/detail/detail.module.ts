import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { DocumentModule } from './components/document/document.module';
import { EmailModule } from './components/email/email.module';
import { UploadModule } from './components/upload/upload.module';
import { DetailComponent } from './detail.component';

const components = [
  DetailComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    DocumentModule,
    EmailModule,
    UploadModule,
    SharedModule,
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class DetailModule {
}
