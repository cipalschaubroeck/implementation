import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { AdministrativeDocumentsModel } from '../../../../../../models/administrative-documents.model';

import { AdministrativeDocumentsService } from '../../../../../../services/administrative-documents.service';
import { AdministrativeDocumentsFormGroup } from '../../administrative-documents-form-group';

@Component({
  selector: 'app-duplicate-item-dialog',
  templateUrl: './duplicate-item.component.html',
  styleUrls: ['./duplicate-item.component.css']
})
export class DuplicateItemComponent implements OnInit {

  private formDuplicate: AdministrativeDocumentsFormGroup;
  private selected: AdministrativeDocumentsModel;

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService,
               public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit () {
    this.formDuplicate = new AdministrativeDocumentsFormGroup();
    this.formDuplicate.removeValidators('type', []);
  }

  duplicateItem () {
    this.selected = this.config.data.selected;
    if (!this.selected) {
      throw new Error('No selected process has been passed');
    }
    this.administrativeDocumentsService.duplicateItem(this.selected, this.formDuplicate.name.value)
      .subscribe(() => {
        this.closeDialog();
      });
  }

  closeDialog () {
    this.config.data.update();
    this.ref.close();
  }

}
