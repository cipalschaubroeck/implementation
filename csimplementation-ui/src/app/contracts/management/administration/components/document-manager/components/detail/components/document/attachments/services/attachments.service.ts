import { HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { BaseHttpService } from '../../../../../../../../../../../core/services/base-http.service';
import { Utils } from '../../../../../../../../../../../shared/classes/utils';
import { PageModel } from '../../../../../../../../../../../shared/models/page-model';
import { AttachmentLinkModel, AttachmentModel } from '../models/attachment.model';

@Injectable()
export class AttachmentsService extends BaseHttpService<AttachmentModel> {
  selected$: BehaviorSubject<AttachmentModel> = new BehaviorSubject(null);
  list$: BehaviorSubject<AttachmentModel[]> = new BehaviorSubject<AttachmentModel[]>([]);
  showUploadDialog$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  attachmentsUrl: string;
  pageModel: PageModel = {
    data: new BehaviorSubject<AttachmentModel[]>([]),
    page: {
      totalNumber: 0
    }
  };

  private static generateLinks (links): AttachmentLinkModel {
    return {
      itemInAssociation: Utils.cleanUrl(links.itemInAssociation.href),
      self: Utils.cleanUrl(links.self.href)
    };
  }

  getAllList (): PageModel {
    // TODO CSPROC-1655 Remove params for this issue
    this.httpClient.get<AttachmentModel[]>(this.attachmentsUrl, {
      params: {
        size: '100'
      }
    }).pipe(
      map((data: any) => {
        const attachments: AttachmentModel[] = [];
        for (const item of data._embedded.contentManagerFiles) {
          attachments.push({
            name: item.filename,
            links: AttachmentsService.generateLinks(item._links)
          });
        }
        return attachments;
      })
    ).subscribe(attachments => {
      this.list$.next(attachments);
      this.selected$.next(null);
    }, error => {
      console.log(error);
    });

    this.pageModel.data = this.list$;
    return this.pageModel;
  }

  getAllListAttachments (attachmentsUrl: string): BehaviorSubject<AttachmentModel[]> {
    this.attachmentsUrl = attachmentsUrl;
    return this.getAllList().data;
  }

  removeItem () {
    if (this.selected$ !== null) {
      this.httpClient.delete(this.selected$.value.links.itemInAssociation).subscribe(() => {
        const attachmentsList = this.list$.getValue();
        const indexToRemove = attachmentsList.indexOf(this.selected$.value);
        attachmentsList.splice(indexToRemove, 1);
        this.list$.next([...attachmentsList]);
        this.selected$.next(null);
      }, error => {
        console.log(error);
      });
    }
  }

  uploadFiles (uploadUrl: string, files: File[]): Observable<HttpEvent<any>> {
    const formData = new FormData();
    for (const file of files) {
      formData.append('files', file);
    }
    return this.httpClient.post(uploadUrl, formData, {
      observe: 'events',
      reportProgress: true
    });
  }

  uploadFilesSuccess (data: any) {
    const attachmentsList = this.list$.getValue();
    for (const item of data._embedded.contentManagerFiles) {
      attachmentsList.push({
        name: item.filename,
        links: AttachmentsService.generateLinks(item._links)
      });
    }

    this.list$.next([...attachmentsList]);
    this.selected$.next(null);
  }

}
