import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../../../../../../../core/services/base-http.service';
import { Utils } from '../../../../../../../../../shared/classes/utils';
import { DocumentProcessType } from '../../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../../../../../../models/administrative-documents.model';
import { DocumentLinkModel } from '../../../../../../models/document-link.model';
import { EmailLinkModel } from '../../../../../../models/email-link.model';
import { UploadLinkModel } from '../../../../../../models/upload-link.model';

@Injectable({
  providedIn: 'root'
})
export class ProcessDetailService extends BaseHttpService<AdministrativeDocumentsModel> {

  private static generateLinks (item): DocumentLinkModel | EmailLinkModel | UploadLinkModel {
    let links: DocumentLinkModel | EmailLinkModel | UploadLinkModel;
    if (item.type === DocumentProcessType.DOCUMENT) {
      links = ProcessDetailService.generateDocumentLinks(item._links);
    } else if (item.type === DocumentProcessType.EMAIL) {
      links = ProcessDetailService.generateEmailLinks(item._links);
    } else if (item.type === DocumentProcessType.UPLOAD) {
      links = ProcessDetailService.generateUploadLinks(item._links);
    } else {
      console.error('unknown document type ' + item.type);
    }
    return links;
  }

  private static generateDocumentLinks (links): DocumentLinkModel {
    return {
      addresseeList: Utils.cleanUrl(links.addresseeList.href),
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      self: Utils.cleanUrl(links.self.href),
      signingOfficerData: Utils.cleanUrl(links.signingOfficerData.href)
    };
  }

  private static generateEmailLinks (links): EmailLinkModel {
    return {
      attachments: Utils.cleanUrl(links.attachments.href),
      body: Utils.cleanUrl(links.body.href),
      self: Utils.cleanUrl(links.self.href)
    };
  }

  private static generateUploadLinks (links): UploadLinkModel {
    return {
      self: Utils.cleanUrl(links.self.href)
    };
  }

  getDocumentProcess (documentId: string) {
    return this.httpClient.get<AdministrativeDocumentsModel>(this.endpoint + 'document-process/' + documentId).pipe(
      map((documentProcess: any) => {
        const processDocument: AdministrativeDocumentsModel = {
          id: documentProcess.id,
          name: documentProcess.name,
          date: documentProcess.date,
          status: documentProcess.status,
          type: documentProcess.type,
          links: ProcessDetailService.generateLinks(documentProcess)
        };
        return processDocument;
      })
    );
  }
}
