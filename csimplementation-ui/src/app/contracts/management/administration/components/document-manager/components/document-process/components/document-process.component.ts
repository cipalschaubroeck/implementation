import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ConfirmationService, DialogService } from 'primeng/api';

import { BehaviorSubject, Subscription } from 'rxjs';
import { GenericTableLazyComponent } from '../../../../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import {
  GenericTableLazyButtonModel,
  GenericTableLazyFilterModel
} from '../../../../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { FilterType } from '../../../../../../../../shared/enums/filter-type.enum';
import { GenericMessageService } from '../../../../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../../../../shared/services/generic-translate.service';
import { StatusType } from '../../../../../../../common/enums/status-type.enum';
import { DocumentProcessType } from '../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsModel } from '../../../../../models/administrative-documents.model';
import { AdministrativeDocumentsService } from '../../../../../services/administrative-documents.service';
import { ContractDocumentProcess } from '../../models/contract-document-process-info';
import { AddItemComponent } from '../dialogs/add-item/add-item.component';
import { DuplicateItemComponent } from '../dialogs/duplicate-item/duplicate-item.component';

@Component({
  selector: 'app-document-process',
  templateUrl: './document-process.component.html',
  styleUrls: ['./document-process.component.css']
})
export class DocumentProcessComponent implements OnInit, OnDestroy {

  private documentTypeOptions: BehaviorSubject<GenericTableLazyFilterModel[]>
    = new BehaviorSubject<GenericTableLazyFilterModel[]>([]);
  private contract: ContractDocumentProcess;
  documents$: BehaviorSubject<AdministrativeDocumentsModel[]> = new BehaviorSubject([]);
  selected: AdministrativeDocumentsModel;
  rightButtons: GenericTableLazyButtonModel[] = [];
  leftButtons: GenericTableLazyButtonModel[] = [];
  subscriptions: Subscription[] = [];
  @ViewChild('sideBaseTable', { static: true }) table: GenericTableLazyComponent;

  private contractId: string;

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService,
               private confirmationService: ConfirmationService,
               private route: ActivatedRoute,
               private router: Router,
               private translateService: GenericTranslateService,
               private dialogService: DialogService,
               private messageService: GenericMessageService) {
  }

  ngOnDestroy (): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {
    this.contractId = this.route.snapshot.parent.paramMap.get('contractId');
    /**
     * This sets contractId in the service so that once the onLazyLoad method in GenericTable is activated it knows
     * how to filter by contract.
     * This is done this way because routeParams cant be accessed from the service.
     */
    this.administrativeDocumentsService.setContractId(this.contractId);

    this.administrativeDocumentsService.getContractLink(this.contractId).then(response => {
      this.contract = {
        status: response.status,
        links: {
          self: response._links.self.href
        }
      };
      GenericTableLazyComponent.loadButton('add', 'ui-icon-plus', '',
        this.showAddDialog, this, this.contract.status === StatusType.PREPARATION, this.leftButtons);
      GenericTableLazyComponent.loadButton('copy', 'ui-icon-content-copy', 'ADMINISTRATIVE.DOCUMENTS.DuplicateItemTitle',
        this.showDuplicateDialog, this, this.contract.status === StatusType.PREPARATION, this.leftButtons);
      GenericTableLazyComponent.loadButton('delete', 'ui-icon-trash', 'ADMINISTRATIVE.DOCUMENTS.RemoveItemTitle',
        this.confirmRemove, this, this.contract.status === StatusType.PREPARATION, this.leftButtons);
      GenericTableLazyComponent.loadButton('hideShowFilters', 'ui-icon-filter', 'COMMON.BUTTONS.HideShow',
        this.showFilters, this, this.contract.status === StatusType.PREPARATION, this.leftButtons);

    });

    this.subscriptions.push(this.table.selectedRowBehaviorSubject.subscribe(selected => {
      this.selected = selected;
    }));

    this.subscriptions.push(this.table.selectedRowBehaviorSubject.subscribe(selected => {
      this.administrativeDocumentsService.selected$.next(selected);
      /**
       * if a document process is selected it will route to its detail
       */
      if (selected && Object.keys(selected).length > 0) {
        this.router.navigate(['./process-detail', selected.id], {relativeTo: this.route}).then(e => {
          if (!e) {
            console.log('Navigation failed', e);
          }
        });
        /**
         * if a document process is not selected it will root to admin-documents
         */
      } else {
        this.subscriptions.push(this.route.parent.url.subscribe(url => {
          this.router.navigate([url[0].path + '/' + url[1].path + '/admin-documents']).then(e => {
            if (!e) {
              // void
            }
          });
        }));
      }
    }));

    this.documentTypeOptions.next([{
      label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Document',
      value: DocumentProcessType.DOCUMENT
    },
      {
        label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Email',
        value: DocumentProcessType.EMAIL
      },
      {
        label: 'ADMINISTRATIVE.DOCUMENTS.FILTER.Upload',
        value: DocumentProcessType.UPLOAD
      }]);
    this.table.initTableConfig({
      columns: [
        {
          field: 'date',
          text: 'ADMINISTRATIVE.DOCUMENTS.Date',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'name',
          text: 'ADMINISTRATIVE.DOCUMENTS.DocumentProcess',
          class: null,
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'status',
          text: 'ADMINISTRATIVE.DOCUMENTS.Status',
          class: 'no-padding icon-size',
          hasSort: false,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'type',
          text: 'ADMINISTRATIVE.DOCUMENTS.DocumentType',
          class: 'no-padding',
          hasSort: false,
          filter: {
            type: FilterType.MULTI_SELECT,
            data: this.documentTypeOptions
          }
        }
      ],
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'OVERVIEW.NoResults',
      paginator: {
        rows: 5
      },
      sortField: ('date'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.administrativeDocumentsService);
    this.table.initButtons(this.rightButtons, this.leftButtons);
  }

  private confirmRemove () {
    if (this.selected) {
      this.confirmationService.confirm({
        message: this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.REMOVE.Message'),
        header: this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.REMOVE.Header'),
        acceptLabel: this.translateService.translate('COMMON.BUTTONS.Confirm'),
        rejectLabel: this.translateService.translate('COMMON.BUTTONS.Cancel'),
        icon: 'pi pi-exclamation-triangle',
        accept: () => {
          this.administrativeDocumentsService.removeItem(this.selected.links.self).subscribe(() => {
            this.table.onRowUnSelection();
            this.table.filterTable('', '', '');
          });
        }
      });
    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }

  private showFilters () {
    this.table.showFilters();
  }

  private showAddDialog () {
    const title = this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.ADD.Header');
    this.dialogService.open(AddItemComponent, {
      header: title,
      width: '300px',
      data: {
        'update': () => this.updateTable(),
        'contract': this.contract
      },
      contentStyle: {'padding-top': '20px'}
    });
  }

  private updateTable () {
    this.table.filterTable('', '', '');
    this.table.onRowUnSelection();
    this.table.selection = null;
  }

  private showDuplicateDialog () {
    this.selected = this.table.selectedRowBehaviorSubject.getValue();
    if (this.selected) {
      const title = this.translateService.translate(('ADMINISTRATIVE.DOCUMENTS.DUPLICATE.Header'));
      this.dialogService.open(DuplicateItemComponent, {
        header: title,
        width: '300px',
        data: {
          'selected': this.selected,
          'update': () => this.updateTable()
        },
        contentStyle: {'padding-top': '20px'}
      });
    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }
}
