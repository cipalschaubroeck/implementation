import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../../../../services/administrative-documents.service';
import { UploadComponent } from './upload.component';

const components = [
  UploadComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    SharedModule
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class UploadModule {
}
