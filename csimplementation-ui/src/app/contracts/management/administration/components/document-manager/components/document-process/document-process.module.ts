import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { DetailModule } from '../detail/detail.module';
import { DocumentProcessComponent } from './components/document-process.component';
import { AddItemComponent } from './dialogs/add-item/add-item.component';
import { DuplicateItemComponent } from './dialogs/duplicate-item/duplicate-item.component';

const components = [
  AddItemComponent,
  DuplicateItemComponent,
  DocumentProcessComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    SharedModule,
    DetailModule
  ],
  exports: [
    components
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class DocumentProcessModule {
}
