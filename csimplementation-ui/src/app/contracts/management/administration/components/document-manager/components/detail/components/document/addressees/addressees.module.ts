import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../../../../../../shared/shared.module';
import { AddresseesComponent } from './addressees.component';
import { PostalAddresseeService } from './services/postal-addressee.service';

const components = [
  AddresseesComponent
];

@NgModule({
  declarations: components,
  imports: [
    SharedModule,
  ],
  exports: components,
  providers: [
    PostalAddresseeService
  ]
})
export class AddresseesModule {
}
