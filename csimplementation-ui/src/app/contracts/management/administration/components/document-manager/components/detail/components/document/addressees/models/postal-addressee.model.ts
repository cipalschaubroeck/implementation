export interface PostalAddresseeLinkModel {
  addressee: string;
  addresseeReplacements: string;
  addresses: string;
  emails: string;
  generatedDocument: string;
  itemInAssociation: string;
  phoneNumbers: string;
  postalAddressee: string;
  process: string;
  self: string;
}

export interface PostalAddresseeModel {
  personName: string;
  companyName: string;
  email: string;
  phone: string;
  links: PostalAddresseeLinkModel;
}
