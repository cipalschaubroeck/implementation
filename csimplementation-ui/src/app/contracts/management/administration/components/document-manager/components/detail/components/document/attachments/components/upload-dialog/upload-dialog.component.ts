import { HttpEvent, HttpEventType, HttpProgressEvent, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

import { DocumentLinkModel } from '../../../../../../../../../models/document-link.model';

import { AdministrativeDocumentsService } from '../../../../../../../../../services/administrative-documents.service';
import { AttachmentsService } from '../../services/attachments.service';

@Component({
  selector: 'app-upload-dialog',
  templateUrl: './upload-dialog.component.html',
  styleUrls: ['./upload-dialog.component.css']
})
export class UploadDialogComponent implements OnInit {
  progress: number;

  constructor (private attachmentsService: AttachmentsService,
               private administrativeDocumentService: AdministrativeDocumentsService,
               public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit () {
  }

  closeDialog () {
    this.progress = null;
    this.ref.close();
  }

  myUploader ($event: any) {
    const url: string = (<DocumentLinkModel>this.administrativeDocumentService.selected$.getValue().links).attachments;
    this.attachmentsService.uploadFiles(url, $event.files).subscribe((httpEvent: HttpEvent<any>) => {
      if (httpEvent.type === HttpEventType.UploadProgress) {
        this.handleProgress(httpEvent);
      } else if (httpEvent instanceof HttpResponse) {
        this.attachmentsService.uploadFilesSuccess(httpEvent.body);
        this.closeDialog();
      }
    });
  }

  private handleProgress (event: HttpProgressEvent) {
    this.progress = Math.round((event.loaded * 100) / event.total);
  }

}
