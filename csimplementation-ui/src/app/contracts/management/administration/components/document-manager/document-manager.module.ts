import { NgModule } from '@angular/core';

import { SharedModule } from '../../../../../shared/shared.module';
import { AdministrativePipeModule } from '../../pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from '../../services/administrative-documents.service';
import { DetailModule } from './components/detail/detail.module';
import { DocumentProcessModule } from './components/document-process/document-process.module';
import { DocumentManagerComponent } from './document-manager.component';

const components = [
  DocumentManagerComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    AdministrativePipeModule,
    DocumentProcessModule,
    SharedModule,
    DetailModule,
  ],
  exports: [
    components,
    SharedModule
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class DocumentManagerModule {
}
