import { NgModule } from '@angular/core';
import { DialogService } from 'primeng/api';

import { SharedModule } from '../../../../../../../../../../shared/shared.module';
import { AttachmentsComponent } from './attachments.component';
import { UploadDialogComponent } from './components/upload-dialog/upload-dialog.component';
import { AttachmentsService } from './services/attachments.service';

const entryComponents = [
  UploadDialogComponent
];

const components = [
  AttachmentsComponent,
  UploadDialogComponent
];

@NgModule({
  declarations: [
    components
  ],
  imports: [
    SharedModule,
  ],
  exports: [
    components
  ],
  providers: [
    AttachmentsService,
    DialogService
  ],
  entryComponents: entryComponents
})
export class AttachmentsModule {
}
