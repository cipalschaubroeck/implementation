import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { TranslateService } from '@ngx-translate/core';

import { Subscription } from 'rxjs';
import { AppInjector } from '../../../../../../../shared/services/app-injector.service';
import { DocumentProcessType } from '../../../../enums/document-process-type.enum';

import { AdministrativeDocumentsModel } from '../../../../models/administrative-documents.model';
import { AdministrativeDocumentsService } from '../../../../services/administrative-documents.service';
import { AdministrativeDocumentsFormGroup } from '../document-process/administrative-documents-form-group';
import { ProcessDetailService } from './components/service/process-detail.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent implements OnInit, OnDestroy {

  selected: AdministrativeDocumentsModel;
  formDetail: AdministrativeDocumentsFormGroup;
  private translateService: TranslateService;
  private subscriptions: Subscription[] = [];
  private name = '';

  constructor (private administrativeDocumentsService: AdministrativeDocumentsService,
               private processDetailService: ProcessDetailService,
               private route: ActivatedRoute,
               private router: Router) {
    const injector = AppInjector.getInjector();
    this.translateService = injector.get(TranslateService);
  }

  ngOnDestroy (): void {
    this.subscriptions.forEach(sub => {
      sub.unsubscribe();
    });
  }

  ngOnInit () {
    this.subscriptions.push(this.route.params.subscribe(params => {
      this.processDetailService.getDocumentProcess(params['processId']).subscribe(process => {
        this.selected = process;
        this.name = process.name;
        this.formDetail.name.setValue(process.name);
        let path = '';
        if (this.isDocument()) {
          path = './document';
        } else if (this.isEmail()) {
          path = '';
        } else if (this.isUpload()) {
          path = '';
        } else {

        }
        this.router.navigate([path], {relativeTo: this.route}).then(e => {
          if (!e) {
            // void
          }
        });
      });
    }));

    this.formDetail = new AdministrativeDocumentsFormGroup();
    this.formDetail.removeValidators('type', []);
  }

  reset () {
    this.formDetail.reset();
    if (this.name) {
      this.formDetail.name.setValue(this.name);
    }
  }

  save (selected: AdministrativeDocumentsModel) {
    this.subscriptions.push(this.administrativeDocumentsService
      .updateName(selected.links.self, this.formDetail.name.value).subscribe(name => {
        this.formDetail.name.setValue(name);
      }));
    this.reset();
  }

  isDocument () {
    return this.selected.type === DocumentProcessType.DOCUMENT;
  }

  isEmail () {
    return this.selected.type === DocumentProcessType.EMAIL;
  }

  isUpload () {
    return this.selected.type === DocumentProcessType.UPLOAD;
  }
}
