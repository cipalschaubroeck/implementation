import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocumentProcessComponent } from './document-process.component';

describe('SidebarComponent', () => {
  let component: DocumentProcessComponent;
  let fixture: ComponentFixture<DocumentProcessComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DocumentProcessComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DocumentProcessComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
