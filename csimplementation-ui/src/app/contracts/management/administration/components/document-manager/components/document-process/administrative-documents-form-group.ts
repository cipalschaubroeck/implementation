import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

import { AdministrativeDocumentsModel } from '../../../../models/administrative-documents.model';

export class AdministrativeDocumentsFormGroup extends FormGroup {
  constructor (administrativeDocumentsModel?: AdministrativeDocumentsModel) {
    super({
      name: new FormControl(null, Validators.required),
      type: new FormControl(null, Validators.required)
    });
    if (administrativeDocumentsModel) {
      this.patch({...administrativeDocumentsModel});
    }
  }

  patch (administrativeDocumentsModel: AdministrativeDocumentsModel) {
    // Currently it is taking all properties at Model, in case only some are needed do the mapping here.
    this.patchValue({...administrativeDocumentsModel});
  }

  get name (): FormControl {
    return this.get('name') as FormControl;
  }

  get type (): FormControl {
    return this.get('type') as FormControl;
  }

  removeValidators (field: string, validators: ValidatorFn[]) {
    const formControlField = this.get(field) as FormControl;
    if (formControlField) {
      formControlField.setValidators(validators);
    }
  }
}
