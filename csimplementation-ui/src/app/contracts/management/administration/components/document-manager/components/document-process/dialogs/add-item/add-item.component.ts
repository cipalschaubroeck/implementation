import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { GenericMessageService } from '../../../../../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../../../../../shared/services/generic-translate.service';

import { DocumentProcessType } from '../../../../../../enums/document-process-type.enum';
import { AdministrativeDocumentsService } from '../../../../../../services/administrative-documents.service';
import { ContractDocumentProcess } from '../../../models/contract-document-process-info';
import { DocumentProcessModel } from '../../../models/document-process-info';
import { AdministrativeDocumentsFormGroup } from '../../administrative-documents-form-group';

@Component({
  selector: 'app-add-item-dialog',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  formAdd: AdministrativeDocumentsFormGroup;
  types: SelectItem[];
  private contract: ContractDocumentProcess;

  constructor (private documentProcessService: AdministrativeDocumentsService,
               private translateService: GenericTranslateService,
               public ref: DynamicDialogRef, public config: DynamicDialogConfig,
               private route: ActivatedRoute,
               private messageService: GenericMessageService) {
  }

  ngOnInit () {
    this.contract = this.config.data.contract;
    this.formAdd = new AdministrativeDocumentsFormGroup();
    this.initTypes();
  }

  addItem () {
    const name = this.formAdd.name.value;
    const type = this.formAdd.type.value;
    const document: DocumentProcessModel = {
      name: name,
      type: this.formAdd.type.value,
      links: {
        contract: this.contract.links.self
      }
    };
    this.documentProcessService.createDocumentProcess(document).subscribe(
      () => this.messageService.success(),
      () => this.messageService.error(),
      () => this.closeDialog()
    );
  }

  closeDialog () {
    this.formAdd.reset();
    this.config.data.update();
    this.ref.close();
  }

  private initTypes () {
    this.types = [
      {
        label: this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Document'),
        value: DocumentProcessType.DOCUMENT
      },
      {
        label: this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Email'),
        value: DocumentProcessType.EMAIL
      },
      {
        label: this.translateService.translate('ADMINISTRATIVE.DOCUMENTS.ADD.TYPE.Upload'),
        value: DocumentProcessType.UPLOAD
      }
    ];
  }
}
