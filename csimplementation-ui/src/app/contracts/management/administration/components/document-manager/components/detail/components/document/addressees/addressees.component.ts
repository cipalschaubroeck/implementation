import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import { ConfirmationService } from 'primeng/api';

import { BehaviorSubject, Subscription } from 'rxjs';
import { GenericTableLazyComponent } from '../../../../../../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
// tslint:disable-next-line:max-line-length
import { GenericTableLazyButtonModel } from '../../../../../../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { FilterType } from '../../../../../../../../../../shared/enums/filter-type.enum';
import { PostalAddresseeFormGroup } from './models/postal-addressee-form-group';
import { PostalAddresseeModel } from './models/postal-addressee.model';
import { PostalAddresseeService } from './services/postal-addressee.service';

@Component({
  selector: 'app-addressees',
  templateUrl: './addressees.component.html',
  styleUrls: ['./addressees.component.css']
})
export class AddresseesComponent implements OnInit, OnDestroy {

  rightButtons: GenericTableLazyButtonModel[] = [];
  leftButtons: GenericTableLazyButtonModel[] = [];
  addressees$: BehaviorSubject<PostalAddresseeModel[]> = new BehaviorSubject<PostalAddresseeModel[]>([]);
  selected$: BehaviorSubject<PostalAddresseeModel>;
  selected: PostalAddresseeModel;
  formDetail: PostalAddresseeFormGroup;
  private subscriptions: Subscription[] = [];
  @ViewChild('addressTable', { static: true }) table: GenericTableLazyComponent;

  constructor (private postalAddressService: PostalAddresseeService,
               private confirmationService: ConfirmationService,
               private router: Router,
               private route: ActivatedRoute,
               private translateService: TranslateService) {
  }

  static showAddDialog () {
    // TODO CSPROC-1751 implement
  }

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {
    /**
     * This sets processId in the service so that once the onLazyLoad method in GenericTable is activated it knows
     * how to filter by processId.
     * This is done this way because routeParams cant be accessed from the service.
     */
    this.subscriptions.push(this.route.parent.params.subscribe(params => {
      this.postalAddressService.setProcessId(params.processId);
    }));

    this.selected = null;
    this.selected$ = this.postalAddressService.selected$;
    this.selected$.subscribe(value => this.selected = value);

    this.table.initTableConfig({
      columns: [
        {
          field: 'personName',
          text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Name',
          class: null,
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'companyName',
          text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.ContactName',
          class: null,
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'email',
          text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Email',
          class: null,
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        },
        {
          field: 'phone',
          text: 'ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.Phone',
          class: null,
          hasSort: true,
          filter: {
            type: FilterType.NONE
          }
        }
      ],
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'OVERVIEW.NoResults',
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 15]
      },
      sortField: ('phone'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.postalAddressService);
    this.table.initButtons(this.rightButtons, this.leftButtons);

    GenericTableLazyComponent.loadButton('add', 'ui-icon-plus', '',
      AddresseesComponent.showAddDialog, this, false, this.leftButtons);
    GenericTableLazyComponent.loadButton('remove', 'ui-icon-trash', '',
      this.confirmRemove, this, false, this.leftButtons);

    this.formDetail = new PostalAddresseeFormGroup();
  }

  confirmRemove () {
    this.confirmationService.confirm({
      message: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.REMOVE.Message'),
      header: this.translateService.instant('ADMINISTRATIVE.DOCUMENTS.DETAIL.DOCUMENT.ADDRESSEES.REMOVE.Header'),
      acceptLabel: this.translateService.instant('COMMON.BUTTONS.Confirm'),
      rejectLabel: this.translateService.instant('COMMON.BUTTONS.Cancel'),
      icon: 'pi pi-exclamation-triangle',
      accept: () => {
        this.postalAddressService.removeItem();
      }
    });
  }

  save () {
    // TODO CSPROC-1709 save
  }

  reset () {
    // TODO CSPROC-1709 reset
  }

}
