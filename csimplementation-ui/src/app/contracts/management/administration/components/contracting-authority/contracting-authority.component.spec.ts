import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractingAuthorityComponent } from './contracting-authority.component';

describe('InitiateContractingAuthorityComponent', () => {
  let component: ContractingAuthorityComponent;
  let fixture: ComponentFixture<ContractingAuthorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContractingAuthorityComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractingAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
