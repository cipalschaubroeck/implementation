import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';
import { BailComponent } from './components/bail/bail.component';
import { ContractingAuthorityComponent } from './components/contracting-authority/contracting-authority.component';
import { ContractorComponent } from './components/contractor/contractor.component';
import { DocumentManagerModule } from './components/document-manager/document-manager.module';
import { RelatedPartiesComponent } from './components/related-parties/related-parties.component';

import { AdministrativeComponent } from './containers/administrative/administrative.component';
import { AdministrativePipeModule } from './pipes/administrative.pipe.module';
import { AdministrativeDocumentsService } from './services/administrative-documents.service';

const components = [
  AdministrativeComponent,
  ContractingAuthorityComponent,
  ContractorComponent,
  RelatedPartiesComponent,
  BailComponent
];

@NgModule({
  declarations: components,
  imports: [
    AdministrativePipeModule,
    DocumentManagerModule,
    SharedModule
  ],
  providers: [
    AdministrativeDocumentsService
  ]
})
export class AdministrativeModule {
}
