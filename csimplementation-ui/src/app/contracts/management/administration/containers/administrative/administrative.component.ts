import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MenuItem } from 'primeng/api';

const administrativeTabs: MenuItem[] = [
  {
    label: 'ADMINISTRATIVE.ContractingAuthority',
    title: 'ADMINISTRATIVE.TOOLTIP.ContractingAuthority',
    routerLink: '/administrative/contracting-authority'
  },
  {
    label: 'ADMINISTRATIVE.Contractor',
    title: null,
    routerLink: '/administrative/contractor'
  },
  {
    label: 'ADMINISTRATIVE.RelatedParties',
    title: null,
    routerLink: '/administrative/related-parties'
  },
  {
    label: 'ADMINISTRATIVE.Insurances',
    title: null,
    routerLink: '/administrative/insurances'
  },
  {
    label: 'ADMINISTRATIVE.Bail',
    title: null,
    routerLink: '/administrative/bail'
  },
  {
    label: 'ADMINISTRATIVE.Documents',
    title: null,
    routerLink: '/administrative/documents'
  }
];

@Component({
  selector: 'app-administrative',
  templateUrl: './administrative.component.html',
  styleUrls: ['./administrative.component.css']
})
export class AdministrativeComponent implements OnInit {

  tabItems: MenuItem[];

  constructor(private translateService: TranslateService) {
    this.createTabs();
  }

  ngOnInit() {
  }

  private createTabs() {
    this.tabItems = [];
    for (const tab of administrativeTabs) {
      const label = this.translateService.instant(tab.label);
      const title = tab.title !== null ? this.translateService.instant(tab.title) : null;
      this.tabItems.push({
        label: label,
        title: title,
        routerLink: tab.routerLink
      });
    }
  }
}
