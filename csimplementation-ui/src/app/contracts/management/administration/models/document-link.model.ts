export interface DocumentLinkModel {
  addresseeList: string;
  attachments: string;
  body: string;
  self: string;
  signingOfficerData: string;
}
