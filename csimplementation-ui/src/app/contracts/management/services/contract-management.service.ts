import { EventEmitter, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseHttpService } from '../../../core/services/base-http.service';
import { FormStatusEnum } from '../../common/enums/formStatus.enum';

@Injectable({
  providedIn: 'root'
})
export class ContractManagementService extends BaseHttpService<any> {

  private eventValidateInitiateContractProcess: EventEmitter<FormStatusEnum> = new EventEmitter();
  private initiateContractDataValidationEvent: EventEmitter<FormStatusEnum> = new EventEmitter();
  private initiateContractAuthorityValidationEvent: EventEmitter<FormStatusEnum> = new EventEmitter();
  private initiateContractBailValidationEvent: EventEmitter<FormStatusEnum> = new EventEmitter();
  private initiateContractPricesReviewValidationEvent: EventEmitter<FormStatusEnum> = new EventEmitter();

  private buildUrl (link: string, id: string, endpoint: string) {
    if (!link && !id) {
      throw new Error('Object link or id must be passed');
    }

    let url: string;
    if (link) {
      url = link + endpoint;
    } else {
      url = this.endpoint + 'contract/' + id + endpoint;
    }
    return url;
  }

  validateInitiateContractProcess (id: string) {
    this.httpClient.post(this.endpoint + '/contract/' + id + '/validate/initiate/contract-process', null)
      .subscribe((validated: FormStatusEnum) => {
        this.eventValidateInitiateContractProcess.emit(validated);
    });
  }

  emitValidateInitiateContractProcess (link: string, id: string): Promise<FormStatusEnum> {
    return new Promise<FormStatusEnum>(response => {
      this.httpClient.get(this.buildUrl(link, id, '/validate/initiate/contract-process')).subscribe((validated: FormStatusEnum) => {
        this.eventValidateInitiateContractProcess.emit(validated);
        response(validated);
      });
    });

  }

  emitInitiateContractDataValidation (link: string, id: string): Promise<FormStatusEnum> {
    return new Promise<FormStatusEnum>(response => {
      this.initiateContractDataValidation(link, id).subscribe((validated: FormStatusEnum) => {
        this.initiateContractDataValidationEvent.emit(validated);
        response(validated);
      });
    });
  }

  emitInitiateContractAuthorityValidation (link: string, id: string): Promise<FormStatusEnum> {
    return new Promise<FormStatusEnum>(response => {
      this.initiateContractAuthorityValidation(link, id).subscribe((validated: FormStatusEnum) => {
        this.initiateContractAuthorityValidationEvent.emit(validated);
        response(validated);
      });
    });
  }

  emitInitiateContractBailValidation (link: string, id: string): Promise<FormStatusEnum> {
    return new Promise<FormStatusEnum>(response => {
      this.initiateContractBailValidation(link, id).subscribe((validated: FormStatusEnum) => {
        this.initiateContractBailValidationEvent.emit(validated);
        response(validated);
      });
    });
  }

  emitInitiateContractPricesReviewValidation (link: string, id: string): Promise<FormStatusEnum> {
    return new Promise<FormStatusEnum>(response => {
      this.initiateContractPricesReviewValidation(link, id).subscribe((validated: FormStatusEnum) => {
        this.initiateContractPricesReviewValidationEvent.emit(validated);
        response(validated);
      });
    });
  }

  listenValidateInitiateContractProcess (): EventEmitter<FormStatusEnum> {
    return this.eventValidateInitiateContractProcess;
  }

  listenInitiateContractDataValidation (): EventEmitter<FormStatusEnum> {
    return this.initiateContractDataValidationEvent;
  }

  listenInitiateContractAuthorityValidationEvent (): EventEmitter<FormStatusEnum> {
    return this.initiateContractAuthorityValidationEvent;
  }

  listenInitiateContractBailValidationEvent (): EventEmitter<FormStatusEnum> {
    return this.initiateContractBailValidationEvent;
  }

  listenInitiateContractPricesReviewValidationEvent (): EventEmitter<FormStatusEnum> {
    return this.initiateContractPricesReviewValidationEvent;
  }

  initiateContractDataValidation (link: string, id: string): Observable<FormStatusEnum> {
    return this.httpClient.get<FormStatusEnum>(this.buildUrl(link, id, '/validate/initiate/data-form'));
  }

  initiateContractAuthorityValidation (link: string, id: string): Observable<FormStatusEnum> {
    return this.httpClient.get<FormStatusEnum>(this.buildUrl(link, id, '/validate/initiate/authority-form'));
  }

  initiateContractBailValidation (link: string, id: string): Observable<FormStatusEnum> {
    return this.httpClient.get<FormStatusEnum>(this.buildUrl(link, id, '/validate/initiate/bail-form'));
  }

  initiateContractPricesReviewValidation (link: string, id: string): Observable<FormStatusEnum> {
    return this.httpClient.get<FormStatusEnum>(this.buildUrl(link, id, '/validate/initiate/prices-review-form'));
  }
}
