import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseHttpService } from '../../../../../core/services/base-http.service';
import { InitiateFormulaInfoModel } from '../model/initiate-formula-info.model';

@Injectable({providedIn: 'root'})
export class InitiateFormulaService extends BaseHttpService<any> {

  getInitiateContractFormula(contractId: string): Promise<InitiateFormulaInfoModel> {
    const params: HttpParams = new HttpParams().set('projection', 'initiateContractFormulaProjection');
    return new Promise<InitiateFormulaInfoModel>(response => {
      this.httpClient.get<InitiateFormulaInfoModel>(this.endpoint + 'contract/' + contractId, {params}).subscribe(data => {
        response(data);
      });
    });
  }

  saveFormula(link: string, body: InitiateFormulaInfoModel): Observable<InitiateFormulaInfoModel> {
    return this.httpClient.post<InitiateFormulaInfoModel>(link + '/prices-review', body);
  }

}
