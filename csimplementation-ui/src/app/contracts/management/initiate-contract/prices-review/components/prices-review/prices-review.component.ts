import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { FormStatusEnum } from '../../../../../common/enums/formStatus.enum';
import { ContractManagementService } from '../../../../services/contract-management.service';
import { FormulaTypeEnum } from '../../model/formula-type.enum';
import { InitiateFormulaInfoModel } from '../../model/initiate-formula-info.model';
import { InitiateFormulaService } from '../../services/initiate-formula.service';

@Component({
  selector: 'app-prices-review',
  templateUrl: './prices-review.component.html',
  styleUrls: ['./prices-review.component.css']
})
export class PricesReviewComponent implements OnInit, OnDestroy {

  private pricesReviewForm: FormGroup;
  private formulaType = FormulaTypeEnum;
  private formSubmitAttempt = false;
  private formulaInfo: InitiateFormulaInfoModel;
  private subscription: Subscription;
  private formHasChanges = false;

  constructor(private messageService: GenericMessageService,
              private route: ActivatedRoute,
              private initiateFormulaService: InitiateFormulaService,
              private contractManagementService: ContractManagementService) {
  }

  ngOnInit() {
    const contractId: string = this.route.snapshot.parent.paramMap.get('contractId');
    this.initiateFormulaService.getInitiateContractFormula(contractId).then(response => {
      this.formulaInfo = response;
      this.buildForm();
      this.setFormulaRadioValidations();
      this.onInitPricesReviewValidation();

      this.subscription = this.pricesReviewForm.valueChanges.subscribe(() => {
        this.formHasChanges = true;
      });
    });
  }

  private buildForm() {
    this.pricesReviewForm = new FormGroup({
      formulaRb: new FormControl(
        this.formulaInfo.type ? this.formulaInfo.type : this.formulaType.NONE, Validators.required
      ),
      fixedFormula: new FormControl({
        value: this.formulaInfo.formula && (this.formulaInfo.type === this.formulaType.FIXED) ? this.formulaInfo.formula : '',
        disabled: this.formulaInfo.type !== this.formulaType.FIXED
      }),
      customFormula: new FormControl({
        value: !(this.formulaInfo.formula && (this.formulaInfo.type === this.formulaType.CUSTOM)) ? '' : this.formulaInfo.formula,
        disabled: this.formulaInfo.type !== this.formulaType.CUSTOM
      })
    });
  }

  private setFormulaRadioValidations() {
    const formulaRbCtrl = this.pricesReviewForm.get('formulaRb').value;
    const fixedFormulaCtrl = this.pricesReviewForm.get('fixedFormula');
    const customFormulaCtrl = this.pricesReviewForm.get('customFormula');

    fixedFormulaCtrl.setValidators(null);
    customFormulaCtrl.setValidators(null);
    fixedFormulaCtrl.disable();
    customFormulaCtrl.disable();

    if (formulaRbCtrl === this.formulaType.FIXED) {
      fixedFormulaCtrl.setValidators(Validators.required);
      fixedFormulaCtrl.enable();
    } else if (formulaRbCtrl === this.formulaType.CUSTOM) {
      customFormulaCtrl.setValidators(Validators.required);
      customFormulaCtrl.enable();
    }
  }

  onValidate() {
    this.formSubmitAttempt = true;

    if (this.pricesReviewForm.valid) {
      this.messageService.success();
    } else {
      this.messageService.error();
    }
  }

  onFormulaRadioChange() {
    this.setFormulaRadioValidations();
    this.savePricesReviewForm();
  }

  isFieldValid(field: string) {
    return this.pricesReviewForm.get(field).invalid && (this.pricesReviewForm.get(field).touched ||
      (this.pricesReviewForm.get(field).untouched && this.pricesReviewForm.get(field).invalid && this.formSubmitAttempt));
  }

  savePricesReviewForm() {
    if (this.formHasChanges) {
      let formulaTxt = null;
      if (this.pricesReviewForm.controls.formulaRb.value === this.formulaType.FIXED) {
        formulaTxt = this.pricesReviewForm.controls.fixedFormula.value;
      } else if (this.pricesReviewForm.controls.formulaRb.value === this.formulaType.CUSTOM) {
        formulaTxt = this.pricesReviewForm.controls.customFormula.value;
      }

      const formula: InitiateFormulaInfoModel = {
        type: this.pricesReviewForm.controls.formulaRb.value ? this.pricesReviewForm.controls.formulaRb.value : null,
        formula: formulaTxt
      };

      this.initiateFormulaService.saveFormula(this.formulaInfo._links.self['href'], formula)
        .subscribe(() => {
          this.messageService.success();

          this.contractManagementService.emitInitiateContractPricesReviewValidation(this.formulaInfo._links.self['href'], null).then();
        }, error => {
          this.messageService.error();
        });
      this.formHasChanges = false;
    }
  }

  private onInitPricesReviewValidation () {
    this.contractManagementService.emitInitiateContractPricesReviewValidation(this.formulaInfo._links.self['href'], null)
      .then(formStatus => {
        if (formStatus === FormStatusEnum.READ_ONLY) {
          this.pricesReviewForm.disable();
        }
      });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
