import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PricesReviewComponent } from './prices-review.component';

describe('PricesReviewComponent', () => {
  let component: PricesReviewComponent;
  let fixture: ComponentFixture<PricesReviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PricesReviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PricesReviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
