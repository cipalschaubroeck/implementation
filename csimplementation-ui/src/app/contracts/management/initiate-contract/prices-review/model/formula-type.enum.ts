export enum FormulaTypeEnum {
  NONE = 'NONE',
  FIXED = 'FIXED',
  CUSTOM = 'CUSTOM'
}
