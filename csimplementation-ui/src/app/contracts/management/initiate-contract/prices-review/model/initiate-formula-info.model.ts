import { FormulaTypeEnum } from './formula-type.enum';

export interface InitiateFormulaInfoModel {
  id?: string;
  type?: FormulaTypeEnum;
  formula: string;
  _links?: {
    self: string;
  };
}
