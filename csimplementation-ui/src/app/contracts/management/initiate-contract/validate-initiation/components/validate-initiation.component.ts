import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContractManagementService } from '../../../services/contract-management.service';

@Component({
  selector: 'app-validate-initiation',
  templateUrl: './validate-initiation.component.html',
  styleUrls: ['./validate-initiation.component.css']
})
export class ValidateInitiationComponent {

  constructor (private contractManagementService: ContractManagementService,
               private router: Router,
               private route: ActivatedRoute) {
  }

  validateContractInitiation () {
    const contractId: string = this.route.snapshot.parent.paramMap.get('contractId');
    if (!contractId) {
      this.router.navigate(['home']).then(e => {
        if (!e) {
          console.log('Navigation failed', e);
        }
      });
    }
    this.contractManagementService.validateInitiateContractProcess(contractId);
  }

}
