import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidateInitiationComponent } from './validate-initiation.component';

describe('ValidateInitiationComponent', () => {
  let component: ValidateInitiationComponent;
  let fixture: ComponentFixture<ValidateInitiationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ValidateInitiationComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidateInitiationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
