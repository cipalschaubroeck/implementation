import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { ContactInfoModel } from '../../../../../../core/components/top-menu/organization-contacts/models/contact-info.model';
import { OrganizationInfoModel } from '../../../../../../core/components/top-menu/organization-contacts/models/organization-info.model';
// tslint:disable-next-line:max-line-length
import { OrganizationContactService } from '../../../../../../core/components/top-menu/organization-contacts/services/organization-contact.service';
import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../../../../shared/components/generic-table/models/generic-table.models';

@Component({
  selector: 'app-contractor',
  templateUrl: './contractor.component.html',
  styleUrls: ['./contractor.component.css']
})
export class ContractorComponent implements OnInit {
  private contractorForm: FormGroup;
  private organizationOptions: SelectItem[] = [];
  private contactOrganizationOptions: SelectItem[] = [];
  private jointVentureOptions: SelectItem[] = [];
  private contactJointVentureOptions: SelectItem[] = [];
  private selected: any;
  private selected$: BehaviorSubject<any>;
  private data: BehaviorSubject<OrganizationInfoModel[]> = new BehaviorSubject<OrganizationInfoModel[]>([]);
  private cols: GenericTableColumnModel[];
  private buttons: GenericTableButtonModel[];
  private tableConfig: GenericTableConfigModel;

  private static initForm (): FormGroup {
    return new FormGroup({
      radioOrg: new FormControl(''),
      nameOrg: new FormControl({value: '', disabled: true}),
      mainContactNameOrg: new FormControl({value: '', disabled: true}),
      vatNumberOrg: new FormControl({value: '', disabled: true}),
      mainContactFirstNameOrg: new FormControl({value: '', disabled: true}),
      nameVent: new FormControl({value: '', disabled: true}),
      mainContactNameVent: new FormControl({value: '', disabled: true}),
      mainContactFirstNameVent: new FormControl({value: '', disabled: true}),
      vatNumberVent: new FormControl({value: '', disabled: true})
    });
  }

  constructor (private organizationService: OrganizationContactService) {
  }

  ngOnInit () {
    this.contractorForm = ContractorComponent.initForm();

    this.cols = [
      {
        field: 'organizationName',
        text: 'CONTRACT.CONTRACTOR.JOINT_VENTURE.organizationName',
        hasSort: true,
      },
      {
        field: 'vatNumber',
        text: 'CONTRACT.CONTRACTOR.JOINT_VENTURE.vatNumber',
        hasSort: true,
      }
    ];
    this.buttons = [];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.noResults',
      hasSearchFilter: false,
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 20]
      }
    };
    this.populateOrganizationDropdown();
    this.populateJointVenturesOptions();
  }

  onRadioChange () {
    if (this.contractorForm.controls.radioOrg.value === '1') {
      this.contractorForm.controls.nameOrg.enable();
      this.contractorForm.controls.nameVent.disable();
      this.contractorForm.controls.mainContactNameVent.disable();
      this.contractorForm.controls.nameVent.setValue('');
      this.contractorForm.controls.mainContactNameVent.setValue('');
      this.contractorForm.controls.mainContactFirstNameVent.setValue('');
      this.contractorForm.controls.vatNumberVent.setValue('');
    } else {
      this.contractorForm.controls.nameVent.enable();
      this.contractorForm.controls.nameOrg.disable();
      this.contractorForm.controls.mainContactNameOrg.disable();
      this.contractorForm.controls.nameOrg.setValue('');
      this.contractorForm.controls.mainContactNameOrg.setValue('');
      this.contractorForm.controls.mainContactFirstNameOrg.setValue('');
      this.contractorForm.controls.vatNumberOrg.setValue('');
    }
  }

  private populateOrganizationDropdown () {
    this.organizationService.getOrganizations().subscribe(organizations => {
      organizations.forEach(org => {
        this.organizationOptions.push({label: org.legalName, value: org});
      });
    });
  }

  private populateOrganizationContactsDropdown (contact: ContactInfoModel) {
    this.contactOrganizationOptions = [];
    this.contactOrganizationOptions.push({label: contact.nickname, value: contact});
  }

  private onOrganizationDropdownChange (event) {
    this.contractorForm.controls.mainContactFirstNameOrg.setValue('');
    const organization: OrganizationInfoModel = event.value;
    this.contractorForm.controls.vatNumberOrg.setValue(organization.vatNumber);
    this.contractorForm.controls.mainContactNameOrg.enable();
    this.populateOrganizationContactsDropdown(organization.contact);
  }

  private onContactOrganizationDropdownChange (event) {
    const contact: ContactInfoModel = event.value;
    this.contractorForm.controls.mainContactFirstNameOrg.setValue(contact.nickname);
    this.contractorForm.controls.mainContactNameOrg.enable();
  }

  private populateJointVenturesOptions () {
    // TODO functionality to be developed when analysis is complete
    this.jointVentureOptions.push({label: 'mock joint venture 1 ', value: 'mock'});
    this.jointVentureOptions.push({label: 'mock joint venture 2 ', value: 'mock'});
  }

  private populateJointVentureContactsDropdown () {
    this.contactJointVentureOptions.push({label: 'contact 1', value: null});
  }

  private onJoinVentureDropdownChange (event) {
    this.contractorForm.controls.mainContactFirstNameVent.setValue('');
    const jointVent: OrganizationInfoModel = event.value;
    this.contractorForm.controls.vatNumberVent.setValue(jointVent.vatNumber);
    this.contractorForm.controls.mainContactNameVent.enable();
    this.populateJointVentureContactsDropdown();
  }

  // TODO CSPROC-2127 relate contractor with the contract in the back end
}
