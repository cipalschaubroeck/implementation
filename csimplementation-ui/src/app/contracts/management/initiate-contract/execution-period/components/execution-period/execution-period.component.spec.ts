import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutionPeriodComponent } from './execution-period.component';

describe('ExecutionPeriodComponent', () => {
  let component: ExecutionPeriodComponent;
  let fixture: ComponentFixture<ExecutionPeriodComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutionPeriodComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutionPeriodComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
