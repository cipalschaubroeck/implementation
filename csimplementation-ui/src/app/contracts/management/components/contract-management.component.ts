import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { DataTreeModel } from '../../../shared/components/generic-tree/model/generic-tree.models';
import { FormStatusEnum } from '../../common/enums/formStatus.enum';
import { ContractManagementService } from '../services/contract-management.service';

@Component({
  selector: 'app-contract-management',
  templateUrl: './contract-management.component.html',
  styleUrls: ['./contract-management.component.css']
})
export class ContractManagementComponent implements OnInit, OnDestroy {

  constructor (private contractManagementService: ContractManagementService,
               private route: ActivatedRoute) {
  }

  private data: DataTreeModel[] = [
    {
      label: 'InitiateDataComponent Contract',
      path: '',
      collapsedIcon: 'fa fa-folder',
      expandedIcon: 'fa fa-folder-open',
      type: 'header',
      key: 'initiateContract',
      selectable: false,
      children: [
        {label: 'Contract data', path: 'data', key: 'contractData'},
        {label: 'Contracting Authority', path: 'authority', key: 'contractingAuthority'},
        {label: 'Bail', path: 'bail', key: 'bail'},
        {label: 'Insurances', path: 'insurances', key: 'insurances'},
        {label: 'Contractor', path: 'contractor', key: 'contractor'},
        {label: 'Price reviews', path: 'prices-review', key: 'pricesReview'},
        {label: 'Inventory / Measuring state', path: 'inventory', key: 'inventory'},
        {label: 'Budgets', path: 'budgets', key: 'budgets'},
        {label: 'Execution period', path: 'execution-period', key: 'executionPeriod'},
        {
          label: 'Validate initiation',
          path: 'validate-initiation',
          key: 'validateInitiation'
        }
      ]
    },
    {
      label: 'Administration',
      selectable: false,
      path: '',
      collapsedIcon: 'fa fa-folder',
      expandedIcon: 'fa fa-folder-open',
      type: 'header',
      key: 'administration',
      children: [
        {label: 'Contract data', path: 'admin-data', key: 'adminContractData'},
        {
          label: 'Contracting authority',
          path: 'admin-authority',
          key: 'adminContractingAuthority'
        },
        {label: 'Bail', path: 'admin-bail', key: 'adminBail'},
        {label: 'Contractor', path: 'admin-contractor', key: 'adminContractor'},
        {label: 'Insurances', path: 'admin-insurances', key: 'adminInsurances'},
        {label: 'Related parties', path: 'admin-parties', key: 'adminRelatedParties'},
        {label: 'Documents', path: 'admin-documents', key: 'adminDocuments'}
      ]
    }
  ];
  private subs: Subscription[] = [];

  ngOnInit () {
    this.activateValidationListeners();
    this.listenValidateInitiateContractProcess();

    const contractId: string = this.route.snapshot.paramMap.get('contractId');

    this.emitValidateInitiateContractProcess(contractId, response => {
      if (response === FormStatusEnum.NOT_COMPLETED) {
        console.log('initiate contract process is not complete');
        this.contractManagementService.emitInitiateContractDataValidation(null, contractId).then();

        this.contractManagementService.emitInitiateContractAuthorityValidation(null, contractId).then();

        this.contractManagementService.emitInitiateContractBailValidation(null, contractId).then();

        this.contractManagementService.emitInitiateContractPricesReviewValidation(null, contractId).then();
      } else {
        console.log('initiate contract process is complete');
      }
    });
  }

  ngOnDestroy () {
    this.subs.forEach(sub => {
      sub.unsubscribe();
    });
  }

  private listenValidateInitiateContractProcess () {
    // TODO CSPROC-2260
    //  Since tree component is not validated in block any more
    //  this method is temporal until all forms have their own validations.
    //  to keep the look and feels that each component has its validation
    this.subs.push(this.contractManagementService.listenValidateInitiateContractProcess().subscribe(response => {
      if (response === FormStatusEnum.READ_ONLY) {
        this.data.find(obj => obj.key === 'initiateContract').children.forEach(option => {
          option.statusIcon = 'ui-icon-lock';
        });
        this.data.find(obj => obj.key === 'administration').children.forEach(option => {
          option.statusIcon = 'ui-icon-edit';
        });
      } else {
        this.data.find(obj => obj.key === 'initiateContract').children.forEach(option => {
          // TODO CSPROC-2256 create a jira and note it here, so we know when this is going to be done
          // TODO excludes tree components that have validations implemented
          if (option.key !== 'contractData' && option.key !== 'contractingAuthority' && option.key !== 'bail'
            && option.key !== 'pricesReview') {
            option.statusIcon = 'ui-icon-edit';
          }
        });
        this.data.find(obj => obj.key === 'administration').children.forEach(option => {
          option.statusIcon = 'ui-icon-lock';
        });
      }
    }));
  }

  private listenValidateInitiateContractData (): Promise<any> {
    return new Promise(response => {
      response(this.contractManagementService.listenInitiateContractDataValidation().subscribe(validated => {
        this.placeIcon('initiateContract', 'contractData', validated);
      }));
    });
  }

  private listenValidateInitiateContractAuthority (): Promise<any> {
    return new Promise(response => {
      response(this.contractManagementService.listenInitiateContractAuthorityValidationEvent().subscribe(validated => {
        this.placeIcon('initiateContract', 'contractingAuthority', validated);
      }));
    });
  }

  private listenValidateInitiateContractBail (): Promise<any> {
    return new Promise(response => {
      response(this.contractManagementService.listenInitiateContractBailValidationEvent().subscribe(validated => {
        this.placeIcon('initiateContract', 'bail', validated);
      }));
    });
  }

  private listenValidateInitiateContractPricesReview (): Promise<any> {
    return new Promise(response => {
      response(this.contractManagementService.listenInitiateContractPricesReviewValidationEvent().subscribe(validated => {
        this.placeIcon('initiateContract', 'pricesReview', validated);
      }));
    });
  }

  private emitValidateInitiateContractProcess (contractId: string, validated) {
    this.contractManagementService.emitValidateInitiateContractProcess(null, contractId).then(res =>
      validated(res));
  }

  private activateValidationListeners () {
    Promise.all([
      this.listenValidateInitiateContractData(),
      this.listenValidateInitiateContractAuthority(),
      this.listenValidateInitiateContractBail(),
      this.listenValidateInitiateContractPricesReview()
    ]).then(responses => {
      responses.push(response => {
        this.subs.push(response);
      });
    });
  }

  private placeIcon (ParentKey: string, childKey: string, validated: FormStatusEnum) {
    let icon;

    if (validated === FormStatusEnum.READ_ONLY) {
      icon = 'ui-icon-lock';
    } else if (validated === FormStatusEnum.COMPLETED) {
      icon = 'ui-icon-check';
    } else if (validated === FormStatusEnum.NOT_COMPLETED) {
      icon = 'ui-icon-edit';
    } else {
      throw new Error('Validation error');
    }

    this.data.find(obj => obj.key === ParentKey)
      .children.find(child => child.key === childKey).statusIcon = icon;
  }
}
