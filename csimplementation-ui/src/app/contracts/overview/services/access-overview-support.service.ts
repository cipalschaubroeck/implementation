import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { GenericTableLazyComponent } from '../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import {
  GenericTableLazyColumnModel,
  GenericTableLazyFilterModel
} from '../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { FilterType } from '../../../shared/enums/filter-type.enum';
import { StatusType } from '../../common/enums/status-type.enum';
import { ContractService } from './contract.service';

@Injectable({
  providedIn: 'root'
})
export class AccessOverviewSupportService {
  private filterStatusOptions: BehaviorSubject<GenericTableLazyFilterModel[]> = new BehaviorSubject<GenericTableLazyFilterModel[]>([]);
  private filterContractTypeOptions: BehaviorSubject<GenericTableLazyFilterModel[]>
    = new BehaviorSubject<GenericTableLazyFilterModel[]>([]);
  private allowArchived = false;
  private options: GenericTableLazyFilterModel[] = [
    {
      label: 'OVERVIEW.FILTER.preparation',
      value: StatusType.PREPARATION,
      disabled: false,
      selected: false,
    },
    {
      label: 'OVERVIEW.FILTER.progress',
      value: StatusType.PROGRESS,
      disabled: false,
      selected: false,
    },
    {
      label: 'OVERVIEW.FILTER.after',
      value: StatusType.AFTER,
      disabled: false,
      selected: false,
    },
    {
      label: 'OVERVIEW.FILTER.cancel',
      value: StatusType.CANCEL,
      disabled: false,
      selected: false,
    },
    {
      label: 'OVERVIEW.FILTER.executed',
      value: StatusType.EXECUTED,
      disabled: false,
      selected: false,
    },
    {
      label: 'OVERVIEW.FILTER.archived',
      value: StatusType.ARCHIVED,
      disabled: false,
      selected: false,
    }
  ];

  constructor (private accessOverviewService: ContractService) {
  }

  getFilterStatusOptions (): BehaviorSubject<GenericTableLazyFilterModel[]> {
    return this.filterStatusOptions;
  }

  setUpTableColumn (table: GenericTableLazyComponent): GenericTableLazyColumnModel[] {
    this.filterStatusOptions.next(this.options);

    this.accessOverviewService.getContractTypes().subscribe(data => {
      this.filterContractTypeOptions.next(data);
    });

    this.setOptionSelected(this.filterStatusOptions).subscribe(item => {
      if (item) {
        table.filterValues['status'] = item;
      }
    });
    return [
      {
        field: 'contractId',
        text: 'CONTRACT.contractId',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.TEXT,
          data: null,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.SEARCH_PLACEHOLDER'
        }
      },
      {
        field: 'contractType',
        text: 'CONTRACT.contractType',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.DROPDOWN,
          data: this.filterContractTypeOptions,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.CONTRACT_TYPE_PLACEHOLDER'
        },
      },
      {
        field: 'status',
        text: 'CONTRACT.status',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.MULTI_SELECT,
          data: this.filterStatusOptions,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.STATUS_PLACEHOLDER'
        }
      },
      {
        field: 'name',
        text: 'CONTRACT.contractName',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.TEXT,
          data: null,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.SEARCH_PLACEHOLDER'
        }
      },
      {
        field: 'clientName',
        text: 'CONTRACT.client',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.TEXT,
          data: null,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.SEARCH_PLACEHOLDER'
        }
      },
      {
        field: 'valueIncVat',
        text: 'CONTRACT.awardedAmount',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.NONE,
          data: null,
          placeHolder: ''
        }
      },
      {
        field: 'startDate',
        text: 'CONTRACT.startDate',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.NONE,
          data: null,
          placeHolder: ''
        }
      },
      {
        field: 'endDate',
        text: 'CONTRACT.endDate',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.NONE,
          data: null,
          placeHolder: ''
        }
      },
      {
        field: 'procurementName',
        text: 'CONTRACT.procurementName',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.TEXT,
          data: null,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.SEARCH_PLACEHOLDER'
        }
      },
      {
        field: 'projectName',
        text: 'CONTRACT.project',
        class: 'no-padding small-size',
        hasSort: true,
        filter: {
          type: FilterType.TEXT,
          data: null,
          placeHolder: 'OVERVIEW.FILTER_COLUMN.SEARCH_PLACEHOLDER'
        }
      }
    ];

  }

  getOptions (): GenericTableLazyFilterModel[] {
    return this.options;
  }

  setOptionSelected (filter: BehaviorSubject<GenericTableLazyFilterModel[]>): BehaviorSubject<string[]> {
    const ret: BehaviorSubject<string[]> = new BehaviorSubject<string[]>([]);
    if (filter) {
      filter.subscribe(item => {
        ret.next(item.filter(value => value.selected).map(i => i.value));
      });
    }
    return ret;
  }

  setAllowArchived (allowArchived: boolean) {
    this.allowArchived = allowArchived;
  }

  getAllowArchived () {
    return this.allowArchived;
  }

}
