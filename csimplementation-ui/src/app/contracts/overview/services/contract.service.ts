import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../core/services/base-http.service';
import { Utils } from '../../../shared/classes/utils';
import { GenericTableLazyFilterModel } from '../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { PageModel } from '../../../shared/models/page-model';
import { StatusType } from '../../common/enums/status-type.enum';
import { InitiateContractDataInfoModel } from '../../data/models/contract-data-info';

import { ContractInfoModel } from '../models/contract-info.model';

@Injectable({
    providedIn: 'root'
})
export class ContractService extends BaseHttpService<ContractInfoModel> {
    private page: PageModel = {data: {}, page: {totalNumber: 0}};
    private data: ContractInfoModel;

    private static mapToContract (data: any): ContractInfoModel {
        return {
            id: data.id,
            contractId: data.contractId,
            contractType: data.contractType,
            status: data.status,
            name: data.name,
            procurementName: data.procurementName,
            client: data.clientName,
            valueIncVat: data.valueIncVat,
            startDate: data.startDate,
            endDate: data.endDate,
            project: data.project,
            links: {
                self: getLink(data._links, 'self'),
            }
        } as ContractInfoModel;

        function getLink (links: any, key: string): string {
            return (links && links[key] && links[key].href) ? Utils.cleanUrl(links[key].href) : '';
        }
    }

    private static setAllowedOptions (val: StatusType): StatusType[] {
        const allowed: StatusType[] = [];
        switch (val) {
            case StatusType.PREPARATION:
                allowed.push(StatusType.CANCEL);
                break;
            case StatusType.PROGRESS:
                allowed.push(StatusType.AFTER);
                allowed.push(StatusType.EXECUTED);
                allowed.push(StatusType.CANCEL);
                break;
            case StatusType.AFTER:
                allowed.push(StatusType.EXECUTED);
                allowed.push(StatusType.CANCEL);
                allowed.push(StatusType.PROGRESS);
                break;
            case StatusType.EXECUTED:
                allowed.push(StatusType.ARCHIVED);
                allowed.push(StatusType.PROGRESS);
                allowed.push(StatusType.AFTER);
                break;
            case StatusType.CANCEL:
                allowed.push(StatusType.ARCHIVED);
                allowed.push(StatusType.PROGRESS);
                break;
            case StatusType.ARCHIVED:
                allowed.push(StatusType.CANCEL);
                allowed.push(StatusType.EXECUTED);
                break;
            default:
                allowed.push(StatusType.PROGRESS);
                allowed.push(StatusType.ARCHIVED);
                allowed.push(StatusType.AFTER);
                allowed.push(StatusType.CANCEL);
                allowed.push(StatusType.EXECUTED);
                allowed.push(StatusType.PROGRESS);
                break;
        }
        return allowed;
    }

    private static containsObject (list, obj) {
        for (let i = 0; list && i < list.length; i++) {
            if (list[i] === obj) {
                return false;
            }
        }
        return true;
    }

    static prepareOptions (val: any, translateValues: any[]): GenericTableLazyFilterModel[] {
        const allowed: StatusType[] = ContractService.setAllowedOptions(val);
        return [
            {
                label: translateValues[StatusType.PREPARATION],
                value: StatusType.PREPARATION.valueOf(),
                disabled: ContractService.containsObject(allowed, StatusType.PREPARATION),
                selected: false
            },
            {
                label: translateValues[StatusType.PROGRESS],
                value: StatusType.PROGRESS.valueOf(),
                disabled: ContractService.containsObject(allowed, StatusType.PROGRESS),
                selected: false
            },
            {
                label: translateValues[StatusType.AFTER],
                value: StatusType.AFTER.valueOf(),
                disabled: ContractService.containsObject(allowed, StatusType.AFTER),
                selected: false
            },
            {
                label: translateValues[StatusType.CANCEL],
                value: StatusType.CANCEL.valueOf(),
                disabled: ContractService.containsObject(allowed, StatusType.CANCEL),
                selected: false
            },
            {
                label: translateValues[StatusType.ARCHIVED],
                value: StatusType.ARCHIVED.valueOf(),
                disabled: ContractService.containsObject(allowed, StatusType.ARCHIVED),
                selected: false
            },
            {
                label: translateValues[StatusType.EXECUTED],
                value: StatusType.EXECUTED,
                disabled: ContractService.containsObject(allowed, StatusType.EXECUTED),
                selected: false
            }
        ];
    }

    getAllList (params: HttpParams) {

        params = params.append('projection', 'contractInfo');

        return this.httpClient.get<ContractInfoModel[]>(this.endpoint + 'contract/overview', {
            params
        }).pipe(
            map((data: any) => {
                const contractProcesses: ContractInfoModel[] = [];
                for (const item of data._embedded.contracts) {
                    const contract: ContractInfoModel = ContractService.mapToContract(item);
                    contractProcesses.push(contract);
                }
                this.page.page.totalNumber = data.page.totalElements;
                this.page.data = contractProcesses;
                return this.page;
            })
        );
    }

    getContractTypes () {
        return this.httpClient.get<GenericTableLazyFilterModel[]>(this.endpoint + 'contract/types').pipe(
            map((filterModel: any) => {
                const filters: GenericTableLazyFilterModel[] = [];
                for (const item of filterModel) {
                    const filter: GenericTableLazyFilterModel = {
                        label: item,
                        value: item,
                        disabled: false,
                        selected: false
                    };
                    filters.push(filter);
                }
                return filters;
            })
        );
    }

    // TODO CSPROC-2149 check depending on contract state if it can be deleted or not
    deleteContract (contract: ContractInfoModel) {
        if (contract && contract.links.self) {
            return this.httpClient.delete<ContractInfoModel>(contract.links.self);
        }
    }

    getTotalResults () {
        return this.page.page.totalNumber;
    }

    validateContractName (contractName: string, contract: InitiateContractDataInfoModel) {
        const params = new HttpParams()
            .set('name', contractName);
        if (contract) {
            return this.httpClient.get<boolean>(contract.links.self + '/validate/duplicate-name', {params});
        } else {
            return this.httpClient.get<boolean>(this.endpoint + 'contract/validate/duplicate-name', {params});
        }
    }
}
