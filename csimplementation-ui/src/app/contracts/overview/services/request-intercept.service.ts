import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { StatusType } from '../../common/enums/status-type.enum';
import { AccessOverviewSupportService } from './access-overview-support.service';

@Injectable({
  providedIn: 'root'
})
// TODO rename this to something that indicates what it does
export class RequestInterceptService implements HttpInterceptor {

  constructor (private accessOverviewSupportService: AccessOverviewSupportService) {
  }

  // TODO use extraHttpParams in GenericLazyTable to add that modifiedDate CSPROC-1997
  intercept (req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const status: StatusType[] = [StatusType.EXECUTED, StatusType.PROGRESS, StatusType.PREPARATION,
      StatusType.CANCEL, StatusType.AFTER, StatusType.ARCHIVED];
    if (req.params.get('sort') && req.url.indexOf('contract/overview') !== -1 && req.params.get('status').length === 0) {
      req = req.clone({setParams: {'sort': req.params.get('sort') + ',' + 'modifiedDate,ASC'}});
      /* TODO CSPROC-1998 - Try if the archived external button may modify the filter and if that fires the
      onLazyLoad; another possibility is adding refresh method to table and call that */
      if (!this.accessOverviewSupportService.getAllowArchived()) {
        req = req.clone({setParams: {'status': status.slice(0, 5).join(' ,')}});
      } else {
        req = req.clone({setParams: {'status': status.join(' ,')}});
      }
    }
    return next.handle(req);
  }
}
