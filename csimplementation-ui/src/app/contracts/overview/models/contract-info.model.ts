export interface ContractInfoModel {
  id: number;
  contractId: string;
  contractType: string;
  status: string;
  name: string;
  procurementName: string;
  client: string;
  valueIncVat: number;
  startDate: Date;
  endDate: Date;
  project: string;
  links: {
    self: string;
  };
}
