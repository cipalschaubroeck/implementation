import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { GenericTableLazyComponent } from '../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import {
  GenericTableLazyButtonModel,
  GenericTableLazyFilterModel
} from '../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { DeleteConfirmationComponent } from '../../../shared/dialogs/delete-confirmation/delete-confirmation.component';
import { WarningComponent } from '../../../shared/dialogs/warning/warning.component';
import { PageModel } from '../../../shared/models/page-model';
import { CrudResourceService } from '../../../shared/services/crud-resource.service';
import { GenericMessageService } from '../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../shared/services/generic-translate.service';
import { StatusType } from '../../common/enums/status-type.enum';
import { AddContractComponent } from '../dialogs/add-contract/add-contract.component';
import { SetStatusComponent } from '../dialogs/set-status/set-status.component';
import { ContractInfoModel } from '../models/contract-info.model';
import { AccessOverviewSupportService } from '../services/access-overview-support.service';
import { ContractService } from '../services/contract.service';

@Component({
  selector: 'app-access-overview',
  templateUrl: './access-overview.component.html',
  styleUrls: ['./access-overview.component.scss']
})
export class AccessOverviewComponent implements OnInit {

  dialogSelection: string;
  page: PageModel = {data: [], page: {totalNumber: 0}};
  elements: BehaviorSubject<ContractInfoModel[]> = new BehaviorSubject<ContractInfoModel[]>([]);
  rightButtons: GenericTableLazyButtonModel[] = [];
  leftButtons: GenericTableLazyButtonModel[] = [];

  @ViewChild(GenericTableLazyComponent, { static: true }) table: GenericTableLazyComponent;

  constructor (
    private translate: GenericTranslateService,
    private accessOverviewService: ContractService,
    private accessOverviewSupportService: AccessOverviewSupportService,
    private dialogService: DialogService,
    private crudService: CrudResourceService,
    private router: Router,
    private messageService: GenericMessageService) {
  }

  ngOnInit () {

    GenericTableLazyComponent.loadButton('refresh', 'ui-icon-refresh', 'COMMON.BUTTONS.Refresh',
      this.refresh, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('addContract', 'ui-icon-plus', 'OVERVIEW.BUTTONS.AddContract',
      this.triggerAddContractDialog, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('editContract', 'ui-icon-edit', 'OVERVIEW.BUTTONS.EditContract',
      this.triggerEditContract, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('deleteContract', 'ui-icon-trash', 'OVERVIEW.BUTTONS.DeleteContract',
      this.triggerDeleteContractDialog, this, false, this.rightButtons);

    GenericTableLazyComponent.loadButton('hideShowFilters', 'ui-icon-filter', 'OVERVIEW.BUTTONS.HideShow',
      this.showFilters, this, false, this.leftButtons);
    GenericTableLazyComponent.loadButton('clearFilters', 'ui-icon-close', 'COMMON.BUTTONS.clearFilters',
      this.clearFilters, this, false, this.leftButtons);
    GenericTableLazyComponent.loadButton('archivedShow', 'ui-icon-archive', 'OVERVIEW.BUTTONS.ArchivedShow',
      this.toggleArchivedContracts, this, false, this.leftButtons);

    this.table.initButtons(this.rightButtons, this.leftButtons);

    this.table.initTableConfig({
      columns: this.accessOverviewSupportService.setUpTableColumn(this.table),
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'OVERVIEW.NoResults',
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 15]
      },
      sortField: ('contractType'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.accessOverviewService);

  }

  showSetStatusDialog (elementStatus: StatusType) {
    this.table.selectedRow = null;
    let statusOptions: GenericTableLazyFilterModel[];
    statusOptions = ContractService.prepareOptions(elementStatus, this.translateOptions());

    const title = this.translate.translate('OVERVIEW.BUTTONS.SetStatus');

    this.dialogService.open(SetStatusComponent, {
      data: {
        'options': statusOptions,
        'save': (ref: DynamicDialogRef) => this.patchContract(ref),
        'onChange': ($event: Event) => this.dialogOptionSelected($event)
      },
      header: title,
      width: '30%',
      contentStyle: {'height': '130px', 'overflow': 'visible'}
    });
  }

  dialogOptionSelected ($event) {
    this.dialogSelection = $event.value;
  }

  patchContract (ref: DynamicDialogRef) {
    const patchProperties = {};

    patchProperties['status'] = this.dialogSelection;
    if (this.table.selectedRow) {
      this.crudService.patchResource(this.table.selectedRow.links.self, patchProperties).then(() => {
        this.messageService.popMessage(
          'success',
          'MESSAGE_SERVICE.success',
          'MESSAGE_SERVICE.updated');
      });
      this.table.filterTable('', '', '');
      ref.close();
    }
  }

  triggerDeleteContractDialog () {
    const title = this.translate.translate('OVERVIEW.BUTTONS.Warning');

    if (this.table.selectedRow) {
      this.dialogService.open(DeleteConfirmationComponent, {
        data: {
          'delete': {click: (ref: DynamicDialogRef) => this.deleteContract(ref)},
          'confirmation_msg': 'OVERVIEW.BUTTONS.DeleteConfirmation'
        },
        header: title,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    } else {
      const message = this.translate.translate('OVERVIEW.BUTTONS.ContractSelected');
      this.dialogService.open(WarningComponent, {
        data: {
          'message': message
        },
        header: title,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    }
  }

  triggerEditContract () {
    const title = this.translate.translate('OVERVIEW.BUTTONS.Warning');

    if (this.table.selectedRow) {
      this.router.navigate(['contract/' + this.table.selectedRow.id + '/data']).then((e) => {
        if (!e) {
          console.log('Navigation error');
        }
      });
    } else {
      const message = this.translate.translate('OVERVIEW.BUTTONS.ContractSelected');
      this.dialogService.open(WarningComponent, {
        data: {
          'message': message
        },
        header: title,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    }
  }

  deleteContract (ref: DynamicDialogRef) {
    this.accessOverviewService.deleteContract(this.table.selectedRow).subscribe(response => {
      this.messageService.popMessage(
        'success',
        'MESSAGE_SERVICE.success',
        'MESSAGE_SERVICE.success');
    }, error => {
      this.messageService.popMessage(
        'error',
        'MESSAGE_SERVICE.error',
        'MESSAGE_SERVICE.error');
    });
    this.table.filterTable('', '', '');
    this.table.selectedRow = null;
    ref.close();
  }

  refresh () {
    this.table.selectedRow = null;
    this.table.filterTable('', '', '');
  }

  triggerAddContractDialog () {
    const title = this.translate.translate('OVERVIEW.BUTTONS.AddContract');
    this.dialogService.open(AddContractComponent, {
      data: {},
      header: title,
      width: '30%',
      contentStyle: {'height': '230px', 'overflow': 'visible'}
    });
  }

  translateOptions (): any[] {
    const translateValues = [];
    translateValues[StatusType.PROGRESS] = 'OVERVIEW.FILTER.progress';
    translateValues[StatusType.CANCEL] = 'OVERVIEW.FILTER.cancel';
    translateValues[StatusType.AFTER] = 'OVERVIEW.FILTER.after';
    translateValues[StatusType.ARCHIVED] = 'OVERVIEW.FILTER.archived';
    translateValues[StatusType.EXECUTED] = 'OVERVIEW.FILTER.executed';
    translateValues[StatusType.PREPARATION] = 'OVERVIEW.FILTER.preparation';
    const keys = Object.keys(translateValues);

    keys.forEach(key => {
      translateValues[key] = this.translate.translate(translateValues[key]);
    });
    return translateValues;
  }

  /**
   * Clears all the child table filters
   */
  clearFilters () {
    this.table.clearFilters();
  }

  /**
   * Shows or hide child table filter
   */
  showFilters () {
    this.table.showFilters();
  }

  toggleArchivedContracts () {
    const hasFilterValuesSelected: string[] = this.table.filterValues['status'];
    let optionsAux: GenericTableLazyFilterModel[] = [];
    if (this.accessOverviewSupportService.getAllowArchived()) {
      this.accessOverviewSupportService.setAllowArchived(false);
      this.table.leftButtons['archivedShow'].icon = 'ui-icon-archive';
      this.table.leftButtons['archivedShow'].title = 'OVERVIEW.BUTTONS.ArchivedShow';
      /**
       *  Removes Archived option in the status column filter multiSelect
       */
      optionsAux = this.accessOverviewSupportService.getOptions().filter(key => key.value !== StatusType.ARCHIVED);

    } else {
      this.accessOverviewSupportService.setAllowArchived(true);
      this.table.leftButtons['archivedShow'].icon = 'ui-icon-airplay';
      this.table.leftButtons['archivedShow'].title = 'OVERVIEW.BUTTONS.ArchivedHide';

      if (hasFilterValuesSelected.length > 0) {
        this.accessOverviewSupportService.getOptions()
          .find(key => key.value === StatusType.ARCHIVED).selected = true;

      } else {
        this.accessOverviewSupportService.getOptions().map(option => option.selected = false);
      }
      optionsAux = this.accessOverviewSupportService.getOptions();
    }

    this.accessOverviewSupportService.getOptions().forEach(option => {
      if (hasFilterValuesSelected.includes(option.value)) {
        option.selected = true;
      }
    });
    this.accessOverviewSupportService.getFilterStatusOptions().next(optionsAux);
    this.table.filterTable('', '', '');
  }
}
