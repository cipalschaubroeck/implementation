import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { BehaviorSubject, Subscription } from 'rxjs';
import { CrudResourceService } from '../../../../shared/services/crud-resource.service';
import { ContractService } from '../../services/contract.service';

@Component({
  selector: 'app-add-contract',
  templateUrl: './add-contract.component.html',
  styleUrls: ['./add-contract.component.css']
})
export class AddContractComponent implements OnInit, OnDestroy {

  addContractForm: FormGroup = this.fb.group([]);
  duplicateName: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(null);
  subscription: Subscription;

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig, private router: Router,
               private fb: FormBuilder, private accessOverviewService: ContractService,
               private crudService: CrudResourceService) {
  }

  ngOnInit () {
    const controls: [][] = [];
    controls['contractName'] = new FormControl('',
      Validators.compose([Validators.required]));
    this.addContractForm = this.fb.group(controls);
    this.setEvents();
  }

  ngOnDestroy (): void {
    this.subscription.unsubscribe();
  }

  cancel () {
    this.ref.close();
  }

  save () {
    const contractName = this.addContractForm.value['contractName'];

    this.crudService.createResource('/contract', {'name': contractName}).then(data => {

      this.router.navigate(['contract/' + data.id + '/data']).then(e => {
        if (!e) {
          console.log('Navigation has failed!');
        }
      });
      this.ref.close();
    }, error1 => {
      console.log(error1);
      this.ref.close();
    });
  }

  validateName (value) {
    this.accessOverviewService.validateContractName(value, null)
      .subscribe(response => {
        this.duplicateName.next(!response);
      });
  }

  setEvents (): void {
    const input = this.addContractForm.valueChanges;
    if (input) {
      this.subscription = input.subscribe(val => {
        this.validateName(val.contractName);
      });
    }
  }
}
