import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-set-status',
  templateUrl: './set-status.component.html',
  styleUrls: ['./set-status.component.css']
})
export class SetStatusComponent implements OnInit {

  options: [] = [];

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit () {
    this.options = this.config.data.options;
  }

  cancel () {
    this.ref.close();
  }
}
