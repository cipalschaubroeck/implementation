import { StatusType } from '../../common/enums/status-type.enum';
import { BailTypeEnum } from './bail-type.enum';

export interface BailInfoModel {
  status?: StatusType;
  id?: string;
  type?: BailTypeEnum;
  amount?: number;
  percentage?: string;
  description?: string;
  amountMinusVat?: number;
  _links?: {
    self: string;
  };
}
