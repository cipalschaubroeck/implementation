import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { BaseHttpService } from '../../../core/services/base-http.service';
import { BailInfoModel } from '../model/bail-model';

@Injectable({
  providedIn: 'root'
})
export class BailService extends BaseHttpService<any> {

  getInitiateContractBail (contractId: string): Promise<BailInfoModel> {
    const headers = new HttpHeaders().set('Cache-Control', 'no-cache');
    const params: HttpParams = new HttpParams().set('projection', 'initiateContractBailInfo');
    return new Promise<BailInfoModel>(response => {
      this.httpClient.get<BailInfoModel>(this.endpoint + 'contract/' + contractId, {headers: headers, params: params})
        .subscribe(data => {
          response(data);
        });
    });
  }

  saveUpdateBail (link: string, body: BailInfoModel): Observable<BailInfoModel> {
    return this.httpClient.post<BailInfoModel>(link + '/bail', body);
  }

}
