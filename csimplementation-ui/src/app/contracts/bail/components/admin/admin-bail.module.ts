import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { BailService } from '../../services/bail.service';
import { AdminBailRoutingModule } from './admin-bail-routing.module';
import { AdminBailComponent } from './admin-bail.component';

const imports = [
  SharedModule,
  AdminBailRoutingModule
];

const components = [
  AdminBailComponent
];

@NgModule({
  declarations: components,
  imports: imports,
  providers: [
    BailService
  ]
})
export class AdminBailModule {
}
