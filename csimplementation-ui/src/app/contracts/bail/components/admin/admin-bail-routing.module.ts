import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminBailComponent } from './admin-bail.component';

const routes: Routes = [
  {path: 'admin-bail', component: AdminBailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminBailRoutingModule {
}
