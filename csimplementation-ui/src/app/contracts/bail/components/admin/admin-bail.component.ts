import { Component, OnDestroy, OnInit } from '@angular/core';
import { StatusType } from '../../../common/enums/status-type.enum';
import { ContractBail } from '../contract-bail';

@Component({
  selector: 'app-bail',
  templateUrl: '../bail.component.html',
  styleUrls: ['../bail.component.css']
})
export class AdminBailComponent extends ContractBail implements OnInit, OnDestroy {

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit (): any {
    return super.ngOnInit().then(response => {
      if (response.status === StatusType.PREPARATION) {
        this.bailInfo = {};
      } else {
        this.bailInfo = response;
      }
      this.initForm(this.bailInfo);
      this.setValidations(this.bailInfo.type);

      if (response.status === StatusType.PREPARATION) {
        this.form.disable();
      }
    });
  }

  protected updateBailData () {
    this.updateBail().subscribe(() => {
      this.messageService.success();
      this.formHasChanges = false;
    }, () => this.messageService.error());
  }
}
