import { Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { GenericMessageService } from '../../../shared/services/generic-message.service';
import { ContractManagementService } from '../../management/services/contract-management.service';
import { BailInfoModel } from '../model/bail-model';
import { BailTypeEnum } from '../model/bail-type.enum';
import { BailService } from '../services/bail.service';

@Injectable()
export abstract class ContractBail implements OnInit {

  protected form: FormGroup;
  protected subscriptions: Subscription[] = [];
  protected bailType = BailTypeEnum;
  protected bailInfo: BailInfoModel;
  protected formSubmitAttempt: boolean;
  protected formHasChanges: boolean;
  private contractId: string;

  protected static calculateBailAmount (value: string): string {
    // TODO bail amount needs to be calculated
    return value;
  }

  protected static setAmount (contract: BailInfoModel, type: BailTypeEnum): number {
    let amount = null;
    if (contract.type === type) {
      amount = contract.amount;
    }
    return amount;
  }

  protected abstract updateBailData ();

  constructor (protected initiateBailService: BailService,
               protected router: Router,
               protected route: ActivatedRoute,
               protected messageService: GenericMessageService,
               protected contractManagementService: ContractManagementService) {
  }

  ngOnInit (): Promise<any> {
    this.contractId = this.route.snapshot.parent.paramMap.get('contractId');
    return this.initiateBailService.getInitiateContractBail(this.contractId);
  }

  private disableResetControl (key: string) {
    this.form.controls[key].disable();
    this.form.controls[key].reset();
  }

  private setConditionalValidations () {
    this.subscriptions.push(this.form.get('selection').valueChanges.subscribe(select => {
      this.setValidations(select);
    }));
  }

  private listenToStandardBailPercentageChange () {
    this.subscriptions.push(this.form.controls.standardBailPercentage.valueChanges.subscribe(value => {
      this.form.controls.bailAmount.setValue(ContractBail.calculateBailAmount(value));
    }));
  }

  protected initForm (contract: BailInfoModel) {
    this.form = new FormGroup({
      selection: new FormControl(contract.type ? contract.type : ''),
      specialBailAmount: new FormControl({
        value: ContractBail.setAmount(contract, this.bailType.SPECIAL_BAIL),
        disabled: contract.type !== this.bailType.SPECIAL_BAIL
      }),
      bailAmount: new FormControl({value: ContractBail.setAmount(contract, this.bailType.BAIL), disabled: true}),
      standardBailPercentage: new FormControl({
        value: contract.percentage,
        disabled: contract.type !== this.bailType.BAIL
      }),
      bailDescription: new FormControl(contract.description)
    });
    this.setConditionalValidations();
    this.listenToStandardBailPercentageChange();

    this.subscriptions.push(this.form.valueChanges.subscribe(input => {
      this.formHasChanges = true;
    }));
  }

  protected setValidations (bailType: BailTypeEnum) {
    this.form.clearValidators();
    if (this.bailType.SPECIAL_BAIL === bailType) {
      this.form.controls.specialBailAmount.setValidators(Validators.required);
    } else if (this.bailType.BAIL === bailType) {
      this.form.controls.standardBailPercentage.setValidators(Validators.required);
    }
  }

  protected validate () {
    this.formSubmitAttempt = true;

    if (this.form.valid) {
      this.messageService.validated();
    } else {
      this.messageService.notValidated();
    }
  }

  protected isFieldValid (field: string) {
    return this.form.get(field).invalid && (this.form.get(field).touched ||
      (this.form.get(field).untouched && this.form.get(field).invalid && this.formSubmitAttempt));
  }

  protected updateBail (): Observable<any> {
    if (this.formHasChanges) {
      let val;
      if (this.form.controls.selection.value === this.bailType.SPECIAL_BAIL) {
        val = this.form.controls.specialBailAmount.value;
      } else if (this.form.controls.selection.value === this.bailType.BAIL) {
        val = this.form.controls.bailAmount.value;
      } else {
        val = null;
      }

      const bail: BailInfoModel = {
        amount: val,
        description: this.form.controls.bailDescription.value,
        percentage: this.form.controls.standardBailPercentage.value,
        type: this.form.controls.selection.value ? this.form.controls.selection.value : null
      };
      return this.initiateBailService.saveUpdateBail(this.bailInfo._links.self['href'], bail);
    }
  }

  protected onRadioClick () {
    if (this.form.controls.selection.enabled) {
      if (!this.form.controls.selection.value) {
        this.disableResetControl('specialBailAmount');
        this.disableResetControl('standardBailPercentage');
        this.disableResetControl('bailDescription');
      } else if (this.bailType.SPECIAL_BAIL === this.form.controls.selection.value) {
        this.form.controls.specialBailAmount.enable();
        if (this.form.controls.bailDescription.disabled) {
          this.form.controls.bailDescription.enable();
        }
        this.disableResetControl('standardBailPercentage');
      } else if (this.bailType.BAIL === this.form.controls.selection.value) {
        this.disableResetControl('specialBailAmount');
        if (this.form.controls.bailDescription.disabled) {
          this.form.controls.bailDescription.enable();
        }
        this.form.controls.standardBailPercentage.enable();
      }
      this.formSubmitAttempt = false;
      this.form.markAsUntouched();
      this.updateBailData();
    }
  }
}
