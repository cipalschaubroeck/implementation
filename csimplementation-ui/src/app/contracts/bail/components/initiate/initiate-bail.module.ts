import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { BailService } from '../../services/bail.service';

import { InitiateBailRoutingModule } from './initiate-bail-routing.module';
import { InitiateBailComponent } from './initiate-bail.component';

const imports = [
  SharedModule,
  InitiateBailRoutingModule
];

const components = [
  InitiateBailComponent
];

@NgModule({
  declarations: components,
  imports: imports,
  providers: [
    BailService
  ]
})
export class InitiateBailModule {
}
