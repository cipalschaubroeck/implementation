import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormStatusEnum } from '../../../common/enums/formStatus.enum';
import { ContractBail } from '../contract-bail';

@Component({
  selector: 'app-bail',
  templateUrl: '../bail.component.html',
  styleUrls: ['../bail.component.css']
})
export class InitiateBailComponent extends ContractBail implements OnInit, OnDestroy {

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit (): any {
    return super.ngOnInit().then(response => {
      this.bailInfo = response;
      this.initForm(this.bailInfo);
      this.setValidations(this.bailInfo.type);
      this.emitInitiateContractBailValidation();
    });
  }

  private emitInitiateContractBailValidation () {
    this.contractManagementService.emitInitiateContractBailValidation(this.bailInfo._links.self['href'], null)
      .then(formStatus => {
        if (formStatus === FormStatusEnum.READ_ONLY) {
          this.form.disable();
        }
      });
  }

  private isValidateInitiateContractProcessComplete (): Promise<FormStatusEnum> {
    return this.contractManagementService.emitValidateInitiateContractProcess(this.bailInfo._links.self['href'], null);
  }

  protected updateBailData () {
    this.updateBail().subscribe(() => {
      this.messageService.success();
      this.emitInitiateContractBailValidation();
      this.formHasChanges = false;
    }, () => this.messageService.error());
  }

}
