import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitiateBailComponent } from './initiate-bail.component';

const routes: Routes = [
  {path: 'bail', component: InitiateBailComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateBailRoutingModule {
}
