import { HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../core/services/base-http.service';
import { Utils } from '../../../shared/classes/utils';
import { InitiateContractDataInfoModel } from '../models/contract-data-info';
import { ProcurementContractFieldModel } from '../models/procurement-contract-field';

@Injectable({
  providedIn: 'root'
})
export class ContractDataService extends BaseHttpService<any> {

  private static mapInitiateContractData (data: any): InitiateContractDataInfoModel {
    return {
      contractId: data.contractId,
      name: data.name,
      status: data.status,
      amountVat: data.amountVat,
      amountMinusVat: data.amountMinusVat,
      valueIncVat: data.valueIncVat,
      vat: data.vat,
      decisionMakingBody: data.decisionMakingBody,
      procurementName: data.procurementName,
      qualification: data.qualification,
      procedure: data.procedureType,
      parcel: data.parcel,
      description: data.description,
      referenceNumber: data.referenceNumber,
      links: {
        self: getLink(data._links, 'self')
      }
    };

    function getLink (links: any, key: string): string {
      return (links && links[key] && links[key].href) ? Utils.cleanUrl(links[key].href) : '';
    }
  }

  getInitiateContractData (contractId: string): Observable<InitiateContractDataInfoModel> {
    const headers = new HttpHeaders().set('Cache-Control', 'no-cache');
    const params: HttpParams = new HttpParams().set('projection', 'initiateContractDataInfo');
    return this.httpClient.get<any>(this.endpoint + 'contract/' + contractId, {headers: headers, params: params})
      .pipe(
        map(ContractDataService.mapInitiateContractData)
      );
  }

  saveUpdateProcurementInContract (link: string, body: ProcurementContractFieldModel) {
    return this.httpClient.post(link + ' /procurement', body);
  }

  patchContract (link: string, body: any) {
    return new Promise<any>(response => {
      this.httpClient.patch(link, body).subscribe(response);
    });
  }

}
