export interface InitiateContractDataInfoModel {
  contractId?: string;
  name?: string;
  status?: string;
  procurementName?: string;
  parcel?: string;
  description?: string;
  referenceNumber?: string;
  qualification?: string;
  procedure?: string;
  decisionMakingBody?: string;
  amountVat?: number;
  amountMinusVat?: number;
  valueIncVat?: number;
  vat?: number;
  links?: {
    self: string;
  };
}
