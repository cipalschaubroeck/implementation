export interface ProcurementContractFieldModel {
  name?: string;
  referenceNumber?: string;
  description?: string;
}
