export enum QualificationTypes {
  WORKS = 'WORKS',
  DELIVERIES = 'DELIVERIES',
  SERVICES = 'SERVICES'
}
