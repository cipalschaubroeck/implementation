import { Injectable, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import { GenericMessageService } from '../../../shared/services/generic-message.service';
import { ValidateDuplicateName } from '../../../shared/validations/ValidateDuplicateName.validation';
import { ValidateNumber } from '../../../shared/validations/ValidateNumber.validation';
import { ContractManagementService } from '../../management/services/contract-management.service';
import { ProcedureTypeRestrictions } from '../enums/procedureTypes.enums';
import { QualificationTypes } from '../enums/qualificationTypes.enum';
import { InitiateContractDataInfoModel } from '../models/contract-data-info';
import { ProcurementContractFieldModel } from '../models/procurement-contract-field';
import { ContractDataService } from '../services/contract-data.service';

@Injectable()
export abstract class ContractData implements OnInit {

  protected form: FormGroup;
  protected formSubmitAttempt: boolean;
  protected contract: InitiateContractDataInfoModel;
  protected qualificationOptions: any[];
  protected procedureTypeOptions: any[];
  protected decisionMakingBodyOptions: any[];
  protected vatOptions: any[] = [];
  protected formHasChanges: boolean;
  protected subscriptions: Subscription[] = [];
  protected contractId: string;

  constructor (protected route: ActivatedRoute,
               protected contractDataService: ContractDataService,
               protected router: Router,
               protected messageService: GenericMessageService,
               protected contractManagementService: ContractManagementService) {
  }

  protected static setProcedureTypeOptions (): any[] {
    return [
      {label: 'CONTRACT_DATA.PROCEDURE_TYPES.PublicProcedure', value: ProcedureTypeRestrictions.PUBLIC_PROCEDURE},
      {
        label: 'CONTRACT_DATA.PROCEDURE_TYPES.NonPublicProcedure',
        value: ProcedureTypeRestrictions.NON_PUBLIC_PROCEDURE
      },
      {
        label: 'CONTRACT_DATA.PROCEDURE_TYPES.CompetitiveProcedureNegotiation',
        value: ProcedureTypeRestrictions.COMPETITIVE_PROCEDURE_NEGOTIATION
      },
      {
        label: 'CONTRACT_DATA.PROCEDURE_TYPES.SimplifiedNegotiatedProcedureWithPriorPublication',
        value: ProcedureTypeRestrictions.SIMPLIFIED_NEGOTIATED_PROCEDURE
      },
      {
        label: 'CONTRACT_DATA.PROCEDURE_TYPES.NegotiatedWithoutPriorPublication',
        value: ProcedureTypeRestrictions.NEGOTIATED_PROCEDURE_WITHOUT_PRIOR_PUBLICATION
      },
      {label: 'CONTRACT_DATA.PROCEDURE_TYPES.AcceptedInvoice', value: ProcedureTypeRestrictions.ACCEPTED_INVOICE},
      {label: 'CONTRACT_DATA.PROCEDURE_TYPES.OngoingContract', value: ProcedureTypeRestrictions.ONGOING_CONTRACT}];
  }

  protected static intiForm (contract: InitiateContractDataInfoModel): FormGroup {
    return new FormGroup({
      contractId: new FormControl({value: contract.contractId, disabled: true}),
      name: new FormControl(contract.name, {
        validators: [Validators.required],
        asyncValidators: [ValidateDuplicateName(contract)], updateOn: 'blur'
      }),
      procurementName: new FormControl(contract.procurementName, Validators.compose([Validators.required])),
      parcel: new FormControl(contract.parcel),
      description: new FormControl(contract.description),
      referenceNumber: new FormControl(contract.referenceNumber),
      qualification: new FormControl(contract.qualification, Validators.compose([Validators.required])),
      procedureType: new FormControl(contract.procedure, Validators.compose([Validators.required])),
      decisionMakingBody: new FormControl(contract.decisionMakingBody, Validators.compose([Validators.required])),
      approvalDocument: new FormControl(),
      dateOfApproval: new FormControl(),
      dateClosingOrderLetter: new FormControl(),
      ClosingOrderDocument: new FormControl(),
      dateInformationLetter: new FormControl(),
      informationLetterDocument: new FormControl(),
      dateStartDecision: new FormControl(),
      startDecisionDocument: new FormControl(),
      amountMinusVat: new FormControl(contract.amountMinusVat, Validators.compose([Validators.required, ValidateNumber()])),
      amountVat: new FormControl(contract.amountVat, Validators.compose([Validators.required, ValidateNumber()])),
      vat: new FormControl(contract.vat, Validators.compose([Validators.required, ValidateNumber()])),
      valueIncVat: new FormControl({value: contract.valueIncVat, disabled: true})
    });
  }

  protected static setQualificationOptions () {
    return [
      {label: 'CONTRACT_DATA.QUALIFICATION_TYPES.Works', value: QualificationTypes.WORKS},
      {label: 'CONTRACT_DATA.QUALIFICATION_TYPES.Deliveries', value: QualificationTypes.DELIVERIES},
      {label: 'CONTRACT_DATA.QUALIFICATION_TYPES.Services', value: QualificationTypes.SERVICES}
    ];
  }

  protected static setDecisionMakingBodyOptions () {
    return [
      {label: 'Decision making body MOCK 1', value: 'Decision making body MOCK 1'},
      {label: 'Decision making body MOCK 2', value: 'Decision making body MOCK 2'},
      {label: 'Decision making body MOCK 3', value: 'Decision making body MOCK 3'}
    ];
  }

  protected static setVatOptions () {
    return [
      {label: '15%', value: 15.00},
      {label: '20%', value: 20.00},
      {label: '30%', value: 30.00}
    ];
  }

  ngOnInit (): void {
    this.qualificationOptions = ContractData.setQualificationOptions();
    this.procedureTypeOptions = ContractData.setProcedureTypeOptions();
    this.decisionMakingBodyOptions = ContractData.setDecisionMakingBodyOptions();
    this.vatOptions = ContractData.setVatOptions();

    this.contractId = this.route.snapshot.parent.paramMap.get('contractId');
  }

  protected validate () {
    this.formSubmitAttempt = true;

    if (this.form.valid) {
      this.messageService.validated();
    } else {
      this.messageService.notValidated();
    }
  }

  protected isFieldValid (field: string) {
    return this.form.get(field).invalid && (this.form.get(field).touched ||
      (this.form.get(field).untouched && this.form.get(field).invalid && this.formSubmitAttempt));
  }

  /**
   * Selects correct error text based on the validation error produced
   * @param key input to check for validation error
   */
  protected getErrorList (key): string {
    if (this.form.controls[key].errors.required) {
      return 'VALIDATIONS.Required';
    } else if (this.form.controls[key].errors.numeric) {
      return 'VALIDATIONS.Numeric';
    } else if (this.form.controls[key].errors.duplicateName) {
      return 'VALIDATIONS.DuplicateName';
    } else {
      return 'error';
    }
  }

// TODO CSPROC-2159 define VAT calculation
  protected vatChange () {
    // TODO CSPROC-2172 get this value from the backend
    if (this.form.controls['amountMinusVat'].value && this.form.controls['amountVat'].value) {
      this.form.controls['valueIncVat'].setValue(
        (+this.form.controls['amountMinusVat'].value) + (+this.form.controls['amountVat'].value)
      );
    }
  }

  protected emitInitiateContractDataValidation () {
    this.contractManagementService.emitInitiateContractDataValidation(this.contract.links.self, null)
      .then();
  }

  protected disableForm () {
    this.form.disable();
  }

  protected saveUpdateProc (key: string, value: any): Observable<any> {
    if (this.formHasChanges) {
      const procurement: ProcurementContractFieldModel = {};
      procurement[key] = value;
      return this.contractDataService.saveUpdateProcurementInContract(this.contract.links.self, procurement);
    } else {
      return new Observable(null);
    }
  }

  protected patchCon (key: string): Promise<any> {
    if (this.formHasChanges) {
      const resourceBody = {};
      resourceBody[key] = this.form.controls[key].value === '' ? null : this.form.controls[key].value;
      return this.contractDataService.patchContract(this.contract.links.self, resourceBody);
    } else {
      return new Promise<any>(null);
    }
  }

}
