import { Component, OnDestroy, OnInit } from '@angular/core';
import { StatusType } from '../../../common/enums/status-type.enum';
import { ContractData } from '../contract-data';

@Component({
  selector: 'app-administration-contract-data',
  templateUrl: '../contract-data.component.html',
  styleUrls: ['../contract-data.component.css']
})
export class AdminDataComponent extends ContractData implements OnInit, OnDestroy {

  ngOnDestroy (): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit (): void {
    super.ngOnInit();

    this.contractDataService.getInitiateContractData(this.contractId).subscribe(dat => {
      if (dat) {
        if (dat.status === StatusType.PREPARATION) {
          this.contract = {};
        } else {
          this.contract = dat;
        }
        this.form = ContractData.intiForm(this.contract);

        if (dat.status === StatusType.PREPARATION) {
          this.disableForm();
        }
      }

      this.subscriptions.push(this.form.valueChanges.subscribe(input => {
        this.formHasChanges = true;
      }));

    }, () => {
      this.messageService.error();
      this.router.navigate(['home']).then(e => {
        if (!e) {
          console.log('Navigation has failed!');
        }
      });
    });
  }

  private saveUpdateProcurement (key: string, value: any) {
    this.saveUpdateProc(key, value).subscribe(() => {
      this.messageService.success();
      this.form.controls[key].markAsPristine();
      this.formHasChanges = false;
    });
  }

  private patchContract (key: string) {
    this.patchCon(key).then(response => {
      if (response) {
        this.messageService.success();
        this.form.controls[key].markAsPristine();
        this.formHasChanges = false;
      }
    });
  }
}
