import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { AdministrativeDocumentsService } from '../../../management/administration/services/administrative-documents.service';
import { AdminDataRoutingModule } from './admin-data-routing.module';
import { AdminDataComponent } from './admin-data.component';

const imports = [
  SharedModule,
  AdminDataRoutingModule
];

const components = [
  AdminDataComponent
];

@NgModule({
  imports: imports,
  declarations: components,
  providers: [
    AdministrativeDocumentsService
  ]
})
export class AdminDataModule {
}
