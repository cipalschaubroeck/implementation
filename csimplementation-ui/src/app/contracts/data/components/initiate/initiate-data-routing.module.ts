import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitiateDataComponent } from './initiate-data.component';

const routes: Routes = [
  {path: 'data', component: InitiateDataComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateDataRoutingModule {
}
