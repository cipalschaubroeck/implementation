import { NgModule } from '@angular/core';
import { SharedModule } from '../../../../shared/shared.module';
import { AdministrativeDocumentsService } from '../../../management/administration/services/administrative-documents.service';
import { InitiateDataRoutingModule } from './initiate-data-routing.module';
import { InitiateDataComponent } from './initiate-data.component';

const imports = [
  SharedModule,
  InitiateDataRoutingModule
];

const components = [
  InitiateDataComponent
];

@NgModule({
  imports: imports,
  declarations: components,
  providers: [
    AdministrativeDocumentsService
  ]
})
export class InitiateDataModule {
}
