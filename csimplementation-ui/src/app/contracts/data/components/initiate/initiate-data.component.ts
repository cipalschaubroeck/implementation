import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormStatusEnum } from '../../../common/enums/formStatus.enum';
import { ContractData } from '../contract-data';

@Component({
  selector: 'app-initiate',
  templateUrl: '../contract-data.component.html',
  styleUrls: ['../contract-data.component.css']
})

export class InitiateDataComponent extends ContractData implements OnInit, OnDestroy {

  ngOnDestroy (): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit (): void {
    super.ngOnInit();

    this.contractDataService.getInitiateContractData(this.contractId).subscribe(dat => {
      if (dat) {
        this.contract = dat;
        this.form = ContractData.intiForm(this.contract);

        this.contractManagementService.emitInitiateContractDataValidation(this.contract.links.self, null)
          .then(isComplete => {
            if (isComplete === FormStatusEnum.READ_ONLY) {
              this.disableForm();
            }
          });
      }

      this.subscriptions.push(this.form.valueChanges.subscribe(input => {
        this.formHasChanges = true;
      }));

    }, () => {
      this.messageService.error();
      this.router.navigate(['home']).then(e => {
        if (!e) {
          console.log('Navigation has failed!');
        }
      });
    });
  }

  private saveUpdateProcurement (key: string, value: any) {
    this.saveUpdateProc(key, value).subscribe(() => {
      this.messageService.success();
      this.form.controls[key].markAsPristine();
      this.formHasChanges = false;
      this.emitInitiateContractDataValidation();
    });
  }

  private patchContract (key: string) {
    this.patchCon(key).then(() => {
      this.emitInitiateContractDataValidation();
      this.messageService.success();
      this.form.controls[key].markAsPristine();
      this.formHasChanges = false;
    });
  }
}
