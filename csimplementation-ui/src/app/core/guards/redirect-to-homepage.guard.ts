import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable()
export class RedirectToHomepageGuard implements CanActivate {

  constructor (private router: Router) {
    this.router.navigate(['csimplementation/home']);
  }

  canActivate () {
    return true;
  }
}
