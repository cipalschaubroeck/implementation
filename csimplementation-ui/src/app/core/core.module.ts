import { NgModule, Optional, SkipSelf } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { ConfirmationService } from 'primeng/api';

import { SharedModule } from '../shared/shared.module';
import { AppBreadcrumbComponent } from './components/app.breadcrumb.component';
import { AppFooterComponent } from './components/app.footer.component';
// tslint:disable:max-line-length
import { MenuCollaboratorsComponent } from './components/top-menu/collaborators/components/menu-collaborators/menu-collaborators.component';
import { CollaboratorsDetailComponent } from './components/top-menu/collaborators/dialogs/collaborators-detail/collaborators-detail.component';
import { MenuContractingAuthorityComponent } from './components/top-menu/contracting-authority/components/menu-contracting-authority/menu-contracting-authority.component';
import { ContractingAuthorityDetailComponent } from './components/top-menu/contracting-authority/dialogs/contracting-authority-detail/contracting-authority-detail.component';
import { MenuOrganizationContactsComponent } from './components/top-menu/organization-contacts/components/menu-organization-contacts/menu-organization-contacts.component';
import { OrganizationContactsDetailComponent } from './components/top-menu/organization-contacts/dialogs/organization-contacts-detail/organization-contacts-detail.component';
// tslint:enable:max-line-length
import { TopMenuComponent } from './components/top-menu/top-menu.component';
import { throwIfAlreadyLoaded } from './guards/module-import.guard';
import { RedirectToHomepageGuard } from './guards/redirect-to-homepage.guard';

const components = [
  AppBreadcrumbComponent,
  AppFooterComponent,
  TopMenuComponent,
  MenuContractingAuthorityComponent,
  ContractingAuthorityDetailComponent,
  MenuCollaboratorsComponent,
  CollaboratorsDetailComponent,
  MenuOrganizationContactsComponent,
  OrganizationContactsDetailComponent
];

@NgModule({
  imports: [
    RouterModule,
    BrowserAnimationsModule,
    SharedModule,
  ],
  declarations: components,
  exports: components,
  providers: [
    ConfirmationService,
    RedirectToHomepageGuard,
  ]
})
export class CoreModule {

  constructor (@Optional() @SkipSelf() parentModule: CoreModule) {
    throwIfAlreadyLoaded(parentModule, 'CoreModule');
  }

}
