import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { GenericMessageService } from '../../shared/services/generic-message.service';

@Injectable()
export abstract class BaseHttpService<T> {

  static API_BASE: string;
  protected endpoint: string;
  protected context: string;

  public constructor (protected httpClient: HttpClient, protected messageService: GenericMessageService) {
    this.context = window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/')).split('/')[1];
    this.endpoint = window.location.origin + '/' + (this.context !== undefined ? this.context + '/' : '') + 'api/';
  }

  getAllList? (params: HttpParams);
}
