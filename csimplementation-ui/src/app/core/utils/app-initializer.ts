import configUrl from '../../../props/properties.json';
import { BaseHttpService } from '../services/base-http.service';

import { ConfigurationService } from '../services/configuration.service';

export function appInitializer (configurationService: ConfigurationService): () => any {
  return () => {
    configurationService.load(configUrl);
    BaseHttpService.API_BASE = configUrl.connection + configUrl.api_base;
  };
}
