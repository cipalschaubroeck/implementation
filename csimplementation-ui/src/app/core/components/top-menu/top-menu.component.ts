import { Component, forwardRef, Inject, OnDestroy, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { DialogService, MenuItem } from 'primeng/api';
import { Subscription } from 'rxjs';
import { AppComponent } from '../../../app.component';
import { AppInjector } from '../../../shared/services/app-injector.service';
/* tslint:disable:max-line-length */
import { MenuOrganizationContactsComponent } from './organization-contacts/components/menu-organization-contacts/menu-organization-contacts.component';

/* tslint:enable:max-line-length */

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit, OnDestroy {
  items: MenuItem[];
  private translateService: TranslateService;
  private subscriptions: Subscription[] = [];

  constructor (@Inject(forwardRef(() => AppComponent)) public app: AppComponent,
               private dialogService: DialogService) {
    const injector = AppInjector.getInjector();
    this.translateService = injector.get(TranslateService);
  }

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {

    const translationList = ['MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.menuTitle',
      'MENU.ADMIN_TENANT.COLLABORATORS.menuTitle',
      'MENU.ADMIN_TENANT.REFERENCE_TEXT.menuTitle',
      'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.menuTitle',
      'MENU.ADMIN_TENANT.USERS.menuTitle',
      'MENU.ADMIN_TENANT.DECISION_MAKING_BODIES.menuTitle',
      'MENU.ADMIN_TENANT.RQF_SECTION.menuTitle',
    ];

    this.subscriptions.push(this.translateService.get(translationList).subscribe(response => {
      this.items = [
        {
          label: response['MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.menuTitle'],
          icon: 'pi pi-user',
          routerLink: 'authority',
        },
        {
          label: response['MENU.ADMIN_TENANT.COLLABORATORS.menuTitle'],
          icon: 'pi pi-users',
          routerLink: 'collaborators'
        },
        {label: response['MENU.ADMIN_TENANT.REFERENCE_TEXT.menuTitle'], icon: 'pi pi-file'},
        {
          label: response['MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.menuTitle'],
          icon: 'pi pi-fw ui-icon-location-city',
          command: () => this.openOrganizationContactsDialog()
        },
        {label: response['MENU.ADMIN_TENANT.USERS.menuTitle'], icon: 'pi pi-users'},
        {label: response['MENU.ADMIN_TENANT.DECISION_MAKING_BODIES.menuTitle'], icon: 'pi pi-fw ui-icon-thumb-up'},
        {label: response['MENU.ADMIN_TENANT.RQF_SECTION.menuTitle'], icon: 'pi pi-fw  ui-icon-insert-drive-file'},
      ];
    }));

  }

  private openOrganizationContactsDialog () {
    const title = this.translateService.instant('MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.dialogTitle');
    this.dialogService.open(MenuOrganizationContactsComponent, {
      header: title,
      width: '50%',
      contentStyle: {'min-height': '95%', 'overflow': 'visible'}
    });
  }

  changeLanguage (lang: string) {
    this.app.changeLanguage(lang);
  }

}
