import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DialogService } from 'primeng/api';
import { BehaviorSubject, Subscription } from 'rxjs';
import { GenericTableLazyComponent } from '../../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import { GenericTableLazyButtonModel } from '../../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { FilterType } from '../../../../../../shared/enums/filter-type.enum';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../../shared/services/generic-translate.service';
import { ContractingAuthorityDetailComponent } from '../../dialogs/contracting-authority-detail/contracting-authority-detail.component';
import { AuthorityInfoModel } from '../../models/authority-info.model';
import { MenuContractingAuthorityService } from '../../services/menu-contracting-authority.service';

@Component({
  selector: 'app-contracting-authority-dialog',
  templateUrl: './menu-contracting-authority.component.html',
  styleUrls: ['./menu-contracting-authority.component.scss']
})
export class MenuContractingAuthorityComponent implements OnInit, OnDestroy {

  private elements: BehaviorSubject<AuthorityInfoModel[]> = new BehaviorSubject<AuthorityInfoModel[]>([]);
  private rightButtons: GenericTableLazyButtonModel[] = [];
  private leftButtons: GenericTableLazyButtonModel[] = [];
  private subscriptions: Subscription[] = [];
  @ViewChild(GenericTableLazyComponent, { static: true }) table: GenericTableLazyComponent;

  constructor (private service: MenuContractingAuthorityService,
               private translate: GenericTranslateService,
               private router: Router,
               private dialogService: DialogService,
               private messageService: GenericMessageService) {
  }

  private addContractingAuthorities () {
    const title = this.translate.translate('MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.addAuthority');
    this.dialogService.open(ContractingAuthorityDetailComponent, {
      header: title,
      data: {
        'update': () => this.updateTable(),
        'unselect': () => this.unselect()
      },
      width: '50%',
      contentStyle: {'min-height': '95%', 'overflow': 'visible'}
    });
  }

  private editContractingAuthority (link_self) {
    const title = this.translate.translate('MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.editAuthority');
    this.dialogService.open(ContractingAuthorityDetailComponent, {
      header: title,
      data: {
        authorityLink: link_self,
        'update': () => this.updateTable(),
        'unselect': () => this.unselect()
      },
      width: '50%',
      contentStyle: {'min-height': '95%', 'overflow': 'visible'}
    });
  }

  private removeContractingAuthorities () {
    if (this.service.removeContractingAuthority(this.table.selectedRow)) {
      this.table.filterTable('', '', '');
      this.table.selectedRow = null;

      this.messageService.popMessage(
        'success',
        'MESSAGE_SERVICE.success',
        'MESSAGE_SERVICE.removed');

    } else {
      this.messageService.popMessage(
        'error',
        'MESSAGE_SERVICE.error',
        'MESSAGE_SERVICE.error');
    }
  }

  /**
   * Clears all the child table filters
   */
  private clearFilters () {
    this.table.clearFilters();
  }

  /**
   * Shows or hide child table filter
   */
  private showFilters () {
    this.table.showFilters();
  }

  private updateTable () {
    this.table.filterTable('', '', '');
  }

  private unselect () {
    this.table.onRowUnSelection();
  }

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {

    GenericTableLazyComponent.loadButton('add', 'ui-icon-add',
      'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.addAuthority',
      this.addContractingAuthorities, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('remove', 'ui-icon-delete',
      'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.removeAuthority',
      this.removeContractingAuthorities, this, false, this.rightButtons);

    GenericTableLazyComponent.loadButton('hideShowFilters', 'ui-icon-filter', 'OVERVIEW.BUTTONS.HideShow',
      this.showFilters, this, false, this.leftButtons);
    GenericTableLazyComponent.loadButton('clearFilters', 'ui-icon-close', 'COMMON.BUTTONS.clearFilters',
      this.clearFilters, this, false, this.leftButtons);

    this.table.initButtons(this.rightButtons, this.leftButtons);

    this.table.initTableConfig({
      columns: [
        {
          field: 'authority',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.authority',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'address',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.address',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'number',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.number',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'postalCode',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.postCode',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'municipality',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.municipality',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'nationalId',
          text: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.nationalId',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.COLUMNS.SEARCH_PLACEHOLDER'
          }
        }
      ],
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'MENU.ADMIN_TENANT.CONTRACTING_AUTHORITIES.noResults',
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 15]
      },
      sortField: ('authority'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.service);

    this.subscriptions.push(this.table.selectedRowBehaviorSubject.subscribe(selected => {
      if (selected) {
        this.editContractingAuthority(selected.links.self);
      }
    }));
  }
}
