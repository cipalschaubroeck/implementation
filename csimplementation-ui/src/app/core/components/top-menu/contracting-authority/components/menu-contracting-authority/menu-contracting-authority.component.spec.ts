import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuContractingAuthorityComponent } from './menu-contracting-authority.component';

describe('MenuContractingAuthorityComponent', () => {
  let component: MenuContractingAuthorityComponent;
  let fixture: ComponentFixture<MenuContractingAuthorityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuContractingAuthorityComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuContractingAuthorityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
