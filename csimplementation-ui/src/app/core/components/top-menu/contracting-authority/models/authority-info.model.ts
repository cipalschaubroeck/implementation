export interface AuthorityInfoModel {
  id?: string;
  name: string;
  postalCode?: string;
  municipality?: string;
  nationalId?: string;
  address?: string;
  number?: string;
  country?: string;
  phone?: string;
  email?: string;
  links: {
    address?: string;
    contractingAuthority?: string;
    email?: string;
    phone?: string;
    self: string;
  };
}
