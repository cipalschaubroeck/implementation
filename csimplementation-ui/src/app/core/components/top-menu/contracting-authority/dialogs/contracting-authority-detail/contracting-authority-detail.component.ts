import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import i18n_iso_countries_en from 'i18n-iso-countries/langs/en.json';
import i18n_iso_countries_nl from 'i18n-iso-countries/langs/nl.json';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { MenuContractingAuthorityService } from '../../services/menu-contracting-authority.service';

@Component({
  selector: 'app-contracting-authority-detail',
  templateUrl: './contracting-authority-detail.component.html',
  styleUrls: ['./contracting-authority-detail.component.css']
})
export class ContractingAuthorityDetailComponent implements OnInit {
  @ViewChildren('country') countrySelect: QueryList<any>;
  private authorityDataForm: FormGroup;
  private countries: SelectItem[] = [];
  private countries_list: any;
  private authority: any;

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig,
               private fb: FormBuilder,
               private translateService: TranslateService,
               private service: MenuContractingAuthorityService,
               private messageService: GenericMessageService) {
  }

  ngOnInit () {
    this.populateCountryOption();
    this.authorityDataForm = this.setValidations();
    this.onEdit();
  }

  private onEdit () {
    if (this.config.data.authorityLink) {
      this.service.getContractingAuthority(this.config.data.authorityLink).subscribe(response => {
        this.authority = response;
        const keys = Object.keys(response);
        keys.forEach(key => {
          if (key in this.authorityDataForm.controls) {
            this.authorityDataForm.controls[key].setValue(response[key]);
          }
        });
        this.countrySelect.find(desiredMultiSelect => desiredMultiSelect.name === 'country').options.value = response['country'];
      });
    }
  }

  private onSubmit () {
    if (this.config.data.authorityLink) {
      this.edit();
    } else {
      this.save();
    }
  }

  private edit () {
    const changedProperties = this.getGroupedChangedProperties();
    if (this.authorityDataForm.dirty) {
      this.editGroupedByResource(changedProperties.email, this.authority.links.email, 'email', 'email');
      this.editGroupedByResource(changedProperties.address, this.authority.links.address, 'postal-address', 'address');
      this.editGroupedByResource(changedProperties.phone, this.authority.links.phone, 'phone', 'phone');
      this.editGroupedByResource(changedProperties.authority, this.authority.links.self, 'self', 'self');
    } else {
      this.ref.close();
      this.messageService.popMessage(
        'warn',
        'MESSAGE_SERVICE.warning',
        'MESSAGE_SERVICE.noChanges');
    }

  }

  private editGroupedByResource (formGroup, link, resourceName, associationName) {
    if (Object.keys(formGroup).length > 0) {
      const body = {};
      formGroup.forEach(prop => {
        const keys = Object.keys(prop);
        keys.forEach(key => {
          body[key] = prop[key];
        });
      });
      /**
       * if it has a resource it edits it if not it creates it and associates it to the authority
       */
      if (link) {
        this.service.patchResource(link, body).subscribe();
      } else {
        this.service.createResource(resourceName, body).subscribe(resourceLink => {
          const authorityPatch = {};
          authorityPatch[associationName] = resourceLink['_links'].self['href'];
          this.service.patchResource(this.authority.links.self, authorityPatch).subscribe();
        });
      }

      this.messageService.popMessage(
        'success',
        'MESSAGE_SERVICE.success',
        'MESSAGE_SERVICE.edited');

      this.config.data.update();
      this.ref.close();
    }
  }

  private save () {
    const formFields = this.authorityDataForm.controls;

    Promise.all([
      this.createAddress(formFields),
      this.createEmail(formFields),
      this.createPhone(formFields)]).then(response => {
      const authorityBody = {};
      authorityBody['name'] = formFields['name'].value;
      authorityBody['nationalId'] = formFields['nationalId'].value;
      authorityBody['address'] = response[0];
      authorityBody['email'] = response[1];
      authorityBody['phone'] = response[2];
      this.service.createResource('contracting-authority', authorityBody).subscribe(() => {
        this.messageService.popMessage(
          'success',
          'MESSAGE_SERVICE.success',
          'MESSAGE_SERVICE.created');
        this.config.data.update();
        this.ref.close();
      });
    });
  }

  private createResource (resource: string, body): Promise<any> {
    return new Promise(response => {
      this.service.createResource(resource, body).subscribe(data => {
        response(data['_links'].self['href']);
      });
    });
  }

  private createAddress (formFields): Promise<any> {
    const addressBody = {};
    addressBody['street'] = formFields['address'].value;
    addressBody['number'] = formFields['number'].value;
    addressBody['municipality'] = formFields['municipality'].value;
    addressBody['postalCode'] = formFields['postalCode'].value;
    addressBody['country'] = formFields['country'].value;
    addressBody['box'] = formFields['box'].value;
    return this.createResource('postal-address', addressBody);
  }

  private createPhone (formFields): Promise<any> {
    const phoneBody = {};
    phoneBody['phone'] = formFields['phone'].value;
    // TODO CSPROC-2101 Add fax ether as a new resource or part of phone in the model
    phoneBody['fax'] = formFields['fax'].value;
    return this.createResource('phone', phoneBody);
  }

  private createEmail (formFields): Promise<any> {
    const emailBody = {};
    emailBody['email'] = formFields['email'].value;
    return this.createResource('email', emailBody);
  }

  private populateCountryOption () {
    if (this.translateService.currentLang.indexOf('en') !== -1) {
      this.countries_list = i18n_iso_countries_en;
    } else if (this.translateService.currentLang.indexOf('nl') !== -1) {
      this.countries_list = i18n_iso_countries_nl;
    }
    const keys = Object.keys(this.countries_list.countries);
    keys.forEach(country => {
      this.countries.push({
        value: country,
        label: this.countries_list.countries[country]
      });
    });
  }

  private setValidations (): FormGroup {
    const controls: [][] = [];
    controls['name'] = new FormControl('', Validators.required);

    // TODO CSPROC-2097 introduce kbo national number format validations
    controls['nationalId'] = new FormControl('', Validators.required);
    controls['address'] = new FormControl('', Validators.required);
    controls['number'] = new FormControl('', []);
    controls['box'] = new FormControl('', []);
    controls['municipality'] = new FormControl('', Validators.required);
    controls['postalCode'] = new FormControl('', Validators.required);
    controls['country'] = new FormControl('', Validators.required);
    controls['email'] = new FormControl('', []);
    controls['phone'] = new FormControl('', []);
    controls['fax'] = new FormControl('', []);
    return this.fb.group(controls);
  }

  private cancel () {
    this.config.data.unselect();
    return this.ref.close();
  }

  private getGroupedChangedProperties () {

    const arrayOfChangedProperties = {
      phone: [] = [],
      address: [] = [],
      email: [] = [],
      authority: [] = [],
    };

    Object.keys(this.authorityDataForm.controls).forEach((name) => {
      const currentControl = this.authorityDataForm.controls[name];

      if (currentControl.dirty) {
        const changedProperties = {};
        changedProperties[name] = currentControl.value;
        if (name.indexOf('phone') !== -1) {
          arrayOfChangedProperties['phone'].push(changedProperties);
        } else if (name.indexOf('address') !== -1 || name.indexOf('number') !== -1 || name.indexOf('municipality') !== -1
          || name.indexOf('postalCode') !== -1 || name.indexOf('country') !== -1) {
          arrayOfChangedProperties['address'].push(changedProperties);
        } else if (name.indexOf('email') !== -1) {
          arrayOfChangedProperties['email'].push(changedProperties);
        } else {
          arrayOfChangedProperties['authority'].push(changedProperties);
        }
      }
    });
    return arrayOfChangedProperties;
  }
}
