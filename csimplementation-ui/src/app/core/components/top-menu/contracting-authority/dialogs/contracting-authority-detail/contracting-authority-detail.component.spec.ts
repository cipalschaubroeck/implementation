import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractingAuthorityDetailComponent } from './contracting-authority-detail.component';

describe('ContractingAuthorityDetailComponent', () => {
  let component: ContractingAuthorityDetailComponent;
  let fixture: ComponentFixture<ContractingAuthorityDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ContractingAuthorityDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractingAuthorityDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
