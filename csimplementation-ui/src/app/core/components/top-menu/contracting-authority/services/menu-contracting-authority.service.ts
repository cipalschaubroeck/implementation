import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Utils } from '../../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../../shared/models/page-model';
import { BaseHttpService } from '../../../../services/base-http.service';
import { AuthorityInfoModel } from '../models/authority-info.model';

@Injectable({
  providedIn: 'root'
})
export class MenuContractingAuthorityService extends BaseHttpService<any> {

  pageModel: PageModel = {
    data: new BehaviorSubject<any[]>([]),
    page: {
      totalNumber: 0
    }
  };

  getAllList (params: HttpParams) {
    params = params.append(ConstantsEnum.PROJECTION.valueOf(), 'contractingAuthorityInfo');

    return this.httpClient.get<AuthorityInfoModel>(this.endpoint + 'contracting-authority/overview',
      {params})
      .pipe(
        map((data: any) => {
          const authorities: AuthorityInfoModel[] = [];
          data._embedded.contractingAuthorities.forEach(authority => {
            const model: AuthorityInfoModel = {
              id: authority.id,
              name: authority.name,
              address: authority.address,
              number: authority.number,
              postalCode: authority.postalCode,
              municipality: authority.municipality,
              nationalId: authority.nationalId,
              links: {
                address: Utils.cleanUrl(authority._links['address'].href),
                contractingAuthority: Utils.cleanUrl(authority._links['contractingAuthority'].href),
                email: Utils.cleanUrl(authority._links['email'].href),
                phone: Utils.cleanUrl(authority._links['phone'].href),
                self: Utils.cleanUrl(authority._links['self'].href),
              }
            };
            authorities.push(model);
          });

          this.pageModel.page.totalNumber = data.page.totalElements;
          this.pageModel.data = authorities;
          return this.pageModel;
        })
      );
  }

  removeContractingAuthority (authority) {
    if (authority && authority.links.self) {
      const link: string = authority.links.self;
      return this.httpClient.delete(link)
        .subscribe(() => {
        }, error => console.log(error));
    } else {
      return null;
    }
  }

  createResource (resource, body) {
    return this.httpClient.post(this.endpoint + resource, body);
  }

  patchResource (resource, body) {
    return this.httpClient.patch(resource, body);
  }

  getContractingAuthority (link) {
    const params = new HttpParams().set(ConstantsEnum.PROJECTION.valueOf(), 'contractingAuthorityInfo');
    return this.httpClient.get(link, {params}).pipe(
      map((authority: any) => {
        const model: AuthorityInfoModel = {
          id: authority.id,
          name: authority.name,
          address: authority.address,
          number: authority.number,
          postalCode: authority.postalCode,
          municipality: authority.municipality,
          nationalId: authority.nationalId,
          country: authority.country,
          phone: authority.phone,
          email: authority.email,
          links: {
            address: ('postalAddressLink' in authority._links) ? Utils.cleanUrl(authority._links['postalAddressLink'].href) : '',
            contractingAuthority: ('contractingAuthority' in authority._links) ?
              Utils.cleanUrl(authority._links['contractingAuthority'].href) : '',
            email: ('emailAddressLink' in authority._links) ? Utils.cleanUrl(authority._links['emailAddressLink'].href) : '',
            phone: ('phoneNumberLink' in authority._links) ? Utils.cleanUrl(authority._links['phoneNumberLink'].href) : '',
            self: Utils.cleanUrl(authority._links['self'].href),
          }
        };
        return model;
      })
    );
  }

}
