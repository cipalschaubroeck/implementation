import { TestBed } from '@angular/core/testing';

import { MenuContractingAuthorityService } from './menu-contracting-authority.service';

describe('MenuContractingAuthorityService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MenuContractingAuthorityService = TestBed.get(MenuContractingAuthorityService);
    expect(service).toBeTruthy();
  });
});
