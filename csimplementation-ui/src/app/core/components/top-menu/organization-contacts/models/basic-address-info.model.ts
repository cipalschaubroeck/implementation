export interface BasicAddressInfoModel {
  id?: string;
  streetName?: string;
  houseNumber?: string;
  locator?: string;
  zipCode?: string;
  country?: string;
  city?: string;
  fullAddress?: string;
}
