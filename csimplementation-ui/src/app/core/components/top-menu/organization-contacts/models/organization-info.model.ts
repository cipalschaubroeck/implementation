import { BasicAddressInfoModel } from './basic-address-info.model';
import { ContactInfoModel } from './contact-info.model';

export interface OrganizationInfoModel {
  id?: string;
  contact?: ContactInfoModel;
  basicAddress: BasicAddressInfoModel;
  legalName: string;
  kboNumber?: string;
  vatNumber?: string;
  type: string;
  sme?: boolean;
}
