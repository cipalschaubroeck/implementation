import { BasicAddressInfoModel } from './basic-address-info.model';
import { ContactInfoModel } from './contact-info.model';
import { OrganizationInfoModel } from './organization-info.model';
import { PersonInfoModel } from './person-info.model';

export interface PersonInOrganizationInfoModel {
  person: PersonInfoModel;
  contact: ContactInfoModel;
  basicAddress: BasicAddressInfoModel;
  organization: OrganizationInfoModel;
  function: string;
  id: string;
}
