export interface ContactInfoModel {
  id?: string;
  nickname?: string;
  email?: string;
  phoneNumber?: string;
  website?: string;
  fax?: string;
}
