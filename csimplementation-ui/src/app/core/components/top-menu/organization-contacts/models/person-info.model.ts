import { BasicAddressInfoModel } from './basic-address-info.model';
import { ContactInfoModel } from './contact-info.model';

export interface PersonInfoModel {
  identityNumber: string;
  firstName: string;
  familyName: string;
  gender: string;
  id: string;
  basicAddress: BasicAddressInfoModel;
  contact: ContactInfoModel;
}
