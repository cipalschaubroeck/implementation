import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { JointVentureInfoModel } from '../../../../../main/core/components/top-menu/organization-contacts/models/joint-venture-info.model';
import { BaseHttpService } from '../../../../services/base-http.service';
import { BasicAddressInfoModel } from '../models/basic-address-info.model';
import { ContactInfoModel } from '../models/contact-info.model';
import { OrganizationInfoModel } from '../models/organization-info.model';
import { PersonInOrganizationInfoModel } from '../models/person-in-organization-info.model';

@Injectable({
  providedIn: 'root'
})
export class OrganizationContactService extends BaseHttpService<any> {

  private static setInnerValue (obj: any, key1: any, key2: string): string {
    return obj && key1 && obj[key1] && key2 && obj[key1][key2] ? obj[key1][key2] : '';
  }

  private static setValue (value: any, key: string): string {
    return value && key && value[key] ? value[key] : '';
  }

  static mapBasicAddress (basicAddress: any): BasicAddressInfoModel {
    return {
      id: OrganizationContactService.setValue(basicAddress, 'id'),
      locator: OrganizationContactService.setValue(basicAddress, 'locator'),
      zipCode: OrganizationContactService.setValue(basicAddress, 'zipCode'),
      streetName: OrganizationContactService.setValue(basicAddress, 'streetName'),
      houseNumber: OrganizationContactService.setValue(basicAddress, 'houseNumber'),
      fullAddress: OrganizationContactService.setValue(basicAddress, 'fullAddress'),
      country: OrganizationContactService.setValue(basicAddress, 'country'),
      city: OrganizationContactService.setValue(basicAddress, 'city'),
    };
  }

  private static mapContact (contact: any): ContactInfoModel {
    return {
      id: OrganizationContactService.setValue(contact, 'id'),
      website: OrganizationContactService.setValue(contact, 'website'),
      phoneNumber: OrganizationContactService.setValue(contact, 'phoneNumber'),
      nickname: OrganizationContactService.setValue(contact, 'nickName'),
      fax: OrganizationContactService.setValue(contact, 'fax'),
      email: OrganizationContactService.setValue(contact, 'email')
    };
  }

  private static mapTopContactInOrganization (data: any): PersonInOrganizationInfoModel {

    return {
      id: data.id,
      function: data.function,
      basicAddress: OrganizationContactService.mapBasicAddress(data.basicAddress),
      contact: OrganizationContactService.mapContact(data.contact),
      organization: {
        id: OrganizationContactService.setInnerValue(data, 'organization', 'id'),
        vatNumber: OrganizationContactService.setInnerValue(data, 'organization', 'vatNumber'),
        type: OrganizationContactService.setInnerValue(data, 'organization', 'type'),
        sme: data.organization.sme,
        legalName: OrganizationContactService.setInnerValue(data, 'organization', 'legalName'),
        kboNumber: OrganizationContactService.setInnerValue(data, 'organization', 'kboNumber'),
        contact: OrganizationContactService.mapContact(data.organization.contact),
        basicAddress: OrganizationContactService.mapBasicAddress(data.organization.basicAddress)
      },
      person: {
        familyName: OrganizationContactService.setInnerValue(data, 'person', 'familyName'),
        gender: OrganizationContactService.setInnerValue(data, 'person', 'gender'),
        id: OrganizationContactService.setInnerValue(data, 'person', 'id'),
        firstName: OrganizationContactService.setInnerValue(data, 'person', 'firstName'),
        identityNumber: OrganizationContactService.setInnerValue(data, 'person', 'identityNumber'),
        contact: OrganizationContactService.mapContact(data.person.contact),
        basicAddress: OrganizationContactService.mapBasicAddress(data.person.basicAddress)
      }
    };
  }

  private mapToOrganization (response: any): OrganizationInfoModel {
    const keys = Object.keys(response);
    const organization: any = {};
    keys.forEach(key => {
      if (key in response) {
        organization[key] = response[key];
      }
    });
    return organization;
  }

  getOrganizations (filters?): Observable<OrganizationInfoModel[]> {
    let observable;
    if (filters) {
      observable = this.httpClient.get(this.endpoint + 'organizations', {params: filters});
    } else {
      observable = this.httpClient.get(this.endpoint + 'organizations');
    }
    return observable.pipe(map((org: any) => {
      const organizations: OrganizationInfoModel[] = [];
      org.forEach(organization => {
        organizations.push(this.mapToOrganization(organization));
      });
      return organizations;
    }));
  }

  getOrganizationDetail (id: string, type: string): Observable<OrganizationInfoModel> {
    return this.httpClient.get(this.endpoint + 'organizations/' + type + '/' + id).pipe(map((org: any) => {
      return this.mapToOrganization(org);
    }));
  }

  getContactsInOrganization () {
    return this.httpClient.get(this.endpoint + 'person-in-org').pipe(map((persons: any) => {
      const personList: PersonInOrganizationInfoModel[] = [];
      persons.forEach(per => {
        personList.push(OrganizationContactService.mapTopContactInOrganization(per));
      });
      return personList;
    }));
  }

  deleteOrganization (organization: OrganizationInfoModel): Observable<any> {
    return this.httpClient.delete(this.endpoint + 'organizations/' + organization.type + '/' + organization.id);
  }

  createOrganization (body: OrganizationInfoModel | JointVentureInfoModel) {
    return this.httpClient.post(this.endpoint + 'organizations', body);
  }

  editOrganization (id: string, body: OrganizationInfoModel | JointVentureInfoModel) {
    return this.httpClient.patch(this.endpoint + 'organizations/' + id, body);
  }

  getAvailableContacts (member: OrganizationInfoModel) {
    return this.httpClient.get(this.endpoint + 'organizations/' + member.id + '/contacts');
  }
}
