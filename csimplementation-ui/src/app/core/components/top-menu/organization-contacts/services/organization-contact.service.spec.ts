import { TestBed } from '@angular/core/testing';

import { OrganizationContactService } from './organization-contact.service';

describe('OrganizationContactService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: OrganizationContactService = TestBed.get(OrganizationContactService);
    expect(service).toBeTruthy();
  });
});
