import { Component, OnInit, Type, ViewChild } from '@angular/core';
import { DialogService, MenuItem } from 'primeng/api';
import { Menu } from 'primeng/menu';
import { BehaviorSubject } from 'rxjs';
// tslint:disable-next-line:max-line-length
import { JointVentureDetailsComponent } from '../../../../../../main/core/components/top-menu/organization-contacts/components/joint-venture-details/joint-venture-details.component';
import { GenericTableComponent } from '../../../../../../shared/components/generic-table/generic-table.component';
import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../../../../shared/components/generic-table/models/generic-table.models';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../../shared/services/generic-translate.service';
import { OrganizationContactsDetailComponent } from '../../dialogs/organization-contacts-detail/organization-contacts-detail.component';
import { OrganizationInfoModel } from '../../models/organization-info.model';
import { OrganizationContactService } from '../../services/organization-contact.service';

// import { JointVentureDetailsComponent } from '../joint-venture-details/joint-venture-details.component';

@Component({
  selector: 'app-menu-organization-contacts',
  templateUrl: './menu-organization-contacts.component.html',
  styleUrls: ['./menu-organization-contacts.component.css']
})
export class MenuOrganizationContactsComponent implements OnInit {
  private selected: any;
  private selected$: BehaviorSubject<any>;
  private data: BehaviorSubject<OrganizationInfoModel[]> = new BehaviorSubject<OrganizationInfoModel[]>([]);
  private cols: GenericTableColumnModel[];
  private buttons: GenericTableButtonModel[];
  private tableConfig: GenericTableConfigModel;
  @ViewChild(GenericTableComponent, { static: false }) table: GenericTableComponent;
  private newOrganizationOptions: MenuItem[];
  @ViewChild('newOrganizationMenu', {static: false}) newOrganizationMenu: Menu;

  constructor (
    private service: OrganizationContactService,
    private messageService: GenericMessageService,
    private dialogService: DialogService, private translate: GenericTranslateService) {
  }

  private initTableData () {
    this.service.getOrganizations().subscribe((response: OrganizationInfoModel[]) => {
      this.data.next(response);
    });
  }

  private addOrganization (event: any) {
    this.newOrganizationMenu.toggle(event);
  }

  private addSingleOrganization () {
    const title = this.translate.translate('MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.ADD_EDIT_ORGANIZATIONS.addTitle');

    this.dialogService.open(OrganizationContactsDetailComponent, {
      header: title,
      data: {
        'update': () => this.updateTable(),
      },
      width: '75%',
      contentStyle: {'height': '600px'}
    });
  }

  private addJointVenture () {
    const title = this.translate.translate('MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.ADD_EDIT_ORGANIZATIONS.addTitle');

    this.dialogService.open(JointVentureDetailsComponent, {
      header: title,
      data: {
        'update': () => this.updateTable(),
      },
      width: '75%',
      contentStyle: {'height': '600px'}
    });
  }

  private removeOrganization () {
    if (this.table.selectedRowBehaviorSubject.value) {
      this.service.deleteOrganization(this.table.selectedRowBehaviorSubject.value).subscribe(() => {
        this.initTableData();
        this.messageService.popMessage('success', 'MESSAGE_SERVICE.success', 'MESSAGE_SERVICE.removed');
      }, () => {
        this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.error');
      });
    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }

  private updateTable () {
    this.initTableData();
  }

  private editOrganization () {
    if (this.table.selectedRowBehaviorSubject.value && this.table.selectedRowBehaviorSubject.value.id) {
      const title = this.translate.translate('MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.ADD_EDIT_ORGANIZATIONS.editTitle');

      let dialog: Type<any>;
      if (this.table.selectedRowBehaviorSubject.value.type === 'SINGLE_ORGANIZATION') {
        dialog = OrganizationContactsDetailComponent;
      } else {
        dialog = JointVentureDetailsComponent;
      }

      this.dialogService.open(dialog, {
        header: title,
        data: {
          'update': () => this.updateTable(),
          'edit': true,
          'id': this.table.selectedRowBehaviorSubject.value.id
        },
        width: '75%',
        contentStyle: {'height': '600px'}
      });
    } else {
      this.messageService.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
    }
  }

  ngOnInit () {
    this.initTableData();
    this.cols = [
      {
        field: 'legalName',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.legalName',
        hasSort: true,
      },
      {
        field: 'type',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.type',
        hasSort: true,
      },
      {
        field: 'kboNumber',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.kboNumber',
        hasSort: true,
      },
      {
        field: 'basicAddress.fullAddress',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.address',
        hasSort: true,
      }
    ];
    this.buttons = [
      {
        icon: 'ui-icon-add',
        click: this.addOrganization,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        click: this.removeOrganization,
        scope: this
      },
      {
        icon: 'ui-icon-edit',
        click: this.editOrganization,
        scope: this
      }
    ];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.noResults',
      hasSearchFilter: true,
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 20]
      }
    };
    this.newOrganizationOptions = [{
      label: this.translate.translate('CONTRACT.CONTRACTOR.ORGANIZATION.title'),
      command: () => this.addSingleOrganization()
    }, {
      label: this.translate.translate('CONTRACT.CONTRACTOR.JOINT_VENTURE.title'),
      command: () => this.addJointVenture()
    }];
  }
}
