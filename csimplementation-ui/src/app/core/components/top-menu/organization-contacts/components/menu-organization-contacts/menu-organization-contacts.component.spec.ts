import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuOrganizationContactsComponent } from './menu-organization-contacts.component';

describe('MenuOrganizationContactsComponent', () => {
  let component: MenuOrganizationContactsComponent;
  let fixture: ComponentFixture<MenuOrganizationContactsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuOrganizationContactsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuOrganizationContactsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
