import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import i18n_iso_countries_en from 'i18n-iso-countries/langs/en.json';
import i18n_iso_countries_nl from 'i18n-iso-countries/langs/nl.json';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { BehaviorSubject, Observable } from 'rxjs';
import { GenericTableComponent } from '../../../../../../shared/components/generic-table/generic-table.component';
import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from '../../../../../../shared/components/generic-table/models/generic-table.models';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { OrganizationInfoModel } from '../../models/organization-info.model';
import { PersonInOrganizationInfoModel } from '../../models/person-in-organization-info.model';
import { OrganizationContactService } from '../../services/organization-contact.service';

@Component({
  selector: 'app-organization-contacts-detail',
  templateUrl: './organization-contacts-detail.component.html',
  styleUrls: ['./organization-contacts-detail.component.css']
})
export class OrganizationContactsDetailComponent implements OnInit {

  private organizationDataForm: FormGroup;

  private countries_list: any;
  private countries: SelectItem[] = [];
  private selected: any;
  private selected$: BehaviorSubject<any>;
  private data: BehaviorSubject<PersonInOrganizationInfoModel[]> = new BehaviorSubject<PersonInOrganizationInfoModel[]>([]);
  private checked = false;
  private cols: GenericTableColumnModel[];
  private buttons: GenericTableButtonModel[];
  private tableConfig: GenericTableConfigModel;

  @ViewChild(GenericTableComponent, { static: true }) table: GenericTableComponent;

  private static initForm (): FormGroup {
    return new FormGroup({
      name: new FormControl('', Validators.required),
      legalForm: new FormControl(),
      sme: new FormControl(),
      // TODO CSPROC-2097 introduce kbo national number format validations
      kboNumber: new FormControl(),
      vatNumber: new FormControl(),
      street: new FormControl(),
      number: new FormControl(),
      box: new FormControl(),
      postalCode: new FormControl(),
      municipality: new FormControl(),
      country: new FormControl('', Validators.required),
      email: new FormControl('', [Validators.required, Validators.email]),
      website: new FormControl(),
      phone: new FormControl(),
      fax: new FormControl()
    });
  }

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig,
               private service: OrganizationContactService,
               private message: GenericMessageService,
               private translateService: TranslateService) {
  }

  ngOnInit () {
    this.initContactsTableData();

    this.populateCountryOption();

    if (this.config && this.config.data && this.config.data.edit && this.config.data.id) {
      this.service.getOrganizationDetail(this.config.data.id, 'SINGLE_ORGANIZATION').subscribe(data => {
        this.populateEditForm(data);
      });
    }

    this.organizationDataForm = OrganizationContactsDetailComponent.initForm();

    this.cols = [
      {
        field: 'name',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.name',
        hasSort: true
      },
      {
        field: 'function',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.function',
        hasSort: true
      },
      {
        field: 'email',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.email',
        hasSort: true
      },
      {
        field: 'phone',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.phone',
        hasSort: true
      }
    ];
    this.buttons = [
      {
        icon: 'ui-icon-add',
        click: this.addContact,
        scope: this
      },
      {
        icon: 'ui-icon-trash',
        click: this.removeContact,
        scope: this
      },
      {
        icon: 'ui-icon-edit',
        click: this.editContact,
        scope: this
      }
    ];
    this.tableConfig = {
      containerClass: 'ui-g-12 only-padding-top',
      noResultsMessage: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.noContactResults',
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 20]
      }
    };
  }

  private populateCountryOption () {
    if (this.translateService.currentLang.indexOf('en') !== -1) {
      this.countries_list = i18n_iso_countries_en;
    } else if (this.translateService.currentLang.indexOf('nl') !== -1) {
      this.countries_list = i18n_iso_countries_nl;
    }
    const keys = Object.keys(this.countries_list.countries);
    keys.forEach(country => {
      this.countries.push({
        value: country,
        label: this.countries_list.countries[country]
      });
    });
  }

  private cancel () {
    return this.ref.close();
  }

  private getErrorList (key): string {
    if (this.organizationDataForm.controls[key].errors.required) {
      return 'VALIDATIONS.Required';
    } else if (this.organizationDataForm.controls[key].errors.numeric) {
      return 'VALIDATIONS.Numeric';
    } else if (this.organizationDataForm.controls[key].errors.email) {
      return 'VALIDATIONS.Email';
    } else {
      return 'error';
    }
  }

  private addContact () {
    // TODO CSPROC-2222 add contact functionality
  }

  private editContact () {
    // TODO CSPROC-2223 edit contact functionality
  }

  private removeContact () {
    // TODO CSPROC-2224 remove contact functionality
  }

  private initContactsTableData () {
    this.service.getContactsInOrganization().subscribe((response: PersonInOrganizationInfoModel[]) => {
      this.data.next(response);
    });
  }

  private onSubmit () {
    let response: Observable<any>;
    let textAction: string;
    if (this.config && this.config.data && this.config.data.edit && this.config.data.id) {
      textAction = 'MESSAGE_SERVICE.updated';
      response = this.service.editOrganization(this.config.data.id, this.populateRequestBody());
    } else {
      textAction = 'MESSAGE_SERVICE.success';
      response = this.service.createOrganization(this.populateRequestBody());
    }

    response.subscribe(() => {
      this.message.popMessage('success', textAction, textAction);
      this.config.data.update();
      this.ref.close();
    });
  }

  private populateRequestBody (): OrganizationInfoModel {
    return {
      contact: {
        nickname: this.organizationDataForm.controls['name'].value,
        email: this.organizationDataForm.controls['email'].value,
        phoneNumber: this.organizationDataForm.controls['phone'].value,
        website: this.organizationDataForm.controls['website'].value,
        fax: this.organizationDataForm.controls['fax'].value
      },
      basicAddress: {
        streetName: this.organizationDataForm.controls['street'].value,
        houseNumber: this.organizationDataForm.controls['number'].value,
        locator: this.organizationDataForm.controls['box'].value,
        zipCode: this.organizationDataForm.controls['postalCode'].value,
        country: this.organizationDataForm.controls['country'].value,
        city: this.organizationDataForm.controls['municipality'].value
      },
      legalName: this.organizationDataForm.controls['legalForm'].value,
      kboNumber: this.organizationDataForm.controls['kboNumber'].value,
      vatNumber: this.organizationDataForm.controls['vatNumber'].value,
      type: 'SINGLE_ORGANIZATION',
      sme: this.checked
    };
  }

  private populateEditForm (data: OrganizationInfoModel) {
    this.organizationDataForm.controls['name'].setValue(data.contact.nickname);
    this.organizationDataForm.controls['email'].setValue(data.contact.email);
    this.organizationDataForm.controls['phone'].setValue(data.contact.phoneNumber);
    this.organizationDataForm.controls['website'].setValue(data.contact.website);
    this.organizationDataForm.controls['fax'].setValue(data.contact.fax);
    this.organizationDataForm.controls['street'].setValue(data.basicAddress.streetName);
    this.organizationDataForm.controls['number'].setValue(data.basicAddress.houseNumber);
    this.organizationDataForm.controls['box'].setValue(data.basicAddress.locator);
    this.organizationDataForm.controls['postalCode'].setValue(data.basicAddress.zipCode);
    this.organizationDataForm.controls['country'].setValue(data.basicAddress.country);
    this.organizationDataForm.controls['municipality'].setValue(data.basicAddress.city);
    this.organizationDataForm.controls['legalForm'].setValue(data.legalName);
    this.organizationDataForm.controls['kboNumber'].setValue(data.kboNumber);
    this.organizationDataForm.controls['vatNumber'].setValue(data.vatNumber);
    this.organizationDataForm.controls['sme'].setValue(data.sme);
  }
}
