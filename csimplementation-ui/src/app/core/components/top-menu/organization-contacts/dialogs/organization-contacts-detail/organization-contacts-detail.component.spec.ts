import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganizationContactsDetailComponent } from './organization-contacts-detail.component';

describe('OrganizationContactsDetailComponent', () => {
  let component: OrganizationContactsDetailComponent;
  let fixture: ComponentFixture<OrganizationContactsDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OrganizationContactsDetailComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganizationContactsDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
