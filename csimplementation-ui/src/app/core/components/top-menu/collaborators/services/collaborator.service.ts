import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Utils } from '../../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../../shared/models/page-model';
import { BaseHttpService } from '../../../../services/base-http.service';
import { CollaboratorInfoModel } from '../models/CollaboratorInfo.model';

@Injectable({
  providedIn: 'root'
})
export class CollaboratorService extends BaseHttpService<any> {
  page: PageModel = {data: {}, page: {totalNumber: 0}};

  private static mapToCollaborator (item): CollaboratorInfoModel {
    return {
      name: item.name,
      lastName: item.lastName,
      email: item.email,
      function: item.function,
      phone: item.phone,
      authority: item.authority,
      links: {
        self: Utils.cleanUrl(item._links['self'].href),
        personLink: Utils.cleanUrl(item._links['personLink'].href)
      }
    };
  }

  getAllList (params: HttpParams) {

    params = params.append(ConstantsEnum.PROJECTION.valueOf(), 'collaboratorInfo');

    return this.httpClient.get<any>(this.endpoint + 'collaborator/overview',
      {params})
      .pipe(
        map((data: any) => {
          const collaborators: CollaboratorInfoModel[] = [];
          for (const item of data._embedded.collaborators) {
            const collaborator: CollaboratorInfoModel = CollaboratorService.mapToCollaborator(item);
            collaborators.push(collaborator);
          }
          this.page.page.totalNumber = data.page.totalElements;
          this.page.data = collaborators;
          return this.page;
        })
      );
  }

  removeCollaborator (collaborator) {
    if (collaborator && collaborator.links.self) {
      const link: string = collaborator.links.self;
      return this.httpClient.delete(link)
        .subscribe(() => {
        }, error => console.log(error));
    } else {
      return null;
    }
  }

  getCollaborator (collaboratorLink): Observable<CollaboratorInfoModel> {
    if (collaboratorLink) {
      const params: HttpParams = new HttpParams().set(ConstantsEnum.PROJECTION.valueOf(), 'collaboratorInfo');
      return this.httpClient.get(collaboratorLink, {params}).pipe(
        map((data: any) => {
          return CollaboratorService.mapToCollaborator(data);
        })
      );
    }
  }
}
