export interface CollaboratorInfoModel {
  links: {
    personLink: string;
    self: string;
  };
  name: string;
  lastName: string;
  authority: string;
  function: string;
  email: string;
  phone: string;
}
