import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';
import { AuthorityService } from '../../../../../../contract-authority-collaborator/services/authority.service';
import { InputTypeEnum } from '../../../../../../shared/enums/input-type.enum';
import { CrudResourceService } from '../../../../../../shared/services/crud-resource.service';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import add_collaborator_template from '../../form-templates/add-colaborator.template';
import { CollaboratorService } from '../../services/collaborator.service';

@Component({
  selector: 'app-collaborators-detail',
  templateUrl: './collaborators-detail.component.html',
  styleUrls: ['./collaborators-detail.component.css']
})
export class CollaboratorsDetailComponent {

  private formTemplate: any = add_collaborator_template;
  private collaboratorDataForm: FormGroup = this.fb.group([]);
  private inputTypes = InputTypeEnum;

  private setFormEditFields () {
    if (this.config.data.collaboratorLink) {
      this.collaboratorService.getCollaborator(this.config.data.collaboratorLink.links.self).subscribe(response => {
        if (response) {
          const keys = Object.keys(response);
          keys.forEach(key => {
            if (key in this.collaboratorDataForm.controls) {

              const el: Element = document.getElementById(key);
              if (el.getAttribute('data-input-type') === this.inputTypes.DROPDOWN) {
                add_collaborator_template.find(name => name['id'] === key).options.subscribe(() => {
                  // TODO CSPROC-2108 when contractor is done in the analysis part end implementation
                });
              } else {
                this.collaboratorDataForm.controls[key].setValue(response[key]);
              }
            }
          });
        }
      });
    }
  }

  private setAuthority () {
    const options: any[] = [];
    this.authorityService.getContractingAuthorities().subscribe(authorities => {
      authorities.forEach(authority => {
        options.push({value: authority['name'], label: authority['name']});
      });
      // TODO CSPROC-2147 re do when collaborators relationship with contracting authority is analysed
      add_collaborator_template.find(authority => authority.id === 'authority').options.next(options);
    });
  }

  private setDepartment () {
    // TODO CSPROC-2117- create department functionality in contracting authority
  }

  private setValidations (): FormGroup {
    const controls: [][] = [];
    add_collaborator_template.forEach(key => {
      const validations = [];
      if (key.validations) {
        key.validations.forEach(validation => {
          validations.push(validation.validation);
        });
      }
      controls[key.id] = new FormControl('', Validators.compose(validations));
    });
    return this.fb.group(controls);
  }

  private getErrorList (errorObject: FormGroup, key): string {
    let error1 = '';
    let validation;
    if (errorObject && key && key.id) {
      error1 = Object.keys(errorObject.controls[key.id].errors)[0];
    }
    if (add_collaborator_template) {
      const input = add_collaborator_template.find(field => field.id === key.id);
      if (input && input.validations) {
        validation = input.validations.find(val => val.definition === error1);
        return validation.msg;
      }
    }
    return '';
  }

  private onSubmit () {
    if (this.config.data.collaboratorLink) {
      this.edit();
    } else {
      this.save();
    }
  }

  private edit () {
    // TODO CSPROC-2108 create edit collaborators functionality
  }

  private save () {
    const formFields = this.collaboratorDataForm.controls;

    // TODO CSPROC-2138 you might want to do this in a single step with a dto, so that if phone creation fails on back,
    //  email is not left orphan
    Promise.all([
      this.createEmail(formFields),
      this.createPhone(formFields)
    ]).then(response => {
      this.createPerson(
        formFields,
        response[0]['_links'].self['href'],
        response[1]['_links'].self['href']
      ).then(person => {
        const collaboratorBody = {};
        collaboratorBody['person'] = person['_links'].self['href'];
        collaboratorBody['authority'] = formFields['authority'].value;
        collaboratorBody['function'] = formFields['function'].value;
        this.service.createResource('collaborator', collaboratorBody).then(() => {
          this.messageService.popMessage(
            'success',
            'MESSAGE_SERVICE.success',
            'MESSAGE_SERVICE.created');
          this.config.data.update();
          this.ref.close();
        });
      });
    });
  }

  private createPerson (formFields, emailLink, phoneLink): Promise<any> {
    const personBody = {};
    personBody['firstName'] = formFields['name'].value;
    personBody['lastName'] = formFields['lastName'].value;
    personBody['email'] = emailLink;
    personBody['phone'] = phoneLink;
    return this.service.createResource('person', personBody);
  }

  private createPhone (formFields): Promise<any> {
    const phoneBody = {};
    phoneBody['phone'] = formFields['phone'].value;
    return this.service.createResource('phone', phoneBody);
  }

  private createEmail (formFields): Promise<any> {
    const emailBody = {};
    emailBody['email'] = formFields['email'].value;
    return this.service.createResource('email', emailBody);
  }

  private cancel () {
    return this.ref.close();
  }

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig,
               private fb: FormBuilder, private route: ActivatedRoute,
               private translateService: TranslateService,
               private authorityService: AuthorityService,
               private service: CrudResourceService,
               private collaboratorService: CollaboratorService,
               private messageService: GenericMessageService) {

    this.collaboratorDataForm = this.setValidations();
    this.setAuthority();
    this.setFormEditFields();
  }

}
