import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuCollaboratorsComponent } from './menu-collaborators.component';

describe('MenuCollaboratorsComponent', () => {
  let component: MenuCollaboratorsComponent;
  let fixture: ComponentFixture<MenuCollaboratorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MenuCollaboratorsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuCollaboratorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
