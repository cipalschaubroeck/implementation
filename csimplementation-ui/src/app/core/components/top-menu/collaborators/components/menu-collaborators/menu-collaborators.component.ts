import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { DialogService, DynamicDialogRef } from 'primeng/api';
import { BehaviorSubject, Subscription } from 'rxjs';
import { GenericTableLazyComponent } from '../../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import { GenericTableLazyButtonModel } from '../../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { DeleteConfirmationComponent } from '../../../../../../shared/dialogs/delete-confirmation/delete-confirmation.component';
import { WarningComponent } from '../../../../../../shared/dialogs/warning/warning.component';
import { FilterType } from '../../../../../../shared/enums/filter-type.enum';
import { GenericTranslateService } from '../../../../../../shared/services/generic-translate.service';
import { CollaboratorsDetailComponent } from '../../dialogs/collaborators-detail/collaborators-detail.component';
import { CollaboratorInfoModel } from '../../models/CollaboratorInfo.model';
import { CollaboratorService } from '../../services/collaborator.service';

@Component({
  selector: 'app-menu-collaborators',
  templateUrl: './menu-collaborators.component.html',
  styleUrls: ['./menu-collaborators.component.scss']
})
export class MenuCollaboratorsComponent implements OnInit, OnDestroy {
  private elements: BehaviorSubject<CollaboratorInfoModel[]> = new BehaviorSubject<CollaboratorInfoModel[]>([]);
  private rightButtons: GenericTableLazyButtonModel[] = [];
  private leftButtons: GenericTableLazyButtonModel[] = [];
  private subscriptions: Subscription[] = [];
  private collaborator: any;
  @ViewChild(GenericTableLazyComponent, { static: true }) table: GenericTableLazyComponent;

  constructor (private service: CollaboratorService, private dialogService: DialogService,
               private translate: GenericTranslateService) {
  }

  ngOnDestroy () {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {
    GenericTableLazyComponent.loadButton('add', 'ui-icon-add',
      'MENU.ADMIN_TENANT.COLLABORATORS.addCollaborator',
      this.addCollaborators, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('edit', 'ui-icon-edit',
      'MENU.ADMIN_TENANT.COLLABORATORS.editCollaborator',
      this.editCollaborators, this, false, this.rightButtons);
    GenericTableLazyComponent.loadButton('remove', 'ui-icon-delete',
      'MENU.ADMIN_TENANT.COLLABORATORS.removeCollaborator',
      this.removeCollaborator, this, false, this.rightButtons);

    GenericTableLazyComponent.loadButton('hideShowFilters', 'ui-icon-filter', 'OVERVIEW.BUTTONS.HideShow',
      this.showFilters, this, false, this.leftButtons);
    GenericTableLazyComponent.loadButton('clearFilters', 'ui-icon-close', 'COMMON.BUTTONS.clearFilters',
      this.clearFilters, this, false, this.leftButtons);
    this.table.initButtons(this.rightButtons, this.leftButtons);

    this.table.initTableConfig({
      columns: [
        {
          field: 'name',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.name',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'function',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.function',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'email',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.email',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'phone',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.phone',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'authority',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.contractingAuthority',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'department',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.department',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        },
        {
          field: 'qualification',
          text: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.qualification',
          class: 'no-padding small-size',
          hasSort: true,
          filter: {
            type: FilterType.TEXT,
            placeHolder: 'MENU.ADMIN_TENANT.COLLABORATORS.COLUMNS.SEARCH_PLACEHOLDER'
          }
        }
      ],
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'MENU.ADMIN_TENANT.COLLABORATORS.noResults',
      paginator: {
        rows: 5,
        rowsPerPageOptions: [5, 10, 15]
      },
      sortField: ('name'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.service);

    this.subscriptions.push(this.table.selectedRowBehaviorSubject.subscribe(selected => {
      if (selected) {
        this.collaborator = selected;
      }
    }));
  }

  addCollaborators () {
    const title = this.translate.translate('MENU.ADMIN_TENANT.COLLABORATORS.addCollaborator');
    this.openEditAddCollaboratorDialog(title, null);
  }

  editCollaborators () {
    const title = this.translate.translate('MENU.ADMIN_TENANT.COLLABORATORS.editCollaborator');
    if (this.table.selectedRow) {
      this.openEditAddCollaboratorDialog(title, this.collaborator);
    } else {
      const warning = this.translate.translate('OVERVIEW.BUTTONS.Warning');
      const message = this.translate.translate('MENU.ADMIN_TENANT.COLLABORATORS.collaborator_selected');
      this.dialogService.open(WarningComponent, {
        data: {
          'message': message
        },
        header: warning,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    }

  }

  openEditAddCollaboratorDialog (title, collaborator) {
    this.dialogService.open(CollaboratorsDetailComponent, {
      data: {
        collaboratorLink: collaborator,
        'update': () => this.updateTable()
      },
      header: title,
      width: '50%',
      contentStyle: {'min-height': '95%', 'overflow': 'visible'}
    });
  }

  removeCollaborator () {
    const title = this.translate.translate('OVERVIEW.BUTTONS.Warning');

    if (this.table.selectedRow) {
      this.dialogService.open(DeleteConfirmationComponent, {
        data: {
          'delete': {click: (ref: DynamicDialogRef) => this.deleteCollaborator(ref)},
          'confirmation_msg': 'MENU.ADMIN_TENANT.COLLABORATORS.delete_confirmation'
        },
        header: title,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    } else {
      const message = this.translate.translate('MENU.ADMIN_TENANT.COLLABORATORS.collaborator_selected');
      this.dialogService.open(WarningComponent, {
        data: {
          'message': message
        },
        header: title,
        width: '30%',
        contentStyle: {'height': '130px', 'overflow': 'visible'}
      });
    }
  }

  /**
   * Clears all the child table filters
   */
  clearFilters () {
    this.table.clearFilters();
  }

  /**
   * Shows or hide child table filter
   */
  showFilters () {
    this.table.showFilters();
  }

  deleteCollaborator (ref: DynamicDialogRef) {
    this.service.removeCollaborator(this.table.selectedRow);
    this.table.filterTable('', '', '');
    this.table.selectedRow = null;
    ref.close();
  }

  updateTable () {
    this.table.filterTable('', '', '');
  }
}
