import { Validators } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { InputTypeEnum } from '../../../../../shared/enums/input-type.enum';

import { AddCollaborators } from './add-collaborator.interface';

const add_collaborator_template: AddCollaborators[] = [
  {
    type: InputTypeEnum.TEXT,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.firstName_label',
    id: 'name',
    maxLength: 128,
    validations: [
      {
        definition: 'required',
        validation: Validators.required,
        msg: 'VALIDATIONS.Required'
      }
    ]
  },
  {
    type: InputTypeEnum.TEXT,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.lastName_label',
    id: 'lastName',
    maxLength: 128,
    validations: [
      {
        definition: 'required',
        validation: Validators.required,
        msg: 'VALIDATIONS.Required'
      }
    ]
  },
  {
    type: InputTypeEnum.TEXT,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.function_label',
    id: 'function',
    maxLength: 128,
    validations: [
      {
        definition: 'required',
        validation: Validators.required,
        msg: 'VALIDATIONS.Required'
      }
    ]
  },
  {
    type: InputTypeEnum.TEXT,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.email_label',
    id: 'email',
    maxLength: 128
  },
  {
    type: InputTypeEnum.TEXT,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.phone_label',
    id: 'phone',
    maxLength: 128
  },
  {
    type: InputTypeEnum.DROPDOWN,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.contracting_authority_label',
    id: 'authority',
    options: new BehaviorSubject<any[]>([])
  },
  {
    type: InputTypeEnum.DROPDOWN,
    label: 'MENU.ADMIN_TENANT.COLLABORATORS.ADD_EDIT_COLLABORATOR.department_label',
    id: 'department',
    options: new BehaviorSubject<any[]>([])
  }
];

export default add_collaborator_template;
