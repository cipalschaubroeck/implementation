import { ValidatorFn } from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { InputTypeEnum } from '../../../../../shared/enums/input-type.enum';

export interface AddCollaborators {

  type: InputTypeEnum;
  label: string;
  id: string;
  buttonCombo?: {
    id?: string;
    label?: string;
  };
  options?: BehaviorSubject<SelectItem[][]>;
  maxLength?: number;
  validations?: {
    definition: string;
    validation: ValidatorFn,
    msg: string;
  }[];
}
