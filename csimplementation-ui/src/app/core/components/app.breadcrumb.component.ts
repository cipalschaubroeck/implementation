import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { filter } from 'rxjs/operators';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './app.breadcrumb.component.html'
})
export class AppBreadcrumbComponent implements OnInit {

  items: MenuItem[];
  lastLabel: '';

  constructor(private activatedRoute: ActivatedRoute,
              private router: Router,
              private translateService: TranslateService) {
    this.items = [];
  }

  ngOnInit() {
    this.router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(() => {
      const root: ActivatedRoute = this.activatedRoute.root;
      this.items = this.getBreadcrumbs(root);
    });
  }

  getBreadcrumbs(route: ActivatedRoute, breadcrumbs: MenuItem[] = [], previousPath: string = ''): MenuItem[] {
    const ROUTE_DATA_BREADCRUMB = 'breadcrumb';
    const children: ActivatedRoute[] = route.children;

    if (children.length === 0) {
      return breadcrumbs;
    }

    for (const child of children) {
      // Workaround for import-certificates-route, otherwise we'd get "Migrations / Imports / Imports"
      if (child.snapshot.data[ROUTE_DATA_BREADCRUMB] && child.snapshot.data[ROUTE_DATA_BREADCRUMB] !== this.lastLabel) {
        const breadCrumbLabel = this.translateService.instant(child.snapshot.data[ROUTE_DATA_BREADCRUMB]);
        child.url.subscribe(urlSegment => {
          const path: String = urlSegment[0].path;
          if (path !== 'home') {
            const breadcrumb: MenuItem = {
              label: breadCrumbLabel,
              routerLink: previousPath + path
            };
            previousPath += path + '/';
            breadcrumbs.push(breadcrumb);
          }
        });
      }

      this.lastLabel = child.snapshot.data[ROUTE_DATA_BREADCRUMB];
      return this.getBreadcrumbs(child, breadcrumbs, previousPath);
    }
  }
}
