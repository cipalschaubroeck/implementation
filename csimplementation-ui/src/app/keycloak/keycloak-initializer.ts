import { APP_INITIALIZER, Provider } from '@angular/core';

import { KeycloakAngularModule, KeycloakService } from 'keycloak-angular';

import { environment } from '../../environments/environment';
import { AuthenticatedGuard } from './authenticated.guard';

function keycloakInitializer (keycloak: KeycloakService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise(async (resolve, reject) => {
      try {
        await keycloak.init({
          config: environment.keycloak,
          initOptions: {
            onLoad: 'login-required',
            checkLoginIframe: false
          },
          bearerExcludedUrls: []
        });
        resolve();
      } catch (error) {
        reject(error);
      }
    });
  };
}

export function provideKeycloakIfActive (providers: Provider[]): void {
  if (environment.keycloak) {
    providers.push({
        provide: APP_INITIALIZER,
        useFactory: keycloakInitializer,
        multi: true,
        deps: [KeycloakService]
      },
      AuthenticatedGuard);
  }
}

export function importKeycloakIfActive (imports: any[]): void {
  if (environment.keycloak) {
    imports.push(KeycloakAngularModule);
  }
}

/**
 * adds the Authentication Guard to the route. To check particular roles, add them to data.roles of the route
 * @param guards current array of guards
 */
export function authenticateIfActive (guards: any[]) {
  if (environment.keycloak) {
    guards.push(AuthenticatedGuard);
  }
  return guards;
}
