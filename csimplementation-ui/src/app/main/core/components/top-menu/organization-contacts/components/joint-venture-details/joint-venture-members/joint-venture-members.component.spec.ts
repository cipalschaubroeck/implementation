import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointVentureMembersComponent } from './joint-venture-members.component';

describe('JointVentureMembersComponent', () => {
  let component: JointVentureMembersComponent;
  let fixture: ComponentFixture<JointVentureMembersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JointVentureMembersComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureMembersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
