import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JointVentureDetailsComponent } from './joint-venture-details.component';

describe('JointVentureDetailsComponent', () => {
  let component: JointVentureDetailsComponent;
  let fixture: ComponentFixture<JointVentureDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [JointVentureDetailsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JointVentureDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
