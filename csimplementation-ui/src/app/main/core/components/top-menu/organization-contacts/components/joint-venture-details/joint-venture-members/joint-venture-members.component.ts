import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { SelectItem } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
// tslint:disable-next-line:max-line-length
import { OrganizationInfoModel } from '../../../../../../../../core/components/top-menu/organization-contacts/models/organization-info.model';
// tslint:disable-next-line:max-line-length
import { OrganizationContactService } from '../../../../../../../../core/components/top-menu/organization-contacts/services/organization-contact.service';
import { GenericTableColumnModel } from '../../../../../../../../shared/components/generic-table/models/generic-table.models';

@Component({
  selector: 'app-joint-venture-members',
  templateUrl: './joint-venture-members.component.html',
  styleUrls: ['./joint-venture-members.component.css']
})
export class JointVentureMembersComponent implements OnInit {
  private cols: GenericTableColumnModel[];
  private selected$: BehaviorSubject<any>;
  private members: BehaviorSubject<OrganizationInfoModel[]> = new BehaviorSubject<OrganizationInfoModel[]>([]);
  private organizations: SelectItem[] = [];
  private selectedRowBehaviorSubject: BehaviorSubject<any> = new BehaviorSubject(null);
  private selection: any;
  private nextOrganization: OrganizationInfoModel;
  @Output() private memberAdded: EventEmitter<OrganizationInfoModel> = new EventEmitter();
  @Output() private memberRemoved: EventEmitter<OrganizationInfoModel> = new EventEmitter();

  constructor (private orgService: OrganizationContactService) {
  }

  ngOnInit () {
    this.cols = [
      {
        field: 'legalName',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.legalName',
        hasSort: true,
      },
      {
        field: 'vatNumber',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.vatNumber',
        hasSort: true,
      },
      {
        field: 'kboNumber',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.kboNumber',
        hasSort: true,
      },
      {
        field: 'basicAddress.country',
        text: 'MENU.ADMIN_TENANT.ORGANIZATION_CONTACTS.COLUMNS.country',
        hasSort: true,
      }
    ];
    this.orgService.getOrganizations({type: 'SINGLE_ORGANIZATION'}).toPromise().then(orgs => {
      this.organizations = [];
      orgs.forEach(org => this.organizations.push({value: org, label: org.legalName + ' KBO ' + org.kboNumber}));
      this.nextOrganization = this.organizations[0].value;
    });
  }

  private addOrganization () {
    const current: OrganizationInfoModel[] = this.members.getValue();
    if (current.indexOf(this.nextOrganization) < 0) {
      current.push(this.nextOrganization);
      this.members.next(current);
      this.memberAdded.emit(this.nextOrganization);
    }
  }

  private removeOrganization () {
    const index = this.members.getValue().indexOf(this.selectedRowBehaviorSubject.getValue());
    if (index > -1) {
      const current: OrganizationInfoModel[] = this.members.getValue();
      const removed = current[index];
      current.splice(index, 1);
      this.members.next(current);
      this.memberRemoved.emit(removed);
    }
  }

  private onRowSelection (event): any {
    this.selectedRowBehaviorSubject.next(event.data);
  }

  private onRowUnSelection (): any {
    this.selectedRowBehaviorSubject.next(null);
  }

  setMembers (members: OrganizationInfoModel[]) {
    this.members.next(members);
  }

  getMembers (): OrganizationInfoModel[] {
    return this.members.getValue();
  }
}
