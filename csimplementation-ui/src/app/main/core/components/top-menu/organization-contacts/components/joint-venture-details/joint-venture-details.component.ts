import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import i18n_iso_countries_en from 'i18n-iso-countries/langs/en.json';
import i18n_iso_countries_nl from 'i18n-iso-countries/langs/nl.json';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { forkJoin, Observable } from 'rxjs';
import { OrganizationInfoModel } from '../../../../../../../core/components/top-menu/organization-contacts/models/organization-info.model';
// tslint:disable-next-line:max-line-length
import { OrganizationContactService } from '../../../../../../../core/components/top-menu/organization-contacts/services/organization-contact.service';
import { GenericMessageService } from '../../../../../../../shared/services/generic-message.service';
import { ContactBasic } from '../../models/contact-basic.model';
import { JointVentureInfoModel } from '../../models/joint-venture-info.model';
import { JointVentureMembersComponent } from './joint-venture-members/joint-venture-members.component';

@Component({
  selector: 'app-joint-venture-details',
  templateUrl: './joint-venture-details.component.html',
  styleUrls: ['./joint-venture-details.component.css']
})
export class JointVentureDetailsComponent implements OnInit {

  private countries_list: any;
  private countries: SelectItem[] = [];
  private availableContacts: SelectItem[] = [];
  @ViewChild(JointVentureMembersComponent, {static: false}) private members: JointVentureMembersComponent;
  private form: FormGroup;

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig,
               private service: OrganizationContactService,
               private message: GenericMessageService,
               private translateService: TranslateService) {
  }

  ngOnInit () {
    this.populateCountryOption();
    this.form = new FormGroup({
      name: new FormControl('', Validators.required),
      street: new FormControl(),
      number: new FormControl(),
      box: new FormControl(),
      postalCode: new FormControl(),
      municipality: new FormControl(),
      country: new FormControl('', Validators.required),
      mainContact: new FormControl()
    });

    if (this.config && this.config.data && this.config.data.edit && this.config.data.id) {
      this.service.getOrganizationDetail(this.config.data.id, 'JOINT_VENTURE').subscribe(data => {
        this.populateEditForm(data as JointVentureInfoModel);
      });
    }
  }

  private populateEditForm (data: JointVentureInfoModel) {
    this.form.controls['name'].setValue(data.legalName);
    this.form.controls['street'].setValue(data.basicAddress.streetName);
    this.form.controls['postalCode'].setValue(data.basicAddress.zipCode);
    this.form.controls['box'].setValue(data.basicAddress.locator);
    this.form.controls['number'].setValue(data.basicAddress.houseNumber);
    this.form.controls['country'].setValue(data.basicAddress.country);
    this.form.controls['municipality'].setValue(data.basicAddress.city);
    this.members.setMembers(data.members);
    const calls = [];
    for (let i = 0; i < data.members.length; i++) {
      calls.push(this.service.getAvailableContacts(data.members[i]));
    }
    forkJoin(calls).subscribe(colOfColsOfContacts => {
      for (let i = 0; i < colOfColsOfContacts.length; i++) {
        this.addAvailableContacts(colOfColsOfContacts[i] as ContactBasic[]);
      }
      if (data.mainContact) {
        this.form.controls['mainContact'].setValue(this.availableContacts
          .find(x => x.value.externalPersonInOrganizationId === data.mainContact.externalPersonInOrganizationId).value);
      }
    });
  }

  private populateCountryOption () {
    if (this.translateService.currentLang.indexOf('en') !== -1) {
      this.countries_list = i18n_iso_countries_en;
    } else if (this.translateService.currentLang.indexOf('nl') !== -1) {
      this.countries_list = i18n_iso_countries_nl;
    }
    const keys = Object.keys(this.countries_list.countries);
    keys.forEach(country => {
      this.countries.push({
        value: country,
        label: this.countries_list.countries[country]
      });
    });
  }

  private onSubmit () {
    let response: Observable<any>;
    let textAction: string;
    if (this.config && this.config.data && this.config.data.edit && this.config.data.id) {
      textAction = 'MESSAGE_SERVICE.updated';
      response = this.service.editOrganization(this.config.data.id, this.populateRequestBody());
    } else {
      textAction = 'MESSAGE_SERVICE.success';
      response = this.service.createOrganization(this.populateRequestBody());
    }

    response.subscribe(() => {
      this.message.popMessage('success', textAction, textAction);
      this.config.data.update();
      this.ref.close();
    });
  }

  private cancel () {
    return this.ref.close();
  }

  private populateRequestBody (): JointVentureInfoModel {
    return {
      id: (this.config && this.config.data && this.config.data.edit) ? this.config.data.id : null,
      legalName: this.form.controls['name'].value,
      basicAddress: {
        streetName: this.form.controls['street'].value,
        houseNumber: this.form.controls['number'].value,
        locator: this.form.controls['box'].value,
        zipCode: this.form.controls['postalCode'].value,
        country: this.form.controls['country'].value,
        city: this.form.controls['municipality'].value
      },
      type: 'JOINT_VENTURE',
      members: this.members.getMembers(),
      mainContact: this.form.controls['mainContact'].value
    };
  }

  private onMemberAdded (member: OrganizationInfoModel) {
    this.service.getAvailableContacts(member).subscribe(contacts => {
      const casted = contacts as ContactBasic[];
      this.addAvailableContacts(casted);
    });
  }

  private onMemberRemoved (exMember: OrganizationInfoModel) {
    for (let i = this.availableContacts.length; i >= 0; i--) {
      if (this.availableContacts[i].value.externalOrganizationId === exMember.id) {
        this.availableContacts.splice(i, 1);
      }
    }
    if (this.form.controls['mainContact'].value
      && this.form.controls['mainContact'].value.externalOrganizationId === exMember.id) {
      this.form.controls['mainContact'].setValue(null);
    }
  }

  private addAvailableContacts (contacts: ContactBasic[]) {
    for (let i = 0; i < contacts.length; i++) {
      let pos = 0;
      while (pos < this.availableContacts.length && this.availableContacts[pos].label < contacts[i].fullName) {
        pos++;
      }
      if (pos >= this.availableContacts.length) {
        this.availableContacts.push({label: contacts[i].fullName, value: contacts[i]});
      } else if (this.availableContacts[pos].value.externalPersonInOrganizationId !== contacts[i].externalPersonInOrganizationId) {
        this.availableContacts.splice(pos, 0, {label: contacts[i].fullName, value: contacts[i]});
      }
    }
  }
}
