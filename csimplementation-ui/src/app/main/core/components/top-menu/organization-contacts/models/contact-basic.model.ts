export class ContactBasic {
  fullName: string;
  externalOrganizationId: string;
  externalPersonInOrganizationId: string;
}
