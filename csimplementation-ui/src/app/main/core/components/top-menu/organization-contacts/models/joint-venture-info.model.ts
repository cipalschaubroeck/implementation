import { OrganizationInfoModel } from '../../../../../../core/components/top-menu/organization-contacts/models/organization-info.model';
import { ContactBasic } from './contact-basic.model';

export interface JointVentureInfoModel extends OrganizationInfoModel {
  members: OrganizationInfoModel[];
  mainContact?: ContactBasic;
}
