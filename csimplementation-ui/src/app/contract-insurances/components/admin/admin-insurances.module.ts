import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';

import { AdminInsurancesRoutingModule } from './admin-insurances-routing.module';
import { AdminInsurancesComponent } from './admin-insurances.component';
import { InsurancesInfoComponent } from './insurances-info/insurances-info.component';
import { AddEditInsuranceComponent } from './insurances-info/policies-taken-out/add-edit-insurance/add-edit-insurance.component';
import { PoliciesTakenOutComponent } from './insurances-info/policies-taken-out/policies-taken-out.component';
import { AddEditRemindersComponent } from './insurances-info/reminders/add-edit-reminders/add-edit-reminders.component';
import { ReminderComponent } from './insurances-info/reminders/reminder.component';

const components = [
  AdminInsurancesComponent,
  InsurancesInfoComponent,
  PoliciesTakenOutComponent,
  ReminderComponent,
  AddEditInsuranceComponent,
  AddEditRemindersComponent
];

const entryComponents = [AddEditInsuranceComponent, AddEditRemindersComponent];

@NgModule({
  declarations: components,
  entryComponents: entryComponents,
  imports: [
    AdminInsurancesRoutingModule,
    SharedModule
  ]
})
export class AdminInsurancesModule {
}
