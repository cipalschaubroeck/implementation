import { DocumentProcessModel } from './document-process-model';

export interface RemindersModel {
  refNum?: string;
  dateReminder?: string;
  documentName?: string;
  documents?: DocumentProcessModel[];
  links?: {
    self: string;
    documentLink: string
    insuredParty: string;
  };
}
