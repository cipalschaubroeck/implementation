import { OrganizationInsuranceModel } from './organization-insurance-model';

export interface RelatedPartiesModel {
  organization: OrganizationInsuranceModel;
  link?: {
    self: string;
  };
}
