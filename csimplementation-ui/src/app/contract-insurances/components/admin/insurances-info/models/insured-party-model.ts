export interface InsuredPartyModel {
  refNumber?: string;
  deadline?: string;
  insuredParty?: string;
  policyType?: string;
  subject?: string;
  parties?: any[];
  links?: {
    self: string
  };
}
