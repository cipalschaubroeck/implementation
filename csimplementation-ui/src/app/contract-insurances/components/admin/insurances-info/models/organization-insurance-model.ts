export interface OrganizationInsuranceModel {
  name: string;
  link: {
    self: string;
  };
}
