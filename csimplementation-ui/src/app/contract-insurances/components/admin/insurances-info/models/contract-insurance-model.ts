import { StatusType } from '../../../../../contracts/common/enums/status-type.enum';
import { AuthorityInsuranceModel } from './authority-insurance-model';
import { OrganizationInsuranceModel } from './organization-insurance-model';
import { RelatedPartiesModel } from './related-parties-model';

export interface ContractInsuranceModel {
  id?: string;
  status?: StatusType;
  authority?: AuthorityInsuranceModel;
  contractor?: OrganizationInsuranceModel;
  relatedParties?: RelatedPartiesModel[];
  link?: {
    self: string;
  };
}
