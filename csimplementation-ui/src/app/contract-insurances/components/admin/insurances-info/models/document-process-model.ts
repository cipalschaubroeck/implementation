export interface DocumentProcessModel {
  id?: string;
  name?: string;
  date?: Date;
  status?: string;
  type?: string;
  links?: {
    self: string;
  };
}
