import { HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { Utils } from '../../../../../../shared/classes/utils';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { ContractInsuranceModel } from '../../models/contract-insurance-model';
import { InsuredPartyModel } from '../../models/insured-party-model';
import { RemindersModel } from '../../models/reminders-model';
import { TakenOutService } from '../../policies-taken-out/services/taken-out.service';
import { ReminderService } from '../services/reminder.service';

@Component({
  selector: 'app-add-edit-reminders',
  templateUrl: './add-edit-reminders.component.html',
  styleUrls: ['./add-edit-reminders.component.css']
})
export class AddEditRemindersComponent implements OnInit {
  private Utils = Utils;
  private form: FormGroup;
  private insuredPartyOptions: SelectItem[] = [];
  private documentLinksOptions: SelectItem[] = [];
  private contract: ContractInsuranceModel;
  private insuredPartyLink: InsuredPartyModel;
  private reminder: RemindersModel;

  private static initForm (reminder: RemindersModel): FormGroup {
    return new FormGroup({
      reminderDate: new FormControl(
        reminder.dateReminder ? new Date(reminder.dateReminder.trim()) : '', Validators.required),
      refNum: new FormControl(reminder.links ? reminder.links.insuredParty : '', Validators.required),
      documentLinks: new FormControl(reminder.links ? reminder.links.documentLink : '')
    });
  }

  constructor (private reminderService: ReminderService,
               public ref: DynamicDialogRef,
               public config: DynamicDialogConfig,
               private takenOutService: TakenOutService,
               private messageService: GenericMessageService) {
  }

  ngOnInit () {
    if (this.config.data.reminder) {
      this.reminder = this.config.data.reminder;
    }

    this.contract = this.config.data.contract;
    if (!this.contract) {
      throw new Error('Contract info has not been provided');
    }
    this.populateOptions();
  }

  private populateOptions () {
    this.takenOutService.getAllList(new HttpParams()).subscribe(response => {
      response.data.forEach(data => {
        this.insuredPartyOptions.push({
          label: data.refNumber,
          value: data.links.self
        });
      });
      this.reminderService.getAllContractInsuranceDocumentProcesses(this.contract).then(processes => {
        processes.forEach(process => {
          this.documentLinksOptions.push({
            label: process.name, value: process.links.self
          });
        });

        this.form = AddEditRemindersComponent.initForm(this.reminder ? this.reminder : {});
      });
    });
  }

  private save () {
    this.insuredPartyLink = this.form.controls.refNum.value;

    const documentLinks = [];
    if (this.form.controls.documentLinks.value) {
      this.form.controls.documentLinks.value.forEach((docLink) => {
        documentLinks.push(docLink);
      });
    }

    this.reminderService.saveOrUpdateReminder(this.insuredPartyLink + '/reminder', {
      reminderDate: this.form.controls.reminderDate.value,
      documentProcesses: documentLinks,
      reminder: this.reminder ? this.reminder.links.self : null
    }).subscribe(() => {
      this.config.data.updateTable();
      this.messageService.success();
    }, () => this.messageService.error(), () => {
      this.config.data.updateTable();
      this.ref.close();
    });
  }

  private cancel () {
    this.ref.close();
  }

}
