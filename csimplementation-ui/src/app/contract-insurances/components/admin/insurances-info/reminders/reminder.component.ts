import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DialogService } from 'primeng/api';
import { BehaviorSubject, Subscription } from 'rxjs';
import { StatusType } from '../../../../../contracts/common/enums/status-type.enum';
import { GenericTableLazyComponent } from '../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import {
  GenericTableLazyButtonModel,
  GenericTableLazyColumnModel
} from '../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { GenericMessageService } from '../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../shared/services/generic-translate.service';
import { ContractInsuranceModel } from '../models/contract-insurance-model';
import { RemindersModel } from '../models/reminders-model';
import { InsurancesService } from '../services/insurances.service';
import { AddEditRemindersComponent } from './add-edit-reminders/add-edit-reminders.component';
import { ReminderService } from './services/reminder.service';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.css'],
  providers: [DialogService]
})
export class ReminderComponent implements OnInit, OnDestroy {

  private elements: BehaviorSubject<RemindersModel[]> = new BehaviorSubject<RemindersModel[]>([]);
  private rightButtons: GenericTableLazyButtonModel[] = [];
  private subscriptions: Subscription[] = [];

  private columns: GenericTableLazyColumnModel[] = [
    {
      field: 'refNum',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.refNum',
      class: 'no-padding small-size',
      hasSort: true
    },
    {
      field: 'reminderDate',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.reminderDate',
      class: 'no-padding small-size',
      hasSort: true
    },
    {
      field: 'documentLinks',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.documentLinks',
      class: 'no-padding small-size',
      hasSort: false
    }
  ];

  @ViewChild(GenericTableLazyComponent, {static: true}) table: GenericTableLazyComponent;

  @Input()
  contract: ContractInsuranceModel;

  constructor (private reminderService: ReminderService,
               protected route: ActivatedRoute,
               private dialogService: DialogService,
               private translate: GenericTranslateService,
               private messageService: GenericMessageService,
               private insuranceService: InsurancesService) {
  }

  ngOnDestroy (): void {
    this.subscriptions.forEach(subscription => {
      subscription.unsubscribe();
    });
  }

  ngOnInit () {
    if (!this.contract) {
      throw new Error('Contract info has not been provided');
    }

    this.subscriptions.push(this.insuranceService.getRefreshEvent().subscribe(() => {
      this.table.filterTable('', '', '');
    }));

    this.reminderService.contractId = this.contract.id;
    GenericTableLazyComponent.loadButton('addInsurance', 'ui-icon-plus',
      'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.addReminders',
      this.addReminder, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    GenericTableLazyComponent.loadButton(
      'editInsurance', 'ui-icon-edit',
      'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.editReminders',
      this.editReminder, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    GenericTableLazyComponent.loadButton(
      'deleteInsurance', 'ui-icon-trash',
      'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.removeReminders',
      this.deleteReminder, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    this.table.initButtons(this.rightButtons, []);

    this.table.initTableConfig({
      columns: this.columns,
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'CONTRACT.INSURANCES.ADMIN.noReminders',
      paginator: {
        rows: 5
      },
      sortField: ('refNum'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.reminderService);
  }

  private addReminder () {
    const title = this.translate.translate('CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.addRemindersTitle');
    this.dialogService.open(AddEditRemindersComponent, {
      header: title,
      width: '35%',
      data: {
        'contract': this.contract,
        'updateTable': () => this.table.filterTable('', '', '')
      }
    });
    this.table.selectedRow = null;
  }

  private editReminder () {
    if (this.table.selectedRow) {
      const title = this.translate.translate('CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.editRemindersTitle');
      this.dialogService.open(AddEditRemindersComponent, {
        header: title,
        width: '35%',
        data: {
          'contract': this.contract,
          'reminder': this.table.selectedRow,
          'updateTable': () => this.table.filterTable('', '', '')
        }
      });
    } else {
      this.messageService.mustSelect();
    }

  }

  private deleteReminder () {
    this.reminderService.deleteReminder(this.table.selectedRow.links.self).subscribe(
      () => this.messageService.success(),
      () => this.messageService.error(),
      () => {
        this.table.filterTable('', '', '');
        this.table.selectedRow = null;
      });
  }
}
