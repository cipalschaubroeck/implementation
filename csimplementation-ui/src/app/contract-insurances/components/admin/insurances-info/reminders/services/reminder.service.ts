import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../../../../core/services/base-http.service';
import { Utils } from '../../../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../../../shared/models/page-model';
import { ContractInsuranceModel } from '../../models/contract-insurance-model';
import { DocumentProcessModel } from '../../models/document-process-model';

import { RemindersModel } from '../../models/reminders-model';

@Injectable({
  providedIn: 'root'
})
export class ReminderService extends BaseHttpService<any> {
  page: PageModel = {data: {}, page: {totalNumber: 0}};
  contractId: string;

  private static processDocuments (documents: any): DocumentProcessModel[] {
    const docs: DocumentProcessModel[] = [];
    if (documents) {
      documents.forEach(document => {
        docs.push({
          name: document.documentProcess.name,
          links: {
            self: Utils.cleanUrl(document.documentProcess._links ? document.documentProcess._links.self.href : '')
          }
        });
      });
    }
    return docs;
  }

  getAllList (params: HttpParams) {
    if (!this.contractId) {
      throw new Error('Contract id cannot be null');
    }

    params = params.append(ConstantsEnum.PROJECTION, 'remindersProjection');

    return this.httpClient.get(this.endpoint + 'contract/' + this.contractId + '/reminders', {params})
      .pipe(map((data: any) => {
          const reminders: RemindersModel [] = [];
        for (const item of data._embedded.reminders) {
            const reminder: RemindersModel = {
              dateReminder: item.reminderDate,
              documentName: item.documentName,
              documents: ReminderService.processDocuments(item.documents),
              refNum: item.referenceNumber,
              links: {
                self: Utils.cleanUrl(item._links.self.href),
                documentLink: Utils.cleanUrl(item.documentProcess ? item.documentProcess._links.self.href : ''),
                insuredParty: Utils.cleanUrl(item.insuredParty ? item.insuredParty._links.self.href : '')
              }
            };
            reminders.push(reminder);
          }
          this.page.page.totalNumber = data.page.totalElements;
          this.page.data = reminders;
          return this.page;
        }
      ));
  }

  getAllContractInsuranceDocumentProcesses (contract: ContractInsuranceModel): Promise<DocumentProcessModel[]> {
    const params: HttpParams = new HttpParams().set('projection', 'documentProcessInfo');
    return new Promise<any>(response => {
      this.httpClient.get(contract.link.self + '/document-process', {params}).pipe(map((data: any) => {
        const processes: DocumentProcessModel[] = [];
        data._embedded.documentProcesses.forEach(process => {
          processes.push({
            name: process.name,
            type: process.type,
            status: process.status,
            date: process.date,
            id: process.id,
            links: {
              self: process._links.self.href
            }
          });
        });
        return processes;
      })).subscribe(response);
    });
  }

  saveOrUpdateReminder (link: string, body: any): Observable<any> {
    return this.httpClient.post(link, body);
  }

  deleteReminder (link: string): Observable<any> {
    return this.httpClient.delete(link);
  }
}
