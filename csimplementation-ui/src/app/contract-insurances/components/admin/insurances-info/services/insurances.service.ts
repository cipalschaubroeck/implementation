import { HttpParams } from '@angular/common/http';
import { EventEmitter, Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../../../core/services/base-http.service';
import { ConstantsEnum } from '../../../../../shared/enums/constants.enum';
import { ContractInsuranceModel } from '../models/contract-insurance-model';

@Injectable({
  providedIn: 'root'
})
export class InsurancesService extends BaseHttpService<any> {
  private refreshTableEvent: EventEmitter<any> = new EventEmitter<any>();

  getRefreshEvent (): EventEmitter<any> {
    return this.refreshTableEvent;
  }

  getContractStatus (contractId: string): Promise<ContractInsuranceModel> {
    if (!contractId) {
      throw new Error('Contract id cant be null');
    }
    const params = new HttpParams().set(ConstantsEnum.PROJECTION, 'contractStatusInfo');
    return new Promise<ContractInsuranceModel>(response =>
      this.httpClient.get<ContractInsuranceModel>(this.endpoint + 'contract/' + contractId, {params})
        .pipe(map((data: any) => {
          return {
            id: data.id,
            status: data.status,
            link: {
              self: data._links.self.href
            }
          } as ContractInsuranceModel;
        })).subscribe(response));
  }
}
