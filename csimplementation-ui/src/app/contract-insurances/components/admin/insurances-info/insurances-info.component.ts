import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ContractInsuranceModel } from './models/contract-insurance-model';
import { InsurancesService } from './services/insurances.service';

@Component({
  selector: 'app-insurances-info',
  templateUrl: './insurances-info.component.html',
  styleUrls: ['./insurances-info.component.css']
})
export class InsurancesInfoComponent implements OnInit {

  private contract: ContractInsuranceModel;
  private contractId: string;

  constructor (private insurancesService: InsurancesService,
               protected route: ActivatedRoute) {
  }

  ngOnInit () {
    this.contractId = this.route.snapshot.parent.paramMap.get('contractId');
    this.insurancesService.getContractStatus(this.contractId).then(contract => {
      this.contract = contract;
    });
  }

}
