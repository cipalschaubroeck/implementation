import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { DynamicDialogConfig, DynamicDialogRef, SelectItem } from 'primeng/api';
import { Utils } from '../../../../../../shared/classes/utils';
import { GenericMessageService } from '../../../../../../shared/services/generic-message.service';
import { ContractInsuranceModel } from '../../models/contract-insurance-model';
import { InsuredPartyModel } from '../../models/insured-party-model';
import { TakenOutService } from '../services/taken-out.service';

@Component({
  selector: 'app-add-edit-insurance',
  templateUrl: './add-edit-insurance.component.html',
  styleUrls: ['./add-edit-insurance.component.css']
})
export class AddEditInsuranceComponent implements OnInit {

  constructor (public ref: DynamicDialogRef,
               public config: DynamicDialogConfig,
               private takenOutService: TakenOutService,
               private messageService: GenericMessageService) {
  }

  private insuredParty: InsuredPartyModel;
  private insuredPartyLink: string;
  private form: FormGroup;
  private contract: ContractInsuranceModel;
  private insuredPartyOptions: SelectItem[] = [];
  private Utils = Utils;

  private static initForm (insuredParty: InsuredPartyModel): FormGroup {
    return new FormGroup({
      deadline: new FormControl(insuredParty.deadline ? new Date(insuredParty.deadline.trim()) : ''),
      insuredParty: new FormControl(insuredParty.parties, Validators.required),
      policyType: new FormControl(insuredParty.policyType, Validators.required),
      subject: new FormControl(insuredParty.subject)
    });
  }

  ngOnInit () {
    this.contract = this.config.data.contract;
    this.takenOutService.getPossibleInsuredComponentsForContract(this.contract.link.self).then((response) => {
      response.forEach(r => {
        if (r.label && r.value) {
          this.insuredPartyOptions.push(r);
        }
      });
    });

    if (this.config.data.insuredParty) {
      this.insuredParty = this.config.data.insuredParty;
      this.insuredPartyLink = this.insuredParty.links.self;
    } else {
      this.insuredParty = {};
    }

    this.insuredParty = this.config.data.insuredParty ? this.config.data.insuredParty : {};
    this.form = AddEditInsuranceComponent.initForm(this.insuredParty);
  }

  private save () {
    if (this.form.controls.insuredParty.value) {
      interface Body {
        relatedParty?: any[];
        authority?: string;
        contractor?: string;
        policyType?: string;
        subject?: string;
        deadline?: Date;
        insuredParty?: string;
      }

      const body: Body = {
        relatedParty: []
      };

      if (this.insuredPartyLink) {
        body.insuredParty = this.insuredPartyLink;
      }

      body.subject = this.form.controls.subject.value;
      body.deadline = this.form.controls.deadline.value;
      body.policyType = this.form.controls.policyType.value;
      this.form.controls.insuredParty.value.forEach(control => {
        if ('relatedParty' in control) {
          body.relatedParty.push(control.relatedParty);
        } else if ('authority' in control) {
          body.authority = control.authority;
        } else if ('contractor' in control) {
          body.contractor = control.contractor;
        } else {
          throw new Error('A related party member must be selected');
        }
      });

      this.takenOutService.saveUpdateInsuredParties(this.contract.link.self, body).subscribe(() => {
        this.messageService.success();
      }, () => {
        this.messageService.error();
      });
    } else {
      this.messageService.error();
    }
    this.config.data.update();
    this.ref.close();
  }

  private cancel () {
    this.ref.close();
  }
}
