import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DialogService } from 'primeng/api';
import { BehaviorSubject } from 'rxjs';
import { StatusType } from '../../../../../contracts/common/enums/status-type.enum';
import { GenericTableLazyComponent } from '../../../../../shared/components/generic-table-lazy/generic-table-lazy.component';
import {
  GenericTableLazyButtonModel,
  GenericTableLazyColumnModel
} from '../../../../../shared/components/generic-table-lazy/models/generic-table-lazy.models';
import { GenericMessageService } from '../../../../../shared/services/generic-message.service';
import { GenericTranslateService } from '../../../../../shared/services/generic-translate.service';
import { ContractInsuranceModel } from '../models/contract-insurance-model';
import { InsuredPartyModel } from '../models/insured-party-model';
import { InsurancesService } from '../services/insurances.service';
import { AddEditInsuranceComponent } from './add-edit-insurance/add-edit-insurance.component';
import { TakenOutService } from './services/taken-out.service';

@Component({
  selector: 'app-policies-taken-out',
  templateUrl: './policies-taken-out.component.html',
  styleUrls: ['./policies-taken-out.component.css'],
  providers: [DialogService]
})
export class PoliciesTakenOutComponent implements OnInit {

  private rightButtons: GenericTableLazyButtonModel[] = [];
  private elements: BehaviorSubject<InsuredPartyModel[]> = new BehaviorSubject<InsuredPartyModel[]>([]);
  private options: GenericTableLazyColumnModel[] = [
    {
      field: 'refNum',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.refNum',
      class: 'no-padding small-size',
      hasSort: true
    },
    {
      field: 'deadline',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.deadline',
      class: 'no-padding small-size',
      hasSort: true
    },
    {
      field: 'insuredParty',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.insuredParty',
      class: 'no-padding small-size',
      hasSort: false
    },
    {
      field: 'policyType',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.policyType',
      class: 'no-padding small-size',
      hasSort: false
    },
    {
      field: 'subject',
      text: 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.subject',
      class: 'no-padding small-size',
      hasSort: false
    }
  ];

  @Input()
  contract: ContractInsuranceModel;

  @ViewChild(GenericTableLazyComponent, {static: true}) table: GenericTableLazyComponent;

  constructor (private takenOutService: TakenOutService,
               private insuranceService: InsurancesService,
               protected route: ActivatedRoute,
               private dialogService: DialogService,
               private translate: GenericTranslateService,
               private messageService: GenericMessageService) {
  }

  ngOnInit () {

    if (!this.contract) {
      throw new Error('Contract information has not been provided');
    }

    this.takenOutService.contractId = this.contract.id;

    GenericTableLazyComponent.loadButton('addInsurance', 'ui-icon-plus', 'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.add',
      this.addInsurance, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    GenericTableLazyComponent.loadButton(
      'editInsurance', 'ui-icon-edit',
      'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.edit',
      this.editInsurance, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    GenericTableLazyComponent.loadButton(
      'deleteInsurance', 'ui-icon-trash',
      'CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.columns.buttons.remove',
      this.deleteInsurance, this, this.contract.status === StatusType.PREPARATION, this.rightButtons);
    this.table.initButtons(this.rightButtons, []);

    this.table.initTableConfig({
      columns: this.options,
      containerClass: 'ui-g-12 smaller no-padding-right',
      noResultsMessage: 'CONTRACT.INSURANCES.ADMIN.noInsurances',
      paginator: {
        rows: 5
      },
      sortField: ('refNum'),
      sortOrder: -1,
      totalNumberRows: 0
    }, this.takenOutService);
  }

  private addInsurance () {
    const title = this.translate.translate('CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.addInsurancePolicyOut');
    this.dialogService.open(AddEditInsuranceComponent, {
      data: {
        'contract': this.contract,
        'update': () => this.table.filterTable('', '', '')
      },
      header: title,
      width: '35%'
    });
  }

  private editInsurance () {
    if (this.table.selectedRow) {
      const title = this.translate.translate('CONTRACT.INSURANCES.ADMIN.INSURANCES_INFO.editInsurancePolicyOut');
      this.dialogService.open(AddEditInsuranceComponent, {
        data: {
          'contract': this.contract,
          'update': () => this.table.filterTable('', '', ''),
          'insuredParty': this.table.selectedRow
        },
        header: title,
        width: '35%'
      });
    } else {
      this.messageService.mustSelect();
    }
  }

  private deleteInsurance () {
    if (this.table.selectedRow) {
      const insuredPartyLink = this.table.selectedRow.links.self;
      this.takenOutService.deleteTakenOutPolicies(insuredPartyLink).subscribe(
        () => this.messageService.success(), () => this.messageService.error(),
        () => {
          this.table.filterTable('', '', '');
          this.table.selectedRow = null;
          this.insuranceService.getRefreshEvent().emit();
        });
    } else {
      this.messageService.mustSelect();
    }
  }

}
