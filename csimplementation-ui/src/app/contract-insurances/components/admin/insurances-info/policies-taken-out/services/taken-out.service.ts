import { HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { BaseHttpService } from '../../../../../../core/services/base-http.service';
import { Utils } from '../../../../../../shared/classes/utils';
import { ConstantsEnum } from '../../../../../../shared/enums/constants.enum';
import { PageModel } from '../../../../../../shared/models/page-model';
import { InsuredPartyModel } from '../../models/insured-party-model';

@Injectable({
  providedIn: 'root'
})
export class TakenOutService extends BaseHttpService<any> {
  page: PageModel = {data: {}, page: {totalNumber: 0}};
  contractId: string;

  private static mapRelatedParty (listRelatedParties) {
    const relatedParties: any[] = [];
    listRelatedParties.forEach(rp => {
      relatedParties.push({
        label: rp.organization.name,
        value: Utils.cleanUrl(rp._links.self.href)
      });
    });
    return relatedParties;
  }

  private static mapParties (insuredParty) {
    const items = [];
    if (insuredParty) {
      this.mapAuthorityContractor(insuredParty, items);
      if (insuredParty.relatedParties) {
        insuredParty.relatedParties.forEach(rp => {
          items.push({
            label: rp.organization.name,
            value: {relatedParty: Utils.cleanUrl(rp._links.self.href)}
          });
        });
      }
    }
    return items;
  }

  private static mapRelatedPartiesFromInsuredParty (insuredParty) {
    const items = [];
    if (insuredParty) {
      if (insuredParty.authority) {
        items.push({authority: Utils.cleanUrl(insuredParty.authority._links.self.href)});
      }
      if (insuredParty.contractor) {
        items.push({contractor: Utils.cleanUrl(insuredParty.contractor._links.self.href)});
      }
      if (insuredParty.relatedParties) {
        insuredParty.relatedParties.forEach(rp => {
          items.push({relatedParty: Utils.cleanUrl(rp.relatedParty._links.self.href)});
        });
      }
    }
    return items;
  }

  private static mapAuthorityContractor (insuredParty, items: any[]) {
    if (insuredParty.authority) {
      items.push({
        label: insuredParty.authority ? insuredParty.authority.name : '',
        value: {authority: insuredParty.authority ? Utils.cleanUrl(insuredParty.authority._links.self.href) : ''}
      });
    }
    if (insuredParty.contractor) {
      items.push({
        label: insuredParty.contractor ? insuredParty.contractor.name : '',
        value: {collaborator: insuredParty.contractor ? Utils.cleanUrl(insuredParty.contractor._links.self.href) : ''}
      });
    }
  }

  getAllList (params: HttpParams) {
    params = params.append(ConstantsEnum.PROJECTION, 'insuredPartyProjection');
    if (!this.contractId) {
      throw new Error('Contract id cannot be null');
    }

    return this.httpClient.get(this.endpoint + 'contract/' + this.contractId + '/insured-party', {params})
      .pipe(map((data: any) => {
        const insurances: InsuredPartyModel[] = [];
        for (const item of data._embedded.insuredParties) {
          const insurance: InsuredPartyModel = {
            deadline: item.deadline,
            insuredParty: item.insurancePartyName,
            policyType: item.type,
            refNumber: item.referenceNumber,
            parties: TakenOutService.mapRelatedPartiesFromInsuredParty(item),
            subject: item.subject,
            links: {
              self: Utils.cleanUrl(item._links.self.href)
            }
          };
          insurances.push(insurance);
        }
        this.page.page.totalNumber = data.page.totalElements;
        this.page.data = insurances;
        return this.page;
      }));
  }

  getPossibleInsuredComponentsForContract (contractLink: string): Promise<any> {
    const params = new HttpParams().set(ConstantsEnum.PROJECTION, 'contractInsuredPartyProjection');
    return new Promise<any>(response => this.httpClient.get(contractLink, {params})
      .pipe(map((c: any) => {
        return TakenOutService.mapParties(c);
      })).subscribe(response));
  }

  saveUpdateInsuredParties (contractLink: string, body): Observable<any> {
    return this.httpClient.post(contractLink + '/insured-party', body);
  }

  deleteTakenOutPolicies (insuredPartyLink) {
    return this.httpClient.delete(insuredPartyLink);
  }
}
