import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminInsurancesComponent } from './admin-insurances.component';

const routes: Routes = [{path: 'admin-insurances', component: AdminInsurancesComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminInsurancesRoutingModule {
}
