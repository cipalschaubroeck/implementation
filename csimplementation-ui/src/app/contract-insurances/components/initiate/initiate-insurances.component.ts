import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { GenericMessageService } from '../../../shared/services/generic-message.service';

@Component({
  selector: 'app-insurances',
  templateUrl: './initiate-insurances.component.html',
  styleUrls: ['./initiate-insurances.component.css']
})
export class InitiateInsurancesComponent implements OnInit {

  private insurancesForm: FormGroup;

  constructor(private messageService: GenericMessageService) {}

  ngOnInit(): void {
    this.insurancesForm = new FormGroup({
      insurance: new FormControl('', Validators.required)
    });
  }

  onSubmit() {
    // TODO CSPROC-2229: Propagate the data to server app and database. Create entities and tables to handle this

    this.messageService.popMessage(
      'success', 'MESSAGE_SERVICE.success', 'MESSAGE_SERVICE.created'
    );
  }

}
