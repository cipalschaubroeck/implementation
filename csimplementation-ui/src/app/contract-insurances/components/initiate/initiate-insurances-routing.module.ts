import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InitiateInsurancesComponent } from './initiate-insurances.component';

const routes: Routes = [{path: 'insurances', component: InitiateInsurancesComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InitiateInsurancesRoutingModule {
}
