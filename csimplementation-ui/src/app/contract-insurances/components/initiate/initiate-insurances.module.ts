import { NgModule } from '@angular/core';
import { SharedModule } from '../../../shared/shared.module';

import { InitiateInsurancesRoutingModule } from './initiate-insurances-routing.module';
import { InitiateInsurancesComponent } from './initiate-insurances.component';

const components = [InitiateInsurancesComponent];

@NgModule({
  declarations: components,
  imports: [
    SharedModule,
    InitiateInsurancesRoutingModule
  ]
})
export class InitiateInsurancesModule {
}
