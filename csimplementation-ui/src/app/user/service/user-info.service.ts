import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class UserInfoService {
  protected baseUrl: string;

  public constructor (private httpClient: HttpClient) {
    const context = window.location.pathname.substr(0, window.location.pathname.lastIndexOf('/')).split('/')[1];
    this.baseUrl = window.location.origin + '/' + (context !== undefined ? context + '/' : '') + 'user';
  }

  public getUserTheme (): Observable<string> {
    return this.httpClient.get(this.baseUrl + '/theme', {responseType: 'text'});
  }
}
