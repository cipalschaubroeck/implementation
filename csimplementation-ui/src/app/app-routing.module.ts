import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { environment } from '../environments/environment';
// tslint:disable-next-line:max-line-length
import { DocumentComponent } from './contracts/management/administration/components/document-manager/components/detail/components/document/document.component';
import { DetailComponent } from './contracts/management/administration/components/document-manager/components/detail/detail.component';
import { DocumentManagerComponent } from './contracts/management/administration/components/document-manager/document-manager.component';
import { ContractManagementComponent } from './contracts/management/components/contract-management.component';
import { BudgetsComponent } from './contracts/management/initiate-contract/budgets/components/budgets/budgets.component';
import { ContractorComponent } from './contracts/management/initiate-contract/contractor/components/contractor/contractor.component';
// tslint:disable-next-line:max-line-length
import { ExecutionPeriodComponent } from './contracts/management/initiate-contract/execution-period/components/execution-period/execution-period.component';
import { InventoryComponent } from './contracts/management/initiate-contract/inventory/components/inventory/inventory.component';
// tslint:disable-next-line:max-line-length
import { PricesReviewComponent } from './contracts/management/initiate-contract/prices-review/components/prices-review/prices-review.component';
// tslint:disable-next-line:max-line-length
import { ValidateInitiationComponent } from './contracts/management/initiate-contract/validate-initiation/components/validate-initiation.component';
import { AccessOverviewComponent } from './contracts/overview/components/access-overview.component';
// tslint:disable-next-line:max-line-length
import { MenuCollaboratorsComponent } from './core/components/top-menu/collaborators/components/menu-collaborators/menu-collaborators.component';
// tslint:disable-next-line:max-line-length
import { MenuContractingAuthorityComponent } from './core/components/top-menu/contracting-authority/components/menu-contracting-authority/menu-contracting-authority.component';
import { RedirectToHomepageGuard } from './core/guards/redirect-to-homepage.guard';

const routes: Routes = [
  {
    path: 'home',
    component: AccessOverviewComponent,
    canActivate: [RedirectToHomepageGuard]
  },
  {
    path: 'authority',
    component: MenuContractingAuthorityComponent
  },
  {
    path: 'collaborators',
    component: MenuCollaboratorsComponent
  },
  {
    path: 'contract/:contractId',
    component: ContractManagementComponent,
    children: [
      {
        path: '',
        loadChildren: './contracts/data/components/initiate/initiate-data.module#InitiateDataModule'
      },
      {
        path: '',
        loadChildren: './contracts/data/components/admin/admin-data.module#AdminDataModule'
      },
      {
        path: '',
        loadChildren: './contract-authority-collaborator/initiate-authority-collaborator.module#InitiateAuthorityCollaboratorModule'
      },
      {
        path: '',
        loadChildren: './contract-authority-collaborator/admin-authority-collaborator.module#AdminAuthorityCollaboratorModule'
      },
      {
        path: '',
        loadChildren: './contracts/bail/components/initiate/initiate-bail.module#InitiateBailModule'
      },
      {
        path: '',
        loadChildren: './contracts/bail/components/admin/admin-bail.module#AdminBailModule'
      },
      {
        path: '',
        loadChildren: './contract-insurances/components/initiate/initiate-insurances.module#InitiateInsurancesModule'
      },
      {
        path: '',
        loadChildren: './contract-insurances/components/admin/admin-insurances.module#AdminInsurancesModule'
      },
      {
        path: 'contractor',
        component: ContractorComponent
      },
      {
        path: 'prices-review',
        component: PricesReviewComponent
      },
      {
        path: 'inventory',
        component: InventoryComponent
      },
      {
        path: 'budgets',
        component: BudgetsComponent
      },
      {
        path: 'execution-period',
        component: ExecutionPeriodComponent
      },
      {
        path: 'validate-initiation',
        component: ValidateInitiationComponent
      },
      {
        path: 'admin-documents',
        component: DocumentManagerComponent,
        children: [
          {
            path: 'process-detail/:processId',
            component: DetailComponent,
            children: [
              {
                path: 'document',
                component: DocumentComponent
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: '**', redirectTo: 'home'
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home', // redundant, but this is required for the route configuration to work
    canActivate: [RedirectToHomepageGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {enableTracing: environment.enableTracing} // <-- debugging purposes only
    )
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
