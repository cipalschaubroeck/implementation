import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-delete-confirmation',
  templateUrl: './delete-confirmation.component.html',
  styleUrls: ['./delete-confirmation.component.css']
})
export class DeleteConfirmationComponent implements OnInit {

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit () {
  }

  cancel () {
    this.ref.close();
  }
}
