import { Component, OnInit } from '@angular/core';
import { DynamicDialogConfig, DynamicDialogRef } from 'primeng/api';

@Component({
  selector: 'app-warning',
  templateUrl: './warning.component.html',
  styleUrls: ['./warning.component.css']
})
export class WarningComponent implements OnInit {

  constructor (public ref: DynamicDialogRef, public config: DynamicDialogConfig) {
  }

  ngOnInit () {
  }

  cancel () {
    this.ref.close();
  }
}
