import { AbstractControl, ValidatorFn } from '@angular/forms';

export function ValidateNumber (): ValidatorFn {
  const reg = /^\d*\.?\d*$/;
  return (control: AbstractControl): { [key: string]: any } | null => {
    const forbidden = !reg.test(control.value);
    return forbidden ? {'numeric': {value: control.value}} : null;
  };
}
