import { AbstractControl, AsyncValidatorFn, ValidationErrors } from '@angular/forms';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { InitiateContractDataInfoModel } from '../../contracts/data/models/contract-data-info';
import { ContractService } from '../../contracts/overview/services/contract.service';
import { AppInjector } from '../services/app-injector.service';

export function ValidateDuplicateName (contract: InitiateContractDataInfoModel): AsyncValidatorFn {
  return (control: AbstractControl): Promise<ValidationErrors | null> | Observable<ValidationErrors | null> => {
    const injector = AppInjector.getInjector();
    const service: ContractService = injector.get(ContractService);
    return service.validateContractName(control.value, contract).pipe(
      map(res => {
        return res ? null : {'duplicateName': {value: control.value}};
      })
    );
  };
}
