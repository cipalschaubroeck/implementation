import { FormGroup } from '@angular/forms';

export interface QueryParamModel {
  excluded?: boolean;
  key: string;
  value: string | boolean | number;
}

export class Utils {

  // Generates query params for url call based on params provided
  static buildQuery (params: QueryParamModel[]): string {
    const query = [];
    for (const param of params) {
      if (!param.excluded) {
        query.push(encodeURIComponent(param.key) + '=' + encodeURIComponent(String(param.value)));
      }
    }

    return '?' + query.join('&');
  }

  // Removes any query param or its description from the urls
  static cleanUrl (url: string): string {
    const index = url.indexOf('{');
    if (index !== -1) {
      return url.substring(0, index);
    }
    return url;
  }

  // checks is form field is valid
  static isValid (field: string, form: FormGroup) {
    return form.get(field).invalid && form.get(field).touched;
  }

  // checks is form field is valid or if its valid when tried to be submitted
  static isFieldValid (field: string, form: FormGroup, submitAttempt: boolean) {
    return form.get(field).invalid && (form.get(field).touched ||
      (form.get(field).untouched && form.get(field).invalid && submitAttempt));
  }
}
