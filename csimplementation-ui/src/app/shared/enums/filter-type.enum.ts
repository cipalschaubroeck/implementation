export enum FilterType {
  TEXT = 'text',
  DROPDOWN = 'dropdown',
  MULTI_SELECT = 'multiSelect',
  NONE = 'none'
}
