export enum InputTypeEnum {
  TEXT = 'TEXT',
  DROPDOWN = 'DROPDOWN',
  UPLOAD = 'UPLOAD'
}
