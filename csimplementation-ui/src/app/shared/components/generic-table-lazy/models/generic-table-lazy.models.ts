import { BehaviorSubject, Observable } from 'rxjs';
import { FilterType } from '../../../enums/filter-type.enum';

export interface GenericTableLazyButtonModel {
  disabled?: Observable<boolean>;
  title: string;
  icon: string;
  click: (...any) => any;
  scope: any;
}

export interface GenericTableLazyColumnModel {
  field: string;
  text: string;
  class: string;
  hasSort: boolean;
  filter?: {
    type: FilterType,
    data?: BehaviorSubject<GenericTableLazyFilterModel[]>,
    placeHolder?: string
  };
}

export interface GenericTableLazyFilterModel {
  label: string;
  value: string;
  disabled?: boolean;
  selected?: boolean;
}
export interface GenericTableLazyConfigModel {
  columns: GenericTableLazyColumnModel[];
  containerClass?: string;
  noResultsMessage: string;
  paginator?: GenericTablePaginatorModel;
  sortField?: string;
  sortOrder?: number;
  totalNumberRows: number;
}

export interface GenericTablePaginatorModel {
  rows: number;
  rowsPerPageOptions?: number[];
}

