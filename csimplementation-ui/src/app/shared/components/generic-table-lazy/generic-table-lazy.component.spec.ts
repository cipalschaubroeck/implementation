import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericTableLazyComponent } from './generic-table-lazy.component';

describe('GenericTableLazyComponent', () => {
  let component: GenericTableLazyComponent;
  let fixture: ComponentFixture<GenericTableLazyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [GenericTableLazyComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenericTableLazyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
