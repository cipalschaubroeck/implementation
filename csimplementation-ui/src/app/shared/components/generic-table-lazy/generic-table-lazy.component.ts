import { animate, style, transition, trigger } from '@angular/animations';
import { HttpParams } from '@angular/common/http';
import { Component, Input, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { Table } from 'primeng/table';

import { BehaviorSubject } from 'rxjs';
import { BaseHttpService } from '../../../core/services/base-http.service';
import { FilterType } from '../../enums/filter-type.enum';
import { AppInjector } from '../../services/app-injector.service';

import {
  GenericTableLazyButtonModel,
  GenericTableLazyColumnModel,
  GenericTableLazyConfigModel
} from './models/generic-table-lazy.models';

@Component({
  selector: 'app-generic-table-lazy',
  templateUrl: './generic-table-lazy.component.html',
  styleUrls: ['./generic-table-lazy.component.css'],
  animations: [
    trigger('slideInOut', [
      transition(':enter', [
        style({transform: 'translateY(-100%)'}),
        animate('0ms ease-in', style({transform: 'translateY(0%)'}))
      ]),
      transition(':leave', [
        animate('0ms ease-in', style({transform: 'translateY(-100%)'}))
      ])
    ])
  ]
})

export class GenericTableLazyComponent implements OnInit {

  selection: any;

  extraHttpParams: [any][any] = [];

  columns: GenericTableLazyColumnModel[];

  @Input()
  elements$: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  @Input()
  templateRef: TemplateRef<any>;

  @Input()
  captionTemplateRef: TemplateRef<any>;

  @ViewChild('genericLazyTable', { static: false }) table: Table;

  public filterType = FilterType;

  tableConfig: GenericTableLazyConfigModel;

  leftButtons: GenericTableLazyButtonModel[];
  rightButtons: GenericTableLazyButtonModel[];

  selectedRow: any;
  selectedRowBehaviorSubject: BehaviorSubject<any> = new BehaviorSubject(null);

  service: BaseHttpService<any>;

  translateService: TranslateService;

  filterVisible: boolean;
  filterValues: any[any] = [];

  static loadButton (index: string, icon: string, title: string, callback: () => void,
                     scope: any, disabled: boolean, buttons: GenericTableLazyButtonModel[]) {
    const button: GenericTableLazyButtonModel = {
      icon: icon,
      disabled: new BehaviorSubject(disabled),
      title: title,
      click: callback,
      scope: scope
    };
    return buttons[index] = button;
  }

  constructor () {
    const injector = AppInjector.getInjector();
    this.translateService = injector.get(TranslateService);
  }

  ngOnInit () {
  }

  /**
   * This is called when u perform ether sorting or filter action
   * @param $event Produced when lazy loading the table
   */
  onLazyLoad ($event) {
    let params = new HttpParams()
      .set('size', $event.rows.toString())
      .set('page', (($event.first / $event.rows)) + '');

    params = this.appendFilterParameters($event, params);

    if ($event.sortField !== null) {
      params = params.append('sort', $event.sortField + ($event.sortOrder === 1 ? ',asc' : ',desc'));
    }
    const keys = Object.keys(this.extraHttpParams);
    keys.forEach(key => {
      if (Array.isArray(this.extraHttpParams[key])) {
        const value: [] = this.extraHttpParams[key];
        /*
          key is deleted to free next petition
         */
        delete this.extraHttpParams[key];
        value.forEach(item => {
          params = params.append(key, item);
        });
      } else {
        params = params.append(key, this.extraHttpParams[key]);
        /*
          key is deleted to free next petition
         */
        delete this.extraHttpParams[key];
      }
    });

    this.service.getAllList(params).subscribe(behaviorSubject => {

      /**
       * sets the number of rows in table from the server response
       */
      this.tableConfig.totalNumberRows = behaviorSubject.page.totalNumber;

      /**
       * server response data
       */
      this.elements$.next(behaviorSubject.data);
    }, error => {
      console.log(error);
      if (this.elements$) {
        this.elements$.next(null);
      }

    });
  }

  /**
   * Shows or hide table filter
   */
  showFilters () {
    this.filterVisible = !this.filterVisible;
  }

  /**
   * Clears all the table filters
   */
  clearFilters () {
    if (this.columns) {
      this.columns.forEach(column => {
        this.filterValues[column.field] = [];
      });
      this.table.filter('', '', '');
    }
  }

  filterTable (value, field, matchMode) {
    this.table.filter(value, field, matchMode);
  }

  /**
   * Prepares the params to be sent to server for filtering
   * @param $event produced when filtering
   * @param params HttpParams to be sent to server
   */
  appendFilterParameters ($event, params: HttpParams): HttpParams {
    const keys = Object.keys(this.filterValues);
    keys.forEach(key => {
      if (this.filterValues[key]) {
        params = params.append(key, this.filterValues[key]);
      }
    });
    return params;
  }

  /**
   * Inits the table buttons.
   */
  initButtons (rightButtons: GenericTableLazyButtonModel[], leftButtons: GenericTableLazyButtonModel[]) {
    this.rightButtons = rightButtons;
    this.leftButtons = leftButtons;
  }

  /**
   * Initiates service to get request from server.
   * Has to be initiated from parent component where table is created
   * @param configModel object to build table initially
   * @param service that has to extend BaseHttpService
   */
  initTableConfig (configModel: GenericTableLazyConfigModel, service: BaseHttpService<any>) {
    this.columns = configModel.columns;
    this.service = service;
    this.tableConfig = configModel;
  }

  /**
   * @param $event produced when filters change value
   * @param co the column to map in an array the value of the event with the column name
   * This value is ngModeled to the filter input
   */
  prepareFilterReset ($event, co) {
    this.filterValues[co] = $event;
  }

  onRowSelection (event) {
    this.selectedRow = event.data;
    this.selectedRowBehaviorSubject.next(event.data);
  }

  onRowUnSelection () {
    this.selectedRow = null;
    this.selectedRowBehaviorSubject.next(null);
  }
}

