import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericTreeService {

  /*
  TODO CSPROC-2007 this service is not necessary. TreeComponent may configure its own path through a base url (given by it parent)
  and the activatedRoute, while contract-data is already configuring itself with activatedRoute
   */

  private path: string;

  setPath (path: string) {
    this.path = path;
  }

  getPath (): string {
    return this.path;
  }

}
