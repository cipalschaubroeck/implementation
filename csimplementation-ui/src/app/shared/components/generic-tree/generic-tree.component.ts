import { ViewEncapsulation } from '@angular/cli/lib/config/schema';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Message, MessageService, TreeNode } from 'primeng/api';
import { DataTreeModel } from './model/generic-tree.models';

@Component({
  selector: 'app-generic-tree',
  templateUrl: './generic-tree.component.html',
  styleUrls: ['./generic-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class GenericTreeComponent implements OnInit {

  constructor (
    private messageService: MessageService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  @Input()
  protected data: DataTreeModel[];
  @Input()
  private hasButtons: boolean;
  private selectedFile: any = {};
  private msg: Message[] = [];

  private expandAll () {
    if (this.data) {
      this.data.forEach(node => {
        this.expandRecursive(node, true);
      });
    }
  }

  private collapseAll () {
    if (this.data) {
      this.data.forEach(node => {
        this.expandRecursive(node, false);
      });
    }
  }

  private expandRecursive (node: TreeNode, isExpand: boolean) {
    node.expanded = isExpand;
    if (node.children) {
      node.children.forEach(childNode => {
        this.expandRecursive(childNode, isExpand);
      });
    }
  }

  private nodeSelect (event) {
    this.router.navigate([event.node.path], {relativeTo: this.route}).then(e => {
      if (!e) {
        console.log('Navigation has failed!', e);
      }
    });
  }

  private hasButton (): boolean {
    return this.hasButtons;
  }

  ngOnInit () {
    this.expandAll();

    const url = this.router.url.split('/');

    if (this.data && this.data[0] && this.data[0]['children'] && this.data[0]['children'] && url && url[url.length - 1]) {
      this.selectedFile = this.data[0]['children'].find(option => option.path === url[url.length - 1]);
    }
  }
}
