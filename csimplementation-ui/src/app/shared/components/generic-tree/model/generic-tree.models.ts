import { TreeNode } from 'primeng/api';

export interface DataTreeModel extends TreeNode {
  path: string;
  click?: (...any) => any;
  children?: DataTreeModel[];
  statusIcon?: string;
}
