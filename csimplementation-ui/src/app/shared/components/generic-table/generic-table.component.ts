import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

import { Table } from 'primeng/table';

import { BehaviorSubject } from 'rxjs';

import {
  GenericTableButtonModel,
  GenericTableColumnModel,
  GenericTableConfigModel
} from './models/generic-table.models';

@Component({
  selector: 'app-generic-table',
  templateUrl: './generic-table.component.html',
  styleUrls: ['./generic-table.component.css']
})
export class GenericTableComponent {

  @Input()
  selectedRowBehaviorSubject: BehaviorSubject<any> = new BehaviorSubject(null);

  @Input()
  buttons: GenericTableButtonModel[];

  @Input()
  tableConfig: GenericTableConfigModel;

  @Input()
  columns: GenericTableColumnModel[];

  @Input()
  elements$: BehaviorSubject<any[]>;

  @Input()
  selected$: BehaviorSubject<any>;

  @Input()
  templateRef: TemplateRef<any>;

  @Input()
  captionTemplateRef: TemplateRef<any>;

  @ViewChild('genericTable', { static: false }) table: Table;

  private selection: any;

  constructor () {
  }

  private onRowSelection (event): any {
    this.selectedRowBehaviorSubject.next(event.data);
  }

  onRowUnSelection (): any {
    this.selectedRowBehaviorSubject.next(null);
  }

  hasButtons () {
    return this.buttons && this.buttons.length > 0;
  }

  filter (value, field, matchMode) {
    this.table.filter(value, field, matchMode);
  }

  setSelection (selection: any) {
    this.selection = selection;
  }

}
