import { BehaviorSubject } from 'rxjs';

export interface GenericTableButtonModel {
  disabled?: BehaviorSubject<boolean>;
  title?: string;
  icon: string;
  click: (...any) => any;
  scope?: any;
}

export interface GenericTableColumnModel {
  field: string;
  text: string;
  class?: string;
  hasSort: boolean;
}

export interface GenericTableConfigModel {
  containerClass?: string;
  noResultsMessage?: string;
  paginator?: GenericTablePaginatorModel;
  sortField?: string;
  sortOrder?: number;
  hasSearchFilter?: boolean;
  selectionMode?: string;
}

export interface GenericTablePaginatorModel {
  rows: number;
    rowsPerPageOptions?: number[];
}

