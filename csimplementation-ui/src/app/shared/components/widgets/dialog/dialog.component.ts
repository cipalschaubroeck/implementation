import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})
export class DialogComponent implements OnInit {

  constructor () {
  }

  @Input()
  triggerDialog: boolean;
  @Output()
  triggerDialogChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Input()
  bodyRef: TemplateRef<any>;

  @Input()
  headerText: string;

  ngOnInit () {

  }

  /**
   * Returns to parent component the triggerDialog to
   * close the dialog
   */
  cancel () {
    this.triggerDialogChange.emit(this.triggerDialog);
  }

}
