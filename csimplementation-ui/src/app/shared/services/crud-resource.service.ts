import { Injectable } from '@angular/core';
import { BaseHttpService } from '../../core/services/base-http.service';

@Injectable({
  providedIn: 'root'
})
export class CrudResourceService extends BaseHttpService<any> {

  private create (resource, body) {
    return this.httpClient.post(this.endpoint + resource, body);
  }

  private patch (resource, body) {
    return this.httpClient.patch(resource, body);
  }

  createResource (resource: string, body): Promise<any> {
    return new Promise(response => {
      this.create(resource, body).subscribe(data => {
        response(data);
      }, error => {
        this.messageService.error();
      });
    });
  }

  patchResource (resource: string, body) {
    return new Promise(response => {
      this.patch(resource, body).subscribe(data => {
        response(data);
      }, error => {
        this.messageService.error();
      });
    });
  }
}
