import { TestBed } from '@angular/core/testing';

import { CrudResourceService } from './crud-resource.service';

describe('CrudResourceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CrudResourceService = TestBed.get(CrudResourceService);
    expect(service).toBeTruthy();
  });
});
