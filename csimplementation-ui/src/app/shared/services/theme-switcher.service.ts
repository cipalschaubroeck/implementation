export class ThemeSwitcher {

  private static changeLinkTo (id: string, href: string) {
    let link: HTMLLinkElement = document.getElementById(id) as HTMLLinkElement;
    if (!(link)) {
      link = document.createElement('link') as HTMLLinkElement;
      link.id = id;
      link.rel = 'stylesheet';
      link.type = 'text/css';
      document.head.appendChild(link);
    }
    link.href = href;
  }

  /**
   * changes the theme. Requires that index.html has two link tags with ids theme-css and layout-css;
   * it will change their href to theme-<param>.css and layout-<param>.css
   *
   * @param theme if null or empty, won't do anything
   */
  public static changeTheme (theme) {
    if (theme) {
      ThemeSwitcher.changeLinkTo('theme-css', 'assets/theme/theme-' + theme + '.css');
      ThemeSwitcher.changeLinkTo('layout-css', 'assets/layout/css/layout-' + theme + '.css');
    }
  }

}
