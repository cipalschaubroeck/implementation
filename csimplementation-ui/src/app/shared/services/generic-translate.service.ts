import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { AppInjector } from './app-injector.service';

@Injectable({
  providedIn: 'root'
})
export class GenericTranslateService {

  translateService: TranslateService;

  constructor () {
    const injector = AppInjector.getInjector();
    this.translateService = injector.get(TranslateService);
  }

  translate (key: string): string {
    let translated = '';
    const subscription: Subscription = new Subscription();
    subscription.add(this.translateService.get(key).subscribe(val => {
      translated = val;
      subscription.unsubscribe();
    }));
    return translated;
  }
}
