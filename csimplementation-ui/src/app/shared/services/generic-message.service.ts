import { Injectable } from '@angular/core';
import { MessageService } from 'primeng/api';
import { GenericTranslateService } from './generic-translate.service';

@Injectable({
  providedIn: 'root'
})
export class GenericMessageService {

  constructor (
    private messageService: MessageService,
    private translate: GenericTranslateService) {
  }

  popMessage (severity: string, summary: string, detail: string) {
    this.messageService.add({
      severity: severity,
      summary: this.translate.translate(summary),
      detail: this.translate.translate(detail)
    });
  }

  success () {
    this.popMessage('success', 'MESSAGE_SERVICE.success', 'MESSAGE_SERVICE.updated');
  }

  error () {
    this.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.notUpdated');
  }

  validated () {
    this.popMessage('success', 'MESSAGE_SERVICE.success', 'MESSAGE_SERVICE.validated');
  }

  notValidated () {
    this.popMessage('error', 'MESSAGE_SERVICE.warning', 'MESSAGE_SERVICE.notValidated');
  }

  mustSelect () {
    this.popMessage('error', 'MESSAGE_SERVICE.error', 'MESSAGE_SERVICE.rowMustBeSelected');
  }
}
