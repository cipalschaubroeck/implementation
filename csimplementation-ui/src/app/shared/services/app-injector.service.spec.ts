import { TestBed } from '@angular/core/testing';

import { AppInjector } from './app-injector.service';

describe('AppInjectorService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AppInjector = TestBed.get(AppInjector);
    expect(service).toBeTruthy();
  });
});
