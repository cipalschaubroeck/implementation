export interface PageModel {
  data: any;
  page: {
    totalNumber: number;
  };
}
