# RECKON PLUGIN FOR VERSIONING
Reckon plugin has been implemented to facilitate the versioning of the application when releases are released.
The plugin is based on the tags created in git to get the latest version published and generate the next one.
The complete documentation can be found at: https://github.com/ajoberstar/reckon

## CONFIGURATION
In the "build.gradle" file, three stages of application development have been defined through the "stageFromProp" method provided by the plugin:
* **milestone**: intermediate versions for development.
* **rc**: Release candidate.
* **final**: Final version for production.

## USAGE
The version number is based on the standard https://semver.org/
Therefore a version consists of:
* **Number MAJOR** version when you make incompatible API changes,
* **Number MINOR** version when you add functionality in a backwards-compatible manner, and
* **Number PATCH** version when you make backwards-compatible bug fixes.

The final format of the first version of a product in "final" stage would be 1.0.0.

The plugin runs automatically each time Gradle's build function is executed. If parameters are specified: 
* **reckon.scope** (one of major, minor, or patch (defaults to minor) to specify which component of the previous release should be incremented)
* **reckon.stage** (one of the values passed to stageFromProp (defaults to the first alphabetically) to specify what phase of development you are in).

So, to upload the minor part of the milestone version, just run it:
`$ ./gradlew build -Preckon.scope=minor -Preckon.stage=milestone`
The generated archifacts will have the following name: <archifact-id>-0.1.0-milestone.1
More examples of use in: https://github.com/ajoberstar/reckon/blob/master/docs/index.md

## PUBLISH
In addition, the plugin offers several gradle tasks that serve to generate the version and publish it in Git. They are:
* **"reckonTagCreate"**. Create a tag with the new version in the local repository.
* **"reckonTagPush"** (includes the previous one) Pushes to Git the tag generated in the previous step.

So, to generate a final version and create the tag in GIT just run: 
`$ ./gradlew build reckonTagPush -Preckon.scope=major -Preckon.stage=final`

