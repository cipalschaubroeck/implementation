package com.cipalschaubroeck.csimplementation.bail.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Entity
@Table(name = "bail_request_guarantee")
@Getter
@Setter
public class BailRequestGuarantee extends TemplateData {
    @ManyToOne(optional = false)
    @JoinColumns(foreignKey = @ForeignKey(name = "fk_bail_request_guarantee_collaborator"),
            value = {@JoinColumn(name = "collaborator", referencedColumnName = "person")})
    @NotNull
    private Collaborator collaborator;

    @ManyToOne(optional = false)
    @JoinColumn(name = "phone", foreignKey = @ForeignKey(name = "fk_bail_request_guarantee_phone"))
    private PhoneNumber phone;

    @Column(name = "reference")
    private String reference;

    @Column(name = "bail_amount")
    private BigDecimal bailAmount;

    @ManyToOne
    @JoinColumn(name = "department", foreignKey = @ForeignKey(name = "fk_bail_request_guarantee_department"))
    private Department department;

    @Override
    public TemplateType getType() {
        return TemplateType.BAIL_REQUEST_GUARANTEE;
    }
}
