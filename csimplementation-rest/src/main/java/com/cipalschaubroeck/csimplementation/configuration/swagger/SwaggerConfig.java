package com.cipalschaubroeck.csimplementation.configuration.swagger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.http.HttpMethod;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.data.rest.EntityServicesProvider8;
import springfox.documentation.spring.data.rest.ExtractorConfiguration8;
import springfox.documentation.spring.data.rest.SpringDataRestConfiguration8;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

@Configuration
@EnableSwagger2
@Import(SpringDataRestConfiguration8.class)
@PropertySource("classpath:swagger.properties")
public class SwaggerConfig {

    @Value("${spring.data.rest.base-path}")
    private String dataRestBaseUri;

    @Bean
    public Docket docket() {
        ApiInfo info = new ApiInfo("CS Implementation", "Endpoints for CS Implementation",
                null, null, new Contact(null, null, null),
                null, null, Collections.emptyList());
        return new Docket(DocumentationType.SWAGGER_2)
                .useDefaultResponseMessages(false)
                .ignoredParameterTypes(HttpMethod.class, PersistentEntityResourceAssembler.class)
                .select()
                .paths(PathSelectors.regex(dataRestBaseUri + "/.*"))
                .build()
                .apiInfo(info);
    }

    @Bean
    @Primary
    public EntityServicesProvider8 entityServicesProvider() {
        return new EntityServicesProvider8();
    }

    @Bean
    public ExtractorConfiguration8 extractorConfiguration8() {
        return new ExtractorConfiguration8();
    }

    @Bean
    public RepositoryRestControllerHandlerMapping dataRestControllerHandlerMapping() {
        // TODO: with springfox-data-rest 3.0 this may or may not be still needed
        // the need is because springfox-data-rest ignores handlers annotated with RepositoryRestController
        return new RepositoryRestControllerHandlerMapping();
    }
}
