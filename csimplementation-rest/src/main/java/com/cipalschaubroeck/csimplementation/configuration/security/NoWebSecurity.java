package com.cipalschaubroeck.csimplementation.configuration.security;

import org.keycloak.adapters.springboot.KeycloakAutoConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * needed to activate security only when desired
 */
@Configuration
@Profile("!keycloak")
@EnableAutoConfiguration(exclude = {SecurityAutoConfiguration.class, KeycloakAutoConfiguration.class})
public class NoWebSecurity {
}
