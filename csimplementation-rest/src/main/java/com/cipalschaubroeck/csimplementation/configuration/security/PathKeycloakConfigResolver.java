package com.cipalschaubroeck.csimplementation.configuration.security;

import org.keycloak.adapters.KeycloakConfigResolver;
import org.keycloak.adapters.KeycloakDeployment;
import org.keycloak.adapters.KeycloakDeploymentBuilder;
import org.keycloak.adapters.spi.HttpFacade;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Profile("multitenant")
// used by spring
@SuppressWarnings("unused")
public class PathKeycloakConfigResolver implements KeycloakConfigResolver {
    private final Map<String, KeycloakDeployment> cache = new ConcurrentHashMap<>();

    @Override
    public KeycloakDeployment resolve(HttpFacade.Request request) {
        String path = request.getURI();

        int point = path.indexOf('.');
        int start = request.getURI().indexOf("://") + 3;
        int slash = path.indexOf('/', start);
        if (point < 0 || (slash > 0 && point > slash)) {
            throw new IllegalArgumentException("No subdomain specified");
        }
        String realm = path.substring(start, point);
        KeycloakDeployment deployment = cache.get(realm);
        if (null == deployment) {
            // not found on the simple cache, try to load it from the file system
            String fileName = "/keycloak/" + realm + "-keycloak.json";
            InputStream is = getClass().getResourceAsStream(fileName);
            if (is == null) {
                throw new IllegalStateException("Not able to find the file " + fileName);
            }
            deployment = KeycloakDeploymentBuilder.build(is);
            cache.put(realm, deployment);
        }

        return deployment;
    }
}
