package com.cipalschaubroeck.csimplementation.configuration;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@ControllerAdvice
// used by spring
@SuppressWarnings("unused")
public class ValidationConstraintExceptionHandler {

    private final MessageSourceAccessor messageSourceAccessor;

    public ValidationConstraintExceptionHandler(MessageSource messageSource) {
        Assert.notNull(messageSource, "MessageSource must not be null!");
        this.messageSourceAccessor = new MessageSourceAccessor(messageSource);
    }

    // used by spring exception handling
    @SuppressWarnings("unused")
    @ExceptionHandler
    ResponseEntity<ConstraintViolationErrors> handleRepositoryConstraintViolationException(
            ConstraintViolationException exception, Locale locale) {
        if (exception.getConstraintViolations().isEmpty()) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        } else {
            List<ConstraintViolationError> errors = exception.getConstraintViolations().stream().map(constraintViolation ->
                    new ConstraintViolationError(
                            messageSourceAccessor.getMessage(constraintViolation.getMessage(), constraintViolation.getMessage(), locale),
                            constraintViolation.getRootBeanClass().getName())
            ).collect(Collectors.toList());

            return new ResponseEntity<>(
                    new ConstraintViolationErrors(errors),
                    new HttpHeaders(), HttpStatus.BAD_REQUEST);
        }
    }

    @RequiredArgsConstructor
    @Getter
    // accessed by jackson, can't be private
    @SuppressWarnings("WeakerAccess")
    static class ConstraintViolationErrors {
        private final List<ConstraintViolationError> errors;
    }

    @RequiredArgsConstructor
    @Getter
    // accessed by jackson, can't be private
    @SuppressWarnings("WeakerAccess")
    static class ConstraintViolationError {
        private final String message;
        private final String objectName;
    }

}
