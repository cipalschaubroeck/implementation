package com.cipalschaubroeck.csimplementation.configuration;

import com.cipalschaubroeck.csimplementation.tenant.service.TenantSettingService;
import com.greenvalley.contacts.client.ContactsInterfacedServiceSupport;
import com.greenvalley.contacts.client.OrganizationInterfacedService;
import com.greenvalley.contacts.client.PersonInOrganizationInterfacedService;
import com.greenvalley.contacts.client.PersonInterfacedService;
import com.greenvalley.contacts.service.OrganizationService;
import com.greenvalley.contacts.service.PersonInOrganizationService;
import com.greenvalley.contacts.service.PersonService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

@Configuration
@Profile("contacts")
public class ContactsInterfaceConfiguration {
    @Value("${contacts.url}")
    private String contactsUrl;


    @Bean
    public ContactsInterfacedServiceSupport contactsInterfaceSupport(TenantSettingService tenantSettingService) {
        ContactsInterfacedServiceSupport support = new ContactsInterfacedServiceSupport();

        // TODO CSPROC-2195 check that tenant settings are set when initialising application
        support.setTenantHashValue(tenantSettingService::getHashForTenant);
        return support;
    }

    @Bean
    public OrganizationService organizationInterfacedService() {
        OrganizationInterfacedService service = new OrganizationInterfacedService();
        service.setServiceUrl(contactsUrl + "/OrganizationService");
        return service;
    }

    @Bean
    public PersonService personInterfacedService() {
        PersonInterfacedService service = new PersonInterfacedService();
        service.setServiceUrl(contactsUrl + "/PersonService");
        return service;
    }

    @Bean
    public PersonInOrganizationService personInOrganizationService() {
        PersonInOrganizationInterfacedService service = new PersonInOrganizationInterfacedService();
        service.setServiceUrl(contactsUrl + "/QualificationService");
        return service;
    }
}
