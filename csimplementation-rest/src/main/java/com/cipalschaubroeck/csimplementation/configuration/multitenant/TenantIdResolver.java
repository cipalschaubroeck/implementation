package com.cipalschaubroeck.csimplementation.configuration.multitenant;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.keycloak.KeycloakPrincipal;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
@Profile("multitenant")
// used by spring depending on profile
@SuppressWarnings("unused")
public class TenantIdResolver implements CurrentTenantIdentifierResolver {
    @Value("${multitenant.default-tenant-id}")
    private String defaultTenantId;

    @Override
    public String resolveCurrentTenantIdentifier() {
        String tenantId = defaultTenantId;
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null && authentication.getPrincipal() instanceof KeycloakPrincipal) {
            tenantId = ((KeycloakPrincipal) authentication.getPrincipal()).getKeycloakSecurityContext().getRealm();
        }
        return tenantId;
    }

    @Override
    public boolean validateExistingCurrentSessions() {
        return true;
    }
}
