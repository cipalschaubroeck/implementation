package com.cipalschaubroeck.csimplementation.configuration.multitenant;

import com.cipalschaubroeck.csimplementation.CsImplementationApplication;
import com.greenvalley.common.multitenancy.hibernate.ConnectionProviderImpl;
import com.greenvalley.common.multitenancy.hibernate.MySQLMultiTenantConnectionProviderImpl;
import liquibase.integration.spring.MultiTenantSpringLiquibase;
import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseProperties;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Configuration
@Profile("multitenant")
@EnableAutoConfiguration(exclude = {LiquibaseAutoConfiguration.class})
@EnableConfigurationProperties({DataSourceProperties.class,
        LiquibaseProperties.class})
public class MultiTenancyConfig {

    @Value("${liquibase.schemas}")
    private List<String> schemas;
    @Value("${multitenant.default-tenant-id}")
    private String defaultTenant;
    @Value("${multitenant.schema-prefix}")
    private String schemaPrefix;

    @Autowired
    private LiquibaseProperties liquibaseProperties;
    @Autowired
    private JpaProperties jpaProperties;

    @Bean
    public JpaVendorAdapter jpaVendorAdapter() {
        return new HibernateJpaVendorAdapter();
    }

    @Bean
    public MultiTenantConnectionProvider mySQLMultiTenantConnectionProvider(DataSource dataSource) {
        ConnectionProviderImpl connectionProvider = new ConnectionProviderImpl();
        connectionProvider.setDataSource(dataSource);

        MySQLMultiTenantConnectionProviderImpl mySQLMultiTenantConnectionProvider = new MySQLMultiTenantConnectionProviderImpl();
        mySQLMultiTenantConnectionProvider.setConnectionProvider(connectionProvider);
        mySQLMultiTenantConnectionProvider.setTenantIdentifierForAny(defaultTenant);
        mySQLMultiTenantConnectionProvider.setSchemaPrefix(schemaPrefix);
        return mySQLMultiTenantConnectionProvider;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
                                                                       MultiTenantConnectionProvider multiTenantConnectionProviderImpl,
                                                                       CurrentTenantIdentifierResolver currentTenantIdentifierResolverImpl) {
        Map<String, Object> properties = new HashMap<>(jpaProperties.getProperties());
        properties.put(Environment.MULTI_TENANT, MultiTenancyStrategy.SCHEMA);
        properties.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProviderImpl);
        properties.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolverImpl);
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan(CsImplementationApplication.class.getPackage().getName());
        em.setJpaVendorAdapter(jpaVendorAdapter());
        em.setJpaPropertyMap(properties);
        return em;
    }

    @Bean
    public MultiTenantSpringLiquibase liquibase(DataSource dataSource) {
        MultiTenantSpringLiquibase liquibase = new MultiTenantSpringLiquibase();
        liquibase.setDataSource(dataSource);
        liquibase.setSchemas(schemas);

        liquibase.setChangeLog(this.liquibaseProperties.getChangeLog());
        liquibase.setContexts(this.liquibaseProperties.getContexts());
        liquibase.setDefaultSchema(this.liquibaseProperties.getDefaultSchema());
        liquibase.setLiquibaseSchema(this.liquibaseProperties.getLiquibaseSchema());
        liquibase.setLiquibaseTablespace(this.liquibaseProperties.getLiquibaseTablespace());
        liquibase.setDatabaseChangeLogTable(
                this.liquibaseProperties.getDatabaseChangeLogTable());
        liquibase.setDatabaseChangeLogLockTable(
                this.liquibaseProperties.getDatabaseChangeLogLockTable());
        liquibase.setDropFirst(this.liquibaseProperties.isDropFirst());
        liquibase.setShouldRun(this.liquibaseProperties.isEnabled());
        liquibase.setLabels(this.liquibaseProperties.getLabels());
        liquibase.setParameters(this.liquibaseProperties.getParameters());
        liquibase.setRollbackFile(this.liquibaseProperties.getRollbackFile());
        return liquibase;
    }
}
