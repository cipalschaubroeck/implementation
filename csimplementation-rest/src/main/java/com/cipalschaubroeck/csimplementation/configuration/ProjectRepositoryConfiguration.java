package com.cipalschaubroeck.csimplementation.configuration;

import com.cipalschaubroeck.csimplementation.project.domain.validator.QualifiedPersonValidator;
import com.cipalschaubroeck.csimplementation.template.domain.validator.BailRequestGuaranteeValidator;
import com.cipalschaubroeck.csimplementation.template.domain.validator.TemplateConfigValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class ProjectRepositoryConfiguration implements RepositoryRestConfigurer {

    private static final String BEFORE_SAVE = "beforeSave";
    private static final String BEFORE_CREATE = "beforeCreate";

    @Autowired
    private QualifiedPersonValidator qualifiedPersonValidator;
    @Autowired
    private TemplateConfigValidator templateConfigValidator;
    @Autowired
    private BailRequestGuaranteeValidator bailRequestGuaranteeValidator;

    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator(BEFORE_SAVE, qualifiedPersonValidator);
        validatingListener.addValidator(BEFORE_CREATE, qualifiedPersonValidator);
        validatingListener.addValidator(BEFORE_SAVE, templateConfigValidator);
        validatingListener.addValidator(BEFORE_CREATE, templateConfigValidator);
        validatingListener.addValidator(BEFORE_SAVE, bailRequestGuaranteeValidator);
        validatingListener.addValidator(BEFORE_CREATE, bailRequestGuaranteeValidator);
    }
}
