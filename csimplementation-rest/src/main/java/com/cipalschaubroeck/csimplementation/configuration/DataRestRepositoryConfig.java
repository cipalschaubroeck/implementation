package com.cipalschaubroeck.csimplementation.configuration;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.AuthorityCollaboratorDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.AuthorityDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.CollaboratorOverviewDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractAuthorityCollaboratorQFDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractAuthorityCollaboratorsDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractingAuthorityOverviewDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.InitiateContractAuthorityDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.QualificationFunctionDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.AuthorityRelatedPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.ContractRelatedPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.ContractorRelatedPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.InsuredPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.InsuredRelatedPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.RelatedPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.ReminderDocumentsProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.RemindersInsuredPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.RemindersProjection;
import com.cipalschaubroeck.csimplementation.document.controller.projections.DocumentProcessProjection;
import com.cipalschaubroeck.csimplementation.document.controller.projections.GeneratedDocumentProjection;
import com.cipalschaubroeck.csimplementation.document.controller.projections.PostalAddresseeProjection;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.project.controller.ContractOverviewDto;
import com.cipalschaubroeck.csimplementation.project.controller.InitiateContractDataDto;
import com.cipalschaubroeck.csimplementation.project.controller.InitiateContractFormulaProjection;
import com.cipalschaubroeck.csimplementation.project.controller.projections.ContractLinkProjection;
import com.cipalschaubroeck.csimplementation.project.controller.projections.InitiateContractBailProjection;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.shared.config.BiCompositeIdConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.support.ConfigurableConversionService;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;
import org.springframework.hateoas.mvc.HateoasBackendIdHack;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;

import java.io.Serializable;
import java.util.List;

@Configuration
public class DataRestRepositoryConfig implements RepositoryRestConfigurer {

    @Autowired
    private List<BiCompositeIdConverter> biCompositeIdConverters;

    @Override
    @SuppressWarnings("unchecked")
    public void configureConversionService(ConfigurableConversionService conversionService) {
        ConfigurableConversionService controllerLinkBuilderConversionService;
        try {
            controllerLinkBuilderConversionService =
                    HateoasBackendIdHack.getControllerLinkBuilderConversionService();
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
        biCompositeIdConverters.forEach(converter -> {
                    addBiCompositeIdConverters(conversionService, converter);
                    addBiCompositeIdConverters(controllerLinkBuilderConversionService, converter);
                }
        );
    }

    private <T extends Serializable> void addBiCompositeIdConverters(ConfigurableConversionService conversionService,
                                                                     BiCompositeIdConverter<T> converter) {
        conversionService.addConverter(converter.getIdClass(), String.class, converter.fromIdConverter());
        conversionService.addConverter(String.class, converter.getIdClass(), converter.fromStringConverter());
    }

    @Override
    public void configureHttpMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        messageConverters.add(new StringHttpMessageConverter());
        messageConverters.add(new ResourceHttpMessageConverter());
    }

    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        // add here projections not in the model's packages
        config.getProjectionConfiguration().addProjection(PostalAddresseeProjection.class);
        config.getProjectionConfiguration().addProjection(GeneratedDocumentProjection.class);
        config.getProjectionConfiguration().addProjection(ContractOverviewDto.class);
        config.getProjectionConfiguration().addProjection(DocumentProcessProjection.class);
        config.getProjectionConfiguration().addProjection(ContractingAuthorityOverviewDto.class);
        config.getProjectionConfiguration().addProjection(CollaboratorOverviewDto.class);
        config.getProjectionConfiguration().addProjection(InitiateContractDataDto.class);
        config.getProjectionConfiguration().addProjection(InitiateContractBailProjection.class);
        config.getProjectionConfiguration().addProjection(InitiateContractAuthorityDto.class);
        config.getProjectionConfiguration().addProjection(AuthorityCollaboratorDto.class);
        config.getProjectionConfiguration().addProjection(ContractAuthorityCollaboratorsDto.class);
        config.getProjectionConfiguration().addProjection(QualificationFunctionDto.class);
        config.getProjectionConfiguration().addProjection(ContractAuthorityCollaboratorQFDto.class);
        config.getProjectionConfiguration().addProjection(AuthorityDto.class);
        config.getProjectionConfiguration().addProjection(InitiateContractFormulaProjection.class);
        config.getProjectionConfiguration().addProjection(ContractLinkProjection.class);
        config.getProjectionConfiguration().addProjection(InsuredPartyProjection.class);
        config.getProjectionConfiguration().addProjection(ContractRelatedPartyProjection.class);
        config.getProjectionConfiguration().addProjection(AuthorityRelatedPartyProjection.class);
        config.getProjectionConfiguration().addProjection(ContractorRelatedPartyProjection.class);
        config.getProjectionConfiguration().addProjection(RelatedPartyProjection.class);
        config.getProjectionConfiguration().addProjection(InsuredRelatedPartyProjection.class);
        config.getProjectionConfiguration().addProjection(RemindersProjection.class);
        config.getProjectionConfiguration().addProjection(DocumentProcessProjection.class);
        config.getProjectionConfiguration().addProjection(RemindersInsuredPartyProjection.class);
        config.getProjectionConfiguration().addProjection(ReminderDocumentsProjection.class);
        // TODO CSPROC-2070 check if this is needed, considering processors
        config.exposeIdsFor(Contract.class, DocumentProcess.class);
    }
}
