package com.cipalschaubroeck.csimplementation;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;
import springfox.documentation.spring.data.rest.SpringDataRestConfiguration8;

@SpringBootApplication
@Import(SpringDataRestConfiguration8.class)
@Configuration
@EnableAspectJAutoProxy
@EnableSpringConfigured
public class CsImplementationApplication {

    public static void main(String[] args) {
        SpringApplication.run(CsImplementationApplication.class, args);
    }

}

