//
// This file was generate by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.04.11 at 12:00:38 PM CEST 
//


package com.cipalschaubroeck.csimplementation.xperido.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 *
 * <p>The following schema fragment specifies the expected content contained within this class.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="qualification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="competence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="first-name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="last-name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "signing-officer")
public class SigningOfficer {

    @XmlElement(required = true)
    protected String qualification;
    @XmlElement(required = true)
    protected String competence;
    @XmlElement(name = "first-name", required = true)
    protected String firstName;
    @XmlElement(name = "last-name", required = true)
    protected String lastName;

    /**
     * Gets the value of the qualification property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getQualification() {
        return qualification;
    }

    /**
     * Sets the value of the qualification property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setQualification(String value) {
        this.qualification = value;
    }

    /**
     * Gets the value of the competence property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getCompetence() {
        return competence;
    }

    /**
     * Sets the value of the competence property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setCompetence(String value) {
        this.competence = value;
    }

    /**
     * Gets the value of the firstName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the lastName property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

}
