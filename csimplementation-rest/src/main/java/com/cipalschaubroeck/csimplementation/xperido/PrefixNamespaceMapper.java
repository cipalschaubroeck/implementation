package com.cipalschaubroeck.csimplementation.xperido;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class PrefixNamespaceMapper extends NamespacePrefixMapper {

    @Override
    public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
        switch (namespaceUri) {
            case "http://www.greenvalley.com":
                return "domain";
            case "http://www.greenvalley.com/document":
                return "common";

            default:
                if (namespaceUri.toLowerCase().contains("http://www.greenvalley.com/document/")) {
                    return namespaceUri.replace("http://www.greenvalley.com/document/", "");
                } else {
                    return suggestion;
                }
        }
    }

}
