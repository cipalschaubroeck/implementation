package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Component
@Profile("!xperido")
// configuration
@SuppressWarnings("unused")
public class MockXPeridoService implements XPeridoService {
    @Override
    public List<Template> getTemplates(Locale locale, TemplateType type, boolean includeThumbnails) {
        return Collections.emptyList();
    }

    @Override
    public Optional<Template> getTemplate(String name, TemplateType type) {
        return Optional.empty();
    }

    @Override
    public Document createDocument(PostalAddressee addressee, TemplateData data, Locale locale, OutputType fileType, String fileName) throws JAXBException {
        return null;
    }

    @Override
    public Document createDocument(PostalAddressee addressee, Locale locale, OutputType fileType, String fileName) throws IOException {
        return null;
    }

    @Override
    public Document createDocument(PostalDocumentProcess process, TemplateData data, Locale locale, OutputType fileType, String filaName) throws JAXBException {
        return null;
    }
}
