//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-558 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.04.30 at 10:46:56 AM CEST 
//


package com.cipalschaubroeck.csimplementation.xperido.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="client">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="contracting-authority" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="contact-name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="contact-department" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="contact-phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="project">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;all>
 *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="awarding-date" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="decision-making-body" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="amount-total" type="{}money"/>
 *                   &lt;element name="amount-minus-vat" type="{}money"/>
 *                   &lt;element name="amount-vat" type="{}money"/>
 *                   &lt;element name="amount-bail" type="{}money"/>
 *                 &lt;/all>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "bail-request-guarantee")
public class BailRequestGuarantee {

    @XmlElement(required = true)
    protected BailRequestGuarantee.Client client;
    @XmlElement(required = true)
    protected BailRequestGuarantee.Project project;

    /**
     * Gets the value of the client property.
     * 
     * @return
     *     possible object is
     *     {@link BailRequestGuarantee.Client }
     *     
     */
    public BailRequestGuarantee.Client getClient() {
        return client;
    }

    /**
     * Sets the value of the client property.
     * 
     * @param value
     *     allowed object is
     *     {@link BailRequestGuarantee.Client }
     *     
     */
    public void setClient(BailRequestGuarantee.Client value) {
        this.client = value;
    }

    /**
     * Gets the value of the project property.
     * 
     * @return
     *     possible object is
     *     {@link BailRequestGuarantee.Project }
     *     
     */
    public BailRequestGuarantee.Project getProject() {
        return project;
    }

    /**
     * Sets the value of the project property.
     * 
     * @param value
     *     allowed object is
     *     {@link BailRequestGuarantee.Project }
     *     
     */
    public void setProject(BailRequestGuarantee.Project value) {
        this.project = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="contracting-authority" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="contact-name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="contact-department" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="contact-phone" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Client {

        @XmlElement(name = "contracting-authority", required = true)
        protected String contractingAuthority;
        @XmlElement(name = "contact-name", required = true)
        protected String contactName;
        @XmlElement(name = "contact-department", required = true)
        protected String contactDepartment;
        @XmlElement(name = "contact-phone", required = true)
        protected String contactPhone;

        /**
         * Gets the value of the contractingAuthority property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContractingAuthority() {
            return contractingAuthority;
        }

        /**
         * Sets the value of the contractingAuthority property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContractingAuthority(String value) {
            this.contractingAuthority = value;
        }

        /**
         * Gets the value of the contactName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactName() {
            return contactName;
        }

        /**
         * Sets the value of the contactName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactName(String value) {
            this.contactName = value;
        }

        /**
         * Gets the value of the contactDepartment property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactDepartment() {
            return contactDepartment;
        }

        /**
         * Sets the value of the contactDepartment property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactDepartment(String value) {
            this.contactDepartment = value;
        }

        /**
         * Gets the value of the contactPhone property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getContactPhone() {
            return contactPhone;
        }

        /**
         * Sets the value of the contactPhone property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setContactPhone(String value) {
            this.contactPhone = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;all>
     *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="awarding-date" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="decision-making-body" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="reference" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="amount-total" type="{}money"/>
     *         &lt;element name="amount-minus-vat" type="{}money"/>
     *         &lt;element name="amount-vat" type="{}money"/>
     *         &lt;element name="amount-bail" type="{}money"/>
     *       &lt;/all>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {

    })
    public static class Project {

        @XmlElement(required = true)
        protected String name;
        @XmlElement(name = "awarding-date", required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar awardingDate;
        @XmlElement(name = "decision-making-body", required = true)
        protected String decisionMakingBody;
        @XmlElement(required = true)
        protected String reference;
        @XmlElement(name = "amount-total", required = true)
        protected BigDecimal amountTotal;
        @XmlElement(name = "amount-minus-vat", required = true)
        protected BigDecimal amountMinusVat;
        @XmlElement(name = "amount-vat", required = true)
        protected BigDecimal amountVat;
        @XmlElement(name = "amount-bail", required = true)
        protected BigDecimal amountBail;

        /**
         * Gets the value of the name property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Sets the value of the name property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Gets the value of the awardingDate property.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getAwardingDate() {
            return awardingDate;
        }

        /**
         * Sets the value of the awardingDate property.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setAwardingDate(XMLGregorianCalendar value) {
            this.awardingDate = value;
        }

        /**
         * Gets the value of the decisionMakingBody property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDecisionMakingBody() {
            return decisionMakingBody;
        }

        /**
         * Sets the value of the decisionMakingBody property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDecisionMakingBody(String value) {
            this.decisionMakingBody = value;
        }

        /**
         * Gets the value of the reference property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getReference() {
            return reference;
        }

        /**
         * Sets the value of the reference property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setReference(String value) {
            this.reference = value;
        }

        /**
         * Gets the value of the amountTotal property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmountTotal() {
            return amountTotal;
        }

        /**
         * Sets the value of the amountTotal property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmountTotal(BigDecimal value) {
            this.amountTotal = value;
        }

        /**
         * Gets the value of the amountMinusVat property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmountMinusVat() {
            return amountMinusVat;
        }

        /**
         * Sets the value of the amountMinusVat property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmountMinusVat(BigDecimal value) {
            this.amountMinusVat = value;
        }

        /**
         * Gets the value of the amountVat property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmountVat() {
            return amountVat;
        }

        /**
         * Sets the value of the amountVat property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmountVat(BigDecimal value) {
            this.amountVat = value;
        }

        /**
         * Gets the value of the amountBail property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmountBail() {
            return amountBail;
        }

        /**
         * Sets the value of the amountBail property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmountBail(BigDecimal value) {
            this.amountBail = value;
        }

    }

}
