package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityImageSettingsRepository;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateFormat;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.xperido.generated.CombinedTemplate;
import com.greenvalley.common.utils.threadlocal.ThreadLocalContext;
import com.greenvalley.docgen.client.impl.Constants;
import com.greenvalley.docgen.client.impl.rest.RESTImpl;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.DynamicField;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;
import com.greenvalley.docgen.domain.expression.Predicate;
import com.greenvalley.docgen.domain.expression.Query;
import com.greenvalley.docgen.service.api.DocumentService;
import org.apache.commons.lang3.StringUtils;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

@Component
@Profile("xperido")
public class DefaultXPeridoService implements XPeridoService {
    private static final String DOC_URL_FORMAT = "http://www.greenvalley.com/document/%1$s %1$s.xsd";
    private static final String TENANT_HASH_NAME = "tenanthash";
    private static final String TENANT_HASH_VALUE = "dmpqewdiqw0dqwde12";
    private static final String APPLICATION = "Application";
    private static final String APPLICATION_VALUE = "CSImplementation";
    private static final String IMG_FORMAT = "<img src=\"data:%s;base64, %s\" alt=\"%s\" />";

    @Autowired
    private List<TemplateDataXmlTransformer> xmlTransformers;
    @Autowired
    private ContractingAuthorityImageSettingsRepository caisRepository;
    @Autowired
    private CMCommunicationService contentManager;

    @Autowired
    @RESTImpl
    private DocumentService service;

    @Value("${docgen.url}")
    private String docgenUrl;

    @Value("${docgen.combined-template-name}")
    private String combinedTemplateName;

    @Override
    public Document createDocument(PostalAddressee addressee, Locale locale, OutputType fileType, String fileName) throws IOException {
        Template combined = getTemplate(combinedTemplateName, null).orElseThrow(EntityNotFoundException::new);
        CombinedTemplate combinedData = buildCombinedTemplateData(addressee, locale);
        Optional<ContractingAuthorityImageSettings> optImages = caisRepository.findById(
                addressee.getProcess().getContract().getContractAuthorityRelation().getContractingAuthority().getId());
        if (optImages.isPresent()) {
            imageIdToHtmlBytes(optImages.get().getLogoId()).ifPresent(combinedData::setLogo);
            imageIdToHtmlBytes(optImages.get().getRibbonId()).ifPresent(combinedData::setRibbon);
        }
        return doCreateDocument(combined, fileName, locale, combinedData, fileType);
    }

    @Override
    public Document createDocument(PostalDocumentProcess process, TemplateData data, Locale locale, OutputType fileType, String fileName) throws JAXBException {
        if (data.getType().needsAddressee()) {
            throw new IllegalArgumentException("An addressee should have been selected for this template");
        }
        String xml = getXml(transformToXmlReady(process, data), data.getType().getSchemaName());
        return mergeTemplate(data, locale, fileType, fileName, xml);
    }

    private Document mergeTemplate(TemplateData data, Locale locale, OutputType fileType, String fileName, String xml) {
        Template template = getTemplate(data.getConfig().getTemplateName(), data.getType()).orElseThrow(EntityNotFoundException::new);
        List<DynamicField> list = new ArrayList<>();
        return service.createDocument(template, fileName, locale.toString().toUpperCase(), xml, list, fileType);
    }

    private CombinedTemplate buildCombinedTemplateData(PostalAddressee addressee, Locale locale) throws IOException {
        CombinedTemplate combinedData = new CombinedTemplate();
        try {
            combineStep(addressee, addressee.getHeading(), locale, combinedData::setHeading, combinedData::setHeadingHtml);
            combineStep(addressee, addressee.getProcess().getBody(), locale, combinedData::setBody, combinedData::setBodyHtml);
            combineStep(addressee, addressee.getProcess().getSigningOfficerData(), locale, combinedData::setSigningOfficers, combinedData::setSigningOfficersHtml);
        } catch (JAXBException e) {
            throw new IOException(e);
        }
        return combinedData;
    }

    private Document doCreateDocument(Template combined, String fileName, Locale locale, CombinedTemplate combinedData, OutputType fileType) throws IOException {
        try {
            return service.createDocument(combined, fileName, locale.getLanguage().toUpperCase(),
                    getXml(combinedData, "generate-document.xsd"), new ArrayList<>(), fileType);
        } catch (JAXBException e) {
            throw new IOException(e);
        }
    }

    private Optional<byte[]> imageIdToHtmlBytes(String id) throws IOException {
        if (StringUtils.isNotBlank(id)) {
            org.apache.chemistry.opencmis.client.api.Document image = contentManager.getDocumentById(id);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            IOUtils.copy(image.getContentStream().getStream(), baos);
            return Optional.of(String.format(IMG_FORMAT,
                    image.getContentStream().getMimeType(),
                    new String(Base64.encode(baos.toByteArray())),
                    image.getContentStream().getFileName())
                    .getBytes());
        } else {
            return Optional.empty();
        }
    }

    private void combineStep(PostalAddressee addressee, TemplateData data, Locale locale,
                             Consumer<byte[]> byteConsumer, Consumer<Boolean> setIsHtml) throws JAXBException {
        if (data.getConfig().getTemplateFormat() == TemplateFormat.TEMPLATE) {
            setIsHtml.accept(Boolean.FALSE);
            byteConsumer.accept(createDocument(addressee, data, locale, OutputType.DOCX, "part.docx").getData());
        } else {
            setIsHtml.accept(Boolean.TRUE);
            byteConsumer.accept(data.getConfig().getText().getBytes());
        }
    }

    @Override
    public Document createDocument(PostalAddressee addressee, TemplateData data, Locale locale, OutputType fileType, String fileName) throws JAXBException {
        String xml = getXml(transformToXmlReady(addressee, data), data.getType().getSchemaName());
        return mergeTemplate(data, locale, fileType, fileName, xml);
    }

    private String getXml(Object xmlReady, String schemaName) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(xmlReady.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, String.format(DOC_URL_FORMAT, schemaName));
        marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new PrefixNamespaceMapper());
        StringWriter xml = new StringWriter();
        marshaller.marshal(xmlReady, xml);
        return xml.toString();
    }

    private Object transformToXmlReady(PostalDocumentProcess process, TemplateData data) {
        return transformToXmlReady(data, transformer -> transformer.transform(process, data));
    }

    private Object transformToXmlReady(PostalAddressee addressee, TemplateData data) {
        return transformToXmlReady(data, transformer -> transformer.transform(addressee, data));
    }

    private Object transformToXmlReady(TemplateData data, Function<TemplateDataXmlTransformer, Object> whenFound) {
        for (TemplateDataXmlTransformer transformer : xmlTransformers) {
            if (transformer.supports(data.getClass())) {
                return whenFound.apply(transformer);
            }
        }
        return data;
    }

    /**
     * @param locale locale to get the language from
     * @param type   type of template that may be merged with the resulting elements
     * @return list of templates matching the parameters
     */
    @Override
    public List<Template> getTemplates(Locale locale, TemplateType type, boolean includeThumbnails) {
        ThreadLocalContext.INSTANCE.add(Constants.KEY_DOCGEN_URL, docgenUrl);
        // TODO CSPROC-1757 this should be changed to the current tenant when we are multitenant
        ThreadLocalContext.INSTANCE.add(TENANT_HASH_NAME, TENANT_HASH_VALUE);
        Query query = Query.query(Predicate.and(
                Predicate.isEqual("Language", locale.getLanguage().toUpperCase()),
                Predicate.isEqual(APPLICATION, APPLICATION_VALUE),
                Predicate.isEqual("Type", type.name())
        ));
        return service.getTemplates(query, includeThumbnails);
    }

    @Override
    public Optional<Template> getTemplate(String name, @Nullable TemplateType type) {
        ThreadLocalContext.INSTANCE.add(Constants.KEY_DOCGEN_URL, docgenUrl);
        // TODO CSPROC-1757 this should be changed to the current tenant when we are multitenant
        ThreadLocalContext.INSTANCE.add(TENANT_HASH_NAME, TENANT_HASH_VALUE);
        Predicate predicate = Predicate.isEqual(APPLICATION, APPLICATION_VALUE);
        if (type != null) {
            predicate = Predicate.and(predicate, Predicate.isEqual("Type", type.name()));
        }
        Query query = Query.query(predicate);
        return service.getTemplates(query).stream()
                .filter(template -> StringUtils.equals(name, template.getName()))
                .findFirst();
    }
}
