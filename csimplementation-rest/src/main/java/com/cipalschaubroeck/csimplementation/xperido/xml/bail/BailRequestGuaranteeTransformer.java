package com.cipalschaubroeck.csimplementation.xperido.xml.bail;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.xperido.TemplateDataXmlTransformer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.xml.datatype.DatatypeConfigurationException;

@Component
@Slf4j
public class BailRequestGuaranteeTransformer implements TemplateDataXmlTransformer.JustProcess {
    @Override
    public boolean supports(Class<?> dataClass) {
        return BailRequestGuarantee.class.isAssignableFrom(dataClass);
    }

    @Override
    public Object transform(PostalDocumentProcess process, TemplateData data) {
        return doTransform(process, (BailRequestGuarantee) data);
    }

    private com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee
    doTransform(PostalDocumentProcess process, BailRequestGuarantee data) {
        com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee transformed
                = new com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee();

        transformed.setClient(new com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee.Client());
        transformed.getClient().setContractingAuthority(data.getCollaborator().getAuthority().getName());
        transformed.getClient().setContactName(data.getCollaborator().getEffectivePerson().getFullName());
        if (data.getDepartment() != null) {
            transformed.getClient().setContactDepartment(data.getDepartment().getName());
        }
        if (data.getPhone() != null) {
            transformed.getClient().setContactPhone(data.getPhone().getPhone());
        }

        transformed.setProject(new com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee.Project());
        transformed.getProject().setName(process.getContract().getName());
        transformed.getProject().setReference(data.getReference());
        transformed.getProject().setDecisionMakingBody(process.getContract().getDecisionMakingBody());
        try {
            transformed.getProject().setAwardingDate(TemplateDataXmlTransformer.transform(process.getContract().getAwardingDate()));
        } catch (DatatypeConfigurationException e) {
            log.error("could not instantiate DataTypeFactory", e);
        }
        transformed.getProject().setAmountBail(data.getBailAmount());
        transformed.getProject().setAmountMinusVat(process.getContract().getAmountMinusVat());
        transformed.getProject().setAmountVat(process.getContract().getAmountVat());
        transformed.getProject().setAmountTotal(transformed.getProject().getAmountMinusVat()
                .add(transformed.getProject().getAmountVat()));
        return transformed;
    }
}
