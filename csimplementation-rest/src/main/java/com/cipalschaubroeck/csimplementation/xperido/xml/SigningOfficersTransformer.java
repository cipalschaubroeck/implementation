package com.cipalschaubroeck.csimplementation.xperido.xml;

import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.xperido.TemplateDataXmlTransformer;
import com.cipalschaubroeck.csimplementation.xperido.generated.SigningOfficers;
import org.springframework.stereotype.Component;

@Component
public class SigningOfficersTransformer implements TemplateDataXmlTransformer.JustProcess {
    @Override
    public boolean supports(Class<?> dataClass) {
        return SigningOfficerData.class.isAssignableFrom(dataClass);
    }

    @Override
    public Object transform(PostalDocumentProcess process, TemplateData data) {
        SigningOfficers transformed = new SigningOfficers();
        ((SigningOfficerData) data).getSigningOfficers().stream()
                .map(this::doTransform)
                .forEach(transformed.getSigningOfficer()::add);
        return transformed;
    }

    private SigningOfficers.SigningOfficer doTransform(SigningOfficer officer) {
        SigningOfficers.SigningOfficer transformed = new SigningOfficers.SigningOfficer();
        transformed.setCompetence(officer.getPerson().getCompetence());
        transformed.setQualification(officer.getPerson().getQualification());
        transformed.setFirstName(officer.getPerson().getEffectivePerson().getFirstName());
        transformed.setLastName(officer.getPerson().getEffectivePerson().getLastName());
        return transformed;
    }
}
