package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public interface XPeridoService {

    /**
     * @param locale           locale to get the language from
     * @param type             type of template that may be merged with the resulting elements
     * @param includeTemplates true to include the base64 content of thumbnail images
     * @return list of templates matching the parameters
     */
    List<Template> getTemplates(Locale locale, TemplateType type, boolean includeTemplates);

    Optional<Template> getTemplate(String name, TemplateType type);

    Document createDocument(PostalAddressee addressee, TemplateData data, Locale locale, OutputType fileType, String fileName) throws JAXBException;

    Document createDocument(PostalAddressee addressee, Locale locale, OutputType fileType, String fileName) throws IOException;

    Document createDocument(PostalDocumentProcess process, TemplateData data, Locale locale, OutputType fileType, String filaName) throws JAXBException;
}
