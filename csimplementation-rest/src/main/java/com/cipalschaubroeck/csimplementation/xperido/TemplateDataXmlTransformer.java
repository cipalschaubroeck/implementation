package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.GregorianCalendar;

public interface TemplateDataXmlTransformer {
    boolean supports(Class<?> dataClass);

    Object transform(PostalAddressee addressee, TemplateData data);

    Object transform(PostalDocumentProcess process, TemplateData data);

    static XMLGregorianCalendar transform(LocalDate date) throws DatatypeConfigurationException {
        GregorianCalendar gcal = GregorianCalendar.from(date.atStartOfDay(ZoneId.systemDefault()));
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gcal);
    }

    interface JustProcess extends TemplateDataXmlTransformer {
        default Object transform(PostalAddressee addressee, TemplateData data) {
            return transform(addressee.getProcess(), data);
        }
    }

    interface NeedsAddressee extends TemplateDataXmlTransformer {
        default Object transform(PostalDocumentProcess process, TemplateData data) {
            throw new UnsupportedOperationException("This template needs addressee info");
        }
    }
}
