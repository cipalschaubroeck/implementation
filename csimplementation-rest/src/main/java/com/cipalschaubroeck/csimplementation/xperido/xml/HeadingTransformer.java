package com.cipalschaubroeck.csimplementation.xperido.xml;

import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.xperido.TemplateDataXmlTransformer;
import org.springframework.stereotype.Component;

@Component
public class HeadingTransformer implements TemplateDataXmlTransformer.NeedsAddressee {
    @Override
    public boolean supports(Class<?> dataClass) {
        return Heading.class.isAssignableFrom(dataClass);
    }

    @Override
    public Object transform(PostalAddressee addressee, TemplateData data) {
        return doTransform((Heading) data);
    }

    private Object doTransform(Heading data) {
        com.cipalschaubroeck.csimplementation.xperido.generated.Heading transformed
                = new com.cipalschaubroeck.csimplementation.xperido.generated.Heading();
        transformed.setOurProperty(data.getOurProperty());
        transformed.setYourProperty(data.getYourProperty());
        transformed.setSalutation(data.getSalutation());
        transformed.setSubject(data.getSubject());
        transformed.setRegisteredLetter(data.isRegisteredLetter());
        transformed.setNumberAttachments(data.getNumberAttachments());
        transformed.setFrom(data.getFrom());
        return transformed;
    }
}
