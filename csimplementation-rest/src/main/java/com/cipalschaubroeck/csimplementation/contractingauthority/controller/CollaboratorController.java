package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.dtos.CollaboratorAssignmentDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Qualification;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.CollaboratorService;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.CollaboratorAssignmentProcessDto;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class CollaboratorController {

    @Autowired
    private ControllerSupport controllerSupport;

    @Autowired
    private CollaboratorService collaboratorService;

    @PostMapping("collaborator/{id}/assignment")
    public ResponseEntity<Void> saveUpdate(@PathVariable("id") Integer collaboratorId,
                                           @RequestBody CollaboratorAssignmentDto dto) {

        CollaboratorAssignmentProcessDto info = new CollaboratorAssignmentProcessDto();

        info.setFunction(dto.getFunction());

        if (dto.getQualification() != null) {
            info.setQualification((Integer) controllerSupport.linkToEntityInfo(dto.getQualification(),
                    Collections.singletonList(Qualification.class))
                    .getId());
        }

        if (dto.getContractingAuthority() != null) {
            info.setContractingAuthority((Integer) controllerSupport.linkToEntityInfo(dto.getContractingAuthority(),
                    Collections.singletonList(ContractingAuthority.class))
                    .getId());
        }

        if (dto.getDepartment() != null) {
            info.setDepartment((Integer) controllerSupport.linkToEntityInfo(dto.getDepartment(),
                    Collections.singletonList(Department.class))
                    .getId());
        }

        collaboratorService.saveUpdateCollaborator(collaboratorId, info);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
