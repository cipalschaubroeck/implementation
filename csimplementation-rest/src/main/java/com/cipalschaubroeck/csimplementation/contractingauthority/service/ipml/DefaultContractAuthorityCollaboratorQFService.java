package com.cipalschaubroeck.csimplementation.contractingauthority.service.ipml;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.CollaboratorRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractAuthorityCollaboratorQFRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.QualificationFunctionRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.ContractAuthorityCollaboratorQFService;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.ContractAuthorityCollaboratorQFAssignmentProcessDto;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
public class DefaultContractAuthorityCollaboratorQFService implements ContractAuthorityCollaboratorQFService {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private QualificationFunctionRepository qualificationFunctionRepository;

    @Autowired
    private ContractAuthorityCollaboratorQFRepository repository;

    @Autowired
    private CollaboratorRepository collaboratorRepository;

    @Override
    @Transactional
    public void saveUpdateCollaboratorAssignmentToContract(Integer contractId, ContractAuthorityCollaboratorQFAssignmentProcessDto dto) {

        Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);

        if (dto.getQualificationFunctionId() == null) {
            // TODO CSPROC-2196 Create Exception handler for entity not found Exception
            throw new EntityNotFoundException("Collaborator assigned to contract passed from view is null");
        }

        QualificationFunction qualificationFunction = qualificationFunctionRepository.findById(dto.getQualificationFunctionId())
                .orElseThrow(EntityNotFoundException::new);

        ContractAuthorityCollaboratorQF contractCollaborator;
        if (dto.getContractCollaboratorId() != null) {
            contractCollaborator = repository
                    .findById(dto.getContractCollaboratorId()).orElseThrow(EntityNotFoundException::new);
        } else {
            contractCollaborator = new ContractAuthorityCollaboratorQF();
        }

        if (dto.getCollaboratorId() != null) {
            Collaborator col = collaboratorRepository.findById(dto.getCollaboratorId()).orElseThrow(EntityNotFoundException::new);
            qualificationFunction.getAuthorityCollaborator().setCollaborator(col);
        }

        contractCollaborator.setContract(contract);
        contractCollaborator.setQualificationFunction(qualificationFunction);
        if (dto.getStartDate() != null) {
            contractCollaborator.setStartDate(dto.getStartDate());
        }
        contractCollaborator.setCompetence(dto.getCompetence());
        repository.save(contractCollaborator);
    }

    @Override
    @Transactional
    public void deleteContractCollaborator(Integer id) {
        ContractAuthorityCollaboratorQF c = repository.findById(id).orElseThrow(EntityNotFoundException::new);
        c.getContract().setMainAuthorityCollaboratorContact(null);
        repository.delete(c);
    }
}
