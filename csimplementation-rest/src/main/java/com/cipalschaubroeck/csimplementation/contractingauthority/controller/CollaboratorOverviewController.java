package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.CollaboratorOverviewDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications.CollaboratorOverviewSpecification;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.CollaboratorRepository;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class CollaboratorOverviewController {
    @Autowired
    private CollaboratorRepository repository;

    @Autowired
    private ControllerSupport controllerSupport;

    @GetMapping("collaborator/overview")
    public ResponseEntity<PagedResources> search(
            CollaboratorOverviewSpecification specification,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            PersistentEntityResourceAssembler assembler) {
        Page<Collaborator> page = repository.findAll(specification,
                ControllerSupport.getPageConfig(pageable, sort, CollaboratorOverviewDto.Constants.SORT_ALIASES));
        return controllerSupport.toResources(page, Collaborator.class, assembler);
    }
}
