package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "collaborator")
public interface CollaboratorRepository extends PagingAndSortingRepository<Collaborator, Integer>,
        JpaSpecificationExecutor<Collaborator> {
}
