package com.cipalschaubroeck.csimplementation.contractingauthority.service;

import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.ContractAuthorityCollaboratorQFAssignmentProcessDto;

public interface ContractAuthorityCollaboratorQFService {
    void saveUpdateCollaboratorAssignmentToContract(Integer contractId, ContractAuthorityCollaboratorQFAssignmentProcessDto dto);
    void deleteContractCollaborator(Integer id);
}
