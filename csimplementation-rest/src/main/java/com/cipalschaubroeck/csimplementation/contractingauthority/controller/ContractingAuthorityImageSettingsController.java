package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.service.AuthorityImage;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.ContractingAuthorityImageSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URLConnection;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class ContractingAuthorityImageSettingsController {

    @Autowired
    private ContractingAuthorityImageSettingsService service;

    // TODO CSPROC-2105 this may interfere with some resource end points such as /contracting-authority/12/email
    @RequestMapping(value = "/contracting-authority/{id}/{image}",
            method = {RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
    public void setImage(@PathVariable("id") Integer authorityId,
                         @PathVariable("image") AuthorityImage imageType,
                         MultipartFile file) throws IOException {
        service.setImage(imageType, authorityId, file.getOriginalFilename(), file.getBytes(), getMime(file));
    }

    private String getMime(MultipartFile file) {
        String mime = file.getContentType();
        if (MediaType.APPLICATION_OCTET_STREAM_VALUE.equalsIgnoreCase(mime)) {
            mime = URLConnection.getFileNameMap().getContentTypeFor(file.getOriginalFilename());
        }
        return mime;
    }
}
