package com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class ContractAuthorityCollaboratorQFAssignmentProcessDto {
    private Integer qualificationFunctionId;
    private Integer collaboratorId;
    private LocalDate startDate;
    private String competence;
    private Integer contractCollaboratorId;
    private Integer contractId;
}
