package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "qualification_function")
@Getter
@Setter
public class QualificationFunction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "authority_collaborator", foreignKey = @ForeignKey(name = "fk_authority_collaborator_qualification"))
    @NotNull
    private AuthorityCollaborator authorityCollaborator;

    @ManyToOne
    @JoinColumn(name = "qualification", foreignKey = @ForeignKey(name = "fk_collaborator_qualification"))
    @NotNull
    private Qualification qualification;

    @Column(name = "function")
    @NotNull
    private String function;

    @OneToMany(mappedBy = "qualificationFunction")
    List<ContractAuthorityCollaboratorQF> collaborators = new ArrayList<>();

}
