package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.AuthorityCollaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "authority-collaborator")
public interface AuthorityCollaboratorRepository extends PagingAndSortingRepository<AuthorityCollaborator, Integer>,
        JpaSpecificationExecutor<AuthorityCollaborator> {
    Optional<AuthorityCollaborator> findAuthorityCollaboratorByAuthorityAndCollaboratorAndDepartmentIsNull(ContractingAuthority authority, Collaborator collaborator);

    Optional<AuthorityCollaborator> findAuthorityCollaboratorByAuthorityAndCollaboratorAndDepartment(ContractingAuthority authority, Collaborator collaborator, Department department);
}
