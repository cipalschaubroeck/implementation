package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "contracting-authority")
public interface ContractingAuthorityRepository extends PagingAndSortingRepository<ContractingAuthority, Integer>,
        JpaSpecificationExecutor<ContractingAuthority> {
}
