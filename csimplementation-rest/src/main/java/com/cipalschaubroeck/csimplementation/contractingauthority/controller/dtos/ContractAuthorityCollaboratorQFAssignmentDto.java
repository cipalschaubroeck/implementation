package com.cipalschaubroeck.csimplementation.contractingauthority.controller.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

import java.time.LocalDate;

@Getter
@Setter
public class ContractAuthorityCollaboratorQFAssignmentDto {
    private Link qualificationFunction;
    private Link collaborator;
    private LocalDate startDate;
    private String competence;
    private Link contractCollaborator;
}
