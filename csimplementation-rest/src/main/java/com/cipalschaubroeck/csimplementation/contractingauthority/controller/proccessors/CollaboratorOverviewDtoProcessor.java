package com.cipalschaubroeck.csimplementation.contractingauthority.controller.proccessors;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.CollaboratorOverviewDto;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// spring resource processor
@SuppressWarnings("unused")
public class CollaboratorOverviewDtoProcessor implements ResourceProcessor<Resource<CollaboratorOverviewDto>> {

    @Autowired
    private EntityLinks links;

    @Override
    public Resource<CollaboratorOverviewDto> process(Resource<CollaboratorOverviewDto> resource) {
        Person person = resource.getContent().getPerson();
        if (person != null) {
            resource.getLinks().add(links.linkToSingleResource(Person.class, person.getId()).withRel("personLink"));
        }
        return resource;
    }
}
