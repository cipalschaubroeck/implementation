package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.AuthorityCollaborator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "authorityCollaboratorInfo", types = AuthorityCollaborator.class)
public interface AuthorityCollaboratorDto {

    // used by jackson
    @SuppressWarnings("unused")
    List<QualificationFunctionDto> getQualificationFunction();

    @Value("#{target.collaborator.person.lastName}")
    String getLastName();

    @Value("#{target.collaborator.person.firstName}")
    String getFirstName();

    @Value("#{target.department?.name}")
    String getDepartment();
}