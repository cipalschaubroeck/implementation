package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.dtos.ContractAuthorityCollaboratorQFAssignmentDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.ContractAuthorityCollaboratorQFService;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.ContractAuthorityCollaboratorQFAssignmentProcessDto;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class ContractAuthorityCollaboratorQFController {

    @Autowired
    private ControllerSupport controllerSupport;

    @Autowired
    private ContractAuthorityCollaboratorQFService service;

    /*
     * Assign's existing collaborator that belongs to a contracting authority to an existing contract
     * @param contractId
     * @param dto with link of the collaborator (with function, qualification, department or not and the contracting authority that it belongs)
     * @return
     */
    @PostMapping("contract/{id}/authority-collaborator-q-f/assignment")
    public ResponseEntity<Void> saveUpdate(@PathVariable("id") Integer contractId,
                                           @RequestBody ContractAuthorityCollaboratorQFAssignmentDto dto) {

        ContractAuthorityCollaboratorQFAssignmentProcessDto info = new ContractAuthorityCollaboratorQFAssignmentProcessDto();

        if (dto.getQualificationFunction() != null) {
            info.setQualificationFunctionId((Integer) controllerSupport.linkToEntityInfo(dto.getQualificationFunction(),
                    Collections.singletonList(QualificationFunction.class))
                    .getId());
        }

        if (dto.getCollaborator() != null) {
            info.setCollaboratorId((Integer) controllerSupport.linkToEntityInfo(dto.getCollaborator(),
                    Collections.singletonList(Collaborator.class))
                    .getId());
        }

        if(dto.getContractCollaborator() != null){
            info.setContractCollaboratorId((Integer) controllerSupport.linkToEntityInfo(dto.getContractCollaborator(),
                    Collections.singletonList(ContractAuthorityCollaboratorQF.class))
                    .getId());
        }

        info.setCompetence(dto.getCompetence());

        service.saveUpdateCollaboratorAssignmentToContract(contractId, info);

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @DeleteMapping("/contract-authority-collaborator-q-f/{id}")
    public ResponseEntity<Void> delete(@PathVariable("id") Integer id) {

        this.service.deleteContractCollaborator(id);

        /*
        TODO CSPROC_2267 QUESTION: what happens if deleteContractCollaborator method fail.
        I believe would be interesting handle response in deleteContractCollaborator method
        */
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
