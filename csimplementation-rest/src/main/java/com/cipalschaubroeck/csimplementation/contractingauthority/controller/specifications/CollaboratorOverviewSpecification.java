package com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.cipalschaubroeck.csimplementation.shared.controller.SpecificationSupport.stringContainsIgnoreCase;

@Setter
@Getter
public class CollaboratorOverviewSpecification implements Specification<Collaborator> {

    private static final String PERSON = "person";

    private String name;
    private String function;
    private String email;
    private String phone;
    private String contractingAuthority;
    private String department;
    private String qualification;

    @Override
    public Predicate toPredicate(Root<Collaborator> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        stringContainsIgnoreCase(root.get(PERSON).get("firstName"), name, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(PERSON).get("email").get("email"), email, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(PERSON).get("phone").get("phone"), phone, builder).ifPresent(predicates::add);
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
