package com.cipalschaubroeck.csimplementation.contractingauthority.service;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityImageSettingsRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import org.apache.chemistry.opencmis.client.api.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.Optional;

@Service
@Transactional
// spring service
@SuppressWarnings("unused")
public class DefaultCaisService implements ContractingAuthorityImageSettingsService {
    @Autowired
    private ContractingAuthorityRepository authorityRepository;
    @Autowired
    private ContractingAuthorityImageSettingsRepository repository;
    @Autowired
    private CMCommunicationService contentManager;

    @Override
    public void setImage(AuthorityImage imageType, Integer authorityId, String fileName, byte[] content, String mime) {
        Optional<ContractingAuthorityImageSettings> settings = repository.findById(authorityId);
        ContractingAuthorityImageSettings images;
        if (settings.isPresent()) {
            images = settings.get();
        } else {
            ContractingAuthority authority = authorityRepository.findById(authorityId)
                    .orElseThrow(EntityNotFoundException::new);
            images = new ContractingAuthorityImageSettings();
            images.setAuthority(authority);
        }
        Document uploaded = contentManager.uploadFile(fileName, content,
                Arrays.asList("contracting-authority", authorityId.toString(), imageType.name()),
                mime);
        imageType.getIdSetter().accept(images, uploaded.getId());
        repository.save(images);
    }

}
