package com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.cipalschaubroeck.csimplementation.shared.controller.SpecificationSupport.stringContainsIgnoreCase;

@Setter
@Getter
public class ContractingAuthorityOverviewSpecification implements Specification<ContractingAuthority> {

    private String authority;
    private String address;
    private String number;
    private String postalCode;
    private String municipality;
    private String nationalId;

    private static final String ADDRESS_CONSTANT = "address";
    private static final String NAME_CONSTANT = "name";
    private static final String NATIONAL_ID_CONSTANT = "nationalId";

    @Override
    public Predicate toPredicate(Root<ContractingAuthority> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();
        stringContainsIgnoreCase(root.get(NAME_CONSTANT), authority, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(ADDRESS_CONSTANT).get("street"), address, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(ADDRESS_CONSTANT).get("number"), number, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(ADDRESS_CONSTANT).get("postalCode"), postalCode, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(ADDRESS_CONSTANT).get("municipality"), municipality, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get(NATIONAL_ID_CONSTANT), nationalId, builder).ifPresent(predicates::add);
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
