package com.cipalschaubroeck.csimplementation.contractingauthority.service;

public interface ContractingAuthorityImageSettingsService {

    void setImage(AuthorityImage imageType, Integer authorityId, String originalFilename, byte[] bytes, String mime);
}
