package com.cipalschaubroeck.csimplementation.contractingauthority.controller.proccessors;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractAuthorityCollaboratorQFDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// spring resource processor
@SuppressWarnings("unused")
public class ContractAuthorityCollaboratorQFDtoProcessor implements ResourceProcessor<Resource<ContractAuthorityCollaboratorQFDto>> {

    @Autowired
    private EntityLinks links;

    @Override
    public Resource<ContractAuthorityCollaboratorQFDto> process(Resource<ContractAuthorityCollaboratorQFDto> resource) {
        QualificationFunction qf = resource.getContent().getQualificationFunction();
        if (qf != null) {
            resource.getLinks().add(links.linkToSingleResource(QualificationFunction.class, qf.getId())
                    .withRel("qualification-function"));
        }

        Collaborator c = resource.getContent().getCollaborator();
        if(c != null) {
            resource.getLinks().add(links.linkToSingleResource(Collaborator.class, c.getId())
                    .withRel("collaborator"));
        }
        return resource;
    }
}
