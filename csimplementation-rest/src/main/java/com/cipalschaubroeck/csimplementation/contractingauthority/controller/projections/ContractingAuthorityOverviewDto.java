package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;

@Projection(name = "contractingAuthorityInfo", types = ContractingAuthority.class)
public interface ContractingAuthorityOverviewDto {
    class Constants {

        private Constants() {
            // utility class
        }

        public static final Map<String, String> SORT_ALIASES = Collections.unmodifiableMap(Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("authority", "name"),
                new AbstractMap.SimpleImmutableEntry<>("address", "address.street"),
                new AbstractMap.SimpleImmutableEntry<>("number", "address.number"),
                new AbstractMap.SimpleImmutableEntry<>("postalCode", "address.postalCode"),
                new AbstractMap.SimpleImmutableEntry<>("municipality", "address.municipality")));
    }

    Integer getId();

    String getName();

    @Value("#{target.address?.street}")
    String getAddress();

    @Value("#{target.address?.number}")
    String getNumber();

    @Value("#{target.address?.postalCode}")
    String getPostalCode();

    @Value("#{target.address?.municipality}")
    String getMunicipality();

    @SuppressWarnings("unused")
    String getNationalId();

    @Value("#{target.address?.country}")
    String getCountry();

    @Value("#{target.email?.email}")
    String getEmail();

    @Value("#{target.phone?.phone}")
    String getPhone();

    @JsonIgnore
    @Value("#{target.email}")
    EmailAddress getEmailAddress();

    @JsonIgnore
    @Value("#{target.phone}")
    PhoneNumber getPhoneNumber();

    @JsonIgnore
    @Value("#{target.address}")
    PostalAddress getPostalAddress();

    // TODO CSPROC-2101 Add fax either as a new resource or part of phone

}
