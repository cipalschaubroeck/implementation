package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "initiateContractAuthorityInfo", types = ContractingAuthority.class)
public interface InitiateContractAuthorityDto {

    String getName();

    // used by jackson
    @SuppressWarnings("unused")
    List<AuthorityCollaboratorDto> getCollaborators();
}
