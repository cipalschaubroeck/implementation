package com.cipalschaubroeck.csimplementation.contractingauthority.controller.proccessors;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.QualificationFunctionDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// spring resource processor
@SuppressWarnings("unused")
public class QualificationFunctionDtoProcessor implements ResourceProcessor<Resource<QualificationFunctionDto>> {

    @Autowired
    private EntityLinks links;

    @Override
    public Resource<QualificationFunctionDto> process(Resource<QualificationFunctionDto> resource) {
        Collaborator collaborator = resource.getContent().getCollaborator();
        if (collaborator != null) {
            resource.getLinks().add(links.linkToSingleResource(Collaborator.class, collaborator.getId())
                    .withRel("collaborator"));
        }
        return resource;
    }
}
