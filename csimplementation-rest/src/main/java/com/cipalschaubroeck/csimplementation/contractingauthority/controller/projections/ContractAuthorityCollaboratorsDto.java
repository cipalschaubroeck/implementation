package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "contractAuthorityCollaboratorsInfo", types = Contract.class)
public interface ContractAuthorityCollaboratorsDto {

    Integer getId();

    @Value("#{target.contractAuthorityRelation?.contractingAuthority}")
    AuthorityDto getAuthority();

    @Value("#{target.mainAuthorityCollaboratorContact?.id}")
    Integer getMainContactId();

    // used by jackson
    @SuppressWarnings("unused")
    List<ContractAuthorityCollaboratorQFDto> getCollaborators();

    ContractStatus getStatus();
}