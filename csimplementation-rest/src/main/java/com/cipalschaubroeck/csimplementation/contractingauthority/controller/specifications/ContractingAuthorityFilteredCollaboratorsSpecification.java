package com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

import static com.cipalschaubroeck.csimplementation.shared.controller.SpecificationSupport.stringContainsIgnoreCase;

@Setter
@Getter
public class ContractingAuthorityFilteredCollaboratorsSpecification implements Specification<QualificationFunction> {

    private String qualification;
    private Integer authorityId;

    @Override
    public Predicate toPredicate(Root<QualificationFunction> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder.equal(root.get("authorityCollaborator").get("authority").get("id"), authorityId));
        stringContainsIgnoreCase(root.get("qualification").get("name"), qualification, builder).ifPresent(predicates::add);
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
