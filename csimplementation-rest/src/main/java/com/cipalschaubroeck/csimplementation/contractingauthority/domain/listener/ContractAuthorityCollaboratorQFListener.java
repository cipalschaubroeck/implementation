package com.cipalschaubroeck.csimplementation.contractingauthority.domain.listener;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PrePersist;
import java.util.stream.Collectors;

/**
 * Checks collaborator assigned to contract belongs to the authority assigned to the contract.
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractAuthorityCollaboratorQFListener {

    private void validate(ContractAuthorityCollaboratorQF entity) {
        if (entity.getContract().getContractAuthorityRelation() == null ||
                CollectionUtils.isEmpty(entity.getContract().getContractAuthorityRelation()
                        .getContractingAuthority().getCollaborators()
                        .stream().filter(col -> col.getCollaborator().getId()
                                .equals(entity.getQualificationFunction().getAuthorityCollaborator().getCollaborator().getId()))
                        .collect(Collectors.toList()))) {
            throw new EntityNotFoundException("Collaborator is not part of the contracting authority assigned to the contract");
        }
    }

    @PrePersist
    public void validatePersist(ContractAuthorityCollaboratorQF entity) {
        validate(entity);
    }
}
