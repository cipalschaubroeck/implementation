package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PersonResolvable;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "collaborator")
@Getter
@Setter
public class Collaborator implements PersonResolvable, Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "person", foreignKey = @ForeignKey(name = "fk_collaborator_person"))
    @NotNull
    private Person person;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @ManyToOne
    @JoinColumn(name = "authority", foreignKey = @ForeignKey(name = "fk_collaborator_authority"))
    private ContractingAuthority authority;

    // TODO CSPROC-2273 rework this relationship for bailGuaranteeValidator
    // now department belongs to the authority and the collaborator
    // related through the table authority_collaborator
//    @ManyToMany
//    @JoinTable(name = "collaborator_department",
//            joinColumns = {@JoinColumn(name = "collaborator", referencedColumnName = "person")},
//            inverseJoinColumns = @JoinColumn(name = "department", referencedColumnName = "id"))
//    @JoinColumn(name = "department", foreignKey = @ForeignKey(name = "fk_collaborator_department"))
//    private List<Department> departments = new ArrayList<>();

    @Override
    @Transient
    public Person getEffectivePerson() {
        return person;
    }
}
