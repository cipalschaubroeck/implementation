package com.cipalschaubroeck.csimplementation.contractingauthority.controller.proccessors;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractingAuthorityOverviewDto;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// spring resource processor
@SuppressWarnings("unused")
public class ContractingAuthorityOverviewDtoProcessor implements ResourceProcessor<Resource<ContractingAuthorityOverviewDto>> {

    @Autowired
    private EntityLinks links;

    @Override
    public Resource<ContractingAuthorityOverviewDto> process(Resource<ContractingAuthorityOverviewDto> resource) {
        EmailAddress email = resource.getContent().getEmailAddress();
        if (email != null) {
            resource.getLinks().add(links.linkToSingleResource(EmailAddress.class, email.getId()).withRel("emailAddressLink"));
        }

        PhoneNumber phone = resource.getContent().getPhoneNumber();
        if (phone != null) {
            resource.getLinks().add(links.linkToSingleResource(PhoneNumber.class, phone.getId()).withRel("phoneNumberLink"));
        }

        PostalAddress address = resource.getContent().getPostalAddress();
        if (address != null) {
            resource.getLinks().add(links.linkToSingleResource(PostalAddress.class, address.getId()).withRel("postalAddressLink"));
        }
        return resource;
    }
}
