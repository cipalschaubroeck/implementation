package com.cipalschaubroeck.csimplementation.contractingauthority.service.ipml;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.AuthorityCollaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Qualification;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.AuthorityCollaboratorRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.CollaboratorRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.DepartmentRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.QualificationFunctionRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.QualificationRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.CollaboratorService;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.CollaboratorAssignmentProcessDto;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
public class DefaultCollaboratorService implements CollaboratorService {

    @Autowired
    private CollaboratorRepository collaboratorRepository;

    @Autowired
    private ContractingAuthorityRepository authorityRepository;

    @Autowired
    private AuthorityCollaboratorRepository authorityCollaboratorRepository;

    @Autowired
    private DepartmentRepository departmentRepository;

    @Autowired
    private QualificationRepository qualificationRepository;

    @Autowired
    private QualificationFunctionRepository qualificationFunctionRepository;

    private void saveQualificationFunction(Integer qualificationId, AuthorityCollaborator authorityCollaborator, String function) {
        Qualification qualification;
        if (qualificationId == null) {
            // TODO CSPROC-2196 Create Exception handler for entity not found Exception
            throw new EntityNotFoundException("Qualification passed from view is null");
        }
        qualification = qualificationRepository.findById(qualificationId).orElseThrow(EntityNotFoundException::new);

        if (StringUtils.isEmpty(function)) {
            // TODO CSPROC-2196 Create Exception handler for entity not found Exception
            throw new EntityNotFoundException("Function passed from the view is null");
        }

        QualificationFunction qualificationFunction = qualificationFunctionRepository.
                findAllByAuthorityCollaboratorAndFunctionAndQualification(authorityCollaborator, function, qualification)
                .orElseGet(QualificationFunction::new);

        qualificationFunction.setFunction(function);
        qualificationFunction.setAuthorityCollaborator(authorityCollaborator);
        qualificationFunction.setQualification(qualification);
        qualificationFunctionRepository.save(qualificationFunction);
    }

    private AuthorityCollaborator saveAuthorityCollaborator(Integer departmentId, ContractingAuthority authority, Collaborator collaborator) {
        AuthorityCollaborator authorityCollaborator;
        Department department;
        if (departmentId != null) {
            department = departmentRepository.findById(departmentId).orElseThrow(EntityNotFoundException::new);
            authorityCollaborator = authorityCollaboratorRepository
                    .findAuthorityCollaboratorByAuthorityAndCollaboratorAndDepartment(authority, collaborator, department)
                    .orElseGet(AuthorityCollaborator::new);
            authorityCollaborator.setDepartment(department);
        } else {
            authorityCollaborator = authorityCollaboratorRepository
                    .findAuthorityCollaboratorByAuthorityAndCollaboratorAndDepartmentIsNull(authority, collaborator)
                    .orElseGet(AuthorityCollaborator::new);
        }

        authorityCollaborator.setAuthority(authority);
        authorityCollaborator.setCollaborator(collaborator);

        return authorityCollaboratorRepository.save(authorityCollaborator);
    }

    @Override
    @Transactional
    public void saveUpdateCollaborator(Integer collaboratorId, @RequestBody CollaboratorAssignmentProcessDto dto) {

        Collaborator collaborator = collaboratorRepository.findById(collaboratorId).orElseThrow(EntityNotFoundException::new);

        ContractingAuthority authority;
        if (dto.getContractingAuthority() == null) {
            // TODO CSPROC-2196 Create Exception handler for entity not found Exception
            throw new EntityNotFoundException("Authority passed from view is null");
        }
        authority = authorityRepository.findById(dto.getContractingAuthority()).orElseThrow(EntityNotFoundException::new);

        AuthorityCollaborator authorityCollaborator = saveAuthorityCollaborator(dto.getDepartment(), authority, collaborator);

        saveQualificationFunction(dto.getQualification(), authorityCollaborator, dto.getFunction());
    }
}
