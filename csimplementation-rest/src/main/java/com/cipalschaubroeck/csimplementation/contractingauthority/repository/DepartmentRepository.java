package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "department")
public interface DepartmentRepository extends PagingAndSortingRepository<Department, Integer>,
        JpaSpecificationExecutor<Department> {
}
