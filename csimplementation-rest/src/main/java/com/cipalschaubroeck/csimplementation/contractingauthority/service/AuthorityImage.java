package com.cipalschaubroeck.csimplementation.contractingauthority.service;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;

import java.util.function.BiConsumer;

public enum AuthorityImage {
    LOGO {
        @Override
        BiConsumer<ContractingAuthorityImageSettings, String> getIdSetter() {
            return ContractingAuthorityImageSettings::setLogoId;
        }
    },
    RIBBON {
        @Override
        BiConsumer<ContractingAuthorityImageSettings, String> getIdSetter() {
            return ContractingAuthorityImageSettings::setRibbonId;
        }
    };

    abstract BiConsumer<ContractingAuthorityImageSettings, String> getIdSetter();
}
