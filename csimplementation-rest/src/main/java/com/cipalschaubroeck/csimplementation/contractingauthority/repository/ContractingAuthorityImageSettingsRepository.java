package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(exported = false)
public interface ContractingAuthorityImageSettingsRepository
        extends PagingAndSortingRepository<ContractingAuthorityImageSettings, Integer> {
}
