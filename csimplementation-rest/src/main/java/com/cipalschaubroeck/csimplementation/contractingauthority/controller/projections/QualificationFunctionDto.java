package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "qualificationFunctionInfo", types = QualificationFunction.class)
public interface QualificationFunctionDto {

    Integer getId();

    String getFunction();

    @Value("#{target.qualification.name}")
    String getQualification();

    @Value("#{target.authorityCollaborator.collaborator.person.firstName}")
    String getFirstName();

    @Value("#{target.authorityCollaborator.collaborator.person.lastName}")
    String getLastName();

    @Value("#{target.authorityCollaborator.department.name}")
    String getDepartment();

    //used for link creation
    @Value("#{target.authorityCollaborator.collaborator}")
    @JsonIgnore
    Collaborator getCollaborator();
}
