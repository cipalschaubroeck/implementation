package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.listener.AuthorityCollaboratorListener;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "authority_collaborator",
        indexes = {
                @Index(name = "unique_collaborator_authority_department",
                        columnList = "authority, collaborator, department", unique = true)})
@Getter
@Setter
@EntityListeners(AuthorityCollaboratorListener.class)
public class AuthorityCollaborator {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "authority", foreignKey = @ForeignKey(name = "fk_authority"))
    @NotNull
    private ContractingAuthority authority;

    @ManyToOne
    @JoinColumn(name = "collaborator", foreignKey = @ForeignKey(name = "fk_authority_collaborator"))
    @NotNull
    private Collaborator collaborator;

    @ManyToOne
    @JoinColumn(name = "department", foreignKey = @ForeignKey(name = "fk_authority_collaborator_department"))
    private Department department;

    @OneToMany(mappedBy = "authorityCollaborator")
    private List<QualificationFunction> qualificationFunction = new ArrayList<>();
}
