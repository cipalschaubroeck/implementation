package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications.ContractingAuthorityFilteredCollaboratorsSpecification;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.QualificationFunctionRepository;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class QualificationFunctionController {

    @Autowired
    private QualificationFunctionRepository repository;

    @GetMapping("contracting-authority/{id}/collaborators/filtered")
    public ResponseEntity<Resources> searchFiltered(
            PersistentEntityResourceAssembler assembler,
            ContractingAuthorityFilteredCollaboratorsSpecification specification,
            @PageableDefault Pageable pageable,
            @PathVariable("id") Integer authorityId) {
        specification.setAuthorityId(authorityId);

        return new ResponseEntity<>(ControllerSupport.toResources(repository.findAll(specification, pageable),
                assembler, QualificationFunction.class), HttpStatus.OK);
    }
}
