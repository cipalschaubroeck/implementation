package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contracting_authority")
@Getter
@Setter
public class ContractingAuthority {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "name")
    @NotNull
    private String name;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "email", foreignKey = @ForeignKey(name = "fk_authority_email"))
    private EmailAddress email;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "phone", foreignKey = @ForeignKey(name = "fk_authority_phone"))
    private PhoneNumber phone;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address", foreignKey = @ForeignKey(name = "fk_authority_address"))
    private PostalAddress address;

    // TODO CSPROC-2097 either implement validation on https://wiki.int.cipal.be/display/CP/10H+Components, search KBO number
    @Column(name = "national_id", unique = true)
    @NotNull
    private String nationalId;

    @OneToMany(mappedBy = "authority")
    private List<AuthorityCollaborator> collaborators = new ArrayList<>();
}
