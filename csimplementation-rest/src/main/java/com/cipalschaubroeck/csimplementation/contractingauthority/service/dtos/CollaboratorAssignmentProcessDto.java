package com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CollaboratorAssignmentProcessDto {
    private Integer contractingAuthority;
    private Integer department;
    private String function;
    private Integer qualification;
}
