package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.listener.ContractAuthorityCollaboratorQFListener;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "contract_authority_collaborator_q_f", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"contract", "qualification_function"})})
@Getter
@Setter
@EntityListeners(ContractAuthorityCollaboratorQFListener.class)
public class ContractAuthorityCollaboratorQF {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_contract_qualification_function"))
    @NotNull
    private Contract contract;

    @ManyToOne
    @JoinColumn(name = "qualification_function", foreignKey = @ForeignKey(name = "fk_qualification_function_contract"))
    @NotNull
    private QualificationFunction qualificationFunction;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "MM/dd/yyyy")
    private LocalDate startDate;

    @Column(name = "competence")
    private String competence;
}
