package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.Map;

@Projection(name = "collaboratorInfo", types = Collaborator.class)
public interface CollaboratorOverviewDto {
    class Constants {
        private Constants() {
            // utility class
        }

        public static final Map<String, String> SORT_ALIASES = Collections.unmodifiableMap(Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("name", "person.firstName"),
                new AbstractMap.SimpleImmutableEntry<>("phone", "person.phone"),
                new AbstractMap.SimpleImmutableEntry<>("email", "person.email"),
                new AbstractMap.SimpleImmutableEntry<>("authority", "authority.name"))
        );
    }

    @Value("#{target.person.firstName}")
    String getName();

    @Value("#{target.person.lastName}")
    String getLastName();

    @Value("#{target.person.phone?.phone}")
    String getPhone();

    @Value("#{target.person.email?.email}")
    String getEmail();

    @Value("#{target.authority?.name}")
    String getAuthority();

    @JsonIgnore
    @Value("#{target.person}")
    Person getPerson();
}
