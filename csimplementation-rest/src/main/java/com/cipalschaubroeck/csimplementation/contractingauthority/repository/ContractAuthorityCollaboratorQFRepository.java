package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "contract-authority-collaborator-q-f")
public interface ContractAuthorityCollaboratorQFRepository
        extends PagingAndSortingRepository<ContractAuthorityCollaboratorQF, Integer>,
        JpaSpecificationExecutor<ContractAuthorityCollaboratorQF> {
}
