package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.AuthorityCollaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Qualification;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@RepositoryRestResource(path = "qualification-function")
public interface QualificationFunctionRepository extends PagingAndSortingRepository<QualificationFunction, Integer>,
        JpaSpecificationExecutor<QualificationFunction> {
        Optional<QualificationFunction> findAllByAuthorityCollaboratorAndFunctionAndQualification(
                AuthorityCollaborator authorityCollaborator, String function, Qualification qualification);
}
