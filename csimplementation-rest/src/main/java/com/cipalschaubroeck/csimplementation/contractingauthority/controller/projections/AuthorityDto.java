package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "authorityInfo", types = ContractingAuthority.class)
public interface AuthorityDto {
    String getName();
}