package com.cipalschaubroeck.csimplementation.contractingauthority.controller.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

@Getter
@Setter
public class CollaboratorAssignmentDto {
    private Link contractingAuthority;
    private Link department;
    private String function;
    private Link qualification;
}
