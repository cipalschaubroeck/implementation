package com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.QualificationFunction;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@Projection(name = "contractAuthorityCollaboratorQFInfo", types = ContractAuthorityCollaboratorQF.class)
public interface ContractAuthorityCollaboratorQFDto {

    Integer getId();

    @Value("#{target.qualificationFunction.authorityCollaborator.collaborator.person.lastName}")
    String getLastName();

    @Value("#{target.qualificationFunction.authorityCollaborator.collaborator.person.firstName}")
    String getFirstName();

    @Value("#{target.qualificationFunction.authorityCollaborator.department?.name}")
    String getDepartment();

    @Value("#{target.qualificationFunction.qualification.name}")
    String getQualification();

    @Value("#{target.qualificationFunction.function}")
    String getFunction();

    //used by jackson
    @SuppressWarnings("unused")
    LocalDate getStartDate();

    String getCompetence();

    //used by jackson
    @SuppressWarnings("unused")
    @JsonIgnore
    QualificationFunction getQualificationFunction();

    //used by jackson
    @SuppressWarnings("unused")
    @JsonIgnore
    @Value("#{target.qualificationFunction.authorityCollaborator.collaborator}")
    Collaborator getCollaborator();

}