package com.cipalschaubroeck.csimplementation.contractingauthority.domain.listener;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.AuthorityCollaborator;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.dao.DuplicateKeyException;

import javax.persistence.PrePersist;
import java.util.Objects;

/**
 * Checks that the department actually belongs to the selected authority
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class AuthorityCollaboratorListener {

    private void validate(AuthorityCollaborator authorityCollaborator) {
        if (authorityCollaborator.getDepartment() != null && !Objects.equals(authorityCollaborator.getAuthority().getId(),
                authorityCollaborator.getDepartment().getAuthority().getId())) {
            throw new DuplicateKeyException("The department doesn't belong to the contracting authority");
        }
    }

    @PrePersist
    public void validateUpdate(AuthorityCollaborator authorityCollaborator) {
        validate(authorityCollaborator);
    }
}
