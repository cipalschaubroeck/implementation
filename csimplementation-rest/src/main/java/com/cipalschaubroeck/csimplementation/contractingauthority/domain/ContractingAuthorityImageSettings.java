package com.cipalschaubroeck.csimplementation.contractingauthority.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "contracting_authority_image_settings")
@Getter
@Setter
public class ContractingAuthorityImageSettings {
    @Id
    private Integer id;

    @OneToOne(optional = false)
    @MapsId
    @JoinColumn(name = "id", foreignKey = @ForeignKey(name = "fk_image_contracting_authority"))
    private ContractingAuthority authority;

    @Column(name = "ribbon")
    private String ribbonId;

    @Column(name = "logo")
    private String logoId;
}
