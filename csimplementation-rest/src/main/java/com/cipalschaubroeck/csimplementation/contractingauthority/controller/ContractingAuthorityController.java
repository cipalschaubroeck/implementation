package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.controller.projections.ContractingAuthorityOverviewDto;
import com.cipalschaubroeck.csimplementation.contractingauthority.controller.specifications.ContractingAuthorityOverviewSpecification;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class ContractingAuthorityController {
    @Autowired
    private ContractingAuthorityRepository repository;
    @Autowired
    private ControllerSupport controllerSupport;

    @GetMapping("contracting-authority/overview")
    public ResponseEntity<PagedResources> search(
            ContractingAuthorityOverviewSpecification specification,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            PersistentEntityResourceAssembler assembler) {
        Page<ContractingAuthority> page = repository.findAll(specification,
                ControllerSupport.getPageConfig(pageable, sort, ContractingAuthorityOverviewDto.Constants.SORT_ALIASES));
        return controllerSupport.toResources(page, ContractingAuthority.class, assembler);
    }
}
