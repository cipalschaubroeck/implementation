package com.cipalschaubroeck.csimplementation.contractingauthority.service;

import com.cipalschaubroeck.csimplementation.contractingauthority.service.dtos.CollaboratorAssignmentProcessDto;

public interface CollaboratorService {
    void saveUpdateCollaborator(Integer collaboratorId, CollaboratorAssignmentProcessDto dto);
}
