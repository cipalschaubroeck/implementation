package com.cipalschaubroeck.csimplementation.lazy;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;
import java.util.function.Function;

/**
 * This class has been designed for the following use case: there's a type of object T that we want to load lazily,
 * there's a type of object I we can use as key to obtain the object T through a particular way (initializer), but since
 * initializer may incur in costly work, we want it to be initialized only if it is accessed
 *
 * @param <T> type of the object to proxy
 * @param <I> id of the object to proxy
 */
public class LazyObject<T, I> implements MethodInterceptor {
    private Function<I, T> initializer;
    private T delegate;
    private I id;

    /**
     * creates a lazily loaded instance of O
     *
     * @param targetClass O class
     * @param id          id to use to retrieve the object
     * @param initializer how to initialize the object if and when it is accessed
     * @param <O>         type of the object we want
     * @param <I>         type of the id of the object we want
     * @return the created object
     */
    public static <O, I> O build(Class<O> targetClass, I id, Function<I, O> initializer) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(targetClass);
        enhancer.setCallback(new LazyObject<>(id, initializer));
        return targetClass.cast(enhancer.create());
    }

    private LazyObject(I id, Function<I, T> initializer) {
        this.id = id;
        this.initializer = initializer;
    }

    @Override
    public Object intercept(Object o, Method method, Object[] args, MethodProxy methodProxy) throws Throwable {
        if (delegate == null) {
            delegate = initializer.apply(id);
        }
        return method.invoke(delegate, args);
    }
}
