package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.project.controller.BailDto;

public interface ContractBailService {
    void saveUpdateBailInProcurement(Integer contractId, BailDto bailDto);
}
