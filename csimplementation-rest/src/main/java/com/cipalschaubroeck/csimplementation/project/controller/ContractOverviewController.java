package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
// used by spring
@SuppressWarnings("unused")
public class ContractOverviewController {
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private ControllerSupport controllerSupport;

    @GetMapping("contract/overview")
    public ResponseEntity<PagedResources> search(
            ContractOverviewSpecification specification,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            PersistentEntityResourceAssembler assembler) {
        Page<Contract> page = contractRepository.findAll(specification,
                ControllerSupport.getPageConfig(pageable, sort, ContractOverviewDto.Constants.SORT_ALIASES));
        return controllerSupport.toResources(page, Contract.class, assembler);
    }

    @GetMapping("contract/types")
    public ResponseEntity<List<String>> getAllContractTypes(ContractOverviewSpecification specification) {
        return new ResponseEntity<>(contractRepository.findContractTypes(), HttpStatus.OK);
    }
}
