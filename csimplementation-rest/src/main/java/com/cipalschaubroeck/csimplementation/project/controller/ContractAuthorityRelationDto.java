package com.cipalschaubroeck.csimplementation.project.controller;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

@Getter
@Setter
public class ContractAuthorityRelationDto {
    private Link contract;
    private Link contractingAuthority;

}
