package com.cipalschaubroeck.csimplementation.project.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;


@Entity
@Table(name = "parcel")
@Getter
@Setter
public class Parcel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "procurement", foreignKey = @ForeignKey(name = "fk_parcel_procurement_1"))
    private Procurement procurement;


}
