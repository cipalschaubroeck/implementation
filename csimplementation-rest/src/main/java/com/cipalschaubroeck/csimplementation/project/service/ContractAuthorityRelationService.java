package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;

public interface ContractAuthorityRelationService {
    ContractAuthorityRelation setContractAuthorityRelation(Integer contractId, Integer authorityId);
}
