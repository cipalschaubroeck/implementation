package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.service.ContractDefinitionService;
import com.cipalschaubroeck.csimplementation.project.service.FormStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@BasePathAwareController
public class ContractDefinitionController {
    @Autowired
    private ContractDefinitionService service;

    @GetMapping("/contract/{id}/validate/initiate/contract-process")
    public ResponseEntity<FormStatus> validateInitiateContractProcess(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.getValidateInitiateContractProcessComplete(contractId));
    }

    @PostMapping("/contract/{id}/validate/initiate/contract-process")
    public ResponseEntity setInProgress(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.validateInitiateContractProcessComplete(contractId));
    }

    @GetMapping("/contract/{id}/validate/initiate/data-form")
    public ResponseEntity<FormStatus> isInitiateContractDataFormComplete(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.validateInitiateContractData(contractId));
    }

    @GetMapping("/contract/{id}/validate/initiate/authority-form")
    public ResponseEntity<FormStatus> isInitiateContractAuthorityFormComplete(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.validateInitiateContractAuthority(contractId));
    }

    @GetMapping("/contract/{id}/validate/duplicate-name")
    public ResponseEntity<Boolean> validateContractName(@PathVariable("id") Integer contractId, @RequestParam String name) {
        return ResponseEntity.ok(service.validateContractName(name, contractId));
    }

    @GetMapping("/contract/validate/duplicate-name")
    public ResponseEntity<Boolean> validateContractName(@RequestParam String name) {
        return ResponseEntity.ok(service.validateContractName(name, null));
    }

    @GetMapping("/contract/{id}/validate/initiate/bail-form")
    public ResponseEntity<FormStatus> isInitiateContractBailFormComplete(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.validateInitiateContractBail(contractId));
    }

    @GetMapping("/contract/{id}/validate/initiate/prices-review-form")
    public ResponseEntity<FormStatus> isInitiateContractPricesReviewFormComplete(@PathVariable("id") Integer contractId) {
        return ResponseEntity.ok(service.validateInitiateContractPricesReview(contractId));
    }
}
