package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.domain.ContractSequence;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractSequenceRepository extends PagingAndSortingRepository<ContractSequence, Integer> {
}
