package com.cipalschaubroeck.csimplementation.project.domain;

public enum BailType {
    BAIL,
    SPECIAL_BAIL
}
