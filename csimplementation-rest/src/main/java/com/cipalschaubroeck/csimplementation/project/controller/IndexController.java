package com.cipalschaubroeck.csimplementation.project.controller;

import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * TomcatServletWebServerFactory nor WebServerFactoryCustomizer will work in external tomcat container
 * especially when the scope of spring-boot-starter-tomcat is provided
 * ErrorViewResolver has been introduced to solve this problem.
 * Its an interface that can be implemented by beans that resolve error views.
 */

@Component
// used by spring
@SuppressWarnings("unused")
public class IndexController implements ErrorViewResolver {
    @Override
    public ModelAndView resolveErrorView(HttpServletRequest request,
                                         HttpStatus status, Map<String, Object> model) {
        return new ModelAndView("/index.html");
    }
}
