package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.FormulaType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "initiateContractFormulaProjection", types = Contract.class)
public interface InitiateContractFormulaProjection {

    @Value("#{target.formula?.type}")
    FormulaType getType();

    @Value("#{target.formula?.formula}")
    String getFormula();
}
