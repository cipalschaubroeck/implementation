package com.cipalschaubroeck.csimplementation.project.service.impl;

import com.cipalschaubroeck.csimplementation.project.controller.BailDto;
import com.cipalschaubroeck.csimplementation.project.domain.Bail;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.ContractBailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
public class DefaultContractBailService implements ContractBailService {

    @Autowired
    private ContractRepository contractRepository;

    @Override
    @Transactional
    public void saveUpdateBailInProcurement(Integer contractId, BailDto bailDto) {

        Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);

        if (bailDto != null && bailDto.getType() != null) {
            if (contract.getProcurement() == null) {
                contract.setProcurement(new Procurement());
            }
            if (contract.getProcurement().getBail() == null) {
                contract.getProcurement().setBail(new Bail());
            }
            contract.getProcurement().getBail().setType(bailDto.getType());
            contract.getProcurement().getBail().setAmount(bailDto.getAmount());
            contract.getProcurement().getBail().setDescription(bailDto.getDescription());
            contract.getProcurement().getBail().setPercentage(bailDto.getPercentage());
        } else {
            contract.getProcurement().setBail(null);
        }
    }
}
