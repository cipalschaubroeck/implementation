package com.cipalschaubroeck.csimplementation.project.domain;

public enum FormulaType {
    NONE,
    FIXED,
    CUSTOM
}
