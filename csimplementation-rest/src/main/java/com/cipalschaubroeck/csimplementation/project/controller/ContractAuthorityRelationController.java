package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.service.ContractAuthorityRelationService;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collections;

@BasePathAwareController
public class ContractAuthorityRelationController {

    @Autowired
    private ContractAuthorityRelationService contractAuthorityRelationService;

    @Autowired
    private ControllerSupport support;

    @PutMapping("/contract-authority-relation")
    public ResponseEntity<HttpStatus> setRelation(@RequestBody ContractAuthorityRelationDto dto) {

        ContractAuthorityRelationUpdateInfo info = new ContractAuthorityRelationUpdateInfo();
        if (dto.getContract() != null) {
            info.setContractId((Integer) support.linkToEntityInfo(dto.getContract(), Collections.singletonList(Contract.class))
                    .getId());
            if (dto.getContractingAuthority() != null) {
                info.setAuthorityId((Integer) support.linkToEntityInfo(dto.getContractingAuthority(),
                        Collections.singletonList(ContractingAuthority.class)).getId());
            }
        }

        contractAuthorityRelationService.setContractAuthorityRelation(info.getContractId(), info.getAuthorityId());

        return new ResponseEntity<>(HttpStatus.NO_CONTENT, HttpStatus.CREATED);
    }


}
