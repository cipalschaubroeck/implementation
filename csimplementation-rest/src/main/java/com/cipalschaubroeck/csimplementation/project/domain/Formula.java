package com.cipalschaubroeck.csimplementation.project.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "formula")
@Getter
@Setter
public class Formula {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column
    @Enumerated(EnumType.STRING)
    @NotNull
    private FormulaType type;

    @Column
    private String formula;
}
