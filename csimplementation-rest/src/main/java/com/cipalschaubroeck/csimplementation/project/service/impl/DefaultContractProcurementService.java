package com.cipalschaubroeck.csimplementation.project.service.impl;

import com.cipalschaubroeck.csimplementation.project.controller.ProcurementDto;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.ContractProcurementService;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
@Log4j
public class DefaultContractProcurementService implements ContractProcurementService {

    @Autowired
    private ContractRepository contractRepository;

    @Override
    @Transactional
    public void saveUpdateProcurementInContract(Integer contractId, ProcurementDto procurement) {

        Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);

        if (contract.getProcurement() == null) {
            contract.setProcurement(new Procurement());
        }
        if (procurement.getDescription() != null) {
            contract.getProcurement().setDescription(procurement.getDescription());
        } else if (procurement.getName() != null) {
            contract.getProcurement().setName(procurement.getName());
        } else if (procurement.getReferenceNumber() != null) {
            contract.getProcurement().setReferenceNumber(procurement.getReferenceNumber());
        }
    }
}
