package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "procurement")
public interface ProcurementRepository extends PagingAndSortingRepository<Procurement, Integer> {
}
