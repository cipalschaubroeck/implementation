package com.cipalschaubroeck.csimplementation.project.service;

public interface ContractDefinitionService {
    FormStatus getValidateInitiateContractProcessComplete(Integer contractId);

    /**
     * @param contractId id of contract
     * @return when initiating the validation returns true or false
     * depending on if the contract initiation has passed the pertinent validations.
     * If its true changes the status of the contract to in progress.
     */
    FormStatus validateInitiateContractProcessComplete(Integer contractId);

    FormStatus validateInitiateContractData(Integer contractId);

    FormStatus validateInitiateContractAuthority(Integer contractId);

    boolean validateContractName(String contractName, Integer id);

    FormStatus validateInitiateContractBail(Integer contractId);

    FormStatus validateInitiateContractPricesReview(Integer contractId);
}
