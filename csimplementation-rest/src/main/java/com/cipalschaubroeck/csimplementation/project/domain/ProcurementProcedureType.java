package com.cipalschaubroeck.csimplementation.project.domain;

public enum ProcurementProcedureType {
    PUBLIC,
    NON_PUBLIC,
    COMPETITIVE_NEGOTIATION,
    SIMPLIFIED_NEGOTIATED_WITH_PRIOR_PUBLICATION,
    NEGOTIATED_WITHOUT_PRIOR_PUBLICATION,
    ACCEPTED_INVOICE,
    ONGOING_CONTRACT
}
