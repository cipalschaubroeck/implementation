package com.cipalschaubroeck.csimplementation.project.controller.projections;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "contractStatusInfo", types = Contract.class)
public interface ContractLinkProjection {

    Integer getId();

    ContractStatus getStatus();
}
