package com.cipalschaubroeck.csimplementation.project.domain;

import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PersonResolvable;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

/**
 * a qualified person is a person involved with a contract because of particular qualifications or competences.
 * There may be three kind of qualified persons: external if they work for an organization, internal
 * if they work for the contract's contracting authority and "ad hoc", independent persons that
 * have special knowledge or qualifications to justify their inclusion
 */
@Entity
@Table(name = "qualified_person")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ExternalQualifiedPerson.class, name = QualifiedPersonType.Name.EXTERNAL),
        @JsonSubTypes.Type(value = InternalQualifiedPerson.class, name = QualifiedPersonType.Name.INTERNAL),
        @JsonSubTypes.Type(value = AdHocQualifiedPerson.class, name = QualifiedPersonType.Name.AD_HOC)
})
@Getter
@Setter
public abstract class QualifiedPerson implements PersonResolvable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @ManyToOne(optional = false)
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_qualified_person_parcel"))
    @NotNull
    private Contract contract;

    @Column(name = "qualification")
    private String qualification;

    @Column(name = "competence")
    private String competence;

    @Transient
    public abstract QualifiedPersonType getType();

    @Transient
    public abstract Person getEffectivePerson();
}
