package com.cipalschaubroeck.csimplementation.project.service.impl;

import com.cipalschaubroeck.csimplementation.project.domain.Bail;
import com.cipalschaubroeck.csimplementation.project.domain.BailType;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import com.cipalschaubroeck.csimplementation.project.domain.Formula;
import com.cipalschaubroeck.csimplementation.project.domain.FormulaType;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.ContractDefinitionService;
import com.cipalschaubroeck.csimplementation.project.service.FormStatus;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Objects;
import java.util.stream.Stream;

@Service
public class DefaultContractDefinitionService implements ContractDefinitionService {
    @Autowired
    private ContractRepository repository;

    private static boolean noneNull(Object o1, Object... other) {
        if (o1 == null) {
            return false;
        } else if (other == null || other.length < 1) {
            return true;
        } else {
            return Stream.of(other).noneMatch(Objects::isNull);
        }
    }

    private FormStatus getValidateInitiateContractProcessComplete(Contract contract) {
        return validateInitiateContractData(contract) != FormStatus.NOT_COMPLETED
                && validateInitiateContractAuthority(contract) != FormStatus.NOT_COMPLETED
                && validateInitiateContractBail(contract) != FormStatus.NOT_COMPLETED
                && validateInitiateContractPricesReview(contract) != FormStatus.NOT_COMPLETED
                ? FormStatus.COMPLETED : FormStatus.NOT_COMPLETED;
    }

    private FormStatus getInitiateContractData(Contract contract) {
        if (contract.getStatus() != ContractStatus.IN_PREPARATION) {
            return FormStatus.READ_ONLY;
        } else if (StringUtils.isNotBlank(contract.getName())
                && StringUtils.isNotBlank(contract.getDecisionMakingBody())
                && noneNull(contract.getProcurement(), contract.getQualification(), contract.getProcedureType(),
                contract.getAmountMinusVat(), contract.getVat(), contract.getAmountVat())
                && StringUtils.isNotBlank(contract.getProcurement().getName())) {
            return FormStatus.COMPLETED;
        } else {
            return FormStatus.NOT_COMPLETED;
        }
    }

    private FormStatus validateInitiateContractData(Contract contract) {
        return getInitiateContractData(contract);
    }

    private FormStatus validateInitiateContractAuthority(Contract contract) {
        if (contract.getStatus() != ContractStatus.IN_PREPARATION) {
            return FormStatus.READ_ONLY;
        } else if (contract.getContractAuthorityRelation() != null
                && contract.getContractAuthorityRelation().getContractingAuthority() != null) {
            return FormStatus.COMPLETED;
        } else {
            return FormStatus.NOT_COMPLETED;
        }
    }

    private FormStatus validateInitiateContractBail(Contract contract) {
        if (contract.getStatus() != ContractStatus.IN_PREPARATION) {
            return FormStatus.READ_ONLY;
        } else if (contract.getProcurement() == null || contract.getProcurement().getBail() == null) {
            return FormStatus.COMPLETED;
        } else {
            Bail bail = contract.getProcurement().getBail();
            if ((bail.getType() == BailType.SPECIAL_BAIL && bail.getAmount() != null)
                    || (bail.getType() == BailType.BAIL && bail.getPercentage() != null)) {
                return FormStatus.COMPLETED;
            } else {
                return FormStatus.NOT_COMPLETED;
            }
        }
    }

    private FormStatus validateInitiateContractPricesReview(Contract contract) {
        if (contract.getStatus() != ContractStatus.IN_PREPARATION) {
            return FormStatus.READ_ONLY;
        } else if (contract.getFormula() == null) {
            return FormStatus.COMPLETED;
        } else {
            Formula formula = contract.getFormula();
            if ((formula.getType() == FormulaType.FIXED && StringUtils.isNotEmpty(formula.getFormula()))
                    || (formula.getType() == FormulaType.CUSTOM && StringUtils.isNotEmpty(formula.getFormula()))) {
                return FormStatus.COMPLETED;
            }
            return FormStatus.NOT_COMPLETED;
        }
    }

    @Override
    public FormStatus getValidateInitiateContractProcessComplete(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        return contract.getStatus() != ContractStatus.IN_PREPARATION ? FormStatus.READ_ONLY : FormStatus.NOT_COMPLETED;
    }

    @Override
    @Transactional
    public FormStatus validateInitiateContractProcessComplete(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        if (contract.getStatus() == ContractStatus.IN_PREPARATION
                && getValidateInitiateContractProcessComplete(contract) == FormStatus.COMPLETED) {
            contract.setStatus(ContractStatus.IN_PROGRESS);
            return FormStatus.READ_ONLY;
        } else {
            return FormStatus.NOT_COMPLETED;
        }
    }

    @Override
    // TODO CSPROC-2128 not yet, but you'll possibly want this to be @Transactional(readOnly)
    public FormStatus validateInitiateContractData(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        return getInitiateContractData(contract);
    }

    @Override
    public FormStatus validateInitiateContractAuthority(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        return validateInitiateContractAuthority(contract);
    }

    @Override
    public boolean validateContractName(String contractName, Integer id) {
        if (id == null) {
            return repository.countByName(contractName) == 0;
        } else {
            return repository.countByNameAndIdNot(contractName, id) == 0;
        }

    }

    @Override
    public FormStatus validateInitiateContractBail(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        return validateInitiateContractBail(contract);
    }

    @Override
    public FormStatus validateInitiateContractPricesReview(Integer contractId) {
        Contract contract = repository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        return validateInitiateContractPricesReview(contract);
    }
}
