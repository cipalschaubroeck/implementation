package com.cipalschaubroeck.csimplementation.project.domain;


import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "contract_authority_relation")
@Getter
@Setter

public class ContractAuthorityRelation {

    @Id
    @Column(name = "contract")
    private Integer id;

    @MapsId
    @OneToOne(optional = false)
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_contracting_authority_contract"))
    @NotNull
    private Contract contract;


    @ManyToOne(optional = false)
    @JoinColumn(name = "authority", foreignKey = @ForeignKey(name = "fk_contracting_authority"))
    @NotNull
    private ContractingAuthority contractingAuthority;
}
