package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.config.QualifiedPersonConfigConstants;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "qualified-person",
        collectionResourceRel = QualifiedPersonConfigConstants.QUALIFIED_PERSON_COLLECTION_REL)
public interface QualifiedPersonRepository extends PagingAndSortingRepository<QualifiedPerson, Integer> {

    List<QualifiedPerson> findByContract(Contract contract);
}
