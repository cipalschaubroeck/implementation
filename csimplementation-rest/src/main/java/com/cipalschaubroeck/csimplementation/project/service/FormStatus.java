package com.cipalschaubroeck.csimplementation.project.service;

public enum FormStatus {
    READ_ONLY,
    COMPLETED,
    NOT_COMPLETED
}
