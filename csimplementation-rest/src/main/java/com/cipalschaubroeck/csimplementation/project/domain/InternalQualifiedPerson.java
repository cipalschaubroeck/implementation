package com.cipalschaubroeck.csimplementation.project.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "qualified_person_internal")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class InternalQualifiedPerson extends QualifiedPerson {
    @ManyToOne(optional = false)
    @JoinColumns(foreignKey = @ForeignKey(name = "fk_internal_qualified_collaborator"),
            value = {@JoinColumn(name = "person", referencedColumnName = "person")})
    @NotNull
    private Collaborator collaborator;

    @Override
    public QualifiedPersonType getType() {
        return QualifiedPersonType.INTERNAL;
    }

    @Override
    public Person getEffectivePerson() {
        return collaborator.getPerson();
    }
}
