package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.project.controller.FormulaDto;

public interface ContractFormulaService {
    void saveFormulaInContract(Integer contractId, FormulaDto formulaDto);
}
