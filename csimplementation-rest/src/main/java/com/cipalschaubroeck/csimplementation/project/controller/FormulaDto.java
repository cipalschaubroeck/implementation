package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.FormulaType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FormulaDto {
    private FormulaType type;
    private String formula;
}
