package com.cipalschaubroeck.csimplementation.project.service.impl;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractAuthorityCollaboratorQFRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import com.cipalschaubroeck.csimplementation.project.repository.ContractAuthorityRelationRepository;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.ContractAuthorityRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;


@Service
public class DefaultContractAuthorityRelationService implements ContractAuthorityRelationService {

    @Autowired
    private ContractAuthorityRelationRepository contractAuthorityRelationRepository;

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    private ContractingAuthorityRepository contractingAuthorityRepository;


    @Autowired
    private ContractAuthorityCollaboratorQFRepository repository;

    @Override
    @Transactional
    public ContractAuthorityRelation setContractAuthorityRelation(Integer contractId, Integer authorityId) {
        /*
         * contractId is the same in contract and contractAuthorityRelation
         */
        if (contractId == null) {
            throw new EntityNotFoundException("contract passed from view is null");
        }
        Optional<ContractAuthorityRelation> relationOptional = contractAuthorityRelationRepository.findById(contractId);
        ContractAuthorityRelation relation;

        if (relationOptional.isPresent()) {
            relation = relationOptional.get();
            relation.getContract().setMainAuthorityCollaboratorContact(null);
            if (!CollectionUtils.isEmpty(relation.getContract().getCollaborators())) {
                repository.deleteAll(relation.getContract().getCollaborators());
            }
            if (authorityId == null) {
                contractAuthorityRelationRepository.delete(relation);
                return null;
            }
        } else if (authorityId == null) {
            return null;
        } else {
            Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);
            relation = new ContractAuthorityRelation();
            relation.setContract(contract);
        }

        ContractingAuthority authority = contractingAuthorityRepository.findById(authorityId)
                .orElseThrow(EntityNotFoundException::new);
        relation.setContractingAuthority(authority);
        return contractAuthorityRelationRepository.save(relation);
    }
}
