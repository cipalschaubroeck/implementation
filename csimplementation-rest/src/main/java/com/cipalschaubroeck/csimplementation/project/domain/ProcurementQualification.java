package com.cipalschaubroeck.csimplementation.project.domain;

public enum ProcurementQualification {
    WORKS,
    DELIVERIES,
    SERVICES
}
