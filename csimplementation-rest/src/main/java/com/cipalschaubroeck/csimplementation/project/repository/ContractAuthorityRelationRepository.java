package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractAuthorityRelationRepository extends CrudRepository<ContractAuthorityRelation, Integer> {
}
