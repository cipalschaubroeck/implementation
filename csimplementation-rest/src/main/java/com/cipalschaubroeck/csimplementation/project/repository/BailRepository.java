package com.cipalschaubroeck.csimplementation.project.repository;
import com.cipalschaubroeck.csimplementation.project.domain.Bail;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "bail")
public interface BailRepository extends PagingAndSortingRepository<Bail, Integer> {
}
