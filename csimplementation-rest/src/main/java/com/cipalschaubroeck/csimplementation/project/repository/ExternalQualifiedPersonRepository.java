package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.config.QualifiedPersonConfigConstants;
import com.cipalschaubroeck.csimplementation.project.domain.ExternalQualifiedPerson;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "qualified-person-external",
        collectionResourceRel = QualifiedPersonConfigConstants.QUALIFIED_PERSON_COLLECTION_REL)
public interface ExternalQualifiedPersonRepository extends PagingAndSortingRepository<ExternalQualifiedPerson, Integer> {
}
