package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.service.ContractBailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@BasePathAwareController
public class ContractBailController {

    @Autowired
    private ContractBailService contractBailService;

    @PostMapping("/contract/{id}/bail")
    public ResponseEntity<Void> setInProgress(@PathVariable("id") Integer contractId, @RequestBody BailDto bailInfo) {
        contractBailService.saveUpdateBailInProcurement(contractId, bailInfo);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
