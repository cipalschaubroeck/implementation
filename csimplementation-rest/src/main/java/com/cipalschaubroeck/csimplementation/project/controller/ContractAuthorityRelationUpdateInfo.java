package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContractAuthorityRelationUpdateInfo {

    private Integer contractId;
    private Integer authorityId;

    private EntityInfo personInfo = new EntityInfo();
}
