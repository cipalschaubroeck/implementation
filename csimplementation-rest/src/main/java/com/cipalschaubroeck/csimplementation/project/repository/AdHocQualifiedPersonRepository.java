package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.config.QualifiedPersonConfigConstants;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "qualified-person-ad-hoc",
        collectionResourceRel = QualifiedPersonConfigConstants.QUALIFIED_PERSON_COLLECTION_REL)
public interface AdHocQualifiedPersonRepository extends PagingAndSortingRepository<AdHocQualifiedPerson, Integer> {
}
