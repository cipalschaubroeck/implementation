package com.cipalschaubroeck.csimplementation.project.domain;

public enum ContractStatus {
    IN_PREPARATION,
    IN_PROGRESS,
    IN_AFTER_CARE,
    CANCELLED,
    ARCHIVED,
    EXECUTED
}
