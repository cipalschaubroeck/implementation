package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.BailType;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class BailDto {
    private BigDecimal percentage;
    private BailType type;
    private BigDecimal amount;
    private String description;
}
