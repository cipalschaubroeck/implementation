package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(path = "contract")
public interface ContractRepository extends PagingAndSortingRepository<Contract, Integer>, JpaSpecificationExecutor<Contract>, CrudRepository<Contract, Integer> {

    @Query("SELECT distinct contractType FROM Contract order by contractType asc")
    List<String> findContractTypes();

    long countByName(String name);

    long countByNameAndIdNot(String name, Integer id);
}
