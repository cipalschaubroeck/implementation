package com.cipalschaubroeck.csimplementation.project.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcurementDto {
    private String name;
    private String referenceNumber;
    private String description;
}
