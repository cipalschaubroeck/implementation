package com.cipalschaubroeck.csimplementation.project.service.impl;

import com.cipalschaubroeck.csimplementation.project.controller.FormulaDto;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.Formula;
import com.cipalschaubroeck.csimplementation.project.domain.FormulaType;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.ContractFormulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
public class DefaultContractFormulaService implements ContractFormulaService {

    @Autowired
    private ContractRepository contractRepository;

    @Override
    @Transactional
    public void saveFormulaInContract(Integer contractId, FormulaDto formulaDto) {

        Formula formula = null;
        Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);

        if (formulaDto != null && formulaDto.getType() != null && formulaDto.getType() != FormulaType.NONE) {
            formula = contract.getFormula() == null ? new Formula() : contract.getFormula();
            formula.setType(formulaDto.getType());
            formula.setFormula(formulaDto.getFormula());
        }
        contract.setFormula(formula);
    }
}
