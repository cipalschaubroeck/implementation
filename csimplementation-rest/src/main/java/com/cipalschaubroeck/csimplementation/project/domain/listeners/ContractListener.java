package com.cipalschaubroeck.csimplementation.project.domain.listeners;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractSequence;
import com.cipalschaubroeck.csimplementation.project.repository.ContractSequenceRepository;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PrePersist;
import java.util.Calendar;

@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ContractListener {

    /*
    TODO CSPROC-1999 this works but it is too much of a hack, we should get Configurable work so that we can avoid these constructions.
    An inner static class is needed because the repository depends on entityManager, which depends on the listener, which depends
    on the repository, so there's circular dependencies
     */
    @Component
    static class AutowireSupport {
        @Autowired
        public void setContractSequenceRepository(ContractSequenceRepository repository) {
            contractSequenceRepository = repository;
        }
    }

    private static ContractSequenceRepository contractSequenceRepository;

    @PrePersist
    public void updateContractSequence(Contract ob) {
        Iterable<ContractSequence> sequences = contractSequenceRepository.findAll();
        ContractSequence aux;
        if (sequences.iterator().hasNext()) {
            aux = sequences.iterator().next();
            if (Calendar.getInstance().get(Calendar.YEAR) == aux.getYear()) {
                aux.setSequenceValue(aux.getSequenceValue() + 1);
            } else {
                aux.setYear(Calendar.getInstance().get(Calendar.YEAR));
                aux.setSequenceValue(aux.getSequenceValue() + 1);
            }
        } else {
            aux = new ContractSequence();
            aux.setSequenceValue(1);
            aux.setYear(Calendar.getInstance().get(Calendar.YEAR));
            contractSequenceRepository.save(aux);
        }

        ob.setContractId(aux.getYear() + "-" + aux.getSequenceValue());
    }
}
