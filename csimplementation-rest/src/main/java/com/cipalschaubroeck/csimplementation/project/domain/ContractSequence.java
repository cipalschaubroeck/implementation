package com.cipalschaubroeck.csimplementation.project.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name = "contract_sequence")
public class ContractSequence {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;
    @Column(name = "year")
    @NotNull
    private int year;

    @Column(name = "sequenceValue")
    @NotNull
    private int sequenceValue;
}
