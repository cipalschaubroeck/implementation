package com.cipalschaubroeck.csimplementation.project.domain;

import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "qualified_person_ad_hoc")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class AdHocQualifiedPerson extends QualifiedPerson {
    @ManyToOne(optional = false)
    @JoinColumn(name = "person", foreignKey = @ForeignKey(name = "fk_qualified_person_ad_hoc"))
    @NotNull
    private Person person;

    @Override
    public QualifiedPersonType getType() {
        return QualifiedPersonType.AD_HOC;
    }

    @Override
    public Person getEffectivePerson() {
        return person;
    }
}
