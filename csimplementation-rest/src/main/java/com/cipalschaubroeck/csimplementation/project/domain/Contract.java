package com.cipalschaubroeck.csimplementation.project.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractAuthorityCollaboratorQF;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import com.cipalschaubroeck.csimplementation.project.domain.listeners.ContractListener;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "contract")
@Getter
@Setter
@EntityListeners(ContractListener.class)
public class Contract {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    // TODO check whether a constraint for this exists in liquibase CSPROC-1994
    @Column(name = "contract_id", updatable = false)
    @NotNull
    private String contractId;


    @ManyToOne
    @JoinColumn(name = "contractor", foreignKey = @ForeignKey(name = "fk_parcel_contractor"))
    private Organization contractor;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "procurement", foreignKey = @ForeignKey(name = "fk_parcel_procurement"))
    private Procurement procurement;

    @Column(name = "decision_making_body")
    private String decisionMakingBody;

    @Column(name = "awarding_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate awardingDate;

    @Column(name = "amount_minus_vat")
    private BigDecimal amountMinusVat;

    @Column(name = "amount_vat")
    private BigDecimal amountVat;

    @Column(name = "vat")
    private BigDecimal vat;

    @Column(name = "name", unique = true)
    @NotNull
    private String name;

    @Column(name = "type")
    private String contractType;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @NotNull
    private ContractStatus status = ContractStatus.IN_PREPARATION;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate endDate;

    @UpdateTimestamp
    @Column(name = "modified_date")
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    private LocalDate modifiedDate;

    @OneToOne(mappedBy = "contract", cascade = CascadeType.REMOVE, orphanRemoval = true)
    private ContractAuthorityRelation contractAuthorityRelation;

    @Column(name = "parcel")
    private String parcel;

    @Column(name = "qualification")
    @Enumerated(EnumType.STRING)
    private ProcurementQualification qualification;

    @Column(name = "procedure_type")
    @Enumerated(EnumType.STRING)
    private ProcurementProcedureType procedureType;

    @Column(name = "project")
    private String project;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "formula", foreignKey = @ForeignKey(name = "fk_formula_contract"))
    private Formula formula;

    @OneToOne
    @JoinColumn(name = "main_contact", referencedColumnName = "id", foreignKey = @ForeignKey(name = "fk_authority_main_contact_1"))
    private ContractAuthorityCollaboratorQF mainAuthorityCollaboratorContact;

    @OneToMany(mappedBy = "contract")
    private List<ContractAuthorityCollaboratorQF> collaborators = new ArrayList<>();

    @OneToMany(mappedBy = "contract")
    private List<RelatedParty> relatedParties = new ArrayList<>();
}
