package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.Map;

@Projection(name = "contractInfo", types = Contract.class)
public interface ContractOverviewDto {
    class Constants {

        private Constants() {
            // utility class
        }

        static final Map<String, String> SORT_ALIASES = Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("clientName", "contractAuthorityRelation.contractingAuthority.name"),
                new AbstractMap.SimpleImmutableEntry<>("valueIncVat", "amountMinusVat"),
                new AbstractMap.SimpleImmutableEntry<>("procurementName", "procurement.name"),
                new AbstractMap.SimpleImmutableEntry<>("projectName", "project"));
    }

    Integer getId();

    String getName();

    // used by jackson
    @SuppressWarnings("unused")
    String getContractId();

    String getContractType();

    ContractStatus getStatus();

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getStartDate();

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getEndDate();

    @Value("#{target.contractAuthorityRelation?.contractingAuthority?.name}")
    String getClientName();

    // TODO CSPROC-2159 define VAT calculation
    // used by jackson
    @SuppressWarnings("unused")
    default BigDecimal getValueIncVat() {
        if (getAmountMinusVat() != null && getAmountVat() != null) {
            return getAmountMinusVat().add(getAmountVat());
        } else {
            return null;
        }
    }

    @JsonIgnore
    BigDecimal getAmountMinusVat();

    @JsonIgnore
    BigDecimal getAmountVat();

    // used by jackson
    @SuppressWarnings("unused")
    @JsonIgnore
    BigDecimal getVat();


    @Value("#{target.procurement?.name}")
    String getProcurementName();

    @Value("#{target.project}")
    String getProjectName();

}
