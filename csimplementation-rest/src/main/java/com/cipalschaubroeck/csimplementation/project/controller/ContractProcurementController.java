package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.service.ContractProcurementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@BasePathAwareController
public class ContractProcurementController {

    @Autowired
    private ContractProcurementService contractProcurementService;

    @PostMapping("/contract/{id}/procurement")
    public ResponseEntity<Void> saveUpdateProcurement(@PathVariable("id") Integer contractId, @RequestBody ProcurementDto procurementDto) {
        contractProcurementService.saveUpdateProcurementInContract(contractId, procurementDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
