package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.project.controller.ProcurementDto;

public interface ContractProcurementService {
    void saveUpdateProcurementInContract(Integer contractId, ProcurementDto procurementDto);
}
