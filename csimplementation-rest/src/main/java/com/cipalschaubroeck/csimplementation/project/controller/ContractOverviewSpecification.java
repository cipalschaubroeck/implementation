package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.cipalschaubroeck.csimplementation.shared.controller.SpecificationSupport.stringContainsIgnoreCase;

@Setter
@Getter
public class ContractOverviewSpecification implements Specification<Contract> {
    private String contractId;
    private String name;
    private String contractType;
    private Set<ContractStatus> status;
    private String clientName;
    private String procurementName;
    private String projectName;
    private BigDecimal valueIncVat;

    private static Optional<Predicate> inStatusPredicate(Expression<String> expression, Set<ContractStatus> contained) {
        if (contained != null && !contained.isEmpty()) {
            return Optional.of(expression.in(contained));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Predicate toPredicate(Root<Contract> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();

        stringContainsIgnoreCase(root.get("contractId"), contractId, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get("name"), name, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get("contractType"), contractType, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get("contractAuthorityRelation").get("contractingAuthority").get("name"), clientName, builder).ifPresent(predicates::add);
        Path<Object> procurement = root.get("procurement");
        stringContainsIgnoreCase(procurement.get("name"), procurementName, builder).ifPresent(predicates::add);
        stringContainsIgnoreCase(root.get("project"), projectName, builder).ifPresent(predicates::add);
        inStatusPredicate(root.get("status"), status).ifPresent(predicates::add);
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
