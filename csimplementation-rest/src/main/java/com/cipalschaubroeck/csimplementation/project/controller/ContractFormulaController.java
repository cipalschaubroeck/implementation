package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.service.ContractFormulaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@BasePathAwareController
public class ContractFormulaController {

    @Autowired
    private ContractFormulaService contractFormulaService;

    @PostMapping("/contract/{id}/prices-review")
    public ResponseEntity<Void> setInProgress(@PathVariable("id") Integer contractId, @RequestBody FormulaDto formulaDto) {
        contractFormulaService.saveFormulaInContract(contractId, formulaDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}
