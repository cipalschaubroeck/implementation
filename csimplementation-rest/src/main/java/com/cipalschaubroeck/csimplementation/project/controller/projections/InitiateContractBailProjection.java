package com.cipalschaubroeck.csimplementation.project.controller.projections;

import com.cipalschaubroeck.csimplementation.project.domain.BailType;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

@Projection(name = "initiateContractBailInfo", types = Contract.class)
public interface InitiateContractBailProjection {

    String getId();

    // used by jackson
    @SuppressWarnings("unused")
    BigDecimal getAmountMinusVat();

    @Value("#{target.procurement?.bail?.percentage}")
    BigDecimal getPercentage();

    @Value("#{target.procurement?.bail?.type}")
    BailType getType();

    @Value("#{target.procurement?.bail?.amount}")
    BigDecimal getAmount();

    @Value("#{target.procurement?.bail?.description}")
    String getDescription();

    ContractStatus getStatus();
}
