package com.cipalschaubroeck.csimplementation.project.config;

public class QualifiedPersonConfigConstants {
    private QualifiedPersonConfigConstants() {
    }

    public static final String QUALIFIED_PERSON_COLLECTION_REL = "qualifiedPersons";
}
