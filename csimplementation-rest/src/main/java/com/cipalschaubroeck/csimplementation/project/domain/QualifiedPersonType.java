package com.cipalschaubroeck.csimplementation.project.domain;

public enum QualifiedPersonType {
    INTERNAL,
    EXTERNAL,
    AD_HOC;

    public static class Name {
        public static final String INTERNAL = "INTERNAL";
        public static final String EXTERNAL = "EXTERNAL";
        public static final String AD_HOC = "AD_HOC";
    }
}
