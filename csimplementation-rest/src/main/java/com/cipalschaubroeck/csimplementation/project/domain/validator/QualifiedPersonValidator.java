package com.cipalschaubroeck.csimplementation.project.domain.validator;

import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.repository.QualifiedPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.Objects;

@Component
public class QualifiedPersonValidator implements Validator {

    private static final String ERROR_MESSAGE = "qualified-person.one-per-contract";

    @Autowired
    private QualifiedPersonRepository repository;

    @Override
    public boolean supports(Class<?> clazz) {
        return QualifiedPerson.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        validateQualifiedPerson((QualifiedPerson) target, errors);
    }

    /**
     * A single person may be contact, collaborator and ad hoc qualified person, but only
     * in different projects
     *
     * @param target a qualified person that is about to be saved
     * @param errors a collection of error messages
     */
    private void validateQualifiedPerson(QualifiedPerson target, Errors errors) {
        List<QualifiedPerson> all = repository.findByContract(target.getContract());
        if (all.stream().anyMatch(qp -> Objects.equals(qp.getEffectivePerson().getId(), target.getEffectivePerson().getId())
                && !Objects.equals(qp.getId(), target.getId()))) {
            String fieldName;
            switch (target.getType()) {
                case AD_HOC:
                    fieldName = "person";
                    break;
                case INTERNAL:
                    fieldName = "collaborator";
                    break;
                case EXTERNAL:
                    fieldName = "contact";
                    break;
                default:
                    throw new IllegalArgumentException("Unknown qualified person type");
            }
            errors.rejectValue(fieldName, ERROR_MESSAGE);
        }
    }
}
