package com.cipalschaubroeck.csimplementation.project.domain;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "qualified_person_external")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class ExternalQualifiedPerson extends QualifiedPerson {
    @ManyToOne(optional = false)
    @JoinColumns(foreignKey = @ForeignKey(name = "fk_external_qualified_contact"),
            value = {@JoinColumn(name = "person", referencedColumnName = "person"),
                    @JoinColumn(name = "organization", referencedColumnName = "organization")})
    @NotNull
    private ContactPerson contact;

    @Override
    public QualifiedPersonType getType() {
        return QualifiedPersonType.EXTERNAL;
    }

    @Override
    public Person getEffectivePerson() {
        return contact.getPerson();
    }
}
