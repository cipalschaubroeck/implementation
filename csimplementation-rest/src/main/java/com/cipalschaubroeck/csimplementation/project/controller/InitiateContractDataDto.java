package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import com.cipalschaubroeck.csimplementation.project.domain.ProcurementProcedureType;
import com.cipalschaubroeck.csimplementation.project.domain.ProcurementQualification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.math.BigDecimal;

@Projection(name = "initiateContractDataInfo", types = Contract.class)
public interface InitiateContractDataDto {

    String getContractId();

    String getName();

    ContractStatus getStatus();


    // TODO CSPROC-2159 define VAT calculation
    // used by jackson
    @SuppressWarnings("unused")
    default BigDecimal getValueIncVat() {
        if (getAmountMinusVat() != null && getAmountVat() != null) {
            return getAmountMinusVat().add(getAmountVat());
        } else {
            return null;
        }
    }

    BigDecimal getAmountMinusVat();

    BigDecimal getAmountVat();

    // used by jackson
    @SuppressWarnings("unused")
    BigDecimal getVat();

    // used by jackson
    @SuppressWarnings("unused")
    String getDecisionMakingBody();

    @Value("#{target.procurement?.name}")
    String getProcurementName();

    @Value("#{target.project}")
    String getProjectName();

    @Value("#{target.qualification}")
    ProcurementQualification getQualification();

    @Value("#{target.procedureType}")
    ProcurementProcedureType getProcedureType();

    @Value("#{target.parcel}")
    String getParcel();

    @Value("#{target.procurement?.description}")
    String getDescription();

    @Value("#{target.procurement?.referenceNumber}")
    String getReferenceNumber();

}
