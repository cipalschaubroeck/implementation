package com.cipalschaubroeck.csimplementation.project.repository;

import com.cipalschaubroeck.csimplementation.project.config.QualifiedPersonConfigConstants;
import com.cipalschaubroeck.csimplementation.project.domain.InternalQualifiedPerson;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "qualified-person-internal",
        collectionResourceRel = QualifiedPersonConfigConstants.QUALIFIED_PERSON_COLLECTION_REL)
public interface InternalQualifiedPersonRepository extends PagingAndSortingRepository<InternalQualifiedPerson, Integer> {
}
