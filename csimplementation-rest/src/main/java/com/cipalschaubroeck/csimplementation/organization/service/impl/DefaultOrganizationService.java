package com.cipalschaubroeck.csimplementation.organization.service.impl;

import com.cipalschaubroeck.csimplementation.organization.controller.BasicAddressInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.ContactPersonInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.JointVentureInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.OrganizationInfo;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.JointVenture;
import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;
import com.cipalschaubroeck.csimplementation.organization.domain.PersonFromContacts;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;
import com.cipalschaubroeck.csimplementation.organization.repository.JointVentureRepository;
import com.cipalschaubroeck.csimplementation.organization.repository.OrganizationRepository;
import com.cipalschaubroeck.csimplementation.organization.repository.PersonFromContactsRepository;
import com.cipalschaubroeck.csimplementation.organization.repository.SingleOrganizationRepository;
import com.cipalschaubroeck.csimplementation.organization.service.OrganizationBusinessService;
import com.cipalschaubroeck.csimplementation.organization.service.OrganizationHelper;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import com.greenvalley.contacts.domain.search.OrganizationSearchCriterion;
import com.greenvalley.contacts.domain.search.PersonInOrganizationSearchCriterion;
import com.greenvalley.contacts.service.OrganizationService;
import com.greenvalley.contacts.service.PersonInOrganizationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
// spring managed
@SuppressWarnings("unused")
class DefaultOrganizationService implements OrganizationBusinessService {

    @Autowired
    private OrganizationService organizationService;

    @Autowired
    private PersonInOrganizationService personInOrganizationService;

    @Autowired
    private OrganizationHelper helper;

    @Autowired
    private JointVentureRepository jointVentureRepository;

    @Autowired
    private SingleOrganizationRepository singleOrganizationRepository;

    @Autowired
    private OrganizationRepository organizationRepository;

    @Autowired
    private PersonFromContactsRepository personFromContactsRepository;

    /**
     * @param info         data that should be
     * @param organization data that is
     * @return OrganizationInfo built with the saved organization
     */
    private OrganizationInfo saveOrganization(OrganizationInfo info, Organization organization) {
        Organization org = helper.mapOrganization(info, organization);
        return helper.mapOrganizationInfo(organizationService.save(org));
    }

    @Override
    public OrganizationInfo updateOrganization(OrganizationInfo info, String orgId) {
        Organization organization = Optional.ofNullable(organizationService.detailById(orgId)).orElseThrow(EntityNotFoundException::new);
        return saveOrganization(info, organization);
    }

    @Transactional
    public OrganizationInfo updateJointVenture(OrganizationInfo info, Integer orgId) {
        JointVenture venture = jointVentureRepository.findById(orgId).orElseThrow(EntityNotFoundException::new);
        setContent(info, venture);
        return fromJointVenture(venture);
    }

    @Transactional
    public OrganizationInfo updateJointVenture(JointVentureInfo info, Integer orgId) {
        JointVenture venture = jointVentureRepository.findById(orgId).orElseThrow(EntityNotFoundException::new);
        setContent(info, venture);
        return fromJointVenture(venture);
    }

    @Override
    @Transactional
    public OrganizationInfo saveOrganization(OrganizationInfo info) {
        if (info.getType() == OrganizationType.SINGLE_ORGANIZATION) {
            return saveOrganization(info, null);
        } else {
            JointVenture venture = new JointVenture();
            setContent((JointVentureInfo) info, venture);
            JointVenture saved = jointVentureRepository.save(venture);
            return fromJointVenture(saved);
        }
    }

    /**
     * @param info    data that should be
     * @param venture data that is
     */
    private void setContent(OrganizationInfo info, JointVenture venture) {
        venture.setName(info.getLegalName());
        PostalAddress address = venture.getPostalAddress();
        if (address == null) {
            address = new PostalAddress();
            venture.setPostalAddress(address);
        }
        address.setCountry(info.getBasicAddress().getCountry());
        address.setStreet(info.getBasicAddress().getStreetName());
        address.setNumber(info.getBasicAddress().getHouseNumber());
        address.setPostalCode(info.getBasicAddress().getZipCode());
        address.setMunicipality(info.getBasicAddress().getCity());
        address.setBox(info.getBasicAddress().getLocator());
        venture.setPostalAddress(address);
    }

    /**
     * @param info    data that should be
     * @param venture data that is
     */
    private void setContent(JointVentureInfo info, JointVenture venture) {
        setContent((OrganizationInfo) info, venture);
        venture.getMembers().removeIf(asIsMember -> info.getMembers().stream().noneMatch(
                toBeMember -> StringUtils.equalsIgnoreCase(asIsMember.getExternalId(), toBeMember.getId())));
        info.getMembers().stream()
                .filter(toBeMember -> venture.getMembers().stream()
                        .noneMatch(asIsMember -> StringUtils.equalsIgnoreCase(toBeMember.getId(), asIsMember.getExternalId())))
                .map(toBeMember -> singleOrganizationRepository
                        .findByExternalId(toBeMember.getId())
                        .orElseGet(() -> new SingleOrganization(organizationService.detailById(toBeMember.getId())))
                ).forEach(venture.getMembers()::add);
        if (info.getMainContact() == null || StringUtils.isBlank(info.getMainContact().getExternalPersonInOrganizationId())) {
            venture.setMainContact(null);
        } else if (venture.getMainContact() == null) {
            PersonInOrganization pio = personInOrganizationService.detailById(info.getMainContact().getExternalPersonInOrganizationId());
            venture.setMainContact(new ContactPerson(pio,
                    singleOrganizationRepository.findByExternalId(pio.getOrganization().getId()).orElseGet(() -> new SingleOrganization(pio.getOrganization())),
                    personFromContactsRepository.findByExternalId(pio.getPerson().getId()).orElseGet(() -> new PersonFromContacts(pio.getPerson()))));
        } else if (!StringUtils.equals(info.getMainContact().getExternalPersonInOrganizationId(), venture.getMainContact().getExternalId())) {
            PersonInOrganization pio = personInOrganizationService.detailById(info.getMainContact().getExternalPersonInOrganizationId());
            venture.getMainContact().updateExternals(pio,
                    singleOrganizationRepository.findByExternalId(pio.getOrganization().getId()).orElseGet(() -> new SingleOrganization(pio.getOrganization())),
                    personFromContactsRepository.findByExternalId(pio.getPerson().getId()).orElseGet(() -> new PersonFromContacts(pio.getPerson())));
        }
    }

    @Override
    public OrganizationInfo detailById(String id) {
        return helper.mapOrganizationInfo(organizationService.detailById(id));
    }

    @Override
    @Transactional
    public OrganizationInfo detailById(Integer id) {
        return fromJointVenture(jointVentureRepository.findById(id).orElseThrow(EntityNotFoundException::new));
    }

    @Override
    public void deleteOrganization(String id) {
        // TODO CSPROC-2196 create Exception handler for entity not found exception
        Organization organization = Optional.ofNullable(organizationService.detailById(id)).orElseThrow(EntityNotFoundException::new);
        organizationService.delete(organization.getId());
    }

    @Override
    public void deleteOrganization(Integer id) {
        jointVentureRepository.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganizationInfo> findOrganizations() {
        List<Organization> organizations = organizationService.find(new OrganizationSearchCriterion.Builder().build());

        List<OrganizationInfo> agentInfoList = new ArrayList<>();
        organizations.forEach(organization ->
                agentInfoList.add(helper.mapOrganizationInfo(organization)));
        jointVentureRepository.findAll().forEach(jv -> agentInfoList.add(fromJointVenture(jv)));
        return agentInfoList;
    }

    @Override
    @Transactional(readOnly = true)
    public List<OrganizationInfo> findOrganizations(OrganizationType type) {
        List<OrganizationInfo> agentInfoList;
        switch (type) {
            case SINGLE_ORGANIZATION:
                List<Organization> organizations = organizationService.find(new OrganizationSearchCriterion.Builder().build());
                agentInfoList = new ArrayList<>();
                organizations.forEach(organization ->
                        agentInfoList.add(helper.mapOrganizationInfo(organization)));
                return agentInfoList;
            case JOINT_VENTURE:
                agentInfoList = new ArrayList<>();
                jointVentureRepository.findAll().forEach(jv -> agentInfoList.add(fromJointVenture(jv)));
                return agentInfoList;
            default:
                return findOrganizations();
        }
    }

    private JointVentureInfo fromJointVenture(JointVenture jv) {
        JointVentureInfo info = new JointVentureInfo();
        Optional.ofNullable(jv.getId()).map(Object::toString).ifPresent(info::setId);
        info.setLegalName(jv.getName());
        info.setType(jv.getType());
        BasicAddressInfo addressInfo = new BasicAddressInfo();
        info.setBasicAddress(addressInfo);
        if (jv.getAddress() != null) {
            addressInfo.setCity(jv.getAddress().getMunicipality());
            addressInfo.setCountry(jv.getAddress().getCountry());
            addressInfo.setHouseNumber(jv.getAddress().getNumber());
            addressInfo.setLocator(jv.getAddress().getBox());
            addressInfo.setStreetName(jv.getAddress().getStreet());
            addressInfo.setZipCode(jv.getAddress().getPostalCode());
        }
        jv.getMembers().stream()
                .map(so -> detailById(so.getExternalId()))
                .forEach(info.getMembers()::add);
        if (jv.getMainContact() != null) {
            ContactPersonInfo contactInfo = new ContactPersonInfo();
            contactInfo.setFullName(jv.getMainContact().getEffectivePerson().getFullName());
            contactInfo.setExternalOrganizationId(jv.getMainContact().getOrganization().getExternalId());
            contactInfo.setExternalPersonInOrganizationId(jv.getMainContact().getExternalId());
            info.setMainContact(contactInfo);
        }
        return info;
    }

    @Override
    public List<ContactPersonInfo> contactsOf(String id) {
        return personInOrganizationService.find(new PersonInOrganizationSearchCriterion.Builder()
                .withOrganizationId(id).build())
                .stream()
                .map(pio -> {
                    ContactPersonInfo info = new ContactPersonInfo();
                    info.setFullName(pio.getPerson().getDisplayName());
                    info.setExternalPersonInOrganizationId(pio.getId());
                    info.setExternalOrganizationId(pio.getOrganization().getId());
                    return info;
                })
                .collect(Collectors.toList());
    }
}
