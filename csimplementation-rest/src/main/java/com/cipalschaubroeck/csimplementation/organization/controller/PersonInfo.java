package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonInfo extends AgentInfo {
    private String identityNumber;
    private String firstName;
    private String familyName;
    private Gender gender = Gender.FEMALE;
}
