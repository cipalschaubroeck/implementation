package com.cipalschaubroeck.csimplementation.organization.domain.validation;

import javax.validation.Constraint;

@Constraint(validatedBy = JointVentureContactValidator.class)
public @interface JointVentureValidContact {
}
