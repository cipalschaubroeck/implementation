package com.cipalschaubroeck.csimplementation.organization.config;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.greenvalley.contacts.service.PersonInOrganizationService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.PostLoad;

@Configurable(autowire = Autowire.BY_TYPE)
public class ContactPersonListener {
    @Autowired
    private PersonInOrganizationService personInOrganizationService;

    @PostLoad
    public void setDelegate(ContactPerson contactPerson) {
        contactPerson.setPersonInOrganization(personInOrganizationService.detailById(contactPerson.getExternalId()));
    }
}
