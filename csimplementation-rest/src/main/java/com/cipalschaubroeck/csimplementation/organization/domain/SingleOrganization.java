package com.cipalschaubroeck.csimplementation.organization.domain;

import com.cipalschaubroeck.csimplementation.organization.config.SingleOrganizationListener;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * single organization as opposed to joint ventures
 */
@Entity
@Table(name = "organization_single")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EntityListeners(SingleOrganizationListener.class)
public class SingleOrganization extends Organization {

    @Getter
    @Setter
    @Column(name = "external_id")
    private String externalId;

    @Transient
    @Setter
    @Delegate(types = SingleOrganizationInterfaced.class)
    private com.greenvalley.contacts.domain.contact.Organization delegate;

    /**
     * @param delegate external data
     */
    public SingleOrganization(com.greenvalley.contacts.domain.contact.Organization delegate) {
        this.delegate = delegate;
        this.externalId = delegate.getId();
    }

    @Override
    public String getName() {
        return delegate.getLegalName();
    }

    @Override
    public OrgAddress getAddress() {
        if (delegate.getBasicAddress() == null) {
            return null;
        } else {
            return new OrgAddress.OrgBasicAddress(delegate.getBasicAddress());
        }
    }

    @Override
    public OrganizationType getType() {
        return OrganizationType.SINGLE_ORGANIZATION;
    }

    /**
     * way of delegating methods to external organization shorter than overriding each
     */
    interface SingleOrganizationInterfaced {
        @SuppressWarnings("unused")
            // used to signal what to delegate
        String getKboNumber();

        @SuppressWarnings("unused")
            // used to signal what to delegate
        void setKboNumber(String kboNumber);

        @SuppressWarnings("unused")
            // used to signal what to delegate
        String getVatNumber();

        @SuppressWarnings("unused")
            // used to signal what to delegate
        void setVatNumber(String vatNumber);

        @SuppressWarnings("unused")
            // used to signal what to delegate
        boolean isSme();

        @SuppressWarnings("unused")
            // used to signal what to delegate
        void setSme(boolean sme);
    }
}
