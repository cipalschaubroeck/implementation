package com.cipalschaubroeck.csimplementation.organization.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
public class ContactPersonId implements Serializable {
    @Column(name = "person")
    @NotNull
    private Integer person;

    @Column(name = "organization")
    @NotNull
    private Integer organization;
}
