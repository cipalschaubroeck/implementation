package com.cipalschaubroeck.csimplementation.organization.repository;

import com.cipalschaubroeck.csimplementation.organization.config.OrganizationConfigConstants;
import com.cipalschaubroeck.csimplementation.organization.domain.JointVenture;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository("jointVentureRepo")
@RepositoryRestResource(path = "joint-venture",
        collectionResourceRel = OrganizationConfigConstants.ORGANIZATION_COLLECTION_REL)
public interface JointVentureRepository extends PagingAndSortingRepository<JointVenture, Integer> {
}
