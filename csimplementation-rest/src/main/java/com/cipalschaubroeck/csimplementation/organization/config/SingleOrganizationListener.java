package com.cipalschaubroeck.csimplementation.organization.config;

import com.cipalschaubroeck.csimplementation.lazy.LazyObject;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.PostLoad;

@Configurable(autowire = Autowire.BY_TYPE)
public class SingleOrganizationListener {
    @Autowired
    private OrganizationService organizationService;

    @PostLoad
    public void setDelegate(SingleOrganization so) {
        so.setDelegate(LazyObject.build(Organization.class, so.getExternalId(), organizationService::detailById));
    }
}
