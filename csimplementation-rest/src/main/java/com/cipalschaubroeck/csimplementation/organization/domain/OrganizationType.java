package com.cipalschaubroeck.csimplementation.organization.domain;

public enum OrganizationType {
    SINGLE_ORGANIZATION,
    JOINT_VENTURE;

    public static class Name {
        static final String SINGLE_ORGANIZATION = "SINGLE_ORGANIZATION";
        static final String JOINT_VENTURE = "JOINT_VENTURE";
    }
}
