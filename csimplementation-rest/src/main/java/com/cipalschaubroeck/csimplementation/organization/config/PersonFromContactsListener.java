package com.cipalschaubroeck.csimplementation.organization.config;

import com.cipalschaubroeck.csimplementation.organization.domain.PersonFromContacts;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.service.PersonService;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.PostLoad;
import java.util.Collections;

@Configurable(autowire = Autowire.BY_TYPE)
public class PersonFromContactsListener {

    @Autowired
    private PersonService personService;

    @PostLoad
    public void setDelegate(PersonFromContacts person) {
        Person external = personService.findSingle(Collections.emptySet(), person.getExternalId());
        person.setDelegate(external);
    }
}
