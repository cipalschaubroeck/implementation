package com.cipalschaubroeck.csimplementation.organization.service.mock;

import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.contact.Gender;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import com.greenvalley.contacts.domain.search.PersonSearchCriterion;
import com.greenvalley.contacts.service.PersonService;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


@Component
@Profile("!contacts")
// TODO CSPROC-2194 separate configuration from logic
@Log4j
public class PersonServiceMock implements PersonService {
    private static final int SIZE_1 = 1;

    private Map<String, Person> personsById = createPersons();
    private static final String JOHNNIE_ID = "1";
    private static final String JACKDANIELDS_ID = "2";
    private static final String JIMBEAN_ID = "3";
    private static final String EZRABROOKS_ID = "4";
    private static final String VIRGINIA_ID = "5";
    private static int counter = 50;

    private Map<String, Person> createPersons() {
        Map<String, Person> persons = new HashMap<>();
        persons.put(JOHNNIE_ID, johnnieWalker());
        persons.put(JACKDANIELDS_ID, jackDaniel());
        persons.put(JIMBEAN_ID, jimBeam());
        persons.put(EZRABROOKS_ID, ezraBrooks());
        persons.put(VIRGINIA_ID, virginiaGentleman());
        return persons;
    }

    public Collection<Person> getAll() {
        Collection<Person> personsHashMapValues = personsById.values();
        List<Person> personList = new ArrayList<>(personsHashMapValues.size());
        personList.addAll(personsHashMapValues);
        Collections.sort(personList);
        return personList;
    }

    @Override
    public Person detailById(String id) {
        log.debug("retrieving details of " + id);
        final Person person = personsById.get(id.toLowerCase());
        if (person != null) {
            log.debug("Found person with id " + id + " : " + person);
        } else {
            log.warn("No person found with id " + id);
        }
        return person;
    }

    @Override
    public List<Person> find(Set<PersonSearchCriterion> personSearchCriterions) {
        List<Person> results = new ArrayList<>();
        for (PersonSearchCriterion item : personSearchCriterions) {
            if (item.getKey().equals(PersonSearchCriterion.Key.FIRST_NAME) || item.getKey().equals(PersonSearchCriterion.Key.LAST_NAME)) {
                results.addAll(addPeopleByFirstNameOrLastName(item.getValue()));
            }
        }
        return results;
    }

    @Override
    public List<Person> find(Set<PersonSearchCriterion> set, Predicate.BooleanOperator booleanOperator) {
        return new ArrayList<>();
    }

    private Collection<? extends Person> addPeopleByFirstNameOrLastName(String lastName) {
        List<Person> results = new ArrayList<>();
        for (Map.Entry<String, Person> entry : personsById.entrySet()) {
            Person person = personsById.get(entry.getKey());
            if (person.getFamilyName().toLowerCase().contains(lastName.toLowerCase()) ||
                    StringUtils.isEmpty(lastName) ||
                    person.getFirstName().toLowerCase().contains(lastName.toLowerCase())) {
                results.add(person);
            }
        }
        return results;
    }


    @Override
    public Person findSingle(Set<PersonSearchCriterion> personSearchCriterions) {
        for (PersonSearchCriterion item : personSearchCriterions) {
            if (item.getKey().equals(PersonSearchCriterion.Key.FIRST_NAME) || item.getKey().equals(PersonSearchCriterion.Key.LAST_NAME)) {
                List<Person> filteredByFirstName = new ArrayList<>(addPeopleByFirstNameOrLastName(item.getValue()));
                if (filteredByFirstName.size() == SIZE_1) {
                    return filteredByFirstName.get(0);
                }
            }
        }
        return null;
    }

    @Override
    public Person findSingle(Set<PersonSearchCriterion> personSearchCriterions, String s) {
        if (StringUtils.isNotBlank(s)) {
            return personsById.get(s);
        } else {
            return null;
        }
    }


    private Person johnnieWalker() {
        return createSamplePerson(JOHNNIE_ID, "Johnnie", "Walker", Gender.MALE);
    }

    private Person jackDaniel() {
        return createSamplePerson(JACKDANIELDS_ID, "Jack", "Daniel", Gender.MALE);
    }

    private Person jimBeam() {
        return createSamplePerson(JIMBEAN_ID, "Jim", "Beam", Gender.MALE);
    }

    private Person ezraBrooks() {
        return createSamplePerson(EZRABROOKS_ID, "Ezra", "Brooks", Gender.FEMALE);
    }

    private Person virginiaGentleman() {
        return createSamplePerson(VIRGINIA_ID, "Virginia", "Gentleman", Gender.FEMALE);
    }

    private Person createSamplePerson(String id, String firstName, String lastName, Gender gender) {
        Person person = new Person(id);
        person.setFirstName(firstName);
        person.setFamilyName(lastName);
        person.setGender(gender);
        Contact contact = new Contact();
        contact.setId(Long.valueOf(id));
        contact.setEmail("etendering+" + firstName + lastName + "@greenvalleybelgium.be");
        contact.setWebsite("www.greenvalleybelgium.be");
        contact.setPhoneNumber("+32 11 36 24 01");
        contact.setFax("+32 15 27 17 05");
        person.setContact(contact);
        BasicAddress basicAddress = greenValleyBelgium();
        basicAddress.setId(Long.valueOf(id));
        person.setBasicAddress(basicAddress);
        person.setBasicAddress(greenValleyBelgium());
        return person;
    }


    private BasicAddress greenValleyBelgium() {
        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setId(1L);
        basicAddress.setStreetName("Jaarbeurslaan");
        basicAddress.setHouseNumber("25");
        basicAddress.setLocator("21");
        basicAddress.setZipCode("3600");
        basicAddress.setCity("Genk");
        basicAddress.setCountry("BE");
        return basicAddress;
    }

    @Override
    public Person save(Person person) {
        if (StringUtils.isEmpty(person.getId())) {
            counter++;
            person.setId(String.valueOf(counter));
        }
        personsById.put(person.getId(), person);
        return person;
    }

    @Override
    public void delete(String id) {
        personsById.remove(id);
    }
}
