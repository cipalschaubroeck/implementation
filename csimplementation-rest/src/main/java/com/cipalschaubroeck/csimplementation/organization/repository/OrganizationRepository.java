package com.cipalschaubroeck.csimplementation.organization.repository;

import com.cipalschaubroeck.csimplementation.organization.config.OrganizationConfigConstants;
import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository("organizationRepo")
@RepositoryRestResource(path = "organization",
        collectionResourceRel = OrganizationConfigConstants.ORGANIZATION_COLLECTION_REL)
public interface OrganizationRepository extends PagingAndSortingRepository<Organization, Integer> {
}
