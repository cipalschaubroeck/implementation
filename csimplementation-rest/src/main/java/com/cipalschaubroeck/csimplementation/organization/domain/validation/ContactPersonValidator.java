package com.cipalschaubroeck.csimplementation.organization.domain.validation;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import org.apache.commons.lang3.StringUtils;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ContactPersonValidator implements ConstraintValidator<ContactPersonValid, ContactPerson> {
    @Override
    public boolean isValid(ContactPerson value, ConstraintValidatorContext context) {
        return StringUtils.equals(value.getPerson().getExternalId(), value.getPersonInOrganization().getPerson().getId())
                && StringUtils.equals(value.getOrganization().getExternalId(), value.getPersonInOrganization().getOrganization().getId());
    }
}
