package com.cipalschaubroeck.csimplementation.organization.domain;

import com.cipalschaubroeck.csimplementation.organization.config.PersonFromContactsListener;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

/**
 * person based on an external person from ContactsInterfacer
 */
@Entity
@Table(name = "person_contacts")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EntityListeners(PersonFromContactsListener.class)
public class PersonFromContacts extends Person {
    @Column(name = "external_id")
    @NotNull
    @Getter
    @Setter
    private String externalId;

    @Transient
    private com.greenvalley.contacts.domain.contact.Person delegate;

    public PersonFromContacts(com.greenvalley.contacts.domain.contact.Person delegate) {
        externalId = delegate.getId();
        setDelegate(delegate);
    }

    public void setDelegate(com.greenvalley.contacts.domain.contact.Person delegate) {
        this.delegate = delegate;
        externalId = delegate.getId();
        super.setFirstName(delegate.getFirstName());
        super.setLastName(delegate.getFamilyName());
        if (delegate.getContact() == null) {
            this.setPhone(null);
            this.setEmail(null);
            this.getPhoneNumbers().clear();
            this.getEmails().clear();
        } else {
            updateEmail();
            updatePhone();
        }
        updateAddress();
    }

    private boolean addressNotBlank(BasicAddress address) {
        return address != null && StringUtils.isNotBlank(address.getCountry());
    }

    private void updateAddress() {
        if (addressNotBlank(delegate.getBasicAddress())) {
            if (getAddress() != null) {
                updatePostalAddress();
                if (getAddresses().stream().noneMatch(e -> Objects.equals(e.getId(), getAddress().getId()))) {
                    getAddresses().add(getAddress());
                }
            } else {
                PostalAddress address = new PostalAddress();
                setAddress(address);
                updatePostalAddress();
                Comparator<PostalAddress> comparator = Comparator.comparing(PostalAddress::getMunicipality)
                        .thenComparing(PostalAddress::getCountry)
                        .thenComparing(PostalAddress::getNumber)
                        .thenComparing(PostalAddress::getBox)
                        .thenComparing(PostalAddress::getStreet)
                        .thenComparing(PostalAddress::getPostalCode);
                if (getAddresses().stream().noneMatch(e -> comparator.compare(e, address) == 0)) {
                    getAddresses().add(address);
                }
            }
        } else {
            this.setAddress(null);
            this.getAddresses().clear();
        }
    }

    private void updatePostalAddress() {
        getAddress().setMunicipality(delegate.getBasicAddress().getCity());
        getAddress().setCountry(delegate.getBasicAddress().getCountry());
        getAddress().setNumber(delegate.getBasicAddress().getHouseNumber());
        getAddress().setBox(delegate.getBasicAddress().getLocator());
        getAddress().setStreet(delegate.getBasicAddress().getStreetName());
        getAddress().setPostalCode(delegate.getBasicAddress().getZipCode());
    }

    private void updatePhone() {
        if (StringUtils.isNotBlank(delegate.getContact().getPhoneNumber())) {
            if (getPhone() != null) {
                getPhone().setPhone(delegate.getContact().getPhoneNumber());
                if (getPhoneNumbers().stream().noneMatch(e -> Objects.equals(e.getId(), getPhone().getId()))) {
                    getPhoneNumbers().add(getPhone());
                }
            } else {
                PhoneNumber phoneNumber = new PhoneNumber();
                phoneNumber.setPhone(delegate.getContact().getPhoneNumber());
                setPhone(phoneNumber);
                if (getPhoneNumbers().stream().noneMatch(e -> StringUtils.equals(e.getPhone(), phoneNumber.getPhone()))) {
                    getPhoneNumbers().add(phoneNumber);
                }
            }
        } else {
            this.setPhone(null);
            this.getPhoneNumbers().clear();
        }
    }

    private void updateEmail() {
        if (StringUtils.isNotBlank(delegate.getContact().getEmail())) {
            if (this.getEmail() != null) {
                getEmail().setEmail(delegate.getContact().getEmail());
                if (getEmails().stream().noneMatch(e -> Objects.equals(e.getId(), getEmail().getId()))) {
                    getEmails().add(getEmail());
                }
            } else {
                EmailAddress email = new EmailAddress();
                email.setEmail(delegate.getContact().getEmail());
                setEmail(email);
                if (getEmails().stream().noneMatch(e -> StringUtils.equalsIgnoreCase(e.getEmail(), email.getEmail()))) {
                    getEmails().add(email);
                }
            }
        } else {
            this.setEmail(null);
            this.getEmails().clear();
        }
    }

    @Override
    public String getFirstName() {
        return delegate.getFirstName();
    }

    @Override
    public String getLastName() {
        return delegate.getFamilyName();
    }

    @Override
    public void setFirstName(String firstName) {
        delegate.setFirstName(firstName);
    }

    @Override
    public void setLastName(String lastName) {
        delegate.setFamilyName(lastName);
    }

    @Override
    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        super.setPhoneNumbers(phoneNumbers);
        if (phoneNumbers.isEmpty()) {
            setPhone(null);
        } else {
            setPhone(phoneNumbers.get(0));
        }
    }

    @Override
    public void setEmails(List<EmailAddress> emails) {
        super.setEmails(emails);
        if (emails.isEmpty()) {
            setEmail(null);
        } else {
            setEmail(emails.get(0));
        }
    }

    @Override
    public void setAddresses(List<PostalAddress> addresses) {
        super.setAddresses(addresses);
        if (addresses.isEmpty()) {
            setAddress(null);
        } else {
            setAddress(addresses.get(0));
        }
    }

    @Override
    public void setEmail(EmailAddress email) {
        super.setEmail(email);
        if (email != null) {
            if (delegate.getContact() == null) {
                delegate.setContact(new Contact());
            }
            delegate.getContact().setEmail(email.getEmail());
        } else if (delegate.getContact() != null) {
            delegate.getContact().setEmail(null);
        }
    }

    @Override
    public void setPhone(PhoneNumber phone) {
        super.setPhone(phone);
        if (phone != null) {
            if (delegate.getContact() == null) {
                delegate.setContact(new Contact());
            }
            delegate.getContact().setPhoneNumber(phone.getPhone());
        } else if (delegate.getContact() != null) {
            delegate.getContact().setPhoneNumber(null);
        }
    }

    @Override
    public void setAddress(PostalAddress address) {
        super.setAddress(address);
        if (address != null) {
            if (delegate.getBasicAddress() == null) {
                delegate.setBasicAddress(new BasicAddress());
            }
            delegate.getBasicAddress().setCity(address.getMunicipality());
            delegate.getBasicAddress().setCountry(address.getCountry());
            delegate.getBasicAddress().setHouseNumber(address.getNumber());
            delegate.getBasicAddress().setLocator(address.getBox());
            delegate.getBasicAddress().setStreetName(address.getStreet());
            delegate.getBasicAddress().setZipCode(address.getPostalCode());
        } else {
            delegate.setBasicAddress(null);
        }
    }
}
