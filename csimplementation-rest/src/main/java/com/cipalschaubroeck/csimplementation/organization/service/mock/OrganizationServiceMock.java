package com.cipalschaubroeck.csimplementation.organization.service.mock;

import com.cipalschaubroeck.csimplementation.organization.exceptions.InvalidKboNumberException;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import com.greenvalley.contacts.domain.search.OrganizationSearchCriterion;
import com.greenvalley.contacts.exception.InvalidResponseException;
import com.greenvalley.contacts.service.OrganizationService;
import lombok.extern.log4j.Log4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Component
@Profile("!contacts")
@Log4j
// TODO CSPROC-2194 separate configuration from logic
public class OrganizationServiceMock implements OrganizationService {

    private static final int SIZE_1 = 1;
    private Map<String, Organization> organizationsById = createLegalEntities();
    private static final String MAES_ID = "1";
    private static final String DUVEL_ID = "2";
    private static final String LEFFE_ID = "3";
    private static final String CHIMAY_ID = "4";
    private static final String JUDAS_ID = "5";
    private static final String MOCK_ID = "6";
    private static final String CITY_OF_GENK_ID = "7";
    private static final String CITY_OF_HASSELT_ID = "8";
    private static final String CITY_OF_GEEL_ID = "9";
    private static int counter = 50;

    private static final String MAES_VAT = "0405655681";
    private static final String DUVEL_VAT = "0400764903";
    private static final String LEFFE_VAT = "0413193769";
    private static final String CHIMAY_VAT = "0100123202";
    private static final String JUDAS_VAT = "0501509596";
    private static final String MOCK_VAT = "0000000097";
    private static final String KBO_CITY_OF_GENK = "0207201797";
    private static final String KBO_CITY_OF_HASSELT = "0207466964";
    private static final String KBO_CITY_OF_GEEL = "0207533874";

    private Map<String, Organization> createLegalEntities() {
        Map<String, Organization> legalEntities = new HashMap<>();
        legalEntities.put(MAES_ID, maes());
        legalEntities.put(DUVEL_ID, duvel());
        legalEntities.put(LEFFE_ID, leffe());
        legalEntities.put(CHIMAY_ID, chimay());
        legalEntities.put(JUDAS_ID, judas());
        legalEntities.put(MOCK_ID, mockBeer());

        legalEntities.put(CITY_OF_GENK_ID, cityOfGenk());
        legalEntities.put(CITY_OF_HASSELT_ID, cityOfHasselt());
        legalEntities.put(CITY_OF_GEEL_ID, cityOfGeel());
        return legalEntities;
    }

    private String createEmailAddress(String legalEntityId) {
        // email addresses should physically be deliverable to etendering@greenvalleybelgium.be
        return "etendering+" + legalEntityId + "@greenvalleybelgium.be";
    }

    private Organization maes() {
        final Contact contact = createContact(Long.valueOf(MAES_ID), "maes", "+32 15 30 90 11", "+32 15 31 41 91", "www.alkenmaes.be");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(MAES_ID), "Blarenberglaan", "3", "c", "2800", "Mechelen", "BE");
        return createSampleLegalEntity(MAES_ID, "Maes", MAES_VAT, MAES_VAT, contact, basicAddress);
    }

    private Organization duvel() {
        final Contact contact = createContact(Long.valueOf(DUVEL_ID), "duvel", "+32 3 860 94 00", "+32 3 860 46 22", "www.duvelmoortgat.be");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(DUVEL_ID), "Breendonk-Dorp", "58", null, "2870", "Puurs", "BE");
        return createSampleLegalEntity(DUVEL_ID, "Duvel", DUVEL_VAT, DUVEL_VAT, contact, basicAddress);
    }

    private Organization leffe() {
        final Contact contact = createContact(Long.valueOf(LEFFE_ID), "leffe", "+32 3 456 94 01", "+32 3 860 46 23", "www.leffe.com");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(LEFFE_ID), "congostraat", "45", null, "2047", "Lovaina", "BE");
        return createSampleLegalEntity(LEFFE_ID, "Leffe", LEFFE_VAT, LEFFE_VAT, contact, basicAddress);
    }

    private Organization chimay() {
        final Contact contact = createContact(Long.valueOf(CHIMAY_ID), "chimay", "+32 3 001 78 01", "+32 3 987 46 23", "http://www.chimay.com");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(CHIMAY_ID), "Rue de Poteaupré", "23", null, "4455", "Bourlers", "BE");
        return createSampleLegalEntity(CHIMAY_ID, "Chimay", CHIMAY_VAT, CHIMAY_VAT, contact, basicAddress);
    }

    private Organization judas() {
        final Contact contact = createContact(Long.valueOf(JUDAS_ID), "judas", "+32 3 987 78 01", "+32 3 998 46 01", "www.judas.com");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(JUDAS_ID), "Saint Michel", "12", null, "2014", "Waarloos", "ES");
        return createSampleLegalEntity(JUDAS_ID, "Judas", JUDAS_VAT, JUDAS_VAT, contact, basicAddress);
    }

    private Organization mockBeer() {
        final Contact contact = createContact(Long.valueOf(MOCK_ID), "mock", "+32 3 987 78 01", "+32 3 998 46 01", "www.mock.com");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(MOCK_ID), "Saint Michel", "12", null, "2014", "Waarloos", "BE");
        return createSampleLegalEntity(MOCK_ID, "Mock", MOCK_VAT, MOCK_VAT, contact, basicAddress);
    }

    private Organization cityOfGenk() {
        final Contact contact = createContact(Long.valueOf(CITY_OF_GENK_ID), "", "089 65 36 00", "089 65 34 70", "www.genk.be");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(CITY_OF_GENK_ID), "Stadsplein", "1", null, "3600", "Genk", "BE");
        return createSampleLegalEntity(CITY_OF_GENK_ID, "Stad Genk", KBO_CITY_OF_GENK, KBO_CITY_OF_GENK, contact, basicAddress);
    }

    private Organization cityOfHasselt() {
        final Contact contact = createContact(Long.valueOf(CITY_OF_HASSELT_ID), "", "011 23 90 11", "", "www.hasselt.be");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(CITY_OF_HASSELT_ID), "Groenplein", "1", null, "3500", "Hasselt", "BE");
        return createSampleLegalEntity(CITY_OF_HASSELT_ID, "Stad Hasselt", KBO_CITY_OF_HASSELT, KBO_CITY_OF_HASSELT, contact, basicAddress);
    }

    private Organization cityOfGeel() {
        final Contact contact = createContact(Long.valueOf(CITY_OF_GEEL_ID), "", "014 56 60 00", "", "www.geel.be");
        final BasicAddress basicAddress = createBasicAddress(Long.valueOf(CITY_OF_GEEL_ID), "Werft", "20", null, "2440", "Geel", "BE");
        return createSampleLegalEntity(CITY_OF_GEEL_ID, "Stad Geel", KBO_CITY_OF_GEEL, KBO_CITY_OF_GEEL, contact, basicAddress);
    }


    private Organization createSampleLegalEntity(String id, String companyName, String kbo, String vatNumber, Contact contact, BasicAddress basicAddress) {
        Organization organization = new Organization(id);
        organization.setLegalName(companyName);
        organization.setKboNumber(kbo);
        organization.setVatNumber(vatNumber);
        organization.setBasicAddress(basicAddress);
        organization.setContact(contact);
        organization.setType("Company");
        return organization;
    }

    private Contact createContact(Long id, String nickname, String phone, String fax, String url) {
        Contact contact = new Contact();
        contact.setId(id);
        contact.setNickname(nickname);
        contact.setPhoneNumber(phone);
        contact.setFax(fax);
        contact.setWebsite(url);
        contact.setEmail(createEmailAddress(nickname));
        return contact;
    }

    private BasicAddress createBasicAddress(Long id, String street, String nr, String locator, String zipCode, String city, String country) {
        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setId(id);
        basicAddress.setStreetName(street);
        basicAddress.setHouseNumber(nr);
        basicAddress.setLocator(locator);
        basicAddress.setZipCode(zipCode);
        basicAddress.setCity(city);
        basicAddress.setCountry(country);
        return basicAddress;
    }


    @Override
    public Organization detailById(String id) {
        log.debug("retrieving details of " + id);
        final Organization organization = organizationsById.get(id.toLowerCase());
        if (organization != null) {
            log.debug("Found organization with id " + id + " : " + organization);
        } else {
            log.warn("No organization found with id " + id);
            // TODO CSPROC-2196 create Exception handler for entity not found exception
        }
        return organization;
    }

    /**
     * @param legalEntitySearchCriterions legal search criterions
     * @return a list of Organizations satisfying the criterions
     * @throws InvalidResponseException, SessionExpiredException, NoDataFoundException, NonUniqueResultException
     */
    @Override
    public List<Organization> find(Set<OrganizationSearchCriterion> legalEntitySearchCriterions) {
        Set<Organization> results = new HashSet<>(organizationsById.values());
        for (OrganizationSearchCriterion item : legalEntitySearchCriterions) {
            if (item.getKey().equals(OrganizationSearchCriterion.Key.KBO_NUMBER)) {
                results.retainAll(findByKBONumber(item.getValue()));
            } else if (item.getKey().equals(OrganizationSearchCriterion.Key.NAME)) {
                results.retainAll(findByLegalName(item.getValue()));
            } else if (item.getKey().equals(OrganizationSearchCriterion.Key.VAT_NUMBER)) {
                results.retainAll(findByVATNumber(item.getValue()));
            }
        }
        return new ArrayList<>(results);
    }

    @Override
    public List<Organization> find(Set<OrganizationSearchCriterion> legalEntitySearchCriterions, Predicate.BooleanOperator booleanOperator) {
        if (booleanOperator == Predicate.BooleanOperator.AND) {
            return find(legalEntitySearchCriterions);
        }
        Set<Organization> results = new HashSet<>();
        for (OrganizationSearchCriterion item : legalEntitySearchCriterions) {
            if (item.getKey().equals(OrganizationSearchCriterion.Key.KBO_NUMBER)) {
                results.addAll(findByKBONumber(item.getValue()));
            } else if (item.getKey().equals(OrganizationSearchCriterion.Key.NAME)) {
                results.addAll(findByLegalName(item.getValue()));
            } else if (item.getKey().equals(OrganizationSearchCriterion.Key.VAT_NUMBER)) {
                results.addAll(findByVATNumber(item.getValue()));
            }
        }
        return new ArrayList<>(results);
    }

    private Collection<Organization> findByLegalName(String name) {
        List<Organization> results = new ArrayList<>();

        for (Organization org : organizationsById.values()) {
            if (StringUtils.isEmpty(name) || org.getLegalName().toLowerCase().contains(name.toLowerCase())) {
                results.add(org);
            }
        }

        return results;
    }

    private Collection<Organization> findByKBONumber(String kboNumber) {
        List<Organization> results = new ArrayList<>();

        for (Organization org : organizationsById.values()) {
            String formattedKboNumber;
            try {
                formattedKboNumber = formatKboNumber(org.getKboNumber());
                if (StringUtils.isEmpty(kboNumber) || org.getKboNumber().contains(kboNumber) || formattedKboNumber.contains(kboNumber)) {
                    results.add(org);
                }
            } catch (InvalidKboNumberException e) {
                log.warn(String.format("Invalid KBO number: %s", org.getKboNumber()), e);
            }
        }

        return results;
    }

    private Collection<Organization> findByVATNumber(String vatNumber) {
        List<Organization> results = new ArrayList<>();

        for (Organization org : organizationsById.values()) {
            String formattedKboNumber;
            try {
                formattedKboNumber = formatKboNumber(org.getVatNumber());
                if (StringUtils.isEmpty(vatNumber) || org.getKboNumber().contains(vatNumber) || formattedKboNumber.contains(vatNumber)) {
                    results.add(org);
                }
            } catch (InvalidKboNumberException e) {
                log.warn(String.format("Invalid KBO number: %s", org.getVatNumber()), e);
            }
        }

        return results;
    }

    @Override
    public Organization findSingle(Set<OrganizationSearchCriterion> legalEntitySearchCriterions) {
        for (OrganizationSearchCriterion item : legalEntitySearchCriterions) {
            if (item.getKey().equals(OrganizationSearchCriterion.Key.KBO_NUMBER)) {
                List<Organization> filteredByKboNumber = new ArrayList<>(findByKBONumber(item.getValue()));
                if (filteredByKboNumber.size() == SIZE_1) {
                    return filteredByKboNumber.get(0);
                }
            } else if (item.getKey().equals(OrganizationSearchCriterion.Key.NAME)) {
                List<Organization> filteredByName = new ArrayList<>(findByLegalName(item.getValue()));
                if (filteredByName.size() == SIZE_1) {
                    return filteredByName.get(0);
                }
            }
        }
        return null;
    }

    @Override
    public Organization save(Organization organization) {
        sequentialId(organization);
        organizationsById.put(organization.getId(), organization);
        return organization;
    }

    private static synchronized void sequentialId(Organization organization) {
        if (StringUtils.isEmpty(organization.getId())) {
            organization.setId(String.valueOf(counter++));
        }
    }


    @Override
    public void delete(String id) {
        organizationsById.remove(id);
    }

    private static String formatKboNumber(String kboNumber) throws InvalidKboNumberException {
        if (kboNumber == null) {
            throw new InvalidKboNumberException("Invalid KBO number");
        }

        return kboNumber.replace(".", "").trim().replaceAll("[^\\d.]", "");
    }

}
