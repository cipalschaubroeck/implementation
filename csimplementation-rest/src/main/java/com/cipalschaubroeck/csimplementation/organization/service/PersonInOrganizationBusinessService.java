package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.controller.ContactPersonInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.PersonInOrganizationInfo;

import java.util.List;

public interface PersonInOrganizationBusinessService {
    PersonInOrganizationInfo updatePersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo, String id);

    PersonInOrganizationInfo savePersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo);

    PersonInOrganizationInfo detailById(String id);

    void deletePersonInOrganization(String id);

    List<PersonInOrganizationInfo> findPersonInOrganization();

    List<ContactPersonInfo> getPersonsFor(String organizationId);
}
