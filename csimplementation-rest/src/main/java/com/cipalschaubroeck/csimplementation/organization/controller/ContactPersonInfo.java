package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactPersonInfo {
    private String fullName;
    private String externalOrganizationId;
    private String externalPersonInOrganizationId;
}
