package com.cipalschaubroeck.csimplementation.organization.repository;

import com.cipalschaubroeck.csimplementation.organization.config.OrganizationConfigConstants;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("singleOrganizationRepo")
@RepositoryRestResource(path = "single-organization",
        collectionResourceRel = OrganizationConfigConstants.ORGANIZATION_COLLECTION_REL)
public interface SingleOrganizationRepository extends PagingAndSortingRepository<SingleOrganization, Integer> {
    Optional<SingleOrganization> findByExternalId(String externalId);
}
