package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.config.CustomBeansUtil;
import com.cipalschaubroeck.csimplementation.organization.controller.BasicAddressInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.ContactInfo;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import org.springframework.stereotype.Component;

@Component
class CommonPersonOrgHelper {
    /**
     * Maps ContactInfo attributes into Contact
     *
     * @param contact     source
     * @param contactInfo destination
     * @return ContactInfo updated
     */
    ContactInfo mapContactToContactInfo(final Contact contact, final ContactInfo contactInfo) {
        if (contact != null) {
            if (contactInfo == null) {
                return mapContactToContactInfo(contact, new ContactInfo());
            }
            CustomBeansUtil.copyNonNullProperties(contact, contactInfo);
        }
        return contactInfo;
    }

    /**
     * Maps Contact attributes into ContactInfo
     *
     * @param contact     destination
     * @param contactInfo source
     * @return Contact updated
     */
    Contact mapContactInfoToContact(final ContactInfo contactInfo, final Contact contact) {
        if (contactInfo != null) {
            if (contact == null) {
                return mapContactInfoToContact(contactInfo, new Contact());
            }
            CustomBeansUtil.copyNonNullProperties(contactInfo, contact);
        }
        return contact;
    }

    /**
     * Maps BasicAddress attributes into BasicAddressInfo
     *
     * @param addressInfo destination
     * @param address     source
     * @return BasicAddressInfo updated
     */
    BasicAddressInfo mapBasicAddressToBasicAddressInfo(BasicAddress address, BasicAddressInfo addressInfo) {
        if (address != null) {
            if (addressInfo == null) {
                return mapBasicAddressToBasicAddressInfo(address, new BasicAddressInfo());
            }
            CustomBeansUtil.copyNonNullProperties(address, addressInfo);
        }
        return addressInfo;
    }

    /**
     * Maps BasicAddressInfo attributes into BasicAddress
     *
     * @param addressInfo source
     * @param address     destination
     * @return BasicAddress updated
     */
    BasicAddress mapBasicAddressInfoToBasicAddress(final BasicAddressInfo addressInfo, final BasicAddress address) {
        if (addressInfo != null) {
            if (address == null) {
                return mapBasicAddressInfoToBasicAddress(addressInfo, new BasicAddress());
            }
            CustomBeansUtil.copyNonNullProperties(addressInfo, address);
        }
        return address;
    }
}
