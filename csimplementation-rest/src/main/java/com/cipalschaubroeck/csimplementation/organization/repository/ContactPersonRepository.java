package com.cipalschaubroeck.csimplementation.organization.repository;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPersonId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "contact-person")
public interface ContactPersonRepository extends PagingAndSortingRepository<ContactPerson, ContactPersonId> {
}
