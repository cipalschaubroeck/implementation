package com.cipalschaubroeck.csimplementation.organization.domain;

import com.cipalschaubroeck.csimplementation.organization.domain.validation.JointVentureValidContact;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * a joint venture is an organization composed of other organizations for the purpose of a single contract,
 * so only one level of nesting is allowed
 */
@Entity
@Table(name = "organization_joint")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
@JointVentureValidContact
public class JointVenture extends Organization {

    @Column(name = "name")
    @NotNull
    private String name;

    @JoinTable(name = "joint_member",
            joinColumns = @JoinColumn(name = "joint"),
            foreignKey = @ForeignKey(name = "fk_joint_member"),
            inverseJoinColumns = @JoinColumn(name = "single"),
            inverseForeignKey = @ForeignKey(name = "fk_single_member"))
    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<SingleOrganization> members = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address", foreignKey = @ForeignKey(name = "fk_organization_address"))
    private PostalAddress postalAddress = new PostalAddress();

    @Transient
    private OrgAddress.OrgPostalAddress orgAddress;

    /**
     * the main contact must belong to one of the member organizations
     */
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumns({@JoinColumn(name = "main_contact", referencedColumnName = "person"),
            @JoinColumn(name = "main_contact_organization", referencedColumnName = "organization")})
    private ContactPerson mainContact;

    @Override
    public OrgAddress getAddress() {
        if (orgAddress == null && postalAddress != null) {
            orgAddress = new OrgAddress.OrgPostalAddress(postalAddress);
        }
        return orgAddress;
    }

    public void setPostalAddress(PostalAddress address) {
        this.postalAddress = address;
        if (orgAddress != null) {
            orgAddress.setDelegate(address);
        }
    }

    @Override
    public OrganizationType getType() {
        return OrganizationType.JOINT_VENTURE;
    }
}
