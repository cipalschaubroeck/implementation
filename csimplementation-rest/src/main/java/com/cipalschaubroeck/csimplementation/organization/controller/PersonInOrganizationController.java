package com.cipalschaubroeck.csimplementation.organization.controller;

import com.cipalschaubroeck.csimplementation.organization.service.PersonInOrganizationBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class PersonInOrganizationController {

    @Autowired
    private PersonInOrganizationBusinessService service;

    @GetMapping("person-in-org")
    public ResponseEntity<List<PersonInOrganizationInfo>> getPersonsInOrg() {
        return new ResponseEntity<>(service.findPersonInOrganization(), HttpStatus.OK);
    }

    @PostMapping("person-in-org")
    public ResponseEntity<PersonInOrganizationInfo> postPersonInOrg(@RequestBody PersonInOrganizationInfo body) {
        return new ResponseEntity<>(service.savePersonInOrganization(body), HttpStatus.CREATED);
    }

    @PutMapping("person-in-org/{id}")
    public ResponseEntity<PersonInOrganizationInfo> patchPersonInOrg(@PathVariable String id, @RequestBody PersonInOrganizationInfo body) {
        return new ResponseEntity<>(service.updatePersonInOrganization(body, id), HttpStatus.CREATED);
    }

    @GetMapping("person-in-org/{id}")
    public ResponseEntity<PersonInOrganizationInfo> getDetailPersonInOrg(@PathVariable String id) {
        return new ResponseEntity<>(service.detailById(id), HttpStatus.OK);
    }

    @DeleteMapping("person-in-org/{id}")
    public ResponseEntity<Void> deletePersonInOrg(@PathVariable String id) {
        service.deletePersonInOrganization(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
