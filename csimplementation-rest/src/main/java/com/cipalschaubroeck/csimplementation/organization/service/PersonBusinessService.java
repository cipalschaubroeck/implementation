package com.cipalschaubroeck.csimplementation.organization.service;


import com.cipalschaubroeck.csimplementation.organization.controller.PersonInfo;

import java.util.List;

public interface PersonBusinessService {
    PersonInfo updatePerson(PersonInfo personInfo, String id);

    PersonInfo savePerson(PersonInfo info);

    PersonInfo detailById(String id);

    void deletePerson(String id);

    List<PersonInfo> findPersons();
}
