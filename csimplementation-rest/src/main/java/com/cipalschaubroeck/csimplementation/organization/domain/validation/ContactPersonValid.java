package com.cipalschaubroeck.csimplementation.organization.domain.validation;

import javax.validation.Constraint;

@Constraint(validatedBy = ContactPersonValidator.class)
public @interface ContactPersonValid {
}
