package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class JointVentureInfo extends OrganizationInfo {
    private List<OrganizationInfo> members = new ArrayList<>();
    private ContactPersonInfo mainContact;
}
