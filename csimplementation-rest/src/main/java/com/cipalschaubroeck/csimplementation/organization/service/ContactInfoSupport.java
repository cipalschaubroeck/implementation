package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.cipalschaubroeck.csimplementation.person.service.EmailAddressDuplicator;
import com.cipalschaubroeck.csimplementation.person.service.PhoneNumberDuplicator;
import com.cipalschaubroeck.csimplementation.person.service.PostalAddressDuplicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class ContactInfoSupport {

    @Autowired
    private EmailAddressDuplicator emailAddressDuplicator;
    @Autowired
    private PostalAddressDuplicator postalAddressDuplicator;
    @Autowired
    private PhoneNumberDuplicator phoneNumberDuplicator;

    /**
     * Sometimes a contact info entity must be the original one but, at some point, must be changed into
     * a copy. This method is to ease that copy
     *
     * @param current    the current value
     * @param originList the list the current value may be in
     * @return current if it is not in originList, a copy if it is
     */
    public EmailAddress copyIfNeeded(EmailAddress current, List<EmailAddress> originList) {
        return fixContactInfo(current, originList, emailAddressDuplicator::duplicate);
    }

    /**
     * Sometimes a contact info entity must be the original one but, at some point, must be changed into
     * a copy. This method is to ease that copy
     *
     * @param current    the current value
     * @param originList the list the current value may be in
     * @return current if it is not in originList, a copy if it is
     */
    public PhoneNumber copyIfNeeded(PhoneNumber current, List<PhoneNumber> originList) {
        return fixContactInfo(current, originList, phoneNumberDuplicator::duplicate);
    }

    /**
     * Sometimes a contact info entity must be the original one but, at some point, must be changed into
     * a copy. This method is to ease that copy
     *
     * @param current    the current value
     * @param originList the list the current value may be in
     * @return current if it is not in originList, a copy if it is
     */
    public PostalAddress copyIfNeeded(PostalAddress current, List<PostalAddress> originList) {
        return fixContactInfo(current, originList, postalAddressDuplicator::duplicate);
    }

    /**
     * Applies copy if single is not null and list contains it
     *
     * @param single a single item
     * @param list   a list of items
     * @param copy   how to copy single if needed
     * @param <T>    the type of items
     * @return single if it wasn't necessary to copy it, the copy otherwise
     */
    private <T> T fixContactInfo(T single, List<T> list, Function<T, T> copy) {
        if (single == null || !list.contains(single)) {
            return single;
        } else {
            return copy.apply(single);
        }
    }
}
