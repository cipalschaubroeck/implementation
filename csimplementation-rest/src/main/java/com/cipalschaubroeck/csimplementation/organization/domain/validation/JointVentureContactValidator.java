package com.cipalschaubroeck.csimplementation.organization.domain.validation;

import com.cipalschaubroeck.csimplementation.organization.domain.JointVenture;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

/**
 * validates that joint venture's main contact is either null or one of its members' contacts
 */
public class JointVentureContactValidator implements ConstraintValidator<JointVentureValidContact, JointVenture> {
    @Override
    public boolean isValid(JointVenture value, ConstraintValidatorContext context) {
        return value.getMainContact() == null || value.getMembers().stream().map(SingleOrganization::getId)
                .anyMatch(id -> Objects.equals(id, value.getMainContact().getOrganization().getId()));
    }
}
