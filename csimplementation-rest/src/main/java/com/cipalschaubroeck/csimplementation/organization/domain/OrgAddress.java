package com.cipalschaubroeck.csimplementation.organization.domain;

import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;

public interface OrgAddress {
    String getStreet();

    String getNumber();

    String getPostalCode();

    String getMunicipality();

    String getCountry();

    String getBox();

    @AllArgsConstructor
    class OrgPostalAddress implements OrgAddress {
        @Delegate
        @Setter
        private PostalAddress delegate;
    }

    @AllArgsConstructor
    class OrgBasicAddress implements OrgAddress {
        @Setter
        private BasicAddress delegate;

        @Override
        public String getStreet() {
            return delegate.getStreetName();
        }

        @Override
        public String getNumber() {
            return delegate.getHouseNumber();
        }

        @Override
        public String getPostalCode() {
            return delegate.getZipCode();
        }

        @Override
        public String getMunicipality() {
            return delegate.getCity();
        }

        @Override
        public String getCountry() {
            return delegate.getCountry();
        }

        @Override
        public String getBox() {
            return delegate.getLocator();
        }
    }
}
