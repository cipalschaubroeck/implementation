package com.cipalschaubroeck.csimplementation.organization.controller;

import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;
import com.cipalschaubroeck.csimplementation.organization.service.OrganizationBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class OrganizationController {

    @Autowired
    private OrganizationBusinessService service;

    @GetMapping("organizations")
    public ResponseEntity<List<OrganizationInfo>> getOrganizations(
            @RequestParam(name = "type", required = false) OrganizationType type) {
        List<OrganizationInfo> organizations;
        if (type != null) {
            organizations = service.findOrganizations(type);
        } else {
            organizations = service.findOrganizations();
        }
        return new ResponseEntity<>(organizations, HttpStatus.OK);
    }

    @PostMapping("organizations")
    public ResponseEntity<OrganizationInfo> postOrganization(@RequestBody JointVentureInfo body) {
        return new ResponseEntity<>(service.saveOrganization(body), HttpStatus.CREATED);
    }

    @PatchMapping("organizations/{id}")
    public ResponseEntity<OrganizationInfo> patchOrganization(@PathVariable String id, @RequestBody JointVentureInfo body) {
        OrganizationInfo info;
        if (body.getType() == OrganizationType.SINGLE_ORGANIZATION) {
            info = service.updateOrganization(body, id);
        } else {
            info = service.updateJointVenture(body, Integer.parseInt(id));
        }
        return new ResponseEntity<>(info, HttpStatus.OK);
    }

    @GetMapping("organizations/{type}/{id}")
    public ResponseEntity<OrganizationInfo> getDetailOrganization(@PathVariable("id") String id,
                                                                  @PathVariable("type") OrganizationType type) {
        OrganizationInfo info;
        if (OrganizationType.SINGLE_ORGANIZATION == type) {
            info = service.detailById(id);
        } else {
            info = service.detailById(Integer.parseInt(id));
        }
        return new ResponseEntity<>(info, HttpStatus.OK);
    }

    @GetMapping("organizations/{id}/contacts")
    public ResponseEntity<List<ContactPersonInfo>> getContacts(@PathVariable("id") String id) {
        return new ResponseEntity<>(service.contactsOf(id), HttpStatus.OK);
    }

    @DeleteMapping("organizations/{type}/{id}")
    public ResponseEntity<Void> deleteOrganization(@PathVariable("id") String id,
                                                   @PathVariable("type") OrganizationType type) {
        if (type == OrganizationType.JOINT_VENTURE) {
            service.deleteOrganization(Integer.parseInt(id));
        } else {
            service.deleteOrganization(id);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
