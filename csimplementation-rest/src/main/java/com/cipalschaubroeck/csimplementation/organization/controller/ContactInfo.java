package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactInfo {
    private String nickname;
    private String email;
    private String phoneNumber;
    private String website;
    private String fax;
}
