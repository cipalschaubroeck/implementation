package com.cipalschaubroeck.csimplementation.organization.config;

public class OrganizationConfigConstants {
    private OrganizationConfigConstants() {
    }

    public static final String ORGANIZATION_COLLECTION_REL = "organizations";
}
