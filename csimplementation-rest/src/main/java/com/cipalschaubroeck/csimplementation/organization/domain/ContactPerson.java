package com.cipalschaubroeck.csimplementation.organization.domain;

import com.cipalschaubroeck.csimplementation.organization.config.ContactPersonListener;
import com.cipalschaubroeck.csimplementation.organization.domain.validation.ContactPersonValid;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PersonResolvable;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Delegate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

/**
 * A ContactPerson is a relationship between a particular person and an organization.
 * This implementation allows the same person be the contact of several organizations
 * (for instance, for a joint venture and for one of its member organizations)
 */
@Entity
@Table(name = "contact")
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@EntityListeners(ContactPersonListener.class)
@ContactPersonValid
public class ContactPerson implements PersonResolvable {
    @EmbeddedId
    private ContactPersonId id = new ContactPersonId();

    @Setter
    @Getter
    @NotNull
    @Column(name = "external_id")
    private String externalId;

    @Transient
    @Setter
    @Delegate(types = ContactPersonInterfaced.class)
    private PersonInOrganization personInOrganization;

    @MapsId("person")
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "person", foreignKey = @ForeignKey(name = "fk_contact_person"))
    @NotNull
    private PersonFromContacts person;

    @MapsId("organization")
    @ManyToOne(optional = false, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "fk_contact_organization"))
    @NotNull
    private SingleOrganization organization;

    @Override
    @Transient
    public Person getEffectivePerson() {
        return person;
    }

    public ContactPerson(PersonInOrganization personInOrganization, SingleOrganization singleOrganization, PersonFromContacts personFromContacts) {
        this.personInOrganization = personInOrganization;
        organization = singleOrganization;
        person = personFromContacts;
        externalId = personInOrganization.getId();
    }

    /**
     * updates the external information of this entity
     *
     * @param personInOrganization person within the organization
     * @param singleOrganization   local organization backed by the organization of personInOrganization
     * @param personFromContacts   local person backed by the person of personFromContacts
     */
    public void updateExternals(PersonInOrganization personInOrganization, SingleOrganization singleOrganization, PersonFromContacts personFromContacts) {
        this.personInOrganization = personInOrganization;
        organization = singleOrganization;
        person = personFromContacts;
        externalId = personInOrganization.getId();
    }

    interface ContactPersonInterfaced {
        void setFunction(String function);

        String getFunction();
    }
}
