package com.cipalschaubroeck.csimplementation.organization.controller;

import com.cipalschaubroeck.csimplementation.organization.service.PersonBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class ContactController {

    @Autowired
    private PersonBusinessService service;


    @GetMapping("contacts")
    public ResponseEntity<List<PersonInfo>> getPersons() {
        return new ResponseEntity<>(service.findPersons(), HttpStatus.OK);
    }

    @PostMapping("contacts")
    public ResponseEntity<PersonInfo> postPerson(@RequestBody PersonInfo body) {
        return new ResponseEntity<>(service.savePerson(body), HttpStatus.CREATED);
    }

    @PatchMapping("contacts/{id}")
    public ResponseEntity<PersonInfo> patchPerson(@PathVariable String id, @RequestBody PersonInfo body) {
        return new ResponseEntity<>(service.updatePerson(body, id), HttpStatus.CREATED);
    }

    @GetMapping("contacts/{id}")
    public ResponseEntity<PersonInfo> getDetailPerson(@PathVariable String id) {
        return new ResponseEntity<>(service.detailById(id), HttpStatus.OK);
    }

    @DeleteMapping("contacts/{id}")
    public ResponseEntity<Void> deletePerson(@PathVariable String id) {
        service.deletePerson(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
