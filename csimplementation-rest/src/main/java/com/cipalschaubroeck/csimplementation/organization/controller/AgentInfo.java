package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AgentInfo {
    private String id;
    private ContactInfo contact;
    private BasicAddressInfo basicAddress;
}
