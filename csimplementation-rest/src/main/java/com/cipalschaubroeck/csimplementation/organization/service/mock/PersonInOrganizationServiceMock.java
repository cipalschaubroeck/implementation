package com.cipalschaubroeck.csimplementation.organization.service.mock;

import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import com.greenvalley.contacts.domain.search.OrganizationSearchCriterion;
import com.greenvalley.contacts.domain.search.PersonInOrganizationSearchCriterion;
import com.greenvalley.contacts.service.PersonInOrganizationService;
import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Profile("!contacts")
// TODO CSPROC-2194 separate configuration from logic
public class PersonInOrganizationServiceMock implements PersonInOrganizationService {

    private static int counter = 50;
    private Map<String, PersonInOrganization> personInOrganizationMap;

    @Inject
    private OrganizationServiceMock organizationServiceMock;
    @Inject
    private PersonServiceMock personServiceMock;

    @PostConstruct
    private void createPersonInOrganizationMap() {
        Map<String, PersonInOrganization> map = new HashMap<>();
        for (Person person : personServiceMock.getAll()) {
            PersonInOrganization personInOrganization = new PersonInOrganization();
            personInOrganization.setId(person.getId());
            personInOrganization.setPerson(person);
            personInOrganization.setFunction("Developer");
            personInOrganization.setBasicAddress(person.getBasicAddress());
            personInOrganization.setContact(person.getContact());
            map.put(personInOrganization.getId(), personInOrganization);

            List<Organization> contactsInOrgList = organizationServiceMock.find(new OrganizationSearchCriterion.Builder().build());

            final String organizationId;
            if (!CollectionUtils.isEmpty(contactsInOrgList) && Integer.valueOf(person.getId()) < contactsInOrgList.size()) {
                organizationId = person.getId();
            } else {
                organizationId = "1";
            }
            Optional<Organization> organization = contactsInOrgList.stream().filter(org -> organizationId.equalsIgnoreCase(org.getId())).findFirst();
            organization.ifPresent(personInOrganization::setOrganization);
        }
        this.personInOrganizationMap = map;
    }


    @Override
    public PersonInOrganization detailById(String id) {
        return personInOrganizationMap.get(id);
    }

    @Override
    public List<PersonInOrganization> find(Set<PersonInOrganizationSearchCriterion> searchCriteria) {
        if (searchCriteria.isEmpty()) {
            return new ArrayList<>(personInOrganizationMap.values());
        } else {
            Optional<java.util.function.Predicate<PersonInOrganization>> predicate = searchCriteria.stream()
                    .map(this::criterionToPredicate)
                    .filter(Objects::nonNull)
                    .reduce(java.util.function.Predicate::and);
            return predicate.map(p -> personInOrganizationMap.values().stream()
                    .filter(p).collect(Collectors.toList()))
                    .orElseGet(() -> new ArrayList<>(personInOrganizationMap.values()));
        }
    }

    private java.util.function.Predicate<PersonInOrganization> criterionToPredicate(PersonInOrganizationSearchCriterion criterion) {
        switch (criterion.getKey()) {
            case ORGANIZATION_ID:
                return pio -> StringUtils.equals(pio.getId(), criterion.getValue());
            case ORGANIZATION_KBO:
                return pio -> StringUtils.equals(pio.getOrganization().getKboNumber(), criterion.getValue());
            case PERSON_LAST_NAME:
                return pio -> StringUtils.contains(pio.getPerson().getFamilyName(), criterion.getValue());
            case PERSON_FIRST_NAME:
                return pio -> StringUtils.contains(pio.getPerson().getFirstName(), criterion.getValue());
            case PERSON_FIRST_OR_NAME:
                return pio -> StringUtils.contains(pio.getPerson().getFirstName(), criterion.getValue())
                        || StringUtils.contains(pio.getPerson().getFamilyName(), criterion.getValue());
            case PERSON_IDENTITY_NUMBER:
                return pio -> StringUtils.contains(pio.getPerson().getIdentityNumber(), criterion.getValue());
            case PERSON_IN_ORGANIZATION_ADDRESS:
                return pio -> StringUtils.contains(pio.getPerson().getBasicAddress().getFullAddress(), criterion.getValue());
        }
        return null;
    }

    @Override
    public List<PersonInOrganization> find(Set<PersonInOrganizationSearchCriterion> set, Predicate.BooleanOperator booleanOperator) {
        return find(set);
    }

    @Override
    public PersonInOrganization findSingle(Set<PersonInOrganizationSearchCriterion> searchCriteria) {
        return null;
    }

    @Override
    public PersonInOrganization save(PersonInOrganization personInOrganization) {
        sequentialId(personInOrganization);
        personInOrganizationMap.put(personInOrganization.getId(), personInOrganization);
        return personInOrganization;
    }

    private static synchronized void sequentialId(PersonInOrganization personInOrganization) {
        if (StringUtils.isEmpty(personInOrganization.getId())) {
            personInOrganization.setId(String.valueOf(counter++));
        }
    }

    @Override
    public void delete(String id) {
        personInOrganizationMap.remove(id);
    }
}
