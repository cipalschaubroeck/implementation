package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicAddressInfo {
    private String streetName;
    private String houseNumber;
    private String locator;
    private String zipCode;
    private String country;
    private String city;

    // used in serialization
    @SuppressWarnings("unused")
    public String getFullAddress() {
        return String.format("%s %s, %s %s (%s)", streetName, houseNumber, zipCode, city, country);
    }
}
