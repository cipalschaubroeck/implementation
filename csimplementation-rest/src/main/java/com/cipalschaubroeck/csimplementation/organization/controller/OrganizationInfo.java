package com.cipalschaubroeck.csimplementation.organization.controller;

import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrganizationInfo extends AgentInfo {
    private String legalName;
    private String kboNumber;
    private String vatNumber;
    private OrganizationType type;
    private boolean sme;
}
