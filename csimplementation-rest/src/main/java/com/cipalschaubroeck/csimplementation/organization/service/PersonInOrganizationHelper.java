package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.config.CustomBeansUtil;
import com.cipalschaubroeck.csimplementation.organization.controller.PersonInOrganizationInfo;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class PersonInOrganizationHelper extends CommonPersonOrgHelper {

    @Autowired
    private OrganizationHelper organizationHelper;

    @Autowired
    private PersonHelper personHelper;

    /**
     * Maps PersonInOrganization attributes into PersonInOrganizationInfo
     *
     * @param personInOrganizationInfo destination
     * @param personInOrganization     source
     */
    private PersonInOrganizationInfo mapPersonInOrganizationToPersonInOrganizationInfoBasicProperties(final PersonInOrganization personInOrganization, final PersonInOrganizationInfo personInOrganizationInfo) {
        if (personInOrganization != null) {
            if (personInOrganizationInfo == null) {
                return mapPersonInOrganizationToPersonInOrganizationInfoBasicProperties(personInOrganization, new PersonInOrganizationInfo());
            }
            CustomBeansUtil.copyNonNullProperties(personInOrganization, personInOrganizationInfo);
        }
        return personInOrganizationInfo;
    }

    /**
     * Maps AgentInfo attributes into Agent
     *
     * @param personInOrganizationInfo source
     * @param personInOrganization     destination
     */
    private void mapAgentInfoToAgentBasicProperties(PersonInOrganizationInfo personInOrganizationInfo, PersonInOrganization personInOrganization) {
        if (personInOrganizationInfo != null && personInOrganization != null) {
            CustomBeansUtil.copyNonNullProperties(personInOrganizationInfo, personInOrganization);
        }
    }

    /**
     * Maps all properties that are not null from PersonInOrganizationInfo to PersonInOrganization interface-contact client
     *
     * @param personInOrganizationInfo source business model
     * @param personInOrganization     destination to interface-client
     * @return PersonInOrganization interface-client
     */
    public PersonInOrganization mapPersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo, PersonInOrganization personInOrganization) {
        if (personInOrganizationInfo != null) {
            if (personInOrganization == null) {
                return mapPersonInOrganization(personInOrganizationInfo, new PersonInOrganization());
            }
            mapAgentInfoToAgentBasicProperties(personInOrganizationInfo, personInOrganization);
            personInOrganization.setBasicAddress(super.mapBasicAddressInfoToBasicAddress(personInOrganizationInfo.getBasicAddress(), personInOrganization.getBasicAddress()));
            personInOrganization.setContact(super.mapContactInfoToContact(personInOrganizationInfo.getContact(), personInOrganization.getContact()));
            personInOrganization.setOrganization(organizationHelper.mapOrganization(personInOrganizationInfo.getOrganization(), personInOrganization.getOrganization()));
            personInOrganization.setPerson(personHelper.mapPerson(personInOrganizationInfo.getPerson(), personInOrganization.getPerson()));

        }
        return personInOrganization;
    }

    /**
     * Maps all properties that are not null from PersonInOrganization to business model PersonInOrganizationInfo
     *
     * @param personInOrganization from interface-contact client
     * @return PersonInOrganization business model
     */
    public PersonInOrganizationInfo mapPersonInOrganizationInfo(PersonInOrganization personInOrganization) {
        if (personInOrganization == null) {
            // TODO CSPROC-2196 create Exception handler for entity not found exception
            throw new EntityNotFoundException();
        }
        PersonInOrganizationInfo personInOrganizationInfo = mapPersonInOrganizationToPersonInOrganizationInfoBasicProperties(personInOrganization, new PersonInOrganizationInfo());
        personInOrganizationInfo.setBasicAddress(super.mapBasicAddressToBasicAddressInfo(personInOrganization.getBasicAddress(), personInOrganizationInfo.getBasicAddress()));
        personInOrganizationInfo.setContact(super.mapContactToContactInfo(personInOrganization.getContact(), personInOrganizationInfo.getContact()));
        personInOrganizationInfo.setOrganization(organizationHelper.mapOrganizationInfo(personInOrganization.getOrganization()));
        personInOrganizationInfo.setPerson(personHelper.mapPersonInfo(personInOrganization.getPerson()));

        return personInOrganizationInfo;
    }
}
