package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.config.CustomBeansUtil;
import com.cipalschaubroeck.csimplementation.organization.controller.OrganizationInfo;
import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;
import com.greenvalley.contacts.domain.contact.Organization;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class OrganizationHelper extends CommonPersonOrgHelper {

    /**
     * Maps Organization attributes into OrganizationInfo
     *
     * @param organizationInfo destination
     * @param organization     source
     */
    private OrganizationInfo mapOrganizationToOrganizationInfoBasicProperties(final Organization organization, final OrganizationInfo organizationInfo) {
        if (organization != null) {
            if (organizationInfo == null) {
                return mapOrganizationToOrganizationInfoBasicProperties(organization, new OrganizationInfo());
            }
            CustomBeansUtil.copyNonNullProperties(organization, organizationInfo);
        }
        return organizationInfo;
    }

    /**
     * Maps OrganizationInfo attributes into Organization
     *
     * @param organizationInfo source
     * @param organization     destination
     */
    private void mapOrganizationInfoToOrganizationBasicProperties(OrganizationInfo organizationInfo, Organization organization) {
        if (organizationInfo != null && organization != null) {
            CustomBeansUtil.copyNonNullProperties(organizationInfo, organization);
        }
    }

    /**
     * Maps all properties that are not null from organizationInfo to organization interface-contact client
     *
     * @param organizationInfo source business model
     * @param organization     destination to interface-client
     * @return Organization interface-client
     */
    public Organization mapOrganization(OrganizationInfo organizationInfo, Organization organization) {
        if (organizationInfo != null) {
            if (organization == null) {
                return mapOrganization(organizationInfo, new Organization());
            }
            mapOrganizationInfoToOrganizationBasicProperties(organizationInfo, organization);
            organization.setBasicAddress(super.mapBasicAddressInfoToBasicAddress(organizationInfo.getBasicAddress(), organization.getBasicAddress()));
            organization.setContact(super.mapContactInfoToContact(organizationInfo.getContact(), organization.getContact()));
        }
        return organization;
    }

    /**
     * Maps all properties that are not null from Organization to business model OrganizationInfo
     *
     * @param organization from interface-contact client
     * @return OrganizationInfo business model
     */
    public OrganizationInfo mapOrganizationInfo(Organization organization) {
        if (organization == null) {
            // TODO CSPROC-2196 create Exception handler for entity not found exception
            throw new EntityNotFoundException();
        }
        OrganizationInfo organizationInfo = mapOrganizationToOrganizationInfoBasicProperties(organization, new OrganizationInfo());
        organizationInfo.setBasicAddress(super.mapBasicAddressToBasicAddressInfo(organization.getBasicAddress(), organizationInfo.getBasicAddress()));
        organizationInfo.setContact(super.mapContactToContactInfo(organization.getContact(), organizationInfo.getContact()));
        organizationInfo.setType(OrganizationType.SINGLE_ORGANIZATION);
        return organizationInfo;
    }
}
