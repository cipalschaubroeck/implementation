package com.cipalschaubroeck.csimplementation.organization.controller;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PersonInOrganizationInfo extends AgentInfo {
    private String id;
    private String function;
    private ContactInfo contact;
    private BasicAddressInfo basicAddress;
    private PersonInfo person;
    private OrganizationInfo organization;
}
