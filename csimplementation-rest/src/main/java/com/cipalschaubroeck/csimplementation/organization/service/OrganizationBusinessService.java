package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.controller.ContactPersonInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.JointVentureInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.OrganizationInfo;
import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;

import java.util.List;

public interface OrganizationBusinessService {
    OrganizationInfo updateOrganization(OrganizationInfo organization, String id);

    OrganizationInfo updateJointVenture(JointVentureInfo organization, Integer id);

    OrganizationInfo saveOrganization(OrganizationInfo info);

    /**
     * @param id of a single organization
     * @return info of a single organization
     */
    OrganizationInfo detailById(String id);

    /**
     * @param id of a joint venture
     * @return info of a joint venture
     */
    OrganizationInfo detailById(Integer id);

    /**
     * @param id of single organization
     */
    void deleteOrganization(String id);

    /**
     * @param id of joint venture
     */
    void deleteOrganization(Integer id);

    List<OrganizationInfo> findOrganizations();

    List<OrganizationInfo> findOrganizations(OrganizationType type);

    List<ContactPersonInfo> contactsOf(String id);
}
