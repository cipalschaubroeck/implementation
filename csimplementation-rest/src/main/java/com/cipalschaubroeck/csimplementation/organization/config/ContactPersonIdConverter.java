package com.cipalschaubroeck.csimplementation.organization.config;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPersonId;
import com.cipalschaubroeck.csimplementation.shared.config.BiCompositeIdConverter;
import org.springframework.stereotype.Component;

@Component
public class ContactPersonIdConverter extends BiCompositeIdConverter<ContactPersonId> {

    @Override
    protected ContactPersonId createId(Integer id1, Integer id2) {
        ContactPersonId objectId = new ContactPersonId();
        objectId.setOrganization(id1);
        objectId.setPerson(id2);
        return objectId;
    }

    @Override
    public Class<ContactPersonId> getIdClass() {
        return ContactPersonId.class;
    }

    @Override
    protected Integer getId1(ContactPersonId id) {
        return id.getOrganization();
    }

    @Override
    protected Integer getId2(ContactPersonId id) {
        return id.getPerson();
    }

    @Override
    public boolean supports(Class<?> delimiter) {
        return ContactPerson.class.isAssignableFrom(delimiter);
    }
}
