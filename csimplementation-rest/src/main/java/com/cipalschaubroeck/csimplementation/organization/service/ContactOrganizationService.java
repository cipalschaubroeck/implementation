package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.controller.AgentInfo;
import com.greenvalley.contacts.domain.contact.Agent;

import java.util.List;

public interface ContactOrganizationService {
    AgentInfo update(AgentInfo info, String id);

    AgentInfo save(AgentInfo info, Agent agent);

    AgentInfo detailById(String id);

    void delete(String id);

    List<AgentInfo> find();
}
