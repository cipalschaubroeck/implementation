package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.organization.config.CustomBeansUtil;
import com.cipalschaubroeck.csimplementation.organization.controller.PersonInfo;
import com.greenvalley.contacts.domain.contact.Person;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;

@Component
public class PersonHelper extends CommonPersonOrgHelper {

    /**
     * Maps Person attributes into PersonInfo
     *
     * @param personInfo destination
     * @param person     source
     * @return PersonInfo business model
     */
    private PersonInfo mapPersonToPersonInfoBasicProperties(final Person person, final PersonInfo personInfo) {
        if (person != null) {
            if (personInfo == null) {
                return mapPersonToPersonInfoBasicProperties(person, new PersonInfo());
            }
            CustomBeansUtil.copyNonNullProperties(person, personInfo);
        }
        return personInfo;
    }

    /**
     * Maps all properties that are not null from PersonInfo to Person interface-contact client
     *
     * @param personInfo source business model
     * @param person     destination to interface-client
     * @return Person interface-client
     */
    public Person mapPerson(PersonInfo personInfo, Person person) {
        if (personInfo != null) {
            if (person == null) {
                return mapPerson(personInfo, new Person());
            }
            CustomBeansUtil.copyNonNullProperties(personInfo, person);
            person.setBasicAddress(super.mapBasicAddressInfoToBasicAddress(personInfo.getBasicAddress(), person.getBasicAddress()));
            person.setContact(super.mapContactInfoToContact(personInfo.getContact(), person.getContact()));
        }
        return person;
    }

    /**
     * Maps all properties that are not null from Person to business model PersonInfo
     *
     * @param person from interface-contact client
     * @return PersonInfo business model
     */
    public PersonInfo mapPersonInfo(Person person) {
        if (person == null) {
            // TODO CSPROC-2196 create Exception handler for entity not found exception
            throw new EntityNotFoundException();
        }
        PersonInfo personInfo = mapPersonToPersonInfoBasicProperties(person, new PersonInfo());
        personInfo.setBasicAddress(super.mapBasicAddressToBasicAddressInfo(person.getBasicAddress(), personInfo.getBasicAddress()));
        personInfo.setContact(super.mapContactToContactInfo(person.getContact(), personInfo.getContact()));
        return personInfo;
    }

}
