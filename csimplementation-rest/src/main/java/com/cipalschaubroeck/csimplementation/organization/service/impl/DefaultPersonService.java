package com.cipalschaubroeck.csimplementation.organization.service.impl;

import com.cipalschaubroeck.csimplementation.organization.controller.PersonInfo;
import com.cipalschaubroeck.csimplementation.organization.service.PersonBusinessService;
import com.cipalschaubroeck.csimplementation.organization.service.PersonHelper;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.domain.search.PersonSearchCriterion;
import com.greenvalley.contacts.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
class DefaultPersonService implements PersonBusinessService {

    @Autowired
    private PersonService personService;

    @Autowired
    private PersonHelper helper;

    private PersonInfo savePerson(final PersonInfo personInfo, final Person person) {
        if (person == null) {
            return savePerson(personInfo, new Person());
        }
        helper.mapPerson(personInfo, person);
        return helper.mapPersonInfo(personService.save(person));
    }

    @Override
    public PersonInfo updatePerson(PersonInfo personInfo, String personId) {
        Person person = Optional.ofNullable(personService.detailById(personId)).orElseThrow(EntityNotFoundException::new);
        return savePerson(personInfo, person);
    }

    @Override
    public PersonInfo savePerson(final PersonInfo personInfo) {
        return savePerson(personInfo, null);
    }

    @Override
    public PersonInfo detailById(String id) {
        return helper.mapPersonInfo(personService.detailById(id));
    }

    @Override
    public void deletePerson(String id) {
        Person person = Optional.ofNullable(personService.detailById(id)).orElseThrow(EntityNotFoundException::new);
        personService.delete(person.getId());
    }

    @Override
    public List<PersonInfo> findPersons() {
        List<Person> persons = personService.find(new PersonSearchCriterion.Builder().withFirstName("").build());
        return persons.stream().map(helper::mapPersonInfo).collect(Collectors.toList());
    }
}
