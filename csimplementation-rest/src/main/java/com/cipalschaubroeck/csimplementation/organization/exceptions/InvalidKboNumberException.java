package com.cipalschaubroeck.csimplementation.organization.exceptions;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class InvalidKboNumberException extends Exception {

    public InvalidKboNumberException(String message) {
        super(message);
    }

    public InvalidKboNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidKboNumberException(Throwable cause) {
        super(cause);
    }

}
