package com.cipalschaubroeck.csimplementation.organization.repository;

import com.cipalschaubroeck.csimplementation.organization.domain.PersonFromContacts;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface PersonFromContactsRepository extends PagingAndSortingRepository<PersonFromContacts, Integer> {
    Optional<PersonFromContacts> findByExternalId(String externalId);
}
