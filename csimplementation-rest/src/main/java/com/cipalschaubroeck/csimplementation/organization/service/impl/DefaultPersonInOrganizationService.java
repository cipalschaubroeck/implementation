package com.cipalschaubroeck.csimplementation.organization.service.impl;

import com.cipalschaubroeck.csimplementation.organization.controller.ContactPersonInfo;
import com.cipalschaubroeck.csimplementation.organization.controller.PersonInOrganizationInfo;
import com.cipalschaubroeck.csimplementation.organization.service.PersonInOrganizationBusinessService;
import com.cipalschaubroeck.csimplementation.organization.service.PersonInOrganizationHelper;
import com.greenvalley.contacts.domain.contact.Agent;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import com.greenvalley.contacts.domain.search.PersonInOrganizationSearchCriterion;
import com.greenvalley.contacts.service.PersonInOrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
// spring managed
@SuppressWarnings("unused")
class DefaultPersonInOrganizationService implements PersonInOrganizationBusinessService {

    @Autowired
    private PersonInOrganizationService service;

    @Autowired
    private PersonInOrganizationHelper helper;

    private PersonInOrganizationInfo savePersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo, PersonInOrganization personInOrganization) {
        if (personInOrganization == null) {
            return savePersonInOrganization(personInOrganizationInfo, new PersonInOrganization());
        }
        helper.mapPersonInOrganization(personInOrganizationInfo, personInOrganization);
        return helper.mapPersonInOrganizationInfo(service.save(personInOrganization));
    }

    @Override
    public PersonInOrganizationInfo updatePersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo, String personId) {
        PersonInOrganization person = Optional.ofNullable(service.detailById(personId)).orElseThrow(EntityNotFoundException::new);
        return savePersonInOrganization(personInOrganizationInfo, person);
    }

    @Override
    public PersonInOrganizationInfo savePersonInOrganization(PersonInOrganizationInfo personInOrganizationInfo) {
        return savePersonInOrganization(personInOrganizationInfo, null);
    }

    @Override
    public PersonInOrganizationInfo detailById(String id) {
        return helper.mapPersonInOrganizationInfo(service.detailById(id));
    }

    @Override
    public void deletePersonInOrganization(String id) {
        Agent agent = Optional.ofNullable(service.detailById(id)).orElseThrow(EntityNotFoundException::new);
        service.delete(agent.getId());
    }

    @Override
    public List<PersonInOrganizationInfo> findPersonInOrganization() {
        List<PersonInOrganization> personInOrganizations = service.find(new PersonInOrganizationSearchCriterion.Builder().withOrganizationId("").build());
        List<PersonInOrganizationInfo> agentInfoList = new ArrayList<>();
        if (!CollectionUtils.isEmpty(personInOrganizations)) {
            for (PersonInOrganization person : personInOrganizations) {
                agentInfoList.add(helper.mapPersonInOrganizationInfo(person));
            }
        }
        return agentInfoList;
    }

    @Override
    public List<ContactPersonInfo> getPersonsFor(String organizationId) {
        List<PersonInOrganization> result = service.find(new PersonInOrganizationSearchCriterion.Builder()
                .withOrganizationId(organizationId)
                .build());
        return result.stream().map(pio -> {
            ContactPersonInfo info = new ContactPersonInfo();
            info.setExternalOrganizationId(pio.getOrganization().getId());
            info.setExternalPersonInOrganizationId(pio.getId());
            info.setFullName(pio.getPerson().getDisplayName());
            return info;
        }).collect(Collectors.toList());
    }
}
