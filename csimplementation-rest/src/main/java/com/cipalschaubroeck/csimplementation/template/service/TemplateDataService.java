package com.cipalschaubroeck.csimplementation.template.service;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;

import javax.xml.bind.JAXBException;
import java.util.List;

public interface TemplateDataService {

    /**
     * The duplication copies the value of basic fields, duplicates related entities if the relationship depends on
     * the document process, and references related entities otherwise. Generated fields are not copied
     *
     * @param original a template data
     * @return a copy of template data
     */
    TemplateData duplicate(TemplateData original);

    List<Template> getAvailableTemplates(Integer templateDataId, boolean includeThumbnails);

    /**
     * @param templateDataId id of a template that does not use information in addressee or process
     * @param format         format of the expected return document
     * @param fileName       filename to set in the resulting document
     * @return a document generated with the fields in template data
     * @throws JAXBException if there are errors translating to xml
     */
    Document downloadSingle(Integer templateDataId, OutputType format, String fileName) throws JAXBException;
}
