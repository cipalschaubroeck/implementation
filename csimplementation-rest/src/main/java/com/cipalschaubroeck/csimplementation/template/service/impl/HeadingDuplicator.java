package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataDuplicator;
import org.springframework.stereotype.Component;

@Component
public class HeadingDuplicator implements TemplateDataDuplicator {
    @Override
    public TemplateType getSupportedType() {
        return TemplateType.HEADING;
    }

    @Override
    public TemplateData duplicate(TemplateData original) {
        Heading copy = new Heading();
        Heading heading = (Heading) original;
        copy.setFrom(heading.getFrom());
        copy.setNumberAttachments(heading.getNumberAttachments());
        copy.setRegisteredLetter(heading.isRegisteredLetter());
        copy.setSubject(heading.getSubject());
        copy.setOurProperty(heading.getOurProperty());
        copy.setYourProperty(heading.getYourProperty());
        copy.setSalutation(heading.getSalutation());
        return copy;
    }

}
