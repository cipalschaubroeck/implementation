package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "signing-officer-data")
public interface SigningOfficerDataRepository extends PagingAndSortingRepository<SigningOfficerData, Integer> {
}
