package com.cipalschaubroeck.csimplementation.template.domain.validator;


import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.xperido.XPeridoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class TemplateConfigValidator implements Validator {

    private static final String ERROR_MESSAGE = "template-config.type.data-equals-template";

    @Autowired
    private XPeridoService service;

    private boolean isValid(TemplateData data) {
        return data.getConfig() != null &&
                (data.getConfig().getTemplateName() == null
                        || data.getType() == null
                        || service.getTemplate(data.getConfig().getTemplateName(), data.getType()).isPresent());
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return TemplateData.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        if (!isValid((TemplateData) target)) {
            errors.rejectValue("config.templateName", ERROR_MESSAGE);
        }
    }
}
