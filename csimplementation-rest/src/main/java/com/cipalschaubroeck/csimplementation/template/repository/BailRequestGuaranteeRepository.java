package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "bail-request-guarantee")
public interface BailRequestGuaranteeRepository extends PagingAndSortingRepository<BailRequestGuarantee, Integer> {
}
