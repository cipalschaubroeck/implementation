package com.cipalschaubroeck.csimplementation.template.config;

import com.cipalschaubroeck.csimplementation.shared.config.BiCompositeIdConverter;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerId;
import org.springframework.stereotype.Component;

@Component
public class SigningOfficerIdConverter extends BiCompositeIdConverter<SigningOfficerId> {

    @Override
    protected SigningOfficerId createId(Integer id1, Integer id2) {
        SigningOfficerId objectId = new SigningOfficerId();
        objectId.setPerson(id1);
        objectId.setData(id2);
        return objectId;
    }

    @Override
    public Class<SigningOfficerId> getIdClass() {
        return SigningOfficerId.class;
    }

    @Override
    protected Integer getId1(SigningOfficerId id) {
        return id.getPerson();
    }

    @Override
    protected Integer getId2(SigningOfficerId id) {
        return id.getData();
    }

    @Override
    public boolean supports(Class<?> delimiter) {
        return SigningOfficer.class.isAssignableFrom(delimiter);
    }
}
