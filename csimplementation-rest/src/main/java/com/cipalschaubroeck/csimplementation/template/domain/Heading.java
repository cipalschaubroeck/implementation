package com.cipalschaubroeck.csimplementation.template.domain;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Min;

@Entity
@Table(name = "heading")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class Heading extends TemplateData {

    @Column(name = "registered_letter")
    private boolean registeredLetter;

    @Column(name = "from_text")
    private String from;

    @Column(name = "subject")
    private String subject;

    @Column(name = "number_attachments")
    @Min(0)
    private int numberAttachments;

    @Column(name = "salutation")
    private String salutation;

    @Column(name = "our_property")
    private String ourProperty;

    @Column(name = "your_property")
    private String yourProperty;

    @Override
    public TemplateType getType() {
        return TemplateType.HEADING;
    }
}
