package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerId;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "signing-officer")
public interface SigningOfficerRepository extends PagingAndSortingRepository<SigningOfficer, SigningOfficerId> {
}
