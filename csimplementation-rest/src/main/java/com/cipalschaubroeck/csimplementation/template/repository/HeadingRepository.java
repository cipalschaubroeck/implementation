package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "heading")
public interface HeadingRepository extends PagingAndSortingRepository<Heading, Integer> {
}
