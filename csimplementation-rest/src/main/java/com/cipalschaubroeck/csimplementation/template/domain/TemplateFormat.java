package com.cipalschaubroeck.csimplementation.template.domain;

/**
 * used to distinguish when a template data has to use its text and when it has to use
 * the template+field data
 */
public enum TemplateFormat {
    /**
     * the text is generate by merging the template with the data
     */
    TEMPLATE,
    /**
     * use the text, modifiable by the user
     */
    TEXT
}
