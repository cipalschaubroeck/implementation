package com.cipalschaubroeck.csimplementation.template.controller;

import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import com.greenvalley.docgen.domain.Template;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
// instantiated by Spring
@SuppressWarnings("unused")
public class TemplateDataController {

    @Autowired
    private TemplateDataService service;

    @ApiOperation("${swagger.template-data-controller.available-templates.summary}")
    @ApiResponses(@ApiResponse(code = 200, message = "${swagger.template-data-controller.available-templates.summary}",
            responseContainer = "List", response = Template.class))
    @GetMapping("template-data/{id}/available-templates")
    public ResponseEntity<List<Template>> getAvailableTemplates(
            @ApiParam(name = "id", required = true, value = "${swagger.template-data.id.description}", example = "1") @PathVariable("id") Integer templateDataId,
            @ApiParam(name = "thumbnails", defaultValue = "false", value = "${swagger.template-data-controller.available-templates.thumbnails.description}")
            @RequestParam(name = "thumbnails", required = false, defaultValue = "false") boolean includeThumbnails) {
        return ResponseEntity.ok(service.getAvailableTemplates(templateDataId, includeThumbnails));
    }
}
