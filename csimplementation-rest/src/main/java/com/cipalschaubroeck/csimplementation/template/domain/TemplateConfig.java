package com.cipalschaubroeck.csimplementation.template.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

/**
 * A template of a particular type and the values of a particular set of fields
 * - depending on the template's type -
 * are merged to get the final value of the text.
 * The instances of this class link with the value of the fields,
 */
@Embeddable
@Getter
@Setter
public class TemplateConfig {
    @Column(name = "template_name")
    private String templateName;

    @Column(name = "text")
    private String text;

    @Column(name = "template_format")
    @Enumerated(EnumType.STRING)
    @NotNull
    private TemplateFormat templateFormat = TemplateFormat.TEMPLATE;
}
