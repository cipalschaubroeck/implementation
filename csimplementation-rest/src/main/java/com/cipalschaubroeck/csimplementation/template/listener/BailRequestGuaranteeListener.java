package com.cipalschaubroeck.csimplementation.template.listener;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.organization.service.ContactInfoSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RepositoryEventHandler
public class BailRequestGuaranteeListener {

    @Autowired
    private ContactInfoSupport support;
    @Autowired
    private PostalDocumentProcessRepository repository;

    @HandleBeforeSave
    public void checkPhone(BailRequestGuarantee data) {
        if (data.getId() != null) {
            List<PostalDocumentProcess> processes = repository.findAllByBodyId(data.getId());
            if (!processes.isEmpty()) {
                PostalDocumentProcess process = processes.get(0);
                if (data.getPhone() != null && process.getStatus() != DocumentStatus.IN_PROGRESS) {
                    data.setPhone(support.copyIfNeeded(data.getPhone(), data.getCollaborator().getEffectivePerson().getPhoneNumbers()));
                }
            }
        }
    }
}
