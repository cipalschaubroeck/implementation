package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.template.repository.TemplateDataRepository;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataDuplicator;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import com.cipalschaubroeck.csimplementation.xperido.XPeridoService;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.JAXBException;
import java.util.EnumMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
public class DefaultTemplateDataService implements TemplateDataService {

    private Map<TemplateType, TemplateDataDuplicator> duplicators = new EnumMap<>(TemplateType.class);

    @Autowired
    private XPeridoService templateService;
    @Autowired
    private TemplateDataRepository repository;

    @Autowired
    public void setDuplicators(List<TemplateDataDuplicator> duplicatorList) {
        duplicatorList.forEach(d -> duplicators.put(d.getSupportedType(), d));
    }

    @Override
    public TemplateData duplicate(TemplateData original) {
        if (original == null) {
            return null;
        }
        return duplicators.get(original.getType()).duplicate(original);
    }

    @Override
    public List<Template> getAvailableTemplates(Integer templateDataId, boolean includeThumbnails) {
        TemplateData data = repository.findById(templateDataId).orElseThrow(EntityNotFoundException::new);
        // TODO CSPROC-1774 use locale for the contract owning the process with this template data
        return templateService.getTemplates(Locale.getDefault(), data.getType(), includeThumbnails);
    }

    @Override
    public Document downloadSingle(Integer templateDataId, OutputType format, String fileName) throws JAXBException {
        TemplateData data = repository.findById(templateDataId).orElseThrow(EntityNotFoundException::new);
        // TODO CSPROC-1774 use locale for the contract owning the process with this template data
        return templateService.createDocument(new PostalAddressee(), data, Locale.getDefault(), format, fileName);
    }
}
