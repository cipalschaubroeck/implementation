package com.cipalschaubroeck.csimplementation.template.domain;

import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "signing_officer")
@Getter
@Setter
public class SigningOfficer {
    @Id
    private SigningOfficerId id = new SigningOfficerId();

    @ManyToOne(optional = false)
    @JoinColumn(name = "person", foreignKey = @ForeignKey(name = "fk_signing_officer_person"))
    @MapsId("person")
    @NotNull
    private QualifiedPerson person;

    @ManyToOne(optional = false)
    @JoinColumn(name = "data", foreignKey = @ForeignKey(name = "fk_signing_officer_data"))
    @MapsId("data")
    @NotNull
    private SigningOfficerData data;
}
