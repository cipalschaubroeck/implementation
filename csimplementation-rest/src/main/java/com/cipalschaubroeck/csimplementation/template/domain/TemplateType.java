package com.cipalschaubroeck.csimplementation.template.domain;

public enum TemplateType {
    HEADING,
    SIGNING_OFFICERS,
    BAIL_REQUEST_GUARANTEE;

    public static final class Name {
        private Name() {
            // constant class
        }

        static final String HEADING = "HEADING";
        static final String SIGNING_OFFICERS = "SIGNING_OFFICERS";
    }

    /**
     * @return name (without extension) of the corresponding schema in classpath:template/schema
     */
    public String getSchemaName() {
        return name().toLowerCase().replaceAll("_", "-");
    }

    /**
     * @return true if and only if this template type uses information within the addressee, false if all it needs comes
     * from the template data or the process
     */
    public boolean needsAddressee() {
        return this == HEADING;
    }
}
