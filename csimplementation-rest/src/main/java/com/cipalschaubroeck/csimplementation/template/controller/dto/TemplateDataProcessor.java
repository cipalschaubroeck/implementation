package com.cipalschaubroeck.csimplementation.template.controller.dto;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// instantiated by Spring
@SuppressWarnings("unused")
public class TemplateDataProcessor implements ResourceProcessor<Resource<TemplateData>> {

    @Autowired
    private EntityLinks entityLinks;

    @Override
    public Resource<TemplateData> process(Resource<TemplateData> resource) {
        Link link = entityLinks.linkToSingleResource(TemplateData.class, resource.getContent().getId())
                .withSelfRel();
        resource.add(new Link(link.getHref() + "/available-templates{?thumbnails}", "available-templates"));
        resource.add(new Link(link.getHref() + "/generate{?format,forceDownload,fileName}", "generate"));
        return resource;
    }
}
