package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataDuplicator;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

@Component
public class BailRequestGuaranteeDuplicator implements TemplateDataDuplicator {
    @Override
    public TemplateType getSupportedType() {
        return TemplateType.BAIL_REQUEST_GUARANTEE;
    }

    @Override
    public TemplateData duplicate(TemplateData original) {
        BailRequestGuarantee copy = new BailRequestGuarantee();
        BailRequestGuarantee brg = (BailRequestGuarantee) original;
        copy.setBailAmount(brg.getBailAmount());
        copy.setCollaborator(brg.getCollaborator());
        if (brg.getPhone() != null) {
            copy.setPhone(brg.getCollaborator().getPerson().getPhoneNumbers().stream()
                    .filter(number -> StringUtils.equals(number.getPhone(), brg.getPhone().getPhone()))
                    .findFirst().orElse(null));
        }
        copy.setReference(brg.getReference());
        return copy;
    }
}
