package com.cipalschaubroeck.csimplementation.template.domain.validator;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;
import java.util.Objects;

@Component
public class BailRequestGuaranteeValidator implements Validator {
    @Autowired
    private PostalDocumentProcessRepository repository;

    @Override
    public boolean supports(Class<?> clazz) {
        return BailRequestGuarantee.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        BailRequestGuarantee data = (BailRequestGuarantee) target;
        List<PostalDocumentProcess> processes = repository.findAllByBodyId(data.getId());
        if (!processes.isEmpty()) {
            if (processes.size() > 1) {
                errors.reject("body.should-not-be-shared");
            }
            PostalDocumentProcess process = processes.get(0);
            checkProcess(errors, data, process);
        }

        // TODO CSPROC-2273 rework this relationship for bailGuaranteeValidator
//        if (data.getDepartment() != null &&
//                data.getCollaborator().getDepartments().stream()
//                        .noneMatch(dep -> Objects.equals(dep.getId(), data.getDepartment().getId()))) {
//            errors.rejectValue("department", "bail-request-guarantee.department.should-belong-to-collaborator");
//        }
    }

    private void checkProcess(Errors errors, BailRequestGuarantee data, PostalDocumentProcess process) {
        if (data.getPhone() != null) {
            if (process.getStatus() == DocumentStatus.IN_PROGRESS) {
                if (data.getCollaborator().getEffectivePerson().getPhoneNumbers().stream()
                        .noneMatch(phone -> Objects.equals(phone.getId(), data.getPhone().getId()))) {
                    errors.rejectValue("phone", "bail-request-guarantee.phone.should-belong-to-collaborator");
                }
            } else if (data.getCollaborator().getEffectivePerson().getPhoneNumbers().stream()
                    .anyMatch(phone -> Objects.equals(phone.getId(), data.getPhone().getId()))) {
                errors.rejectValue("phone", "bail-request-guarantee.phone.should-not-belong-to-collaborator");
            }
        }
        if (!Objects.equals(data.getCollaborator().getAuthority().getId(),
                process.getContract().getContractAuthorityRelation().getContractingAuthority().getId())) {
            errors.rejectValue("collaborator", "bail-request-guarantee.collaborator.should-belong-to-authority");
        }
    }
}
