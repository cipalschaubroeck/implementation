package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataDuplicator;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class SigningOfficerDataDuplicator implements TemplateDataDuplicator {
    @Override
    public TemplateType getSupportedType() {
        return TemplateType.SIGNING_OFFICERS;
    }

    @Override
    public TemplateData duplicate(TemplateData original) {
        SigningOfficerData copy = new SigningOfficerData();
        copy.setSigningOfficers(((SigningOfficerData) original).getSigningOfficers().stream()
                .map(this::duplicate).collect(Collectors.toList()));
        copy.getSigningOfficers().forEach(officer -> officer.setData(copy));
        return copy;
    }

    private SigningOfficer duplicate(SigningOfficer officer) {
        SigningOfficer copy = new SigningOfficer();
        copy.setPerson(officer.getPerson());
        return copy;
    }
}
