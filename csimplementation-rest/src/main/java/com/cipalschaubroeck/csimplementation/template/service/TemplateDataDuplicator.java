package com.cipalschaubroeck.csimplementation.template.service;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;

/**
 * DocumentProcess' duplication require the ability to duplicate TemplateData. This duplication
 * should consider that: generate fields should remain as null; relationship fields, when related to
 * something independent of the document process, should copy the reference: For instance, a collaborator
 * is the same collaborator, a contract is the same because document processes are copied within the same
 * contract, so qualified persons are the same, addressees are not. Last a field in neither case
 * should be deep copied with the same considerations for its own fields.
 * Further, the duplicator do not save anything. The caller (more often cascade annotations) should be
 * responsible for that
 */
public interface TemplateDataDuplicator {
    /**
     * @return type of templates this template data duplicator can duplicate
     */
    TemplateType getSupportedType();

    TemplateData duplicate(TemplateData original);
}
