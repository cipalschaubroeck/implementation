package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "template-data")
public interface TemplateDataRepository extends PagingAndSortingRepository<TemplateData, Integer> {
}
