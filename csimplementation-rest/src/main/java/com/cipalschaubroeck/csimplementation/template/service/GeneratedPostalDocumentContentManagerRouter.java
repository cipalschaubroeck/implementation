package com.cipalschaubroeck.csimplementation.template.service;

import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.function.Function;

import static com.cipalschaubroeck.csimplementation.template.domain.TemplateType.BAIL_REQUEST_GUARANTEE;

@Component
public class GeneratedPostalDocumentContentManagerRouter implements Function<PostalDocumentProcess, List<String>> {

    @Override
    public List<String> apply(PostalDocumentProcess process) {
        List<String> result;
        if (process.getBody().getType() == BAIL_REQUEST_GUARANTEE) {
            // TODO CSPROC-1757 add tenant as it is in confluence
            result = Arrays.asList(
                    process.getContract().getContractAuthorityRelation().getContractingAuthority().getName(),
                    process.getContract().getProcurement().getName(),
                    process.getContract().getName(),
                    "Administrative", "Bail", "Requests_guarantee");
        } else {
            throw new IllegalArgumentException(process.getBody().getType().name() + "is not implemented");
        }
        return result;
    }
}
