package com.cipalschaubroeck.csimplementation.template.domain;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "signing_officer_data")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class SigningOfficerData extends TemplateData {
    @OneToMany(mappedBy = "data")
    private List<SigningOfficer> signingOfficers = new ArrayList<>();

    @Override
    public TemplateType getType() {
        return TemplateType.SIGNING_OFFICERS;
    }
}
