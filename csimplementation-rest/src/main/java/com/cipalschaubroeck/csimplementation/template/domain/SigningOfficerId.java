package com.cipalschaubroeck.csimplementation.template.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
public class SigningOfficerId implements Serializable {
    @Column(name = "person")
    private Integer person;

    @Column(name = "data")
    private Integer data;
}
