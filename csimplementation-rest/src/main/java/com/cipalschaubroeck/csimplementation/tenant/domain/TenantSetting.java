package com.cipalschaubroeck.csimplementation.tenant.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tenant_setting")
@Getter
@Setter
public class TenantSetting {
    @Id
    @Column(name = "key", updatable = false, unique = true)
    private String key;
    @Column(name = "value")
    private String value;
}
