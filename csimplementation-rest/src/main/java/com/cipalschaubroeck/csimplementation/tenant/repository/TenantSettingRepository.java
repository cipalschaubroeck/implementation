package com.cipalschaubroeck.csimplementation.tenant.repository;

import com.cipalschaubroeck.csimplementation.tenant.domain.TenantSetting;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantSettingRepository extends CrudRepository<TenantSetting, String> {
}
