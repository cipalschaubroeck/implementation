package com.cipalschaubroeck.csimplementation.tenant.service;

import com.cipalschaubroeck.csimplementation.tenant.domain.TenantSetting;
import com.cipalschaubroeck.csimplementation.tenant.repository.TenantSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TenantSettingService {
    private static final String THEME = "theme";

    @Autowired
    private TenantSettingRepository repository;

    public Optional<String> getThemeForTenant() {
        return repository.findById(THEME).map(TenantSetting::getValue);
    }

    public String getHashForTenant() {
        return repository.findById("tenantHash").map(TenantSetting::getValue).orElseThrow();
    }
}
