package com.cipalschaubroeck.csimplementation.person.domain;

/**
 * a convenient interface to mark several other classes that can be resolved to a person
 */
public interface PersonResolvable {
    Person getEffectivePerson();
}
