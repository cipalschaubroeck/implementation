package com.cipalschaubroeck.csimplementation.person.service.impl;

import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.service.PhoneNumberDuplicator;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("unused")
public class DefaultPhoneNumberDuplicator implements PhoneNumberDuplicator {
    /**
     * duplicates the values, but ignoring generate values
     *
     * @param original a phone to duplicate
     * @return a copy of original with null in the generate fields
     */
    @Override
    public PhoneNumber duplicate(PhoneNumber original) {
        PhoneNumber p = new PhoneNumber();
        p.setPhone(original.getPhone());
        return p;
    }
}
