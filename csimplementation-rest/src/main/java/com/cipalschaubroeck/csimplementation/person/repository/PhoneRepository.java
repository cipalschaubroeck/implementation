package com.cipalschaubroeck.csimplementation.person.repository;

import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "phone")
public interface PhoneRepository extends PagingAndSortingRepository<PhoneNumber, Integer> {
}
