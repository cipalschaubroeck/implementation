package com.cipalschaubroeck.csimplementation.person.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "phone")
@Getter
@Setter
public class PhoneNumber {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "phone")
    @NotNull
    private String phone;

    // TODO CSPROC-2101 Add fax ether as a new resource or part of phone
}
