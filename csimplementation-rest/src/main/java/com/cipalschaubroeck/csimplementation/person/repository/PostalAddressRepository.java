package com.cipalschaubroeck.csimplementation.person.repository;

import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "postal-address")
public interface PostalAddressRepository extends PagingAndSortingRepository<PostalAddress, Integer> {
}
