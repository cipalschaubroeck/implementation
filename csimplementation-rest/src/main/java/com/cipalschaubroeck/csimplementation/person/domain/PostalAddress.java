package com.cipalschaubroeck.csimplementation.person.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "postal_address")
@Getter
@Setter
// TODO CSPROC-2099 redefine postal address mandatory fields in the model
public class PostalAddress {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "street")
    private String street;

    @Column(name = "number")
    private String number;

    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "municipality")
    private String municipality;

    @Column(name = "country")
    @NotNull
    private String country;

    @Column(name = "box")
    private String box;
}
