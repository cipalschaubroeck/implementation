package com.cipalschaubroeck.csimplementation.person.repository;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "email")
public interface EmailRepository extends PagingAndSortingRepository<EmailAddress, Integer> {
}
