package com.cipalschaubroeck.csimplementation.person.service;

import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;

public interface PostalAddressDuplicator {
    PostalAddress duplicate(PostalAddress original);
}
