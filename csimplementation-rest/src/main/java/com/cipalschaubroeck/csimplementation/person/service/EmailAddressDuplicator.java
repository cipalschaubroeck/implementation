package com.cipalschaubroeck.csimplementation.person.service;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;

public interface EmailAddressDuplicator {
    EmailAddress duplicate(EmailAddress original);
}
