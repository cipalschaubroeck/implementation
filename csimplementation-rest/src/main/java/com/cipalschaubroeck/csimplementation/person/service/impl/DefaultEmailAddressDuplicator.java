package com.cipalschaubroeck.csimplementation.person.service.impl;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.service.EmailAddressDuplicator;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("unused")
public class DefaultEmailAddressDuplicator implements EmailAddressDuplicator {
    /**
     * duplicates the values, but ignoring generate values
     *
     * @param original a phone to duplicate
     * @return a copy of original with null in the generate fields
     */
    @Override
    public EmailAddress duplicate(EmailAddress original) {
        EmailAddress p = new EmailAddress();
        p.setEmail(original.getEmail());
        return p;
    }
}
