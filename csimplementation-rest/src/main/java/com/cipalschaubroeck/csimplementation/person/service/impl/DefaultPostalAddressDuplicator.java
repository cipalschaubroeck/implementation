package com.cipalschaubroeck.csimplementation.person.service.impl;

import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.cipalschaubroeck.csimplementation.person.service.PostalAddressDuplicator;
import org.springframework.stereotype.Service;

@Service
@SuppressWarnings("unused")
public class DefaultPostalAddressDuplicator implements PostalAddressDuplicator {
    /**
     * duplicates the values, but ignoring generate values
     *
     * @param original a phone to duplicate
     * @return a copy of original with null in the generate fields
     */
    @Override
    public PostalAddress duplicate(PostalAddress original) {
        PostalAddress p = new PostalAddress();
        p.setStreet(original.getStreet());
        p.setPostalCode(original.getPostalCode());
        p.setNumber(original.getNumber());
        p.setMunicipality(original.getMunicipality());
        p.setCountry(original.getCountry());
        return p;
    }
}
