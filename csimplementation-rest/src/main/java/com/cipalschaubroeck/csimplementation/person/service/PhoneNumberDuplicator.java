package com.cipalschaubroeck.csimplementation.person.service;

import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;

public interface PhoneNumberDuplicator {
    PhoneNumber duplicate(PhoneNumber original);
}
