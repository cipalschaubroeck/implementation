package com.cipalschaubroeck.csimplementation.person.domain;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "person")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter
@Setter
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "first_name")
    @NotNull
    private String firstName;

    @Column(name = "last_name")
    @NotNull
    private String lastName;
    // TODO CSPROC-2112 Check if this relation is needed in the actual business logic
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_phone",
            joinColumns = @JoinColumn(name = "person"),
            foreignKey = @ForeignKey(name = "fk_person_phone_person"),
            inverseJoinColumns = @JoinColumn(name = "phone"),
            inverseForeignKey = @ForeignKey(name = "fk_person_phone_phone"))
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();
    // TODO CSPROC-2112 Check if this relation is needed in the actual business logic
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_email",
            joinColumns = @JoinColumn(name = "person"),
            foreignKey = @ForeignKey(name = "fk_person_email_person"),
            inverseJoinColumns = @JoinColumn(name = "email"),
            inverseForeignKey = @ForeignKey(name = "fk_person_email_email"))
    private List<EmailAddress> emails = new ArrayList<>();
    // TODO CSPROC-2112 Check if this relation is needed in the actual business logic
    @OneToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_address",
            joinColumns = @JoinColumn(name = "person"),
            foreignKey = @ForeignKey(name = "fk_person_address_person"),
            inverseJoinColumns = @JoinColumn(name = "postal_address"),
            inverseForeignKey = @ForeignKey(name = "fk_person_address_address"))
    private List<PostalAddress> addresses = new ArrayList<>();


    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "email", foreignKey = @ForeignKey(name = "fk_collaborator_email_1"))
    private EmailAddress email;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "phone", foreignKey = @ForeignKey(name = "fk_collaborator_phone_1"))
    private PhoneNumber phone;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "address", foreignKey = @ForeignKey(name = "fk_collaborator_address_1"))
    private PostalAddress address;

    @Transient
    // used by jackson
    @SuppressWarnings("unused")
    public String getFullName() {
        if (StringUtils.isBlank(getFirstName())) {
            return StringUtils.defaultString(getLastName());
        } else if (StringUtils.isBlank(getLastName())) {
            return StringUtils.defaultString(getFirstName());
        } else {
            return getFirstName() + " " + getLastName();
        }
    }
}
