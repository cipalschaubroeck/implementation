package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "email_document_process")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@JsonTypeName(DocumentProcessType.Name.EMAIL)
@Getter
@Setter
public class EmailDocumentProcess extends DocumentProcess {

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "body")
    private TemplateData body;

    @ManyToMany
    @JoinTable(name = "email_attachment",
            joinColumns = @JoinColumn(name = "process"),
            foreignKey = @ForeignKey(name = "fk_email_attachment_process"),
            inverseJoinColumns = @JoinColumn(name = "file"),
            inverseForeignKey = @ForeignKey(name = "fk_email_attachment_file"))
    private List<ContentManagerFile> attachments = new ArrayList<>();

    @Override
    public DocumentStatus getStatus() {
        // TODO CSPROC-1508 depends on email communication's status
        return DocumentStatus.IN_PROGRESS;
    }

    @Override
    public DocumentProcessType getType() {
        return DocumentProcessType.EMAIL;
    }
}
