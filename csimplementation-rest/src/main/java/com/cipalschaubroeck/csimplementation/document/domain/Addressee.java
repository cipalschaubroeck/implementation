package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
@Table(name = "addressee")
@Inheritance(strategy = InheritanceType.JOINED)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AdHocAddressee.class, name = QualifiedPersonType.Name.AD_HOC),
        @JsonSubTypes.Type(value = ExternalAddressee.class, name = QualifiedPersonType.Name.EXTERNAL),
        @JsonSubTypes.Type(value = InternalAddressee.class, name = QualifiedPersonType.Name.INTERNAL)
})
@Getter
@Setter
public abstract class Addressee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    @Setter
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    @Getter
    @Setter
    private Integer versionNum;

    // can't use cascade all because the same entity may or may be not used in Person
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "phone", foreignKey = @ForeignKey(name = "fk_addressee_phone"))
    private PhoneNumber phone;

    // can't use cascade all because the same entity may or may be not used in Person
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "email", foreignKey = @ForeignKey(name = "fk_addressee_email"))
    private EmailAddress email;

    // can't use cascade all because the same entity may or may be not used in Person
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "address", foreignKey = @ForeignKey(name = "fk_addressee_address"))
    private PostalAddress address;

    @Transient
    public abstract QualifiedPersonType getType();

    @Transient
    public abstract Person getEffectivePerson();

    @Transient
    public abstract String getCompanyName();
}
