package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "generated_document")
@Getter
@Setter
public class GeneratedDocument {
    private static final String DATE_PATTERN = "dd/MM/yyyy";

    @EmbeddedId
    private AddresseeId id;

    @MapsId
    @ManyToOne(optional = false, cascade = CascadeType.MERGE)
    @JoinColumn(name = "addressee", referencedColumnName = "addressee")
    @JoinColumn(name = "process", referencedColumnName = "process")
    private PostalAddressee addressee;

    @OneToOne
    @JoinColumn(name = "generated", foreignKey = @ForeignKey(name = "fk_generated_document_generated"))
    private ContentManagerFile generated;

    @OneToOne
    @JoinColumn(name = "dispatched", foreignKey = @ForeignKey(name = "fk_generated_document_dispatched"))
    private ContentManagerFile dispatched;

    @Column(name = "creation_date", updatable = false)
    @DateTimeFormat(pattern = DATE_PATTERN)
    @CreationTimestamp
    private LocalDate creationDate;

    @Column(name = "signing_date", updatable = false)
    @DateTimeFormat(pattern = DATE_PATTERN)
    private LocalDate signingDate;

    @Column(name = "dispatch_date", updatable = false)
    @DateTimeFormat(pattern = DATE_PATTERN)
    private LocalDate dispatchDate;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private DocumentStatus status = DocumentStatus.IN_PROGRESS;

    @Column(name = "dashboard")
    private boolean dashboard;
}
