package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.AdHocAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeFactory;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.cipalschaubroeck.csimplementation.project.repository.AdHocQualifiedPersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.Optional;

@Service
public class DefaultAdHocAddresseeSupport extends AbstractAddresseeSupport
        implements AddresseeDuplicator, AddresseeFactory {
    @Autowired
    private AdHocQualifiedPersonRepository qualifiedPersonRepository;

    @Override
    public QualifiedPersonType getSupportedType() {
        return QualifiedPersonType.AD_HOC;
    }

    @Override
    public boolean supports(Class<?> domainType) {
        return AdHocQualifiedPerson.class.isAssignableFrom(domainType)
                || QualifiedPerson.class.isAssignableFrom(domainType);
    }

    @Override
    public Addressee buildFromAnotherEntity(Serializable id) {
        Optional<AdHocQualifiedPerson> byId = qualifiedPersonRepository.findById((Integer) id);
        if (byId.isPresent()) {
            AdHocAddressee addressee = new AdHocAddressee();
            addressee.setPerson(byId.get());
            super.defaultContactInfo(addressee);
            return addressee;
        } else {
            throw new EntityNotFoundException("Not found AdHocQualifiedPerson with id " + id.toString());
        }
    }

    @Override
    public AdHocAddressee duplicate(Addressee original) {
        AdHocAddressee copy = new AdHocAddressee();
        copy.setPerson(((AdHocAddressee) original).getPerson());
        super.commonFields(original, copy);
        return copy;
    }
}
