package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.service.ContentManagerFileService;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.domain.EmailDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class EmailDPDuplicator implements DocumentProcessDuplicator {
    @Autowired
    private ContentManagerFileService contentManagerFileService;
    @Autowired
    private TemplateDataService templateDataService;

    @Override
    public DocumentProcessType getSupportedType() {
        return DocumentProcessType.EMAIL;
    }

    @Override
    public DocumentProcess duplicate(DocumentProcess original) {
        EmailDocumentProcess copy = new EmailDocumentProcess();
        copy.setAttachments(((EmailDocumentProcess) original).getAttachments()
                .stream().map(contentManagerFileService::duplicate).collect(Collectors.toList()));
        copy.setBody(templateDataService.duplicate(((EmailDocumentProcess) original).getBody()));
        setCommonFields(original, copy);
        return copy;
    }
}
