package com.cipalschaubroeck.csimplementation.document.controller.projections;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@Projection(name = "generatedDocumentDto", types = GeneratedDocument.class)
public interface GeneratedDocumentProjection {

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getCreationDate();

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getSigningDate();

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getDispatchDate();

    DocumentStatus getStatus();

    // used by jackson
    @SuppressWarnings("unused")
    boolean isDashboard();

    @Value("#{target.addressee.addressee.effectivePerson.fullName}")
    String getAddresseeName();

    @JsonIgnore
    ContentManagerFile getGenerated();

    @JsonIgnore
    ContentManagerFile getDispatched();
}
