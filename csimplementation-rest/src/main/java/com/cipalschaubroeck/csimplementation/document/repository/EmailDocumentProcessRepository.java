package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.config.DocumentProcessConfigConstants;
import com.cipalschaubroeck.csimplementation.document.domain.EmailDocumentProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "email-document-process",
        collectionResourceRel = DocumentProcessConfigConstants.DOCUMENT_PROCESS_COLLECTION_REL)
public interface EmailDocumentProcessRepository extends PagingAndSortingRepository<EmailDocumentProcess, Integer> {
}
