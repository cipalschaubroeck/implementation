package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "addressee_external")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class ExternalAddressee extends Addressee {
    @ManyToOne(optional = false)
    @JoinColumns(foreignKey = @ForeignKey(name = "fk_addressee_external_contact"),
            value = {@JoinColumn(name = "person", referencedColumnName = "person"),
                    @JoinColumn(name = "organization", referencedColumnName = "organization")})
    @NotNull
    private ContactPerson contact;

    @Override
    public QualifiedPersonType getType() {
        return QualifiedPersonType.EXTERNAL;
    }

    @Override
    public Person getEffectivePerson() {
        return contact.getPerson();
    }

    @Override
    public String getCompanyName() {
        return contact.getOrganization().getName();
    }
}
