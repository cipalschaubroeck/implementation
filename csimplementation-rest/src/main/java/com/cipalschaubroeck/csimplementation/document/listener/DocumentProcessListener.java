package com.cipalschaubroeck.csimplementation.document.listener;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import org.springframework.data.rest.core.annotation.HandleBeforeDelete;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

import javax.validation.ValidationException;

@Component
@RepositoryEventHandler
public class DocumentProcessListener {

    /**
     * A document process may be deleted only if its status is IN_PROGRESS
     *
     * @param process the document process that is going to be deleted
     */
    @HandleBeforeDelete
    // used by spring
    @SuppressWarnings("unused")
    void failOnForbiddenDelete(DocumentProcess process) {
        if (DocumentStatus.IN_PROGRESS != process.getStatus()) {
            throw new ValidationException("A document process can only be deleted in \"In progress\" status");
        }
    }

    /**
     * A generate document may be deleted only if its status is in progress
     *
     * @param document a generate document that is about to be deleted
     */
    @HandleBeforeDelete
    // used by spring
    @SuppressWarnings("unused")
    void failOnForbiddenDelete(GeneratedDocument document) {
        if (DocumentStatus.IN_PROGRESS != document.getStatus()) {
            throw new ValidationException("A document can only be deleted in \"In progress\" status");
        }
    }

}
