package com.cipalschaubroeck.csimplementation.document.domain;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "upload_document_process")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@JsonTypeName(DocumentProcessType.Name.UPLOAD)
public class UploadDocumentProcess extends DocumentProcess {

    @Override
    public DocumentStatus getStatus() {
        // TODO CSPROC-1509 depends on uploaded files
        return DocumentStatus.IN_PROGRESS;
    }

    @Override
    public DocumentProcessType getType() {
        return DocumentProcessType.UPLOAD;
    }
}
