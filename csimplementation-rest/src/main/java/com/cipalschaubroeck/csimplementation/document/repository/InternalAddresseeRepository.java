package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.InternalAddressee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "internal-addressee")
public interface InternalAddresseeRepository extends PagingAndSortingRepository<InternalAddressee, Integer> {
}
