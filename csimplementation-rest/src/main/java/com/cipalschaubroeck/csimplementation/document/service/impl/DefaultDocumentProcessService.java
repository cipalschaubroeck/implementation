package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.controller.specifications.DocumentProcessSpecification;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.repository.DocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessService;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

@Service
public class DefaultDocumentProcessService implements DocumentProcessService {

    private Map<DocumentProcessType, DocumentProcessDuplicator> duplicators = new EnumMap<>(DocumentProcessType.class);

    @Autowired
    private DocumentProcessRepository repository;
    @Autowired
    private EntityEventPublisher publisher;

    @Autowired
    public void setDuplicators(List<DocumentProcessDuplicator> duplicatorList) {
        duplicatorList.forEach(d -> duplicators.put(d.getSupportedType(), d));
    }

    @Override
    @Transactional
    public DocumentProcess duplicate(@NotNull Integer id, @NotNull String nameForCopy) {
        DocumentProcess original = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find process with id " + id));
        DocumentProcessDuplicator duplicator = duplicators.get(original.getType());
        if (duplicator == null) {
            throw new IllegalArgumentException("Unknown subclass of DocumentProcess");
        }
        DocumentProcess copy = duplicator.duplicate(original);
        copy.setName(nameForCopy);
        return publisher.create(copy, repository);
    }

    @Override
    @Transactional
    public Page<DocumentProcess> findDocumentProcessWithStatus(DocumentProcessSpecification specification, Pageable pageable) {
        Page<DocumentProcess> page = repository.findAll(specification, pageable);
        page.get().forEach(DocumentProcess::getStatus);
        return page;
    }
}
