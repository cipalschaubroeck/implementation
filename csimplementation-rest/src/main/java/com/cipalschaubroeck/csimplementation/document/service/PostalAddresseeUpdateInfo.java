package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PostalAddresseeUpdateInfo {
    private String salutation;
    private String ourProperty;
    private String yourProperty;
    /**
     * if not null, must belong to the person
     */
    private Integer phoneId;
    /**
     * if not null, must belong to the person
     */
    private Integer emailId;
    /**
     * must correspond to either an Addressee,
     * a Collaborator, a ContactPerson or an AdHocQualifiedPerson
     */
    private EntityInfo personInfo = new EntityInfo();
}
