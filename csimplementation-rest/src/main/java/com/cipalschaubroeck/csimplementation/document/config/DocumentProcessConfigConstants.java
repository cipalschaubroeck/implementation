package com.cipalschaubroeck.csimplementation.document.config;

public class DocumentProcessConfigConstants {
    private DocumentProcessConfigConstants() {
    }

    public static final String DOCUMENT_PROCESS_COLLECTION_REL = "documentProcesses";
    public static final String DOCUMENT_PROCESS_PATH = "document-process";
}
