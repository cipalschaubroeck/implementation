package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalAddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.cipalschaubroeck.csimplementation.document.service.PostalDocumentProcessService;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class DefaultPostalDocumentProcessService implements PostalDocumentProcessService {
    @Autowired
    private PostalDocumentProcessRepository repository;
    @Autowired
    private PostalAddresseeRepository postalAddresseeRepository;
    @Autowired
    private PostalAddresseeService postalAddresseeService;
    @Autowired
    private EntityEventPublisher publisher;
    @Autowired
    private PostalAttachmentComponent attachmentComponent;

    @Override
    public Iterable<PostalAddressee> setAddressees(Integer processId, Iterable<EntityInfo> people) {
        PostalDocumentProcess byId = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);
        byId.getAddresseeList().forEach(postalAddresseeRepository::delete);
        byId.getAddresseeList().clear();
        return addPostalAddressees(people, byId);
    }

    private Iterable<PostalAddressee> addPostalAddressees(Iterable<EntityInfo> people, PostalDocumentProcess process) {
        List<PostalAddressee> created = new ArrayList<>();
        for (EntityInfo person : people) {
            if (PostalAddressee.class.isAssignableFrom(person.getDomainType())) {
                postalAddresseeRepository.findById((AddresseeId) person.getId()).ifPresent(postalAddressee -> {
                    postalAddressee.setProcess(process);
                    created.add(publisher.update(postalAddressee, postalAddresseeRepository));
                });
            } else {
                Addressee addressee = postalAddresseeService.build(person);
                PostalAddressee postalAddressee = new PostalAddressee();
                postalAddressee.setAddressee(addressee);
                postalAddressee.setProcess(process);
                created.add(publisher.create(postalAddressee, postalAddresseeRepository));
            }
        }
        return created;
    }

    @Override
    public Iterable<PostalAddressee> addAddressees(Integer processId, Iterable<EntityInfo> people) {
        PostalDocumentProcess byId = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);
        return addPostalAddressees(people, byId);
    }

    @Override
    public ContentManagerFile addAttachment(Integer processId, String fileName, byte[] data, String mime) {
        return attachmentComponent.addAttachment(processId, fileName, data, mime);
    }

    @Override
    public Iterable<ContentManagerFile> linkAsAttachments(Integer processId, Iterable<Integer> cmFileId) {
        return attachmentComponent.linkAsAttachments(processId, cmFileId);
    }

    @Override
    public Iterable<ContentManagerFile> setAttachments(Integer processId, Iterable<Integer> cmFieldId) {
        return attachmentComponent.setAttachments(processId, cmFieldId);
    }

    @Override
    public void clearAttachments(Integer processId) {
        attachmentComponent.clearAttachments(processId);
    }
}
