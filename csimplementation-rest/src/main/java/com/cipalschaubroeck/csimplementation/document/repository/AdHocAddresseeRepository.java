package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.AdHocAddressee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "ad-hoc-addressee")
public interface AdHocAddresseeRepository extends PagingAndSortingRepository<AdHocAddressee, Integer> {

}
