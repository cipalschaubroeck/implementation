package com.cipalschaubroeck.csimplementation.document.controller.processors;

import com.cipalschaubroeck.csimplementation.document.controller.PostalDocumentProcessController;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;

@Component
@Slf4j
public class PostalDocumentProcessProcessor implements ResourceProcessor<Resource<PostalDocumentProcess>> {
    @Override
    public Resource<PostalDocumentProcess> process(Resource<PostalDocumentProcess> resource) {
        PostalDocumentProcess process = resource.getContent();
        try {
            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostalDocumentProcessController.class)
                    .generateAllDocuments(process.getId(), null)).withRel("generateAll"));
            if (process.getBody() != null) {
                resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostalDocumentProcessController.class)
                        .preview(process.getId(), process.getBody().getId(), null)).withRel("previewBody"));
            }
            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostalDocumentProcessController.class)
                    .preview(process.getId(), process.getSigningOfficerData().getId(), null)).withRel("previewSigningOfficers"));
        } catch (IOException | JAXBException e) {
            log.error("Should never happen since it is not an actual invocation", e);
        }
        return resource;
    }
}
