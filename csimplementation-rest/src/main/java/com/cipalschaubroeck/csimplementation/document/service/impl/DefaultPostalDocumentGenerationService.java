package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalAddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.PostalDocumentGenerationService;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.repository.TemplateDataRepository;
import com.cipalschaubroeck.csimplementation.template.service.GeneratedPostalDocumentContentManagerRouter;
import com.cipalschaubroeck.csimplementation.xperido.XPeridoService;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class DefaultPostalDocumentGenerationService implements PostalDocumentGenerationService {
    @Autowired
    private PostalAddresseeRepository postalAddresseeRepository;
    @Autowired
    private PostalDocumentProcessRepository postalProcessRepository;
    @Autowired
    private XPeridoService xPeridoService;
    @Autowired
    private CMCommunicationService contentManager;
    @Autowired
    private GeneratedPostalDocumentContentManagerRouter router;
    @Autowired
    private TemplateDataRepository templateDataRepository;

    @Override
    @Transactional(readOnly = true)
    public Document generate(AddresseeId addresseeId, Integer templateDataId, OutputType format) throws JAXBException {
        PostalAddressee addressee = postalAddresseeRepository.findById(addresseeId).orElseThrow(EntityNotFoundException::new);
        TemplateData data = templateDataRepository.findById(templateDataId).orElseThrow(EntityNotFoundException::new);
        // TODO CSPROC-1774 extract locale from process when we have finished that issue
        return xPeridoService.createDocument(addressee, data, Locale.getDefault(), format, getFileName(addressee, data, format));
    }

    @Transactional(readOnly = true)
    @Override
    public Document generate(Integer processId, Integer templateDataId, OutputType format) throws JAXBException {
        PostalDocumentProcess process = postalProcessRepository.findById(processId).orElseThrow(EntityNotFoundException::new);
        TemplateData data = templateDataRepository.findById(templateDataId).orElseThrow(EntityNotFoundException::new);
        // TODO CSPROC-1774 extract locale from process when we have finished that issue
        return xPeridoService.createDocument(process, data, Locale.getDefault(), format, getFileName(data, format));
    }

    @Transactional(readOnly = true)
    @Override
    public Document generate(AddresseeId id, OutputType format) throws IOException {
        PostalAddressee addressee = postalAddresseeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        return generate(addressee, format);
    }

    private Document generate(PostalAddressee addressee, OutputType format) throws IOException {
        String fileName = getFileName(addressee, format);
        // TODO CSPROC-1774 extract locale from process when we have finished that issue
        return xPeridoService.createDocument(addressee, Locale.getDefault(), format, fileName);
    }

    private String getFileName(PostalAddressee addressee, OutputType format) {
        return getFileName(addressee, addressee.getProcess().getBody(), format);
    }

    private String getFileName(PostalAddressee addressee, TemplateData data, OutputType format) {
        return data.getType().getSchemaName()
                + "-" + addressee.getAddressee().getEffectivePerson().getFullName()
                + format.getExtension();
    }

    private String getFileName(TemplateData data, OutputType format) {
        return data.getType().getSchemaName() + format.getExtension();
    }

    @Transactional
    @Override
    public GeneratedDocument generateAndSave(AddresseeId id) throws IOException {
        PostalAddressee addressee = postalAddresseeRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        if (addressee.getGeneratedDocument() != null && addressee.getGeneratedDocument().getStatus() != DocumentStatus.IN_PROGRESS) {
            throw new IllegalStateException("The document for addressee " + id + " cannot be modified at this point");
        }
        return generateAndSave(addressee);
    }

    private GeneratedDocument generateAndSave(PostalAddressee addressee) throws IOException {
        Document document = generate(addressee, OutputType.PDF);
        GeneratedDocument generatedDocument = Optional.ofNullable(addressee.getGeneratedDocument()).orElseGet(GeneratedDocument::new);
        generatedDocument.setAddressee(addressee);
        if (generatedDocument.getGenerated() != null) {
            contentManager.removeDocument(generatedDocument.getGenerated().getContentManagerId());
        } else {
            generatedDocument.setGenerated(new ContentManagerFile());
        }
        List<String> pathParts = router.apply(addressee.getProcess());
        org.apache.chemistry.opencmis.client.api.Document uploaded = contentManager.uploadFile(document.getFileName(),
                document.getData(), pathParts, MediaType.APPLICATION_PDF_VALUE);
        generatedDocument.getGenerated().setContentManagerId(uploaded.getId());
        generatedDocument.getGenerated().setPublicPath(StringUtils.join(pathParts, '/'));
        return generatedDocument;
    }

    @Transactional
    @Override
    public List<GeneratedDocument> generateAndSaveAll(Integer id) throws IOException {
        PostalDocumentProcess stored = postalProcessRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        List<GeneratedDocument> generated = new ArrayList<>(stored.getAddresseeList().size());
        for (PostalAddressee pa : stored.getAddresseeList()) {
            if (pa.getGeneratedDocument() == null || pa.getGeneratedDocument().getStatus() == DocumentStatus.IN_PROGRESS) {
                generated.add(generateAndSave(pa));
            } else {
                generated.add(pa.getGeneratedDocument());
            }
        }
        return generated;
    }
}
