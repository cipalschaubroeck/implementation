package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "postal_addressee")
@Getter
@Setter
public class PostalAddressee {
    @Id
    private AddresseeId id = new AddresseeId();

    @ManyToOne(optional = false, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinColumn(name = "addressee", foreignKey = @ForeignKey(name = "fk_postal_addressee_addressee"))
    @NotNull
    @MapsId("addressee")
    private Addressee addressee;

    @ManyToOne(optional = false)
    @JoinColumn(name = "process", foreignKey = @ForeignKey(name = "fk_postal_addressee_process"))
    @NotNull
    @MapsId("process")
    private PostalDocumentProcess process;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true, optional = false)
    @JoinColumn(name = "heading")
    @NotNull
    private Heading heading = new Heading();

    @OneToOne(mappedBy = "addressee")
    private GeneratedDocument generatedDocument;
}
