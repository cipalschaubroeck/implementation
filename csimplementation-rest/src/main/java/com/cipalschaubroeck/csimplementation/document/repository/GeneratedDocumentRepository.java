package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "generated-document")
// spring data repository
@SuppressWarnings("unused")
public interface GeneratedDocumentRepository extends PagingAndSortingRepository<GeneratedDocument, AddresseeId> {
}
