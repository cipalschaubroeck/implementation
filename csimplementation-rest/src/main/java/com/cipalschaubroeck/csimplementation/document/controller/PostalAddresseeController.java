package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.controller.dto.PostalAddresseeUpdateDto;
import com.cipalschaubroeck.csimplementation.document.controller.projections.PostalAddresseeProjection;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.repository.PostalAddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeUpdateInfo;
import com.cipalschaubroeck.csimplementation.document.service.PostalDocumentGenerationService;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.support.BackendId;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import springfox.documentation.annotations.ApiIgnore;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RepositoryRestController
// spring controller
@SuppressWarnings("unused")
public class PostalAddresseeController {
    private static final List<Class<?>> ADDRESSEE_ALLOWED_REPLACEMENTS = Arrays.asList(Addressee.class,
            Collaborator.class, ContactPerson.class, QualifiedPerson.class, AdHocQualifiedPerson.class);

    @Autowired
    private PostalAddresseeService service;
    @Autowired
    private ControllerSupport support;
    @Autowired
    private PostalDocumentGenerationService generationService;
    @Autowired
    private PostalAddresseeRepository postalAddresseeRepository;

    @ApiOperation("${swagger.postal-addressee-controller.update.summary}")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dto", dataTypeClass = PostalAddresseeUpdateDto.class,
                    value = "${swagger.postal-addressee-controller.dto.description}"),
            @ApiImplicitParam(name = "projection", example = "postalAddresseeTableItem", required = true,
                    value = "${swagger.postal-addressee-controller.projection.description}")
    })
    @PutMapping(path = "/postal-addressee/{id}", params = "projection")
    public ResponseEntity<Resource> update(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "3-6") @BackendId AddresseeId id,
            @RequestBody PostalAddresseeUpdateDto dto,
            @ApiIgnore PersistentEntityResourceAssembler assembler) {
        PostalAddresseeUpdateInfo info = new PostalAddresseeUpdateInfo();
        info.setOurProperty(dto.getOurProperty());
        info.setYourProperty(dto.getYourProperty());
        info.setSalutation(dto.getSalutation());
        if (dto.getEmail() != null) {
            info.setEmailId((Integer)
                    support.linkToEntityInfo(dto.getEmail(), Collections.singletonList(EmailAddress.class))
                            .getId());
        }
        if (dto.getPhone() != null) {
            info.setPhoneId((Integer)
                    support.linkToEntityInfo(dto.getPhone(), Collections.singletonList(PhoneNumber.class))
                            .getId());
        }
        info.setPersonInfo(support.linkToEntityInfo(dto.getAddressee(), ADDRESSEE_ALLOWED_REPLACEMENTS));
        PostalAddressee updated = service.update(id, info);
        return new ResponseEntity<>(assembler.toFullResource(updated), HttpStatus.OK);
    }

    @GetMapping("/postal-addressee/{idAddressee}/preview/{idTemplate}")
    public ResponseEntity<ByteArrayResource> preview(
            @PathVariable("idAddressee") @ApiParam(value = "${swagger.document-process.id.description}", example = "3-6") @BackendId AddresseeId id,
            @PathVariable("idTemplate") Integer idTemplate,
            @RequestParam(name = "format", required = false, defaultValue = "PDF") OutputType format) throws JAXBException {
        Document document = generationService.generate(id, idTemplate, format);
        return ControllerSupport.download(document.getFileName(), document.getData());
    }

    @PostMapping("/postal-addressee/{id}/generate-document")
    public ResponseEntity<PersistentEntityResource> generate(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "3-6") @BackendId AddresseeId id,
            @ApiIgnore PersistentEntityResourceAssembler assembler) throws IOException {
        GeneratedDocument generated = generationService.generateAndSave(id);
        return ResponseEntity.ok(assembler.toFullResource(generated));
    }

    @GetMapping("/postal-addressee/{id}/preview")
    public ResponseEntity<ByteArrayResource> preview(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "3-6") @BackendId AddresseeId id,
            @RequestParam(name = "format", required = false, defaultValue = "PDF") OutputType format) throws IOException {
        Document document = generationService.generate(id, format);
        return ControllerSupport.download(document.getFileName(), document.getData());
    }

    /**
     * @param pageable page number and size to be returned
     * @param sort     string in the request
     * @return Pageable using the page information from pageable and the sort of sort param
     */
    @GetMapping("/postal-document-process/{id}/addressees")
    public ResponseEntity<PagedResources> search(
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            @PathVariable("id") Integer documentProcessId,
            PersistentEntityResourceAssembler assembler) {

        Page<PostalAddressee> page = postalAddresseeRepository.findAllByProcessId(documentProcessId, ControllerSupport
                .getPageConfig(pageable, sort, PostalAddresseeProjection.Constants.SORT_ALIASES));
        return support.toResources(page, PostalAddressee.class, assembler);
    }

}
