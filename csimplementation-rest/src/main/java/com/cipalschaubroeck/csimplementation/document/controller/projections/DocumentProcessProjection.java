package com.cipalschaubroeck.csimplementation.document.controller.projections;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;

@Projection(name = "documentProcessInfo", types = {DocumentProcess.class})
public interface DocumentProcessProjection {

    Integer getId();

    LocalDate getDate();

    String getName();

    DocumentProcessType getType();

    DocumentStatus getStatus();

}
