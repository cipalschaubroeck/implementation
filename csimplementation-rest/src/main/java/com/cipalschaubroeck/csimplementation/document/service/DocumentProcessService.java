package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.controller.specifications.DocumentProcessSpecification;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.validation.constraints.NotNull;

public interface DocumentProcessService {

    /**
     * Duplicates the document process with the same owner and saves the result to database.
     * The process' name is nameForCopy, the rest of basic fields are copied,
     * relationships with elements independent of document process are referenced,
     * relationships with elements depending on document process are duplicated with
     * these same constraints
     *
     * @param id          id of the document process that has to be duplicated
     * @param nameForCopy name for the resulting document process
     * @return the created document process
     */
    DocumentProcess duplicate(@NotNull Integer id, @NotNull String nameForCopy);

    /**
     * This method is going to be initializing all the fields required for status.
     * Incurring in more time processing.
     *
     * @param pageable      page number and size to be returned
     * @param specification used for query building and filtering
     * @return Pageable using the page information from pageable and the sort of sort param
     */

    Page<DocumentProcess> findDocumentProcessWithStatus(DocumentProcessSpecification specification, Pageable pageable);

}
