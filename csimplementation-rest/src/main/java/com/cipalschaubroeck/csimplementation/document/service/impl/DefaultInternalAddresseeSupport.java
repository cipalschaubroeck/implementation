package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.CollaboratorRepository;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.InternalAddressee;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeFactory;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.Optional;

@Service
public class DefaultInternalAddresseeSupport extends AbstractAddresseeSupport
        implements AddresseeDuplicator, AddresseeFactory {

    @Autowired
    private CollaboratorRepository collaboratorRepository;

    @Override
    public QualifiedPersonType getSupportedType() {
        return QualifiedPersonType.INTERNAL;
    }

    @Override
    public boolean supports(Class<?> domainType) {
        return Collaborator.class.isAssignableFrom(domainType);
    }

    @Override
    public Addressee buildFromAnotherEntity(Serializable id) {
        Optional<Collaborator> byId = collaboratorRepository.findById((Integer) id);
        if (byId.isPresent()) {
            InternalAddressee addressee = new InternalAddressee();
            addressee.setCollaborator(byId.get());
            super.defaultContactInfo(addressee);
            return addressee;
        } else {
            throw new EntityNotFoundException("Not found Collaborator with id " + id.toString());
        }
    }

    @Override
    public InternalAddressee duplicate(Addressee original) {
        InternalAddressee copy = new InternalAddressee();
        copy.setCollaborator(((InternalAddressee) original).getCollaborator());
        super.commonFields(original, copy);
        return copy;
    }
}
