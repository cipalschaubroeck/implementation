package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;

public interface PostalDocumentProcessService {

    /**
     * replaces the list of addressees of a postal document process
     *
     * @param processId id of the process
     * @param people    information to retrieve the persons to be the addressees. The allowed domain types are
     *                  ContactPerson, Collaborator, AdHocQualifiedPerson and QualifiedPerson but only
     *                  if it refers to an AdHocQualifiedPerson
     * @return items created
     */
    Iterable<PostalAddressee> setAddressees(Integer processId, Iterable<EntityInfo> people);

    /**
     * adds elements to the list of addressees of a postal document process
     *
     * @param processId id of the process
     * @param people    information to retrieve the persons to be the addressees. The allowed domain types are
     *                  ContactPerson, Collaborator, AdHocQualifiedPerson and QualifiedPerson but only
     *                  if it refers to an AdHocQualifiedPerson
     * @return items created
     */
    Iterable<PostalAddressee> addAddressees(Integer processId, Iterable<EntityInfo> people);

    /**
     * Uploads a file to content manager and links it to the process as an attachment
     *
     * @param processId id of the process
     * @param fileName  name of the file
     * @param data      content of the file
     * @param mime      type of file
     * @return attachment created
     */
    ContentManagerFile addAttachment(Integer processId, String fileName, byte[] data, String mime);

    /**
     * A process' attachment may be a link to an existing file in content manager. This method adds
     * copies of existing ContentManagerFile to the process
     *
     * @param processId the id of the process
     * @param cmFileId  ids of ContentManagerFile
     * @return ContentManagerFile created and added to the process
     */
    Iterable<ContentManagerFile> linkAsAttachments(Integer processId, Iterable<Integer> cmFileId);

    /**
     * Substitutes the current list of attachments of a process by a new one, so that elements that are in
     * cmFieldId remain, elements that are not are removed and elements of cmFieldId that are not in attachments
     * are linked as in linkAsAttachments
     *
     * @param processId the id of a postal document process
     * @param cmFieldId ids of the content manager files that should be in the document process' attachments
     * @return the new list of ContentManagerFile
     */
    Iterable<ContentManagerFile> setAttachments(Integer processId, Iterable<Integer> cmFieldId);

    /**
     * removes all attachments in the postal document process
     *
     * @param processId id of the process
     */
    void clearAttachments(Integer processId);
}
