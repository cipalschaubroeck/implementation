package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;

public interface DocumentProcessDuplicator {
    DocumentProcessType getSupportedType();

    /**
     * @param original document process with type as this' supported type
     * @return a duplicate of original, considering the constraints described in DocumentProcessService
     */
    DocumentProcess duplicate(DocumentProcess original);

    default void setCommonFields(DocumentProcess original, DocumentProcess copy) {
        copy.setName(original.getName());
        copy.setContract(original.getContract());
    }
}
