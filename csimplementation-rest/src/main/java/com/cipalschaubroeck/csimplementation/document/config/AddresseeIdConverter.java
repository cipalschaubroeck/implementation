package com.cipalschaubroeck.csimplementation.document.config;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.EmailAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.shared.config.BiCompositeIdConverter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collection;

@Component
public class AddresseeIdConverter extends BiCompositeIdConverter<AddresseeId> {

    private static final Collection<Class<?>> VALID_ENTITY_CLASSES = Arrays.asList(
            EmailAddressee.class, GeneratedDocument.class, PostalAddressee.class);

    @Override
    protected AddresseeId createId(Integer id1, Integer id2) {
        AddresseeId objectId = new AddresseeId();
        objectId.setAddressee(id1);
        objectId.setProcess(id2);
        return objectId;
    }

    @Override
    public Class<AddresseeId> getIdClass() {
        return AddresseeId.class;
    }

    @Override
    protected Integer getId1(AddresseeId id) {
        return id.getAddressee();
    }

    @Override
    protected Integer getId2(AddresseeId id) {
        return id.getProcess();
    }

    @Override
    public boolean supports(Class<?> delimiter) {
        return VALID_ENTITY_CLASSES.stream().anyMatch(c -> c.isAssignableFrom(delimiter));
    }

}
