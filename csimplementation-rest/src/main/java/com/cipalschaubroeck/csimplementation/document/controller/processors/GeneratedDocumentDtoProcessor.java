package com.cipalschaubroeck.csimplementation.document.controller.processors;

import com.cipalschaubroeck.csimplementation.contentmanager.controller.ContentManagerFileProcessor;
import com.cipalschaubroeck.csimplementation.document.controller.projections.GeneratedDocumentProjection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

@Component
// spring resource processor
@SuppressWarnings("unused")
public class GeneratedDocumentDtoProcessor implements ResourceProcessor<Resource<GeneratedDocumentProjection>> {
    @Autowired
    private ContentManagerFileProcessor cmProcessor;

    @Override
    public Resource<GeneratedDocumentProjection> process(Resource<GeneratedDocumentProjection> resource) {
        cmProcessor.buildDownloadLinkFor(resource.getContent().getGenerated(), "downloadGenerated")
                .ifPresent(resource::add);
        cmProcessor.buildDownloadLinkFor(resource.getContent().getDispatched(), "downloadDispatched")
                .ifPresent(resource::add);
        return resource;
    }
}
