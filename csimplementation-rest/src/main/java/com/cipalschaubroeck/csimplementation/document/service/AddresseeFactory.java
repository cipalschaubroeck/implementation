package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;

import java.io.Serializable;

public interface AddresseeFactory {
    /**
     * @param domainType a class
     * @return true if this factory can build an Addressee wrapping an instance of the class
     */
    boolean supports(Class<?> domainType);

    /**
     * @param id id of an entity that can be wrapped in an Addressee
     * @return an instance of Addressee wrapping the entity with the id
     */
    Addressee buildFromAnotherEntity(Serializable id);
}
