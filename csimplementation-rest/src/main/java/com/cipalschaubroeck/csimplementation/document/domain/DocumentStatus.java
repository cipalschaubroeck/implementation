package com.cipalschaubroeck.csimplementation.document.domain;

public enum DocumentStatus {
    IN_PROGRESS,
    // TODO CSPROC-1457 will be used when that issue is finished
    FOR_SIGNATURE,
    VALIDATED,
    DISPATCHED,
    UPLOADED
}
