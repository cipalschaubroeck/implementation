package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;

import java.util.Comparator;
import java.util.List;

abstract class AbstractAddresseeSupport {
    private static final Comparator<PhoneNumber> PHONES = Comparator.comparing(PhoneNumber::getPhone);
    private static final Comparator<EmailAddress> EMAILS = Comparator.comparing(EmailAddress::getEmail);
    private static final Comparator<PostalAddress> ADDRESSES = Comparator.comparing(PostalAddress::getStreet)
            .thenComparing(PostalAddress::getNumber)
            .thenComparing(PostalAddress::getMunicipality)
            .thenComparing(PostalAddress::getPostalCode)
            .thenComparing(PostalAddress::getCountry);

    void commonFields(Addressee original, Addressee copy) {
        // copy from person's in case the original is already for signature
        copy.setPhone(findEqual(original.getPhone(), original.getEffectivePerson().getPhoneNumbers(), PHONES));
        copy.setEmail(findEqual(original.getEmail(), original.getEffectivePerson().getEmails(), EMAILS));
        copy.setAddress(findEqual(original.getAddress(), original.getEffectivePerson().getAddresses(), ADDRESSES));
    }

    /**
     * Gets an element just as fromOriginal out of fromPerson
     *
     * @param fromOriginal the element to compare with
     * @param fromPerson   a list of elements from a person to choose from
     * @param comparator   how to determine if two elements are equal
     * @param <T>          the type of element we look for
     * @return if fromOriginal is null, null; otherwise, if there is an element x in fromPerson that is equal to
     * fromOriginal according with areEqual, x; otherwise, null
     */
    private <T> T findEqual(T fromOriginal, List<T> fromPerson, Comparator<T> comparator) {
        return fromOriginal == null ? null : fromPerson.stream().filter(t -> 0 == comparator.compare(fromOriginal, t))
                .findAny().orElse(null);
    }

    void defaultContactInfo(Addressee addressee) {
        addressee.setPhone(getFirstOrNull(addressee.getEffectivePerson().getPhoneNumbers()));
        addressee.setEmail(getFirstOrNull(addressee.getEffectivePerson().getEmails()));
        addressee.setAddress(getFirstOrNull(addressee.getEffectivePerson().getAddresses()));
    }

    private <T> T getFirstOrNull(List<T> items) {
        return items == null || items.isEmpty() ? null : items.get(0);
    }
}
