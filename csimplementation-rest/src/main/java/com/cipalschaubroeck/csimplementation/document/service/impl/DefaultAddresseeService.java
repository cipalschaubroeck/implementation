package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.repository.AddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeService;
import com.cipalschaubroeck.csimplementation.organization.service.ContactInfoSupport;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class DefaultAddresseeService implements AddresseeService {
    @Autowired
    private AddresseeRepository repository;
    @Autowired
    private EntityEventPublisher publisher;
    @Autowired
    private ContactInfoSupport contactInfoSupport;

    @Override
    public void fixAddressee(Addressee addressee) {
        addressee.setAddress(contactInfoSupport.copyIfNeeded(addressee.getAddress(),
                addressee.getEffectivePerson().getAddresses()));
        addressee.setEmail(contactInfoSupport.copyIfNeeded(addressee.getEmail(),
                addressee.getEffectivePerson().getEmails()));
        addressee.setPhone(contactInfoSupport.copyIfNeeded(addressee.getPhone(),
                addressee.getEffectivePerson().getPhoneNumbers()));
    }

    @Override
    public void fixAddressee(Integer id) {
        repository.findById(id).ifPresent(addressee -> {
            fixAddressee(addressee);
            publisher.update(addressee, repository);
        });
    }
}
