package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.service.ContentManagerFileService;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class PostalDPDuplicator implements DocumentProcessDuplicator {
    @Autowired
    private ContentManagerFileService contentManagerFileService;
    @Autowired
    private PostalAddresseeService postalAddresseeService;
    @Autowired
    private TemplateDataService templateDataService;

    @Override
    public DocumentProcessType getSupportedType() {
        return DocumentProcessType.DOCUMENT;
    }

    @Override
    public DocumentProcess duplicate(DocumentProcess original) {
        PostalDocumentProcess copy = new PostalDocumentProcess();
        copy.setAttachments(((PostalDocumentProcess) original).getAttachments()
                .stream().map(contentManagerFileService::duplicate).collect(Collectors.toList()));
        copy.setBody(templateDataService.duplicate(((PostalDocumentProcess) original).getBody()));
        copy.setSigningOfficerData((SigningOfficerData)
                templateDataService.duplicate(((PostalDocumentProcess) original).getSigningOfficerData()));
        copy.setAddresseeList(((PostalDocumentProcess) original).getAddresseeList()
                .stream().map(postalAddresseeService::duplicate).collect(Collectors.toList()));
        copy.getAddresseeList().forEach(a -> a.setProcess(copy));
        setCommonFields(original, copy);
        return copy;
    }
}
