package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "postal-addressee")
public interface PostalAddresseeRepository extends PagingAndSortingRepository<PostalAddressee, AddresseeId> {
    Page<PostalAddressee> findAllByProcessId(Integer processId, Pageable page);
}
