package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * A DocumentProcess is an instance of communication
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "document_process")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.EXISTING_PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(EmailDocumentProcess.class),
        @JsonSubTypes.Type(PostalDocumentProcess.class),
        @JsonSubTypes.Type(UploadDocumentProcess.class)
})
@ApiModel(value = "${swagger.document-process.name}", description = "${swagger.document-process.description}")
public abstract class DocumentProcess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @Getter
    @Setter
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    @Getter
    @Setter
    private Integer versionNum;

    @Column(name = "name")
    @Getter
    @Setter
    @NotNull
    @ApiModelProperty("${swagger.document-process.name.description}")
    private String name;

    @Column(name = "creation_date", updatable = false)
    @DateTimeFormat(pattern = "dd/MM/yyyy")
    @CreationTimestamp
    @Getter
    private LocalDate date;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_process_parcel"))
    @Getter
    @Setter
    @NotNull
    private Contract contract;

    // asked by rest users
    @SuppressWarnings("unused")
    @Transient
    public abstract DocumentStatus getStatus();

    // used by jackson to tell rest users about the subclass
    @SuppressWarnings("unused")
    @Transient
    public abstract DocumentProcessType getType();
}
