package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.List;

public interface PostalDocumentGenerationService {
    /**
     * id of the addressee is needed only for some template data, {@link TemplateType#needsAddressee()}
     *
     * @param addresseeId    id of the addressee
     * @param templateDataId id of the template data
     * @param format         format of the expected file
     * @return a file generated for a particular addressee and a particular template data
     * @throws JAXBException if there is any xml problem
     */
    Document generate(AddresseeId addresseeId, Integer templateDataId, OutputType format) throws JAXBException;

    /**
     * @param processId      id of the process
     * @param templateDataId id of the template data. The template data must not need addressee info, see {@link TemplateType#needsAddressee()}
     * @param format         format of the expected file
     * @return a file generated for a particular process and a particular template data
     * @throws JAXBException if there is any xml problem
     */
    Document generate(Integer processId, Integer templateDataId, OutputType format) throws JAXBException;

    /**
     * Generates the whole file of a particular addressee
     *
     * @param id     id of the addressee
     * @param format format of the expected file
     * @return generated file
     * @throws IOException if there is any problem with the xml or loading the authority's images
     */
    Document generate(AddresseeId id, OutputType format) throws IOException;

    /**
     * Generates the whole file for a particular addressee and saves it
     *
     * @param id of the addressee
     * @return generated file
     * @throws IOException if there is any problem with the xml or loading the authority's images
     */
    GeneratedDocument generateAndSave(AddresseeId id) throws IOException;

    /**
     * Generates and saves the whole document for all addressees that have not generated it yet or that are still in progress
     *
     * @param id id of the document process
     * @return list with the generated documents
     * @throws IOException if there is any problem with the xml or loading the authority's images
     */
    List<GeneratedDocument> generateAndSaveAll(Integer id) throws IOException;
}
