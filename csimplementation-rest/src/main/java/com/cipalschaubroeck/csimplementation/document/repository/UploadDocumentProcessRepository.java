package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.config.DocumentProcessConfigConstants;
import com.cipalschaubroeck.csimplementation.document.domain.UploadDocumentProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "upload-document-process",
        collectionResourceRel = DocumentProcessConfigConstants.DOCUMENT_PROCESS_COLLECTION_REL)
public interface UploadDocumentProcessRepository extends PagingAndSortingRepository<UploadDocumentProcess, Integer> {
}
