package com.cipalschaubroeck.csimplementation.document.controller.processors;

import com.cipalschaubroeck.csimplementation.document.config.AddresseeIdConverter;
import com.cipalschaubroeck.csimplementation.document.controller.PostalAddresseeController;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.lang.reflect.Method;

/**
 * adds the links for preview the templates and generate the full document
 */
@Component
@Slf4j
// spring resource processor
@SuppressWarnings("unused")
public class PostalAddresseeProcessor implements ResourceProcessor<Resource<PostalAddressee>> {
    @Autowired
    private AddresseeIdConverter converter;

    @Override
    public Resource<PostalAddressee> process(Resource<PostalAddressee> resource) {
        PostalAddressee addressee = resource.getContent();
        try {
            // ugly, but other errors when using the method below due to instantiating PersistentEntityResourceAssembler
            Method method = PostalAddresseeController.class.getMethod("generate", AddresseeId.class, PersistentEntityResourceAssembler.class);
            resource.add(ControllerLinkBuilder.linkTo(method, converter.fromIdConverter().convert(addressee.getId()), null).withRel("generate"));

            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostalAddresseeController.class)
                    .preview(addressee.getId(), addressee.getHeading().getId(), null)).withRel("previewHeading"));
            resource.add(ControllerLinkBuilder.linkTo(ControllerLinkBuilder.methodOn(PostalAddresseeController.class)
                    .preview(addressee.getId(), null)).withRel("preview"));
        } catch (IOException | JAXBException e) {
            log.error("Should never happen since it is not an actual invocation", e);
        } catch (NoSuchMethodException e) {
            log.error("Should have been detected with automatic testing", e);
        }
        return resource;
    }
}
