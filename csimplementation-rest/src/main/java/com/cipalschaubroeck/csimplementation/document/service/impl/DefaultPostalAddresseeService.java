package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.repository.AddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.repository.PostalAddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeFactory;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeUpdateInfo;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.repository.EmailRepository;
import com.cipalschaubroeck.csimplementation.person.repository.PhoneRepository;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.service.impl.HeadingDuplicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class DefaultPostalAddresseeService implements PostalAddresseeService {

    private Map<QualifiedPersonType, AddresseeDuplicator> addresseeServices = new EnumMap<>(QualifiedPersonType.class);
    @Autowired
    private List<AddresseeFactory> addresseeFactories = new ArrayList<>();
    @Autowired
    private PostalAddresseeRepository repository;
    @Autowired
    private PhoneRepository phoneRepository;
    @Autowired
    private EmailRepository emailRepository;
    @Autowired
    private AddresseeRepository addresseeRepository;
    @Autowired
    private HeadingDuplicator headingDuplicator;

    @Autowired
    public void setAddresseeServices(List<AddresseeDuplicator> addresseeDuplicators) {
        addresseeDuplicators.forEach(as -> this.addresseeServices.put(as.getSupportedType(), as));
    }

    @Override
    public PostalAddressee duplicate(PostalAddressee original) {
        PostalAddressee copy = new PostalAddressee();
        Addressee original1 = original.getAddressee();
        copy.setAddressee(getFor(original1).duplicate(original1));

        copy.setHeading((Heading) headingDuplicator.duplicate(original.getHeading()));

        return copy;
    }

    private AddresseeDuplicator getFor(Addressee addressee) {
        AddresseeDuplicator service = addresseeServices.get(addressee.getType());
        if (service == null) {
            throw new IllegalArgumentException(addressee.getClass() + " is not a known subclass of Addressee");
        }
        return service;
    }

    @Override
    public Addressee build(EntityInfo info) {
        for (AddresseeFactory factory : addresseeFactories) {
            if (factory.supports(info.getDomainType())) {
                return factory.buildFromAnotherEntity(info.getId());
            }
        }
        throw new IllegalArgumentException("There is no addressee factory supporting " + info.getDomainType().getName());
    }

    @Override
    public PostalAddressee update(AddresseeId id, PostalAddresseeUpdateInfo info) {
        PostalAddressee postalAddressee = repository.findById(id).orElseThrow(() -> new EntityNotFoundException("Could not find a postal addressee with id " + id));
        if (postalAddressee.getGeneratedDocument() != null && postalAddressee.getGeneratedDocument().getStatus() != DocumentStatus.IN_PROGRESS) {
            throw new IllegalStateException("postal addressee with id " + id + " cannot be modified at this time");
        }

        Addressee newAddressee = build(info.getPersonInfo());
        Addressee addressee = postalAddressee.getAddressee();
        if (!Objects.equals(newAddressee.getType(), addressee.getType())
                || !Objects.equals(newAddressee.getEffectivePerson().getId(), addressee.getEffectivePerson().getId())) {
            addresseeRepository.delete(addressee);
            postalAddressee.setAddressee(newAddressee);
            addressee = postalAddressee.getAddressee();
        }

        if (info.getPhoneId() != null) {
            PhoneNumber phone = phoneRepository.findById(info.getPhoneId())
                    .orElseThrow(() -> new EntityNotFoundException("could not find phone with id " + info.getPhoneId()));
            if (!addressee.getEffectivePerson().getPhoneNumbers().contains(phone)) {
                throw new IllegalArgumentException("Phone must belong to person");
            }
            addressee.setPhone(phone);
        }

        if (info.getEmailId() != null) {
            EmailAddress email = emailRepository.findById(info.getEmailId())
                    .orElseThrow(() -> new EntityNotFoundException("could not find email with id " + info.getEmailId()));
            if (!addressee.getEffectivePerson().getEmails().contains(email)) {
                throw new IllegalArgumentException("Email must belong to person");
            }
            addressee.setEmail(email);
        }

        return repository.save(postalAddressee);
    }

}
