package com.cipalschaubroeck.csimplementation.document.domain;

public enum DocumentProcessType {
    DOCUMENT {
        @Override
        public Class<? extends DocumentProcess> getDocumentProcessClass() {
            return PostalDocumentProcess.class;
        }
    },
    EMAIL {
        @Override
        public Class<? extends DocumentProcess> getDocumentProcessClass() {
            return EmailDocumentProcess.class;
        }
    },
    UPLOAD {
        @Override
        public Class<? extends DocumentProcess> getDocumentProcessClass() {
            return UploadDocumentProcess.class;
        }
    };

    public static class Name {
        private Name() {
            // class of constants
        }

        static final String DOCUMENT = "DOCUMENT";
        static final String EMAIL = "EMAIL";
        static final String UPLOAD = "UPLOAD";
    }

    public abstract Class<? extends DocumentProcess> getDocumentProcessClass();
}
