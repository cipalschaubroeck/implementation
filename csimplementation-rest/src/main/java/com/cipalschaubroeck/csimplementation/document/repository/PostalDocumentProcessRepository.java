package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.config.DocumentProcessConfigConstants;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

@RepositoryRestResource(path = "postal-document-process",
        collectionResourceRel = DocumentProcessConfigConstants.DOCUMENT_PROCESS_COLLECTION_REL)
public interface PostalDocumentProcessRepository extends PagingAndSortingRepository<PostalDocumentProcess, Integer>,
        JpaSpecificationExecutor<PostalDocumentProcess>, CrudRepository<PostalDocumentProcess, Integer> {
    @RestResource(exported = false)
    List<PostalDocumentProcess> findAllByBodyId(Integer bodyId);
}
