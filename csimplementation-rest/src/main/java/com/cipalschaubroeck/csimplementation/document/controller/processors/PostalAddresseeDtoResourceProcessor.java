package com.cipalschaubroeck.csimplementation.document.controller.processors;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.controller.projections.PostalAddresseeProjection;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.mapping.LinkCollector;
import org.springframework.hateoas.EntityLinks;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.EnumMap;
import java.util.Map;
import java.util.stream.StreamSupport;

@Component
// used by spring
@SuppressWarnings("unused")
public class PostalAddresseeDtoResourceProcessor implements ResourceProcessor<Resource<PostalAddresseeProjection>> {

    private static final Map<QualifiedPersonType, Class<?>> REPLACEMENT_CLASSES;

    static {
        Map<QualifiedPersonType, Class<?>> classes = new EnumMap<>(QualifiedPersonType.class);
        classes.put(QualifiedPersonType.AD_HOC, AdHocQualifiedPerson.class);
        classes.put(QualifiedPersonType.INTERNAL, Collaborator.class);
        classes.put(QualifiedPersonType.EXTERNAL, ContactPerson.class);
        REPLACEMENT_CLASSES = Collections.unmodifiableMap(classes);
    }

    @Autowired
    private EntityLinks links;

    @Autowired
    private LinkCollector collector;

    @Override
    public Resource<PostalAddresseeProjection> process(Resource<PostalAddresseeProjection> resource) {
        if (!resource.getContent().isReadOnly()) {
            Addressee addressee = resource.getContent().getAddressee();
            resource.getLinks().add(links
                    .linkToCollectionResource(REPLACEMENT_CLASSES.get(addressee.getType()))
                    .withRel("addresseeReplacements"));

            StreamSupport.stream(collector.getLinksFor(addressee.getEffectivePerson(), Collections.emptyList()).spliterator(), false)
                    .filter(link -> !StringUtils.equals(Link.REL_SELF, link.getRel()))
                    .forEach(resource.getLinks()::add);
        }
        return resource;
    }
}
