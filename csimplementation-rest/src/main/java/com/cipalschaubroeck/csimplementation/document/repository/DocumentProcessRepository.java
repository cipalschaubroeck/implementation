package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.config.DocumentProcessConfigConstants;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Set;

@RepositoryRestResource(path = DocumentProcessConfigConstants.DOCUMENT_PROCESS_PATH,
        collectionResourceRel = DocumentProcessConfigConstants.DOCUMENT_PROCESS_COLLECTION_REL)
public interface DocumentProcessRepository extends PagingAndSortingRepository<DocumentProcess, Integer>,
        JpaSpecificationExecutor<DocumentProcess> {
    //TODO CSPROC-2032 check if its needed
    Page<DocumentProcess> findByContractIdAndTypeIn(Integer contractId, Set<DocumentProcessType> allowedTypes, Pageable pageable);
}
