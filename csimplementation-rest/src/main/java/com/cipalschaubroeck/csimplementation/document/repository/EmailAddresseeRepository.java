package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.EmailAddressee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "email-addressee")
public interface EmailAddresseeRepository extends PagingAndSortingRepository<EmailAddressee, AddresseeId> {
}
