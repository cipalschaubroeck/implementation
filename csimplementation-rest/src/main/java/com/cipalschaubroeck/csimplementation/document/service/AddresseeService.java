package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;

public interface AddresseeService {
    /**
     * When the document of an addressee is set for signature, the contact info for that
     * addressee should not change anymore if the person's change. Thus, this method makes a copy
     * of the used contact info, until now part of the person's
     * <p>
     * If the contact info of addressee is already its own, nothing should change
     *
     * @param addressee an addressee
     */
    void fixAddressee(Addressee addressee);

    /**
     * just as fixAddressee(Addressee), but loading and saving the entity in the service
     *
     * @param id id of the address to update
     */
    void fixAddressee(Integer id);
}
