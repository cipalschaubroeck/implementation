package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table(name = "postal_document_process")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@JsonTypeName(DocumentProcessType.Name.DOCUMENT)
@Getter
@Setter
public class PostalDocumentProcess extends DocumentProcess {

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "body")
    private TemplateData body;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "signing_officer_data", foreignKey = @ForeignKey(name = "fk_process_signing_officer_data"))
    private SigningOfficerData signingOfficerData = new SigningOfficerData();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(name = "postal_attachment",
            joinColumns = @JoinColumn(name = "process"),
            foreignKey = @ForeignKey(name = "fk_postal_attachment_process"),
            inverseJoinColumns = @JoinColumn(name = "file"),
            inverseForeignKey = @ForeignKey(name = "fk_postal_attachment_file"))
    private List<ContentManagerFile> attachments = new ArrayList<>();

    @OneToMany(mappedBy = "process")
    @Getter
    private List<PostalAddressee> addresseeList = new ArrayList<>();

    @Override
    public DocumentStatus getStatus() {
        Set<DocumentStatus> statuses = addresseeList.stream()
                .map(PostalAddressee::getGeneratedDocument)
                .filter(Objects::nonNull)
                .map(GeneratedDocument::getStatus)
                .collect(Collectors.toSet());
        if (statuses.size() == 1) {
            return statuses.iterator().next();
        } else if (statuses.equals(EnumSet.of(DocumentStatus.DISPATCHED, DocumentStatus.VALIDATED))) {
            return DocumentStatus.VALIDATED;
        } else if (statuses.contains(DocumentStatus.FOR_SIGNATURE)) {
            return DocumentStatus.FOR_SIGNATURE;
        } else {
            return DocumentStatus.IN_PROGRESS;
        }
    }

    @Override
    public DocumentProcessType getType() {
        return DocumentProcessType.DOCUMENT;
    }
}
