package com.cipalschaubroeck.csimplementation.document.controller.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

/**
 * input for updating a PostalAddressee
 */
@Getter
@Setter
@ApiModel(description = "${swagger.postal-addressee-model.update.description}")
public class PostalAddresseeUpdateDto {
    @ApiModelProperty("${swagger.postal-addressee-model.update.salutation.description}")
    private String salutation;
    @ApiModelProperty("${swagger.postal-addressee-model.update.our-property.description}")
    private String ourProperty;
    @ApiModelProperty("${swagger.postal-addressee-model.update.your-property.description}")
    private String yourProperty;
    /**
     * uri of the phone to use. It must correspond to a phone of the resolved person
     */
    @ApiModelProperty("${swagger.postal-addressee-model.update.phone.description}")
    private Link phone;
    /**
     * uri of the email to use. It must correspond to a email of the resolved person
     */
    @ApiModelProperty("${swagger.postal-addressee-model.update.email.description}")
    private Link email;
    /**
     * uri of a suitable person resolvable, so, corresponding to an Addressee,
     * a Collaborator, a ContactPerson or an AdHocQualifiedPerson
     */
    @ApiModelProperty(value = "${swagger.postal-addressee-model.update.addressee.description}", required = true)
    private Link addressee;
}
