package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.domain.UploadDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import org.springframework.stereotype.Component;

@Component
public class UploadDPDuplicator implements DocumentProcessDuplicator {

    @Override
    public DocumentProcessType getSupportedType() {
        return DocumentProcessType.UPLOAD;
    }

    @Override
    public DocumentProcess duplicate(DocumentProcess original) {
        UploadDocumentProcess copy = new UploadDocumentProcess();
        setCommonFields(original, copy);
        return copy;
    }
}
