package com.cipalschaubroeck.csimplementation.document.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class AddresseeId implements Serializable {
    @Column(name = "addressee")
    @NotNull
    private Integer addressee;

    @Column(name = "process")
    @NotNull
    private Integer process;
}
