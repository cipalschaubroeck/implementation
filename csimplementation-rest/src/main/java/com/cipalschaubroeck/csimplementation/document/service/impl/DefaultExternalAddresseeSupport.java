package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.ExternalAddressee;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeFactory;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPersonId;
import com.cipalschaubroeck.csimplementation.organization.repository.ContactPersonRepository;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.io.Serializable;
import java.util.Optional;

@Service
public class DefaultExternalAddresseeSupport extends AbstractAddresseeSupport
        implements AddresseeDuplicator, AddresseeFactory {

    @Autowired
    private ContactPersonRepository contactPersonRepository;

    @Override
    public QualifiedPersonType getSupportedType() {
        return QualifiedPersonType.EXTERNAL;
    }

    @Override
    public boolean supports(Class<?> domainType) {
        return ContactPerson.class.isAssignableFrom(domainType);
    }

    @Override
    public Addressee buildFromAnotherEntity(Serializable id) {
        Optional<ContactPerson> byId = contactPersonRepository.findById((ContactPersonId) id);
        if (byId.isPresent()) {
            ExternalAddressee addressee = new ExternalAddressee();
            addressee.setContact(byId.get());
            super.defaultContactInfo(addressee);
            return addressee;
        } else {
            throw new EntityNotFoundException("Not found ContactPerson with id " + id.toString());
        }
    }

    @Override
    public ExternalAddressee duplicate(Addressee original) {
        ExternalAddressee copy = new ExternalAddressee();
        copy.setContact(((ExternalAddressee) original).getContact());
        super.commonFields(original, copy);
        return copy;
    }
}
