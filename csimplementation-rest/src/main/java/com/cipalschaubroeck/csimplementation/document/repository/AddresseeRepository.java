package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "addressee")
public interface AddresseeRepository extends PagingAndSortingRepository<Addressee, Integer> {
}
