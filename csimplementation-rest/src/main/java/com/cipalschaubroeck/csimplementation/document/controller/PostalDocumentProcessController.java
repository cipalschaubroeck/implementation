package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.service.PostalDocumentGenerationService;
import com.cipalschaubroeck.csimplementation.document.service.PostalDocumentProcessService;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RestMediaTypes;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@BasePathAwareController
// spring controller
@SuppressWarnings("unused")
public class PostalDocumentProcessController {

    private static final List<Class<?>> ALLOWED_FOR_ADDRESSEE = Arrays
            .asList(PostalAddressee.class, QualifiedPerson.class, AdHocQualifiedPerson.class,
                    ContactPerson.class, Collaborator.class);

    @Autowired
    private ControllerSupport support;
    @Autowired
    private PostalDocumentProcessService service;
    @Autowired
    private PostalDocumentGenerationService generationService;

    @ApiOperation("${swagger.postal-document-process-controller.add-addressees.summary}")
    @ApiImplicitParam(name = "incoming", dataType = "org.springframework.hateoas.Resources",
            value = "${swagger.postal-document-process-controller.add-addressees.uri-list}")
    @RequestMapping(value = "/postal-document-process/{id}/addresseeList", method = {PATCH, POST, PUT})
    public ResponseEntity<Resources> addAddressees(
            @RequestBody Resources<Object> incoming,
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer id,
            PersistentEntityResourceAssembler assembler,
            HttpMethod method) {
        Iterable<EntityInfo> dtos = incoming.getLinks().stream()
                .map(link -> support.linkToEntityInfo(link, ALLOWED_FOR_ADDRESSEE))
                .peek(info -> {
                    if (QualifiedPerson.class.equals(info.getDomainType())) {
                        info.setDomainType(AdHocQualifiedPerson.class);
                    }
                })
                ::iterator;
        Iterable<PostalAddressee> added = HttpMethod.PUT == method ? service.setAddressees(id, dtos) : service.addAddressees(id, dtos);
        Resources result = ControllerSupport.toResources(added, assembler, DocumentProcess.class);
        return new ResponseEntity<>(result, HttpStatus.CREATED);
    }

    @ApiOperation("${swagger.postal-document-process-controller.upload-attachment.summary}")
    @PostMapping("/postal-document-process/{id}/attachments")
    public ResponseEntity<Resources> uploadAttachment(
            @RequestParam MultipartFile[] files,
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer processId,
            PersistentEntityResourceAssembler assembler) throws IOException {
        List<ContentManagerFile> uploaded = new ArrayList<>();
        for (MultipartFile file : files) {
            uploaded.add(service.addAttachment(processId, file.getOriginalFilename(), file.getBytes(), file.getContentType()));
        }
        return new ResponseEntity<>(ControllerSupport.toResources(uploaded, assembler, ContentManagerFile.class), HttpStatus.CREATED);
    }

    /**
     * Adds the ContentManagerFile indicated by the first uri to the document process
     *
     * @param processId id of process
     * @param incoming  uri list
     * @param assembler to build the response
     * @param method    an allowed method: PATCH or POST to add to attachments, PUT to replace current with incoming
     * @return created ContentManagerFile when adding, the new collection when replacing
     */
    @ApiOperation("${swagger.postal-document-process-controller.link-attachment.summary}")
    @ApiImplicitParam(name = "incoming", dataType = "org.springframework.hateoas.Resources",
            value = "${swagger.postal-document-process-controller.link-attachment.uri-list}")
    @ApiResponses(@ApiResponse(code = 201, message = "CREATED", response = ContentManagerFile.class, responseContainer = "List"))
    @RequestMapping(value = "/postal-document-process/{id}/attachments", method = {PATCH, POST, PUT},
            consumes = RestMediaTypes.TEXT_URI_LIST_VALUE)
    public ResponseEntity<Resources> linkAttachment(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer processId,
            @RequestBody Resources<Object> incoming,
            PersistentEntityResourceAssembler assembler,
            HttpMethod method) {
        List<Class<?>> allowed = Collections.singletonList(ContentManagerFile.class);
        Iterable<Integer> cmFileId = incoming.getLinks().stream()
                .map(link -> support.linkToEntityInfo(link, allowed))
                .map(EntityInfo::getId)
                .map(Integer.class::cast)
                .collect(Collectors.toList());
        Iterable<ContentManagerFile> cmFile;
        if (HttpMethod.PUT.equals(method)) {
            cmFile = service.setAttachments(processId, cmFileId);
        } else {
            cmFile = service.linkAsAttachments(processId, cmFileId);
        }
        Resources resources = ControllerSupport.toResources(cmFile, assembler, ContentManagerFile.class);
        return new ResponseEntity<>(resources, HttpStatus.CREATED);
    }

    /**
     * Deletes all content manager files in attachments
     *
     * @param processId id of process
     * @return no content
     */
    @ApiOperation("${swagger.postal-document-process-controller.clear-attachments.summary}")
    @ApiResponses(@ApiResponse(code = 204, message = "no content"))
    @DeleteMapping("/postal-document-process/{id}/attachments")
    public ResponseEntity<Void> clearAttachments(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer processId) {
        service.clearAttachments(processId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("/postal-document-process/{id}/generate-documents")
    public ResponseEntity<List<PersistentEntityResource>> generateAllDocuments(
            @PathVariable("id") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer processId,
            PersistentEntityResourceAssembler assembler) throws IOException {
        List<GeneratedDocument> generated = generationService.generateAndSaveAll(processId);
        return ResponseEntity.ok(generated.stream().map(assembler::toFullResource).collect(Collectors.toList()));
    }

    @GetMapping("/postal-document-process/{idProcess}/preview/{idTemplate}")
    public ResponseEntity<ByteArrayResource> preview(
            @PathVariable("idProcess") @ApiParam(value = "${swagger.document-process.id.description}", example = "10") Integer idProcess,
            @PathVariable("idTemplate") Integer idTemplate,
            @RequestParam(name = "format", required = false, defaultValue = "PDF") OutputType format) throws JAXBException {
        Document document = generationService.generate(idProcess, idTemplate, format);
        return ControllerSupport.download(document.getFileName(), document.getData());
    }
}
