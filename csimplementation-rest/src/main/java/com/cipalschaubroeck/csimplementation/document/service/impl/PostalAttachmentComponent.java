package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contentmanager.repository.ContentManagerRepository;
import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.contentmanager.service.ContentManagerFileService;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import org.apache.chemistry.opencmis.client.api.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
@Transactional
public class PostalAttachmentComponent {
    private static final String FORMAT_PATH_ATTACHMENTS = "%s/attachments/%s";
    @Autowired
    private PostalDocumentProcessRepository repository;
    @Autowired
    private EntityEventPublisher publisher;
    @Autowired
    private CMCommunicationService contentManager;
    @Autowired
    private ContentManagerRepository fileRepository;
    @Autowired
    private ContentManagerFileService fileService;

    public ContentManagerFile addAttachment(Integer processId, String fileName, byte[] data, String mime) {
        PostalDocumentProcess stored = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);

        Document uploaded = contentManager.uploadFile(fileName, data, Arrays.asList(stored.getName(), "attachments"),
                mime);
        ContentManagerFile file = new ContentManagerFile();
        file.setContentManagerId(uploaded.getId());
        file.setPublicPath(String.format(FORMAT_PATH_ATTACHMENTS, stored.getName(), fileName));
        return addNewAttachment(file, stored);
    }

    public Iterable<ContentManagerFile> linkAsAttachments(Integer processId, Iterable<Integer> cmFileId) {
        PostalDocumentProcess stored = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);
        Iterable<ContentManagerFile> result = StreamSupport.stream(fileRepository.findAllById(cmFileId).spliterator(), false)
                .map(fileService::duplicate)
                .map(created -> addNewAttachment(created, stored))
                .collect(Collectors.toList());
        publisher.update(stored, repository);
        return result;
    }

    private ContentManagerFile addNewAttachment(ContentManagerFile newOne, PostalDocumentProcess stored) {
        ContentManagerFile created = publisher.create(newOne, fileRepository);
        stored.getAttachments().add(created);
        return created;
    }

    public Iterable<ContentManagerFile> setAttachments(Integer processId, Iterable<Integer> cmFieldId) {
        PostalDocumentProcess stored = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);
        List<Integer> listToAdd = new ArrayList<>();
        cmFieldId.forEach(listToAdd::add);
        List<ContentManagerFile> toRemove = new ArrayList<>();

        stored.getAttachments().forEach(cmf -> {
            if (listToAdd.contains(cmf.getId())) {
                // already in current
                listToAdd.remove(cmf.getId());
            } else {
                // not anymore
                toRemove.add(cmf);
            }
        });

        // remove those not in cmFieldId
        stored.getAttachments().removeAll(toRemove);
        toRemove.forEach(cmf -> publisher.delete(cmf, fileRepository));

        // add those not in current list
        StreamSupport.stream(fileRepository.findAllById(listToAdd).spliterator(), false)
                .map(fileService::duplicate)
                .forEach(created -> addNewAttachment(created, stored));

        return stored.getAttachments();
    }

    public void clearAttachments(Integer processId) {
        PostalDocumentProcess stored = repository.findById(processId)
                .orElseThrow(EntityNotFoundException::new);
        stored.getAttachments().forEach(cmf -> publisher.delete(cmf, fileRepository));
        stored.getAttachments().clear();
        publisher.update(stored, repository);
    }
}
