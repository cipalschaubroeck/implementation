package com.cipalschaubroeck.csimplementation.document.config;

import com.cipalschaubroeck.csimplementation.document.domain.validator.BeforeCreateAddresseeValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class DocumentProcessRepositoryConfiguration implements RepositoryRestConfigurer {

    @Autowired
    private BeforeCreateAddresseeValidator validator;

    @Override
    public void configureValidatingRepositoryEventListener(ValidatingRepositoryEventListener validatingListener) {
        validatingListener.addValidator("beforeCreate", validator);
    }
}
