package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;

public interface AddresseeDuplicator {
    QualifiedPersonType getSupportedType();

    Addressee duplicate(Addressee original);
}
