package com.cipalschaubroeck.csimplementation.document.domain.listener;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleBeforeSave;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;
import org.springframework.stereotype.Component;

@Component
@RepositoryEventHandler
// spring listener
@SuppressWarnings("unused")
public class GeneratedDocumentEventListener {

    @Autowired
    private AddresseeService addresseeService;

    /**
     * If the status of the document is at least for signature, the contact info should be fixed
     *
     * @param document a generate document
     */
    @HandleBeforeSave
    // it is used by Spring
    @SuppressWarnings("unused")
    public void fixContactInfoWhenNeeded(GeneratedDocument document) {
        if (document.getStatus() != DocumentStatus.IN_PROGRESS) {
            if (document.getAddressee() != null) {
                addresseeService.fixAddressee(document.getAddressee().getAddressee());
            } else if (document.getId().getAddressee() != null) {
                addresseeService.fixAddressee(document.getId().getAddressee());
            }
        }
    }
}
