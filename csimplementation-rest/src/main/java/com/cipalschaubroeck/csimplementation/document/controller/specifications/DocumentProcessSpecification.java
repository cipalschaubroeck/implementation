package com.cipalschaubroeck.csimplementation.document.controller.specifications;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.cipalschaubroeck.csimplementation.shared.controller.SpecificationSupport.stringContainsIgnoreCase;

@Setter
@Getter
public class DocumentProcessSpecification implements Specification<DocumentProcess> {

    private String name;
    private Set<DocumentProcessType> type = EnumSet.noneOf(DocumentProcessType.class);
    private LocalDate date;
    private Integer contractId;

    private static Optional<Predicate> integerEquals(Expression<Integer> expression, Integer contained, CriteriaBuilder builder) {
        if (contained != null) {
            return Optional.of(builder.equal(expression, contained));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Predicate toPredicate(Root<DocumentProcess> root, CriteriaQuery<?> query, CriteriaBuilder builder) {
        List<Predicate> predicates = new ArrayList<>();

        integerEquals(root.get("contract").get("id"), contractId, builder).ifPresent(predicates::add);

        stringContainsIgnoreCase(root.get("name"), name, builder).ifPresent(predicates::add);

        if (!type.isEmpty() && type.size() != DocumentProcessType.values().length) {
            Predicate predicate = root.type().in(type.stream().map(DocumentProcessType::getDocumentProcessClass).collect(Collectors.toSet()));
            predicates.add(predicate);
        }
        return builder.and(predicates.toArray(new Predicate[0]));
    }
}
