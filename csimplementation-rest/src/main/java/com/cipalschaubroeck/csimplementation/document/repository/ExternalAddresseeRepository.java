package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.document.domain.ExternalAddressee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "external-addressee")
public interface ExternalAddresseeRepository extends PagingAndSortingRepository<ExternalAddressee, Integer> {
}
