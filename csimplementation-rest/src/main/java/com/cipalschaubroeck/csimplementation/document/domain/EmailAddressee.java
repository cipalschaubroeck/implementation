package com.cipalschaubroeck.csimplementation.document.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "email_addressee")
@Getter
@Setter
public class EmailAddressee {
    @Id
    private AddresseeId id = new AddresseeId();

    @ManyToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "addressee", foreignKey = @ForeignKey(name = "fk_email_addressee_addressee"))
    @NotNull
    @MapsId("addressee")
    private Addressee addressee;

    @ManyToOne(optional = false)
    @JoinColumn(name = "process", foreignKey = @ForeignKey(name = "fk_email_addressee_process"))
    @NotNull
    @MapsId("process")
    private EmailDocumentProcess process;

    @Column(name = "email_signature")
    private String signature;
}
