package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.document.controller.specifications.DocumentProcessSpecification;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessService;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@BasePathAwareController
public class DocumentProcessController {

    @Autowired
    private DocumentProcessService service;

    @Autowired
    private ControllerSupport controllerSupport;

    @ApiOperation("${swagger.document-process-controller.duplicate.summary}")
    @ApiResponses(@ApiResponse(code = 201, message = "CREATED", response = DocumentProcess.class))
    @PostMapping(path = {"/document-process/{id}", "/postal-document-process/{id}",
            "/email-document-process/{id}", "/upload-document-process/{id}"},
            consumes = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<Resource> duplicateProcess(
            @ApiParam(value = "${swagger.document-process.id.description}", example = "10") @PathVariable("id") Integer id,
            @ApiParam("${swagger.document-process-controller.duplicate.name.description}") @RequestBody String name,
            PersistentEntityResourceAssembler assembler) {
        return new ResponseEntity<>(assembler.toFullResource(service.duplicate(id, name)), HttpStatus.CREATED);
    }

    /**
     * For this to work with related properties referencing other tables, projection needs to be passed
     * as query parameter because Collections are not going to be populated seen's they are out
     * of the transaction.
     *
     * @param pageable page number and size to be returned
     * @param sort     string in the request
     * @return Pageable using the page information from pageable and the sort of sort param
     */
    @GetMapping("/contract/{id}/document-process")
    public ResponseEntity<PagedResources> search(
            DocumentProcessSpecification specification,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            @PathVariable("id") Integer contractId,
            PersistentEntityResourceAssembler assembler) {
        specification.setContractId(contractId);
        Page<DocumentProcess> page = service.findDocumentProcessWithStatus(specification,
                ControllerSupport.getPageConfig(pageable, sort, null));
        return controllerSupport.toResources(page, DocumentProcess.class, assembler);
    }
}
