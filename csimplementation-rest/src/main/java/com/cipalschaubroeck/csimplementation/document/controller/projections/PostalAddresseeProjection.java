package com.cipalschaubroeck.csimplementation.document.controller.projections;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.AbstractMap;
import java.util.Map;

@Projection(name = "postalAddresseeTableItem", types = PostalAddressee.class)
public interface PostalAddresseeProjection {

    class Constants {

        private Constants() {
            // utility class
        }

        public static final Map<String, String> SORT_ALIASES = Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("personName", "addressee.getEffectivePerson().getFullName()"),
                new AbstractMap.SimpleImmutableEntry<>("companyName", "addressee.companyName"));
    }

    @Value("#{target.addressee.phone?.phone}")
    String getPhone();

    @Value("#{target.addressee.email?.email}")
    String getEmail();

    @Value("#{target.addressee.effectivePerson.fullName}")
    String getPersonName();

    @Value("#{target.addressee.companyName}")
    String getCompanyName();

    /**
     * @return DocumentStatus of the GeneratedDocument, may return null
     */
    @Value("#{target.generatedDocument}")
    @JsonIgnore
    GeneratedDocument getGeneratedDocument();

    /**
     * @return false if GeneratedDocument is null or in progress, true otherwise
     */
    default boolean isReadOnly() {
        return PostalAddresseeService.shouldBeFixed(getGeneratedDocument());
    }

    @JsonIgnore
    Addressee getAddressee();
}
