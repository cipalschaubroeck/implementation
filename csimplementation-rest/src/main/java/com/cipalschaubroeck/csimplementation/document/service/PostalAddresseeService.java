package com.cipalschaubroeck.csimplementation.document.service;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;

public interface PostalAddresseeService {
    /**
     * If original's document has been dispatched, its address, phone and email have been fixed, while in the copy they
     * should be the corresponding address, phone and email of the person addressed. It the document wasn't dispatched
     * yet, we just need to copy the fields
     *
     * @param original an entity to be duplicated
     * @return a copy of the entity. Generated fields are not copied, fields independent of document process are
     * referenced, other fields are duplicated
     */
    PostalAddressee duplicate(PostalAddressee original);

    /**
     * @param info info to retrieve a database object
     * @return an Addressee wrapping the database object
     */
    Addressee build(EntityInfo info);

    PostalAddressee update(AddresseeId id, PostalAddresseeUpdateInfo info);

    /**
     * While a document is not for signature or beyond, an addressee owning it may be modified,
     * since the PostalAddressee information is about where it <em>has to be</em> sent; when the
     * document's status is beyond that, the PostalAddressee information is about where it <em>was sent</em>,
     * so it should be read only
     *
     * @param document a document, possibly owned by a PostalAddressee
     * @return true if a postal addressee owning this document should be considered read only
     */
    static boolean shouldBeFixed(GeneratedDocument document) {
        return document != null && document.getStatus() != DocumentStatus.IN_PROGRESS;
    }
}
