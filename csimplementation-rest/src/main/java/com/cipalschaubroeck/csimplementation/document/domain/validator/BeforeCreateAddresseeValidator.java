package com.cipalschaubroeck.csimplementation.document.domain.validator;

import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import java.util.List;

/**
 * When an addressee is created, its contact info must belong to the person it points
 */
@Component
public class BeforeCreateAddresseeValidator implements Validator {
    @Override
    public boolean supports(Class<?> clazz) {
        return Addressee.class.isAssignableFrom(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        Addressee addressee = (Addressee) target;
        checkContactInfo(addressee.getPhone(), addressee.getEffectivePerson().getPhoneNumbers(),
                "phone", "addressee.phone-belongs-to-person", errors);
        checkContactInfo(addressee.getEmail(), addressee.getEffectivePerson().getEmails(),
                "email", "addressee.email-belongs-to-person", errors);
        checkContactInfo(addressee.getAddress(), addressee.getEffectivePerson().getAddresses(),
                "address", "addressee.address-belongs-to-person", errors);
    }

    private <T> void checkContactInfo(T fromAddressee, List<T> fromPerson,
                                      String fieldName, String errorKey, Errors errors) {
        if (fromAddressee != null && !fromPerson.contains(fromAddressee)) {
            errors.rejectValue(fieldName, errorKey);
        }
    }
}
