package com.cipalschaubroeck.csimplementation.document.domain;

import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "addressee_ad_hoc")
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
@Getter
@Setter
public class AdHocAddressee extends Addressee {
    @ManyToOne(optional = false)
    @JoinColumn(name = "person", foreignKey = @ForeignKey(name = "fk_addressee_ad_hoc_person"))
    @NotNull
    private AdHocQualifiedPerson person;

    @Override
    public QualifiedPersonType getType() {
        return QualifiedPersonType.AD_HOC;
    }

    @Override
    public Person getEffectivePerson() {
        return person.getPerson();
    }

    @Override
    public String getCompanyName() {
        return "";
    }
}
