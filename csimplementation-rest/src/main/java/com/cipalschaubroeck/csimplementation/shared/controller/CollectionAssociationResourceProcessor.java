package com.cipalschaubroeck.csimplementation.shared.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

/**
 * When there is a domain type A with a relationship to many elements of type B,
 * the link /a/{idA}/relationship returns Resources<Resource<Object>>, and the link
 * /a/{idA}/relationship/{1dB} returns the element of the relationship with the corresponding id.
 * DELETE /a/{idA}/relationship/{1dB} eliminates that particular element from the collection,
 * which is handy, but the link is not there. This processor adds it as itemInAssociation
 */
@Component
public class CollectionAssociationResourceProcessor implements ResourceProcessor<Resources<Resource<Object>>> {

    @Autowired
    private ResourceMappings mappings;

    @Override
    public Resources<Resource<Object>> process(Resources<Resource<Object>> resource) {
        /*
            For some kind of bug and empty page enters in this method but its not a resource of resources object
            So we have to return here or it fails in the lambda
         */
        if (resource.getContent().isEmpty() || !(resource.getContent().iterator().next() instanceof Resource)) {
            return resource;
        }
        String[] resourceSelf = getParts(resource.getLink(Link.REL_SELF));
        if (resourceSelf.length > 3) {
            String typeIfAssociation = resourceSelf[resourceSelf.length - 3];
            // it is an association
            mappings.stream().filter(m -> m.getPath().matches(typeIfAssociation)).findFirst().ifPresent(resourceMetadata -> {
                String associationUri = linkWithoutOptions(resource.getLink(Link.REL_SELF)) + "/";
                resource.getContent().forEach(itemResource -> {
                    String[] parts = getParts(itemResource.getLink(Link.REL_SELF));
                    String id = parts[parts.length - 1];
                    itemResource.add(new Link(associationUri + id).withRel("itemInAssociation"));
                });
            });
        }
        return resource;
    }

    private String linkWithoutOptions(Link link) {
        String href = link.getHref().replaceAll("\\{[^}]*}", "");
        int index = href.indexOf('?');
        if (index != -1) {
            href = href.substring(0, index);
        }
        return href;
    }

    private String[] getParts(Link link) {
        String withoutParameters = linkWithoutOptions(link);
        return withoutParameters.split("/");
    }

}
