package com.cipalschaubroeck.csimplementation.shared.controller;

import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.rest.core.mapping.ResourceMappings;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.spi.BackendIdConverter;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.hateoas.core.EmbeddedWrapper;
import org.springframework.hateoas.core.EmbeddedWrappers;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.plugin.core.PluginRegistry;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@Component
public class ControllerSupport {

    private static final EmbeddedWrappers WRAPPERS = new EmbeddedWrappers(false);

    @Autowired
    private ResourceMappings mappings;
    @Autowired
    private PluginRegistry<BackendIdConverter, Class<?>> idConverters;
    @Autowired
    private Repositories repositories;
    @Autowired
    @Qualifier("defaultConversionService")
    private ConversionService conversionService;
    @Autowired
    private PagedResourcesAssembler<Object> pagedResourcesAssembler;

    private static Link getDefaultSelfLink() {
        return new Link(ServletUriComponentsBuilder.fromCurrentRequest().build().toUriString());
    }

    public static Resources toResources(Iterable<?> source, PersistentEntityResourceAssembler assembler, Class<?> domainType) {
        if (!source.iterator().hasNext()) {
            List<EmbeddedWrapper> content = Collections.singletonList(WRAPPERS.emptyCollectionOf(domainType));
            return new Resources<>(content, getDefaultSelfLink());
        }

        List<PersistentEntityResource> resources = StreamSupport.stream(source.spliterator(), false).map(obj ->
                obj == null ? null : assembler.toResource(obj)).collect(Collectors.toList());

        return new Resources<>(resources, getDefaultSelfLink());
    }

    public static ResponseEntity<ByteArrayResource> download(String fileName, byte[] content) {
        ByteArrayResource resource = new ByteArrayResource(content, fileName);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
                .contentLength(content.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    /**
     * expects a link for a particular entity, so the last two parts are repository path and id
     *
     * @param link               the link
     * @param allowedDomainTypes (optional) a collection of allowed domain types
     * @return the info or null if the class is not in the allowed domain types or there is no repository
     * for it
     */
    public EntityInfo linkToEntityInfo(@NotNull Link link, @Nullable Iterable<Class<?>> allowedDomainTypes) {
        String[] href = link.getHref().split("/");
        String itemId = href[href.length - 1];
        String repositoryPath = href[href.length - 2];

        Stream<ResourceMetadata> metadata;
        if (allowedDomainTypes != null) {
            metadata = StreamSupport.stream(allowedDomainTypes.spliterator(), false)
                    .map(mappings::getMetadataFor);
        } else {
            metadata = mappings.stream();
        }
        ResourceMetadata mapping = metadata.filter(m -> m.getPath().matches(repositoryPath))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(link.getHref() + " could not be match to a(n allowed) domain type"));

        EntityInfo info = new EntityInfo();
        info.setDomainType(mapping.getDomainType());
        info.setId(Optional.ofNullable(idConverters.getPluginFor(mapping.getDomainType()))
                .orElse(BackendIdConverter.DefaultIdConverter.INSTANCE)
                .fromRequestId(itemId, mapping.getDomainType()));
        if (info.getId() instanceof String) {
            // default id converter just passes the id, check that it is correct
            repositories.getRepositoryInformationFor(mapping.getDomainType()).ifPresent(repositoryInformation -> {
                if (!repositoryInformation.getIdType().isInstance(info.getId())) {
                    // safe cast since it's type of an id
                    info.setId((Serializable) conversionService.convert((Object) info.getId(), repositoryInformation.getIdType()));
                }
            });
        }

        return info;
    }

    public <T> ResponseEntity<PagedResources> toResources(Page<T> page, Class<T> domainClass, PersistentEntityResourceAssembler assembler) {
        PagedResources resources;
        if (page.getContent().isEmpty()) {
            resources = pagedResourcesAssembler.toEmptyResource(page, domainClass);
        } else {
            resources = pagedResourcesAssembler.toResource(page.map(domainClass::cast), assembler);
        }
        return ResponseEntity.ok(resources);
    }

    /**
     * Spring data rest sort translation does not support nested properties, which we need
     *
     * @param sortStrings     (mandatory, not null not empty) sort string in the request
     * @param propertyAliases (mandatory, not null not empty) sometimes we'll find convenient for front to use sort strings based on what it receives,
     *                        and such aliases need translation to jpa properties
     * @return Sort to apply when there are sort requests but has been not recognized
     * @see <a href="https://jira.spring.io/browse/DATAREST-976">DATAREST-976</a>
     */
    private static Sort bugDataRest976(List<String> sortStrings, Map<String, String> propertyAliases) {
        List<Sort.Order> orders = new ArrayList<>();
        for (int i = 0; i < sortStrings.size(); i++) {
            String property = sortStrings.get(i);
            Sort.Direction direction;
            if (property.indexOf(',') > -1) {
                String[] parts = property.split(",");
                property = parts[0];
                direction = Sort.Direction.fromString(parts[1]);
            } else if (i + 1 < sortStrings.size() && ("asc".equalsIgnoreCase(sortStrings.get(i + 1)) || "desc".equalsIgnoreCase(sortStrings.get(i + 1)))) {
                direction = Sort.Direction.fromString(sortStrings.get(i + 1));

                // TODO CSPROC-2848 maybe with regex we don't need to edit the loop counter
                i++;
            } else {
                direction = Sort.Direction.ASC;
            }
            if (propertyAliases != null && propertyAliases.containsKey(property)) {
                property = propertyAliases.get(property);
            }
            orders.add(Sort.Order.by(property).with(direction).ignoreCase());
        }
        return Sort.by(orders);
    }

    /**
     * @param pageable        page number and size to be returned
     * @param sort            string in the request
     * @param propertyAliases sometimes we'll find convenient for front to use sort strings based on what it receives,
     *                        and such aliases need translation to jpa properties
     * @return Pageable using the page information from pageable and the sort of sort param
     */
    public static Pageable getPageConfig(Pageable pageable, List<String> sort, Map<String, String> propertyAliases) {
        if (CollectionUtils.isEmpty(sort)) {
            return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
        } else {
            return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
                    ControllerSupport.bugDataRest976(sort, propertyAliases));
        }

    }
}
