package com.cipalschaubroeck.csimplementation.shared.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.core.CollectionFactory;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.PersistentProperty;
import org.springframework.data.mapping.PersistentPropertyAccessor;
import org.springframework.data.repository.support.Repositories;
import org.springframework.data.repository.support.RepositoryInvoker;
import org.springframework.data.repository.support.RepositoryInvokerFactory;
import org.springframework.data.rest.core.event.AfterLinkSaveEvent;
import org.springframework.data.rest.core.event.BeforeLinkSaveEvent;
import org.springframework.data.rest.core.mapping.PropertyAwareResourceMapping;
import org.springframework.data.rest.core.mapping.ResourceMetadata;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.ControllerUtils;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.data.rest.webmvc.RootResourceInformation;
import org.springframework.data.rest.webmvc.support.BackendId;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.springframework.data.rest.webmvc.RestMediaTypes.SPRING_DATA_COMPACT_JSON_VALUE;
import static org.springframework.data.rest.webmvc.RestMediaTypes.TEXT_URI_LIST_VALUE;
import static org.springframework.web.bind.annotation.RequestMethod.PATCH;
import static org.springframework.web.bind.annotation.RequestMethod.POST;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

/**
 * org.springframework.data.rest.webmvc.RepositoryPropertyReferenceController, with PATCH, PUT and POST method does not
 * return the created items. It would be convenient to do so because entities may change due to listeners or calculated
 * values and clients could use that information without reloading
 */
@BasePathAwareController
// used by Spring
@SuppressWarnings("unused")
public class AssociationPutController {
    private static final Collection<HttpMethod> AUGMENTING_METHODS = EnumSet.of(HttpMethod.PATCH, HttpMethod.POST);
    private static final String BASE_MAPPING = "/{repository}/{id}/{property}";

    @Autowired
    private Repositories repositories;
    @Autowired
    private RepositoryInvokerFactory repositoryInvokerFactory;
    @Autowired
    private ApplicationEventPublisher publisher;

    @Transactional
    @RequestMapping(value = BASE_MAPPING, method = {PATCH, PUT, POST}, //
            consumes = {MediaType.APPLICATION_JSON_VALUE, SPRING_DATA_COMPACT_JSON_VALUE, TEXT_URI_LIST_VALUE})
    public ResponseEntity<ResourceSupport> createPropertyReference(
            RootResourceInformation resourceInformation,
            HttpMethod requestMethod,
            @RequestBody(required = false) Resources<Object> incoming,
            @BackendId Serializable id,
            @PathVariable("property") String propertyName,
            PersistentEntityResourceAssembler assembler) throws Exception {

        ResourceMetadata metadata = resourceInformation.getResourceMetadata();
        PropertyAwareResourceMapping mapping = metadata.getProperty(propertyName);
        if (mapping == null || !mapping.isExported()) {
            throw new ResourceNotFoundException();
        }
        PersistentProperty<?> property = mapping.getProperty();
        resourceInformation.verifySupportedMethod(requestMethod, property);
        RepositoryInvoker invoker = resourceInformation.getInvoker();

        Object domainItem = invoker.invokeFindById(id).orElseThrow(ResourceNotFoundException::new);
        PersistentPropertyAccessor accessor = property.getOwner().getPropertyAccessor(domainItem);
        // too generic to know which type it actually is
        @SuppressWarnings("unchecked")
        Object propertyValue = accessor.getProperty(property);
        ReferencedProperty prop = new ReferencedProperty(property, propertyValue, accessor);

        ResourceSupport toReturn;
        Resources<Object> source = incoming == null ? new Resources<>(Collections.emptyList()) : incoming;
        if (prop.property.isCollectionLike()) {
            toReturn = createPropertyWhenCollection(requestMethod, assembler, source, prop);
        } else if (prop.property.isMap()) {
            toReturn = createPropertyWhenMap(requestMethod, assembler, source, prop);
        } else {
            toReturn = createPropertyWhenSingle(requestMethod, assembler, source, prop);
        }

        publisher.publishEvent(new BeforeLinkSaveEvent(prop.accessor.getBean(), prop.propertyValue));
        Object result = invoker.invokeSave(prop.accessor.getBean());
        publisher.publishEvent(new AfterLinkSaveEvent(result, prop.propertyValue));

        return ControllerUtils.toResponseEntity(HttpStatus.OK, new HttpHeaders(), toReturn);
    }

    private ResourceSupport createPropertyWhenSingle(HttpMethod requestMethod, PersistentEntityResourceAssembler assembler, Resources<Object> source, ReferencedProperty prop) {
        ResourceSupport toReturn;// single property
        if (HttpMethod.PATCH.equals(requestMethod)) {
            throw new HttpRequestMethodNotSupportedException();
        }

        if (source.getLinks().size() != 1) {
            throw new IllegalArgumentException(
                    "Must send exactly 1 link to update a property reference that isn't a List or a Map.");
        }

        Object newItem = loadPropertyValue(prop.propertyType, source.getLinks().get(0));
        prop.setProperty(newItem);
        toReturn = assembler.toFullResource(newItem);
        return toReturn;
    }

    private ResourceSupport createPropertyWhenMap(HttpMethod requestMethod, PersistentEntityResourceAssembler assembler, Resources<Object> source, ReferencedProperty prop) {
        ResourceSupport toReturn;
        Map<String, Object> map = AUGMENTING_METHODS.contains(requestMethod)
                ? prop.getValueAsMap()
                : CollectionFactory.createMap(prop.property.getType(), 0);
        Map<String, Object> newItems = new HashMap<>();

        // Add to the existing collection
        for (Link link : source.getLinks()) {
            Object newItem = loadPropertyValue(prop.propertyType, link);
            map.put(link.getRel(), newItem);
            newItems.put(link.getRel(), newItem);
        }
        prop.setProperty(map);
        toReturn = ControllerSupport.toResources(newItems.values(), assembler, prop.propertyType);
        return toReturn;
    }

    private ResourceSupport createPropertyWhenCollection(HttpMethod requestMethod, PersistentEntityResourceAssembler assembler, Resources<Object> source, ReferencedProperty prop) {
        ResourceSupport toReturn;// adds to current value or creates new collection, depending on method used
        Collection<Object> collection = AUGMENTING_METHODS.contains(requestMethod)
                ? prop.getValueAsCollection()
                : CollectionFactory.createCollection(prop.property.getType(), 0);
        List<Object> newItems = new ArrayList<>();

        for (Link link : source.getLinks()) {
            Object newItem = loadPropertyValue(prop.propertyType, link);
            collection.add(newItem);
            newItems.add(newItem);
        }
        prop.setProperty(collection);
        toReturn = ControllerSupport.toResources(newItems, assembler, prop.propertyType);
        return toReturn;
    }

    private Object loadPropertyValue(Class<?> type, Link link) {
        String href = link.expand().getHref();
        String id = href.substring(href.lastIndexOf('/') + 1);

        RepositoryInvoker invoker = repositoryInvokerFactory.getInvokerFor(type);

        return invoker.invokeFindById(id).orElse(null);
    }

    private class ReferencedProperty {

        final PersistentEntity<?, ?> entity;
        final PersistentProperty<?> property;
        final Class<?> propertyType;
        final Object propertyValue;
        final PersistentPropertyAccessor accessor;

        private ReferencedProperty(PersistentProperty<?> property, Object propertyValue,
                                   PersistentPropertyAccessor wrapper) {
            this.property = property;
            this.propertyValue = propertyValue;
            this.accessor = wrapper;
            this.propertyType = property.getActualType();
            this.entity = repositories.getPersistentEntity(propertyType);
        }

        // too generic to be more precise
        @SuppressWarnings("unchecked")
        void setProperty(Object newItem) {
            accessor.setProperty(property, newItem);
        }

        // too generic to be more precise
        @SuppressWarnings("unchecked")
        Collection<Object> getValueAsCollection() {
            return (Collection<Object>) propertyValue;
        }

        // too generic to be more precise
        @SuppressWarnings("unchecked")
        Map<String, Object> getValueAsMap() {
            return (Map<String, Object>) propertyValue;
        }
    }

    @ExceptionHandler
    public ResponseEntity<Void> handle(HttpRequestMethodNotSupportedException exception) {
        return exception.toResponse();
    }

    static class HttpRequestMethodNotSupportedException extends RuntimeException {

        private static final long serialVersionUID = 3704212056962845475L;

        /*
         * (non-Javadoc)
         * @see java.lang.Throwable#getMessage()
         */
        @Override
        public String getMessage() {
            return "Cannot PATCH a reference to this singular property since the property type is not a List or a Map.";
        }

        ResponseEntity<Void> toResponse() {
            return ResponseEntity.status(HttpStatus.METHOD_NOT_ALLOWED).allow(HttpMethod.POST, HttpMethod.PUT).build();
        }
    }
}
