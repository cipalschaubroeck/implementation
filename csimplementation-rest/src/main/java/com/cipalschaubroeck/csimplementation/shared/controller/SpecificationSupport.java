package com.cipalschaubroeck.csimplementation.shared.controller;

import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import java.util.Optional;

public class SpecificationSupport {

    private SpecificationSupport() {
        // utility class
    }

    /**
     * @param expression a String expression within a query
     * @param contained  string to check if it's contained in expression
     * @param builder    builder used to create the query
     * @return an optional containing a Predicate to check if "expression" contains "contained", present if contained is not empty
     */
    public static Optional<Predicate> stringContainsIgnoreCase(Expression<String> expression, String contained, CriteriaBuilder builder) {
        if (StringUtils.isNotBlank(contained)) {
            return Optional.of(builder.like(builder.lower(expression), '%' + contained.toLowerCase() + '%'));
        } else {
            return Optional.empty();
        }
    }
}
