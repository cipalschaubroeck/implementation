package com.cipalschaubroeck.csimplementation.user.controller;

import com.cipalschaubroeck.csimplementation.tenant.service.TenantSettingService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("user")
public class UserInfoController {

    @Autowired
    private TenantSettingService service;

    @GetMapping("theme")
    public ResponseEntity<String> getThemeForTenant() {
        Optional<String> theme = service.getThemeForTenant();
        if (theme.isEmpty() || StringUtils.isBlank(theme.get())) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.ok(theme.get());
        }
    }

}
