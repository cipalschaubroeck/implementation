package com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ReminderServiceDto {
    private Integer contractId;
    private List<Integer> documentsId = new ArrayList<>();
    private Integer insuredParty;
    private LocalDate reminderDate;
    private Integer reminderId;
}
