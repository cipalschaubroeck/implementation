package com.cipalschaubroeck.csimplementation.contractinsurances.controller.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class ReminderDto {
    private LocalDate reminderDate;
    private Link reminder;
    private List<Link> documentProcesses = new ArrayList<>();
}
