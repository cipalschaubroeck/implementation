package com.cipalschaubroeck.csimplementation.contractinsurances.services;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.Reminder;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.InsuredPartyDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.ReminderServiceDto;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface InsuredPartyService {
    void saveOrUpdate(InsuredPartyDto dto);

    Page<InsuredParty> getAllContractInsuredParty(Integer contractId, Pageable pageable);

    Contract findAllContractRelatedParties(Integer id);

    Page<Reminder> findAllContractReminders(Integer contractId, Pageable pageable);

    void saveOrUpdateReminder(ReminderServiceDto dto);
}
