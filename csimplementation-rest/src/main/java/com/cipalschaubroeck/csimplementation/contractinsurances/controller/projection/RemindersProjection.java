package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.Reminder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Projection(name = "remindersProjection", types = Reminder.class)
public interface RemindersProjection {

    class Constants {

        private Constants() {
            // utility class
        }

        public static final Map<String, String> SORT_ALIASES = Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("refNum", "id"));
    }

    //used by jackson
    @SuppressWarnings("unused")
    List<ReminderDocumentsProjection> getDocuments();

    //used by jackson
    @SuppressWarnings("unused")
    LocalDate getReminderDate();

    @Value("#{target.insuredParty.referenceNumber}")
    String getReferenceNumber();

    //used by jackson
    @SuppressWarnings("unused")
    RemindersInsuredPartyProjection getInsuredParty();

    //used by jackson
    @SuppressWarnings("unused")
    default String getDocumentName() {
        return getDocuments().stream()
                .map(d -> d.getDocumentProcess().getName())
                .collect(Collectors.joining(" "));
    }
}
