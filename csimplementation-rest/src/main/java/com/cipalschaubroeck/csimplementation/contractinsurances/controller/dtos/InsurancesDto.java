package com.cipalschaubroeck.csimplementation.contractinsurances.controller.dtos;

import lombok.Getter;
import lombok.Setter;
import org.springframework.hateoas.Link;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class InsurancesDto {
    private String policyType;
    private String subject;
    private LocalDate deadline;
    private Link insuredParty;
    private Link contractor;
    private Link authority;
    private List<Link> relatedParty = new ArrayList<>();
}
