package com.cipalschaubroeck.csimplementation.contractinsurances.repository;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.Reminder;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RemindersRepository extends PagingAndSortingRepository<Reminder, Integer>,
        JpaSpecificationExecutor<RelatedParty> {
    Page<Reminder> findAllByInsuredPartyContractAndInsuredPartyContractStatusNotLike(Contract contract, ContractStatus status, Pageable pageable);
}
