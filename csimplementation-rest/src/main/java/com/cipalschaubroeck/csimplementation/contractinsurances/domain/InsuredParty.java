package com.cipalschaubroeck.csimplementation.contractinsurances.domain;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.listener.InsuredPartyListener;
import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "insured_party")
@Getter
@Setter
@EntityListeners(InsuredPartyListener.class)
public class InsuredParty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "comment")
    private String comment;

    @Column(name = "policy_number")
    private String policyNumber;

    @Column(name = "type")
    @NotNull
    private String type;

    @Column(name = "name")
    private String name;

    @Column(name = "subject")
    private String subject;

    @Column(name = "deadline")
    private LocalDate deadline;

    @ManyToOne
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_insured_party_contract"))
    @NotNull
    private Contract contract;

    @ManyToOne
    @JoinColumn(name = "contractor", foreignKey = @ForeignKey(name = "fk_insured_party_contractor"))
    private Organization contractor;

    @ManyToOne
    @JoinColumn(name = "authority", foreignKey = @ForeignKey(name = "fk_insured_party_authority"))
    private ContractingAuthority authority;

    @OneToMany(mappedBy = "insuredParty", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<InsuredRelatedParty> relatedParties = new ArrayList<>();

    @OneToMany(mappedBy = "insuredParty", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reminder> reminders = new ArrayList<>();

    //used by jackson
    @SuppressWarnings("unused")
    @Transient
    public String getReferenceNumber() {
        return "VZ" + getId();
    }
}
