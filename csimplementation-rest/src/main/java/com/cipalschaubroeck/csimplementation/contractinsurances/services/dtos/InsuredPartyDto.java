package com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class InsuredPartyDto {
    private Integer contractId;
    private Integer insuredPartyId;
    private Integer contractorId;
    private Integer authorityId;
    private List<Integer> relatedPartiesId = new ArrayList<>();
    private String subject;
    private String policyType;
    private LocalDate deadline;
}
