package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredRelatedParty;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "insuredRelatedPartyProjection", types = InsuredRelatedParty.class)
public interface InsuredRelatedPartyProjection {

    Integer getId();

    @Value("#{target.relatedParty.organization}")
    ContractorRelatedPartyProjection getOrganization();

    // used by jackson
    @SuppressWarnings("unused")
    RelatedPartyProjection getRelatedParty();

}
