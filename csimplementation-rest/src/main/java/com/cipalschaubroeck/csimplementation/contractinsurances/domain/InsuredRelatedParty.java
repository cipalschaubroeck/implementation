package com.cipalschaubroeck.csimplementation.contractinsurances.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "insured_related_party")
@Getter
@Setter
public class InsuredRelatedParty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "related_party", foreignKey = @ForeignKey(name = "fk_related_party_insured_party"))
    @NotNull
    private RelatedParty relatedParty;

    @ManyToOne
    @JoinColumn(name = "insured_party", foreignKey = @ForeignKey(name = "fk_insured_party_related_party"))
    @NotNull
    private InsuredParty insuredParty;
}
