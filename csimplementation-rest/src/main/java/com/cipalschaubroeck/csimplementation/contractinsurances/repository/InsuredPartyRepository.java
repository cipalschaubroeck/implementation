package com.cipalschaubroeck.csimplementation.contractinsurances.repository;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "insured-party")
public interface InsuredPartyRepository extends PagingAndSortingRepository<InsuredParty, Integer>,
        JpaSpecificationExecutor<InsuredParty> {
    Page<InsuredParty> findAllByContractAndContractStatusNotLike(Contract contract, ContractStatus status, Pageable pageable);
}
