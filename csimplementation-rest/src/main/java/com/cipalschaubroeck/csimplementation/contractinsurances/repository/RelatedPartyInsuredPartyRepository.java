package com.cipalschaubroeck.csimplementation.contractinsurances.repository;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredRelatedParty;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "insured-related-party")
public interface RelatedPartyInsuredPartyRepository extends PagingAndSortingRepository<InsuredRelatedParty, Integer>,
        JpaSpecificationExecutor<InsuredRelatedParty> {
}
