package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "authorityRelatedPartyProjection", types = ContractingAuthority.class)
public interface AuthorityRelatedPartyProjection {

    @Value("#{target.name}")
    String getName();
}
