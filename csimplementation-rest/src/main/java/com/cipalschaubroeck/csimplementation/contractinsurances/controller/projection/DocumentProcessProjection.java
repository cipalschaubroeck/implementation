package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "insuranceDocumentProcessProjection", types = DocumentProcess.class)
public interface DocumentProcessProjection {
    String getName();
}
