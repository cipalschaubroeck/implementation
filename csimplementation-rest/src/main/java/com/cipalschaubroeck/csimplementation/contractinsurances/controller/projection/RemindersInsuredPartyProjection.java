package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "remindersInsuredPartyProjection", types = InsuredParty.class)
public interface RemindersInsuredPartyProjection {
    String getName();
}
