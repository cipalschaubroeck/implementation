package com.cipalschaubroeck.csimplementation.contractinsurances.domain;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "reminders_documents")
@Getter
@Setter
public class ReminderDocuments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "reminder", foreignKey = @ForeignKey(name = "fk_reminders_documents"))
    @NotNull
    private Reminder reminder;

    @ManyToOne
    @JoinColumn(name = "document_process", foreignKey = @ForeignKey(name = "fk_reminders_document_process"))
    private DocumentProcess documentProcess;
}
