package com.cipalschaubroeck.csimplementation.contractinsurances.services.impl;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredRelatedParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.Reminder;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.ReminderDocuments;
import com.cipalschaubroeck.csimplementation.contractinsurances.repository.InsuredPartyRepository;
import com.cipalschaubroeck.csimplementation.contractinsurances.repository.RelatedPartyRepository;
import com.cipalschaubroeck.csimplementation.contractinsurances.repository.RemindersRepository;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.InsuredPartyService;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.InsuredPartyDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.ReminderServiceDto;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.DocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import com.cipalschaubroeck.csimplementation.organization.repository.OrganizationRepository;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;

@Service
public class DefaultInsuredPartyService implements InsuredPartyService {

    @Autowired
    private ContractRepository contractRepository;

    @Autowired
    OrganizationRepository organizationRepository;

    @Autowired
    ContractingAuthorityRepository contractingAuthorityRepository;

    @Autowired
    private InsuredPartyRepository insuredPartyRepository;

    @Autowired
    private RelatedPartyRepository relatedPartyRepository;

    @Autowired
    private RemindersRepository remindersRepository;

    @Autowired
    private DocumentProcessRepository documentProcessRepository;

    @Transactional
    public void saveOrUpdate(InsuredPartyDto dto) {
        if (dto.getContractId() == null) {
            throw new EntityNotFoundException("Contract id cannot be null");
        }

        Contract contract = contractRepository.findById(dto.getContractId()).orElseThrow(EntityNotFoundException::new);

        ContractingAuthority authority = null;
        if (dto.getAuthorityId() != null) {
            authority = contractingAuthorityRepository.findById(dto.getAuthorityId()).orElseThrow(EntityNotFoundException::new);
        }

        Organization organization = null;
        if (dto.getContractorId() != null) {
            organization = organizationRepository.findById(dto.getContractId()).orElseThrow(EntityNotFoundException::new);
        }
        InsuredParty insuredParty;

        if (dto.getInsuredPartyId() != null) {
            insuredParty = insuredPartyRepository.findById(dto.getInsuredPartyId()).orElseThrow(EntityNotFoundException::new);

            if (!contract.getId().equals(insuredParty.getContract().getId())) {
                throw new EntityNotFoundException("Contract from the insured-party is no correct");
            }

            insuredParty.setSubject(dto.getSubject());
            insuredParty.setType(dto.getPolicyType());
            insuredParty.setContractor(organization);
            insuredParty.setAuthority(authority);
            insuredParty.getRelatedParties().clear();
            insuredParty.setType(dto.getPolicyType());
            insuredParty.setSubject(dto.getSubject());
            insuredParty.setDeadline(dto.getDeadline());
            for (Integer relatedPartyId : dto.getRelatedPartiesId()) {
                RelatedParty relatedParty = relatedPartyRepository.findById(relatedPartyId).orElseThrow(EntityNotFoundException::new);
                InsuredRelatedParty insuredRelatedParty = new InsuredRelatedParty();
                insuredRelatedParty.setInsuredParty(insuredParty);
                insuredRelatedParty.setRelatedParty(relatedParty);
                insuredParty.getRelatedParties().add(insuredRelatedParty);
                insuredRelatedParty.setRelatedParty(relatedParty);

            }
        } else {
            insuredParty = new InsuredParty();
            insuredParty.setContract(contract);
            insuredParty.setContractor(organization);
            insuredParty.setAuthority(authority);

            for (Integer relatedPartyId : dto.getRelatedPartiesId()) {
                RelatedParty relatedParty = relatedPartyRepository.findById(relatedPartyId).orElseThrow(EntityNotFoundException::new);
                InsuredRelatedParty insuredRelatedParty = new InsuredRelatedParty();
                insuredRelatedParty.setInsuredParty(insuredParty);
                insuredRelatedParty.setRelatedParty(relatedParty);
                insuredParty.getRelatedParties().add(insuredRelatedParty);
            }

            insuredParty.setType(dto.getPolicyType());
            insuredParty.setSubject(dto.getSubject());
            insuredParty.setDeadline(dto.getDeadline());
            insuredPartyRepository.save(insuredParty);
        }
    }

    @Override
    @Transactional
    public Page<InsuredParty> getAllContractInsuredParty(Integer contractId, Pageable pageable) {
        Contract contract = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        Page<InsuredParty> page =
                insuredPartyRepository.findAllByContractAndContractStatusNotLike(contract, ContractStatus.IN_PREPARATION, pageable);

        //noinspection ResultOfMethodCallIgnored
        page.getContent().forEach(result -> result.getRelatedParties().size());
        return page;
    }

    @Override
    @Transactional
    public Contract findAllContractRelatedParties(Integer id) {
        Contract cl = contractRepository.findById(id).orElseThrow(EntityNotFoundException::new);
        //noinspection ResultOfMethodCallIgnored
        cl.getRelatedParties().size();
        return cl;
    }

    @Override
    @Transactional
    public Page<Reminder> findAllContractReminders(Integer contractId, Pageable pageable) {
        Contract c = contractRepository.findById(contractId).orElseThrow(EntityNotFoundException::new);
        Page<Reminder> page = remindersRepository.findAllByInsuredPartyContractAndInsuredPartyContractStatusNotLike(
                c, ContractStatus.IN_PREPARATION, pageable);
        //noinspection ResultOfMethodCallIgnored
        page.getContent().forEach(b -> b.getDocuments().size());
        return page;
    }

    @Override
    @Transactional
    public void saveOrUpdateReminder(ReminderServiceDto dto) {
        if (dto.getInsuredParty() == null) {
            throw new EntityNotFoundException("Insured Party must be provided");
        }

        InsuredParty insuredParty = insuredPartyRepository.findById(dto.getInsuredParty()).orElseThrow(EntityNotFoundException::new);

        Reminder reminder;
        if (dto.getReminderId() != null) {
            reminder = remindersRepository.findById(dto.getReminderId()).orElseThrow(EntityNotFoundException::new);
        } else {
            reminder = new Reminder();
        }

        reminder.setInsuredParty(insuredParty);
        reminder.setReminderDate(dto.getReminderDate());
        insuredParty.getReminders().add(reminder);
        reminder.getDocuments().clear();
        dto.getDocumentsId().forEach(documentId -> {
            DocumentProcess dp = documentProcessRepository.findById(documentId).orElseThrow(EntityNotFoundException::new);
            ReminderDocuments rd = new ReminderDocuments();
            rd.setDocumentProcess(dp);
            rd.setReminder(reminder);
            reminder.getDocuments().add(rd);
            insuredParty.getReminders().add(reminder);
        });
    }
}
