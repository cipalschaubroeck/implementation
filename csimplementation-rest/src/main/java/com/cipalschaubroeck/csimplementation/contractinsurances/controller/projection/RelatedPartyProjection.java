package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "relatedPartyProjection", types = RelatedParty.class)
public interface RelatedPartyProjection {
    ContractorRelatedPartyProjection getOrganization();
}
