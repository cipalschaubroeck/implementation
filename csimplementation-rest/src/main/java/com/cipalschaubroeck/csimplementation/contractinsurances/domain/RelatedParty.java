package com.cipalschaubroeck.csimplementation.contractinsurances.domain;

import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "related_party", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"contract", "organization"})})
@Getter
@Setter
public class RelatedParty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "contract", foreignKey = @ForeignKey(name = "fk_related_party_contract"))
    @NotNull
    private Contract contract;

    @ManyToOne
    @JoinColumn(name = "organization", foreignKey = @ForeignKey(name = "fk_related_party_organization"))
    @NotNull
    private Organization organization;

}
