package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import org.springframework.data.rest.core.config.Projection;

import java.time.LocalDate;
import java.util.AbstractMap;
import java.util.List;
import java.util.Map;

@Projection(name = "insuredPartyProjection", types = InsuredParty.class)
public interface InsuredPartyProjection {

    class Constants {

        private Constants() {
            // utility class
        }

        public static final Map<String, String> SORT_ALIASES = Map.ofEntries(
                new AbstractMap.SimpleImmutableEntry<>("refNum", "id"));
    }

    // used by jackson
    @SuppressWarnings("unused")
    String getReferenceNumber();

    // used by jackson
    @SuppressWarnings("unused")
    LocalDate getDeadline();

    AuthorityRelatedPartyProjection getAuthority();

    ContractorRelatedPartyProjection getContractor();

    List<InsuredRelatedPartyProjection> getRelatedParties();

    // used by jackson
    @SuppressWarnings("unused")
    default String getInsurancePartyName() {
        final StringBuilder sb = new StringBuilder();
        if (getAuthority() != null) {
            sb.append(getAuthority().getName()).append(" ");
        }

        if (getContractor() != null) {
            sb.append(getContractor().getName()).append(" ");
        }

        getRelatedParties().forEach(relatedParty -> sb.append(relatedParty.getOrganization().getName()).append(" "));

        return sb.toString();
    }

    String getType();

    String getSubject();
}
