package com.cipalschaubroeck.csimplementation.contractinsurances.repository;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "related-party")
public interface RelatedPartyRepository extends PagingAndSortingRepository<RelatedParty, Integer>,
        JpaSpecificationExecutor<RelatedParty> {
}
