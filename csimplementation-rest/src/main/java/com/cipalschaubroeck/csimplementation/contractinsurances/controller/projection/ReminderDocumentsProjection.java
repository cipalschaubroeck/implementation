package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.ReminderDocuments;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "reminderDocumentProcessProjection", types = ReminderDocuments.class)
public interface ReminderDocumentsProjection {
    // used by jackson
    @SuppressWarnings("unused")
    DocumentProcessProjection getDocumentProcess();
}
