package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.organization.domain.Organization;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

@Projection(name = "contractorRelatedPartyProjection", types = Organization.class)
public interface ContractorRelatedPartyProjection {
    @Value("#{target.name}")
    String getName();
}
