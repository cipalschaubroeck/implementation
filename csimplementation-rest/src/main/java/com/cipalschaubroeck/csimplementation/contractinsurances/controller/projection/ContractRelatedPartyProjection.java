package com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection;

import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.rest.core.config.Projection;

import java.util.List;

@Projection(name = "contractInsuredPartyProjection", types = Contract.class)
public interface ContractRelatedPartyProjection {

    ContractorRelatedPartyProjection getContractor();

    @Value("#{target.contractAuthorityRelation?.contractingAuthority}")
    AuthorityRelatedPartyProjection getAuthority();

    // used by jackson
    @SuppressWarnings("unused")
    List<RelatedPartyProjection> getRelatedParties();

}
