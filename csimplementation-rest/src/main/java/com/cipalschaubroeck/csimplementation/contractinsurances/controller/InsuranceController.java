package com.cipalschaubroeck.csimplementation.contractinsurances.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.dtos.InsurancesDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.dtos.ReminderDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.InsuredPartyProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.controller.projection.RemindersProjection;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.RelatedParty;
import com.cipalschaubroeck.csimplementation.contractinsurances.domain.Reminder;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.InsuredPartyService;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.InsuredPartyDto;
import com.cipalschaubroeck.csimplementation.contractinsurances.services.dtos.ReminderServiceDto;
import com.cipalschaubroeck.csimplementation.document.domain.EmailDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.organization.domain.JointVenture;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PageableDefault;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@BasePathAwareController
// spring managed
@SuppressWarnings("unused")
public class InsuranceController {

    @Autowired
    private InsuredPartyService service;

    @Autowired
    private ControllerSupport controllerSupport;

    @GetMapping("contract/{id}/related-party")
    public ResponseEntity<Resources> searchFiltered(
            PersistentEntityResourceAssembler assembler,
            @PathVariable("id") Integer contractId) {
        List<Contract> contracts = new ArrayList<>();
        contracts.add(service.findAllContractRelatedParties(contractId));
        return new ResponseEntity<>(ControllerSupport.toResources(contracts,
                assembler, Contract.class), HttpStatus.OK);
    }

    @PostMapping("contract/{id}/insured-party")
    public ResponseEntity<Void> createPoliciesToBetakenOut(@PathVariable("id") Integer contractId, @RequestBody InsurancesDto dto) {
        InsuredPartyDto serviceDto = new InsuredPartyDto();
        serviceDto.setContractId(contractId);

        serviceDto.setSubject(dto.getSubject());
        serviceDto.setPolicyType(dto.getPolicyType());
        serviceDto.setDeadline(dto.getDeadline());
        if (dto.getAuthority() != null) {
            serviceDto.setAuthorityId((Integer) controllerSupport.linkToEntityInfo(dto.getAuthority(),
                    Collections.singletonList(ContractingAuthority.class)).getId());
        }

        if (dto.getContractor() != null) {
            if (dto.getContractor().getHref().contains("single-organization")) {
                serviceDto.setContractorId((Integer) controllerSupport.linkToEntityInfo(dto.getContractor(),
                        Collections.singletonList(SingleOrganization.class)).getId());
            } else {
                serviceDto.setContractorId((Integer) controllerSupport.linkToEntityInfo(dto.getContractor(),
                        Collections.singletonList(JointVenture.class)).getId());
            }
        }

        for (Link relatedPartyLink : dto.getRelatedParty()) {
            serviceDto.getRelatedPartiesId().add((Integer) controllerSupport.linkToEntityInfo(relatedPartyLink,
                    Collections.singletonList(RelatedParty.class)).getId());
        }

        if (dto.getInsuredParty() != null) {
            serviceDto.setInsuredPartyId((Integer) controllerSupport.linkToEntityInfo(dto.getInsuredParty(),
                    Collections.singletonList(InsuredParty.class)).getId());
        }

        service.saveOrUpdate(serviceDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("contract/{id}/insured-party")
    public ResponseEntity<PagedResources> search(
            @PathVariable Integer id,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            PersistentEntityResourceAssembler assembler) {

        Page<InsuredParty> page = service.getAllContractInsuredParty(id,
                ControllerSupport.getPageConfig(pageable, sort, InsuredPartyProjection.Constants.SORT_ALIASES));
        //noinspection ResultOfMethodCallIgnored
        page.getContent().forEach(result -> result.getRelatedParties().size());
        return controllerSupport.toResources(page, InsuredParty.class, assembler);
    }

    @GetMapping("contract/{id}/reminders")
    public ResponseEntity<PagedResources> searchReminders(
            @PathVariable Integer id,
            @PageableDefault Pageable pageable,
            @RequestParam(name = "sort", required = false) List<String> sort,
            PersistentEntityResourceAssembler assembler) {
        Page<Reminder> page = service.findAllContractReminders(id, ControllerSupport
                .getPageConfig(pageable, sort, RemindersProjection.Constants.SORT_ALIASES));
        return controllerSupport.toResources(page, Reminder.class, assembler);
    }

    @PostMapping("insured-party/{id}/reminder")
    public ResponseEntity<Void> createReminder(@PathVariable("id") Integer insuredParty, @RequestBody ReminderDto dto) {
        ReminderServiceDto serviceDto = new ReminderServiceDto();
        serviceDto.setReminderDate(dto.getReminderDate());
        serviceDto.setInsuredParty(insuredParty);

        for (Link documentProcess : dto.getDocumentProcesses()) {
            List<Class<?>> allowed = new ArrayList<>(Arrays.asList(PostalDocumentProcess.class, EmailDocumentProcess.class));
            serviceDto.getDocumentsId().add((Integer) controllerSupport.linkToEntityInfo(documentProcess,
                    allowed).getId());
        }

        if (dto.getReminder() != null) {
            serviceDto.setReminderId((Integer) controllerSupport.linkToEntityInfo(dto.getReminder(),
                    Collections.singletonList(Reminder.class)).getId());
        }

        service.saveOrUpdateReminder(serviceDto);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }


}
