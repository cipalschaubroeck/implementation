package com.cipalschaubroeck.csimplementation.contractinsurances.domain.listener;

import com.cipalschaubroeck.csimplementation.contractinsurances.domain.InsuredParty;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityNotFoundException;
import javax.persistence.PrePersist;

/**
 * Checks that insured party has at least one member to be insured
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class InsuredPartyListener {

    private void validate(InsuredParty entity) {
        if (entity.getContractor() == null && entity.getAuthority() == null && CollectionUtils.isEmpty(entity.getRelatedParties())) {
            throw new EntityNotFoundException("Insured parties must have at least one member to be insured");
        }
    }

    @PrePersist
    public void validatePersist(InsuredParty entity) {
        validate(entity);
    }
}
