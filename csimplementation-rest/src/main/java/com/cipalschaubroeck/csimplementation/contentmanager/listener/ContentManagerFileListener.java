package com.cipalschaubroeck.csimplementation.contentmanager.listener;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contentmanager.repository.ContentManagerRepository;
import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.persistence.PostRemove;

@Component
public class ContentManagerFileListener {
    /*
    TODO can't autowire because jpa manages instances of this class, instead of Spring. This could be cleaner
    if Spring could add listeners to jpa so we could not rely on plain jpa listeners
     */
    private static ContentManagerRepository repository;
    private static CMCommunicationService service;

    @Autowired
    public void setRepository(ContentManagerRepository repository) {
        ContentManagerFileListener.repository = repository;
    }

    @Autowired
    public void setService(CMCommunicationService service) {
        ContentManagerFileListener.service = service;
    }

    @PostRemove
    // used as Spring listener
    @SuppressWarnings("unused")
    public void deleteFromContentManager(ContentManagerFile deleted) {
        if (repository.countAllByContentManagerIdAndIdNot(deleted.getContentManagerId(), deleted.getId()) < 1L) {
            service.removeDocument(deleted.getContentManagerId());
        }
    }
}
