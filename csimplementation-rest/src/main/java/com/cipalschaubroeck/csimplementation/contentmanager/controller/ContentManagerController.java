package com.cipalschaubroeck.csimplementation.contentmanager.controller;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import lombok.extern.slf4j.Slf4j;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;


@BasePathAwareController
@Slf4j
public class ContentManagerController {

    @Autowired
    private CMCommunicationService service;

    @GetMapping("content-manager/{contentManagerId}")
    public ResponseEntity<ByteArrayResource> download(@PathVariable("contentManagerId") String contentManagerId) throws IOException {
        Document document = service.getDocumentById(contentManagerId);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        IOUtils.copy(document.getContentStream().getStream(), baos);
        return ControllerSupport.download(document.getContentStream().getFileName(), baos.toByteArray());
    }
}
