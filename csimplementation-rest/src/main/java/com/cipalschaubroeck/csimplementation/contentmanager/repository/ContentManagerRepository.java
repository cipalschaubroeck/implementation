package com.cipalschaubroeck.csimplementation.contentmanager.repository;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import javax.transaction.Transactional;

@RepositoryRestResource(path = "content-manager-file")
public interface ContentManagerRepository extends PagingAndSortingRepository<ContentManagerFile, Integer> {

    @Transactional(Transactional.TxType.REQUIRES_NEW)
    long countAllByContentManagerIdAndIdNot(String contentManagerId, Integer id);
}
