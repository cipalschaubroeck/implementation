package com.cipalschaubroeck.csimplementation.contentmanager.service;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;

public interface ContentManagerFileService {
    /**
     * ContentManagerFile is used by several entities and points. If we share one between two
     * other entities because its values are equal, when we delete the relationship to one of the
     * entities is not easy to find other entities using it, so that is why instead of sharing,
     * the ContentManagerFile is duplicated with the same values
     *
     * @param original a content manager file to duplicate
     * @return a content manager file with the same path and actual file id
     */
    ContentManagerFile duplicate(ContentManagerFile original);

}
