package com.cipalschaubroeck.csimplementation.contentmanager.domain;

import com.cipalschaubroeck.csimplementation.contentmanager.listener.ContentManagerFileListener;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

@Entity
@Table(name = "content_manager_file")
@Getter
@Setter
@EntityListeners(ContentManagerFileListener.class)
public class ContentManagerFile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "opt_lock")
    @Version
    private Integer versionNum;

    @Column(name = "content_manager_id")
    @NotNull
    private String contentManagerId;

    /**
     * path to show in the application
     */
    @Column(name = "publicPath")
    @NotNull
    @Pattern(regexp = "/?([^/]+/)*[^/]*")
    private String publicPath;

    public String getFilename() {
        if (StringUtils.isBlank(publicPath)) {
            return null;
        } else {
            int last = publicPath.lastIndexOf('/');
            if (last > -1) {
                return publicPath.substring(last + 1);
            } else {
                return publicPath;
            }
        }
    }
}
