package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contentmanager.service.ContentManagerFileService;
import org.springframework.stereotype.Service;

@Service
public class DefaultContentManagerFileService implements ContentManagerFileService {
    @Override
    public ContentManagerFile duplicate(ContentManagerFile original) {
        ContentManagerFile copy = new ContentManagerFile();
        copy.setContentManagerId(original.getContentManagerId());
        copy.setPublicPath(original.getPublicPath());
        return copy;
    }
}
