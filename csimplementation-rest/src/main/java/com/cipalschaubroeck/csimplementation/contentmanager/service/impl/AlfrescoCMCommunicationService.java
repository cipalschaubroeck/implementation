package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import lombok.extern.slf4j.Slf4j;
import org.alfresco.cmis.client.impl.AlfrescoObjectFactoryImpl;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.api.SessionFactory;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.client.util.FileUtils;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.SessionParameter;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.BindingType;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisBaseException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("alfresco")
@Slf4j
public class AlfrescoCMCommunicationService implements CMCommunicationService {

    @Value("${alfresco.user}")
    private String cmisUser;
    @Value("${alfresco.pass}")
    private String cmisPass;
    @Value("${alfresco.url}")
    private String cmisUrl;
    @Value("${alfresco.root-folder}")
    private String rootFolder;

    private Session createSession() {
        Map<String, String> parameter = new HashMap<>();
        // user credentials
        parameter.put(SessionParameter.USER, cmisUser);
        parameter.put(SessionParameter.PASSWORD, cmisPass);
        // connection settings
        parameter.put(SessionParameter.ATOMPUB_URL, cmisUrl);
        parameter.put(SessionParameter.BINDING_TYPE, BindingType.ATOMPUB.value());

        parameter.put(SessionParameter.OBJECT_FACTORY_CLASS, AlfrescoObjectFactoryImpl.class.getName());

        // create session
        SessionFactory factory = SessionFactoryImpl.newInstance();
        return factory.getRepositories(parameter).get(0).createSession();
    }

    private Folder getOrCreateFolder(Session session, List<String> pathParts) {
        Folder folder = getFolderByPath(session, convertToPath(pathParts));
        if (folder == null) {
            String folderName = pathParts.remove(pathParts.size() - 1);
            Folder parentFolder = getOrCreateFolder(session, pathParts);
            folder = createFolder(parentFolder, folderName);
        }
        return folder;
    }

    private String convertToPath(List<String> folderStructure) {
        return String.join("/", folderStructure);
    }

    private Folder createFolder(Folder parentFolder, String folderName) {
        log.debug("Creating {} in the {} folder", folderName, parentFolder.getName());
        Map<String, String> newFolderProps = new HashMap<>();
        newFolderProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
        newFolderProps.put(PropertyIds.NAME, folderName);
        return parentFolder.createFolder(newFolderProps);
    }

    private Folder getFolderByPath(Session session, String absolutePath) {
        try {
            return FileUtils.getFolder(absolutePath, session);
        } catch (CmisObjectNotFoundException e) {
            log.debug("Querying for folder " + absolutePath + " does not exist", e);
            return null;
        }
    }

    @Override
    public Document uploadFile(final String fileName, final byte[] data, List<String> pathParts, String mimeType) {
        try {
            Session session = createSession();
            ArrayList<String> path = new ArrayList<>();
            path.add(rootFolder);
            path.addAll(pathParts);
            Folder folder = getOrCreateFolder(session, path);
            Map<String, Object> properties = new HashMap<>();
            properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document,P:cm:titled,P:cm:taggable");
            properties.put(PropertyIds.NAME, fileName);
            ByteArrayInputStream input = new ByteArrayInputStream(data);
            ContentStream contentStream = session.getObjectFactory().createContentStream(fileName, data.length, mimeType, input);
            return folder.createDocument(properties, contentStream, VersioningState.MAJOR);
        } catch (CmisBaseException e) {
            log.error("Could not upload file to Alfresco. Path: {}", pathParts);
            throw e;
        }
    }

    @Override
    public void removeDocument(String idDocument) {
        Session session = createSession();
        session.getObject(idDocument).delete();
    }

    @Override
    public Document getDocumentById(String idDocument) {
        Session session = createSession();
        return ((Document) session.getObject(idDocument)).getObjectOfLatestVersion(false);
    }
}
