package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.DocumentType;
import org.apache.chemistry.opencmis.client.api.FileableCmisObject;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ItemIterable;
import org.apache.chemistry.opencmis.client.api.ObjectId;
import org.apache.chemistry.opencmis.client.api.ObjectType;
import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.client.api.Policy;
import org.apache.chemistry.opencmis.client.api.Property;
import org.apache.chemistry.opencmis.client.api.Relationship;
import org.apache.chemistry.opencmis.client.api.Rendition;
import org.apache.chemistry.opencmis.client.api.SecondaryType;
import org.apache.chemistry.opencmis.client.api.Tree;
import org.apache.chemistry.opencmis.client.runtime.util.EmptyItemIterable;
import org.apache.chemistry.opencmis.commons.data.Ace;
import org.apache.chemistry.opencmis.commons.data.Acl;
import org.apache.chemistry.opencmis.commons.data.AllowableActions;
import org.apache.chemistry.opencmis.commons.data.CmisExtensionElement;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.data.ContentStreamHash;
import org.apache.chemistry.opencmis.commons.definitions.PropertyDefinition;
import org.apache.chemistry.opencmis.commons.definitions.TypeMutability;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.Action;
import org.apache.chemistry.opencmis.commons.enums.BaseTypeId;
import org.apache.chemistry.opencmis.commons.enums.ContentStreamAllowed;
import org.apache.chemistry.opencmis.commons.enums.ExtensionLevel;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.impl.dataobjects.ContentStreamImpl;

import java.io.ByteArrayInputStream;
import java.io.OutputStream;
import java.math.BigInteger;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

@RequiredArgsConstructor
public class DocumentMock implements Document {

    private static final DocumentType TYPE = new DocumentType() {
        @Override
        public Boolean isVersionable() {
            return Boolean.FALSE;
        }

        @Override
        public ContentStreamAllowed getContentStreamAllowed() {
            return ContentStreamAllowed.ALLOWED;
        }

        @Override
        public boolean isBaseType() {
            return true;
        }

        @Override
        public ObjectType getBaseType() {
            return null;
        }

        @Override
        public ObjectType getParentType() {
            return null;
        }

        @Override
        public ItemIterable<ObjectType> getChildren() {
            return new EmptyItemIterable<>();
        }

        @Override
        public List<Tree<ObjectType>> getDescendants(int depth) {
            return Collections.emptyList();
        }

        @Override
        public String getId() {
            return "only";
        }

        @Override
        public String getLocalName() {
            return null;
        }

        @Override
        public String getLocalNamespace() {
            return null;
        }

        @Override
        public String getDisplayName() {
            return null;
        }

        @Override
        public String getQueryName() {
            return null;
        }

        @Override
        public String getDescription() {
            return null;
        }

        @Override
        public BaseTypeId getBaseTypeId() {
            return null;
        }

        @Override
        public String getParentTypeId() {
            return null;
        }

        @Override
        public Boolean isCreatable() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isFileable() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isQueryable() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isFulltextIndexed() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isIncludedInSupertypeQuery() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isControllablePolicy() {
            return Boolean.FALSE;
        }

        @Override
        public Boolean isControllableAcl() {
            return Boolean.FALSE;
        }

        @Override
        public Map<String, PropertyDefinition<?>> getPropertyDefinitions() {
            return Collections.emptyMap();
        }

        @Override
        public TypeMutability getTypeMutability() {
            return null;
        }

        @Override
        public List<CmisExtensionElement> getExtensions() {
            return Collections.emptyList();
        }

        @Override
        public void setExtensions(List<CmisExtensionElement> extensions) {
            // mock
        }
    };

    @Getter
    @NonNull
    private final String id;

    @Getter
    @NonNull
    private final String name;

    @Getter
    @NonNull
    private final String contentStreamFileName;

    @NonNull
    private final byte[] content;

    @NonNull
    @Getter
    private final String contentStreamMimeType;

    @Override
    public DocumentType getDocumentType() {
        return TYPE;
    }

    @Override
    public boolean isVersionable() {
        return false;
    }

    @Override
    public Boolean isVersionSeriesPrivateWorkingCopy() {
        return Boolean.FALSE;
    }

    @Override
    public void deleteAllVersions() {
        // not implemented
    }

    @Override
    public ContentStream getContentStream() {
        return new ContentStreamImpl(contentStreamFileName, BigInteger.valueOf(content.length), contentStreamMimeType, new ByteArrayInputStream(content));
    }

    @Override
    public ContentStream getContentStream(BigInteger offset, BigInteger length) {
        return getContentStream();
    }

    @Override
    public ContentStream getContentStream(String streamId) {
        return getContentStream();
    }

    @Override
    public ContentStream getContentStream(String streamId, BigInteger offset, BigInteger length) {
        return getContentStream();
    }

    @Override
    public String getContentUrl() {
        return null;
    }

    @Override
    public String getContentUrl(String streamId) {
        return null;
    }

    @Override
    public Document setContentStream(ContentStream contentStream, boolean overwrite) {
        return null;
    }

    @Override
    public ObjectId setContentStream(ContentStream contentStream, boolean overwrite, boolean refresh) {
        return null;
    }

    @Override
    public Document appendContentStream(ContentStream contentStream, boolean isLastChunk) {
        return null;
    }

    @Override
    public ObjectId appendContentStream(ContentStream contentStream, boolean isLastChunk, boolean refresh) {
        return null;
    }

    @Override
    public Document deleteContentStream() {
        return null;
    }

    @Override
    public ObjectId deleteContentStream(boolean refresh) {
        return null;
    }

    @Override
    public OutputStream createOverwriteOutputStream(String filename, String mimeType) {
        return null;
    }

    @Override
    public OutputStream createOverwriteOutputStream(String filename, String mimeType, int bufferSize) {
        return null;
    }

    @Override
    public OutputStream createAppendOutputStream() {
        return null;
    }

    @Override
    public OutputStream createAppendOutputStream(int bufferSize) {
        return null;
    }

    @Override
    public ObjectId checkOut() {
        return null;
    }

    @Override
    public void cancelCheckOut() {
        // mock
    }

    @Override
    public ObjectId checkIn(boolean major, Map<String, ?> properties, ContentStream contentStream, String checkinComment, List<Policy> policies, List<Ace> addAces, List<Ace> removeAces) {
        return null;
    }

    @Override
    public ObjectId checkIn(boolean major, Map<String, ?> properties, ContentStream contentStream, String checkinComment) {
        return null;
    }

    @Override
    public Document getObjectOfLatestVersion(boolean major) {
        return null;
    }

    @Override
    public Document getObjectOfLatestVersion(boolean major, OperationContext context) {
        return null;
    }

    @Override
    public List<Document> getAllVersions() {
        return Collections.emptyList();
    }

    @Override
    public List<Document> getAllVersions(OperationContext context) {
        return Collections.emptyList();
    }

    @Override
    public Document copy(ObjectId targetFolderId) {
        return null;
    }

    @Override
    public Document copy(ObjectId targetFolderId, Map<String, ?> properties, VersioningState versioningState, List<Policy> policies, List<Ace> addACEs, List<Ace> removeACEs, OperationContext context) {
        return null;
    }

    @Override
    public Boolean isImmutable() {
        return Boolean.FALSE;
    }

    @Override
    public Boolean isLatestVersion() {
        return Boolean.FALSE;
    }

    @Override
    public Boolean isMajorVersion() {
        return Boolean.FALSE;
    }

    @Override
    public Boolean isLatestMajorVersion() {
        return Boolean.FALSE;
    }

    @Override
    public Boolean isPrivateWorkingCopy() {
        return Boolean.FALSE;
    }

    @Override
    public String getVersionLabel() {
        return null;
    }

    @Override
    public String getVersionSeriesId() {
        return null;
    }

    @Override
    public Boolean isVersionSeriesCheckedOut() {
        return Boolean.FALSE;
    }

    @Override
    public String getVersionSeriesCheckedOutBy() {
        return null;
    }

    @Override
    public String getVersionSeriesCheckedOutId() {
        return null;
    }

    @Override
    public String getCheckinComment() {
        return null;
    }

    @Override
    public long getContentStreamLength() {
        return 0;
    }

    @Override
    public String getContentStreamId() {
        return null;
    }

    @Override
    public List<ContentStreamHash> getContentStreamHashes() {
        return Collections.emptyList();
    }

    @Override
    public String getLatestAccessibleStateId() {
        return null;
    }

    @Override
    public FileableCmisObject move(ObjectId sourceFolderId, ObjectId targetFolderId) {
        return null;
    }

    @Override
    public FileableCmisObject move(ObjectId sourceFolderId, ObjectId targetFolderId, OperationContext context) {
        return null;
    }

    @Override
    public List<Folder> getParents() {
        return Collections.emptyList();
    }

    @Override
    public List<Folder> getParents(OperationContext context) {
        return Collections.emptyList();
    }

    @Override
    public List<String> getPaths() {
        return Collections.emptyList();
    }

    @Override
    public void addToFolder(ObjectId folderId, boolean allVersions) {
        // not implemented
    }

    @Override
    public void removeFromFolder(ObjectId folderId) {
        // not implemented
    }

    @Override
    public AllowableActions getAllowableActions() {
        return null;
    }

    @Override
    public boolean hasAllowableAction(Action action) {
        return false;
    }

    @Override
    public List<Relationship> getRelationships() {
        return Collections.emptyList();
    }

    @Override
    public Acl getAcl() {
        return null;
    }

    @Override
    public Set<String> getPermissionsForPrincipal(String principalId) {
        return Collections.emptySet();
    }

    @Override
    public void delete() {
        // not implemented
    }

    @Override
    public void delete(boolean allVersions) {
        // not implemented
    }

    @Override
    public CmisObject updateProperties(Map<String, ?> properties) {
        return null;
    }

    @Override
    public ObjectId updateProperties(Map<String, ?> properties, boolean refresh) {
        return null;
    }

    @Override
    public CmisObject updateProperties(Map<String, ?> properties, List<String> addSecondaryTypeIds, List<String> removeSecondaryTypeIds) {
        return null;
    }

    @Override
    public ObjectId updateProperties(Map<String, ?> properties, List<String> addSecondaryTypeIds, List<String> removeSecondaryTypeIds, boolean refresh) {
        return null;
    }

    @Override
    public CmisObject rename(String newName) {
        return null;
    }

    @Override
    public ObjectId rename(String newName, boolean refresh) {
        return null;
    }

    @Override
    public List<Rendition> getRenditions() {
        return Collections.emptyList();
    }

    @Override
    public void applyPolicy(ObjectId... policyIds) {
        // not implemented
    }

    @Override
    public void applyPolicy(ObjectId policyId, boolean refresh) {
        // not implemented
    }

    @Override
    public void removePolicy(ObjectId... policyIds) {
        // not implemented
    }

    @Override
    public void removePolicy(ObjectId policyId, boolean refresh) {
        // not implemented
    }

    @Override
    public List<Policy> getPolicies() {
        return Collections.emptyList();
    }

    @Override
    public List<ObjectId> getPolicyIds() {
        return Collections.emptyList();
    }

    @Override
    public Acl applyAcl(List<Ace> addAces, List<Ace> removeAces, AclPropagation aclPropagation) {
        return null;
    }

    @Override
    public Acl addAcl(List<Ace> addAces, AclPropagation aclPropagation) {
        return null;
    }

    @Override
    public Acl removeAcl(List<Ace> removeAces, AclPropagation aclPropagation) {
        return null;
    }

    @Override
    public Acl setAcl(List<Ace> aces) {
        return null;
    }

    @Override
    public List<CmisExtensionElement> getExtensions(ExtensionLevel level) {
        return Collections.emptyList();
    }

    @Override
    public <T> T getAdapter(Class<T> adapterInterface) {
        return null;
    }

    @Override
    public long getRefreshTimestamp() {
        return 0;
    }

    @Override
    public void refresh() {
        // not implemented
    }

    @Override
    public void refreshIfOld(long durationInMillis) {
        // not implemented
    }

    @Override
    public List<Property<?>> getProperties() {
        return Collections.emptyList();
    }

    @Override
    public <T> Property<T> getProperty(String id) {
        return null;
    }

    @Override
    public <T> T getPropertyValue(String id) {
        return null;
    }

    @Override
    public String getDescription() {
        return null;
    }

    @Override
    public String getCreatedBy() {
        return null;
    }

    @Override
    public GregorianCalendar getCreationDate() {
        return null;
    }

    @Override
    public String getLastModifiedBy() {
        return null;
    }

    @Override
    public GregorianCalendar getLastModificationDate() {
        return null;
    }

    @Override
    public BaseTypeId getBaseTypeId() {
        return null;
    }

    @Override
    public ObjectType getBaseType() {
        return null;
    }

    @Override
    public ObjectType getType() {
        return null;
    }

    @Override
    public List<SecondaryType> getSecondaryTypes() {
        return Collections.emptyList();
    }

    @Override
    public List<ObjectType> findObjectType(String id) {
        return Collections.emptyList();
    }

    @Override
    public String getChangeToken() {
        return null;
    }
}