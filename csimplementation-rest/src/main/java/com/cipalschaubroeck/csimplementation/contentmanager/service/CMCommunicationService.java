package com.cipalschaubroeck.csimplementation.contentmanager.service;

import org.apache.chemistry.opencmis.client.api.Document;

import java.util.List;

public interface CMCommunicationService {

    Document getDocumentById(String idDocument);

    Document uploadFile(final String fileName, final byte[] data, List<String> pathParts, String mimeType);

    void removeDocument(String idDocument);
}
