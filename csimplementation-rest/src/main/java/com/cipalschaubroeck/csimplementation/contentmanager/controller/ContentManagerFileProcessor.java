package com.cipalschaubroeck.csimplementation.contentmanager.controller;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceProcessor;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Optional;

@Component
@Slf4j
public class ContentManagerFileProcessor implements ResourceProcessor<Resource<ContentManagerFile>> {

    /**
     * @param cmf content manager file to download
     * @param rel name of the link
     * @return link to download the document with that content manager id
     */
    public Optional<Link> buildDownloadLinkFor(ContentManagerFile cmf, String rel) {
        if (cmf != null && StringUtils.isNotBlank(cmf.getContentManagerId())) {
            try {
                return Optional.of(ControllerLinkBuilder.linkTo(
                        ControllerLinkBuilder.methodOn(ContentManagerController.class).download(cmf.getContentManagerId())
                ).withRel(rel));
            } catch (IOException e) {
                log.error("Should never happen because it is not an actual call", e);
                return Optional.empty();
            }
        } else {
            return Optional.empty();
        }
    }

    @Override
    public Resource<ContentManagerFile> process(Resource<ContentManagerFile> resource) {
        buildDownloadLinkFor(resource.getContent(), "download").ifPresent(resource::add);
        return resource;
    }
}
