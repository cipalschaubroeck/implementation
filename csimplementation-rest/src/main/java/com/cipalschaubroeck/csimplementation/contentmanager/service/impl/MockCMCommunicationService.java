package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Profile("!alfresco")
public class MockCMCommunicationService implements CMCommunicationService {

    private Map<String, Document> documents = new HashMap<>();

    @Override
    public Document getDocumentById(String idDocument) {
        return documents.get(idDocument);
    }

    @Override
    public Document uploadFile(String fileName, byte[] data, List<String> pathParts, String mimeType) {
        DocumentMock document = new DocumentMock(RandomStringUtils.randomAlphanumeric(20), fileName, fileName, data, mimeType);
        documents.put(document.getId(), document);
        return document;
    }

    @Override
    public void removeDocument(String idDocument) {
        documents.remove(idDocument);
    }
}
