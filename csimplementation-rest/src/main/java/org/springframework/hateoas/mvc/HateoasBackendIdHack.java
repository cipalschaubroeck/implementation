package org.springframework.hateoas.mvc;

import org.springframework.core.convert.support.ConfigurableConversionService;

import java.lang.reflect.Field;

/**
 * this is ugly as hell, but there is a problem when using {@link ControllerLinkBuilder} to
 * build a link to a method with a BackendId parameter. ControllerLinkBuilder uses AnnotatedParametersParameterAccesor
 * .BoundMethodParameter .CONVERSION_SERVICE which is private (the two classes are package protected).
 * CONVERSION_SERVICE is instantiated as a DefaultFormattingConversionService instead of using one
 * accessible through the application context, but it is not accessible from outside the class, so
 * since we have BackendId parameters in some controllers, we have to use reflection to get that property
 * and add converters to it
 */
public class HateoasBackendIdHack {
    private HateoasBackendIdHack() {
        // utility class
    }

    public static ConfigurableConversionService getControllerLinkBuilderConversionService() throws NoSuchFieldException, IllegalAccessException {
        Field field = AnnotatedParametersParameterAccessor.BoundMethodParameter.class.getDeclaredField("CONVERSION_SERVICE");
        field.setAccessible(true);
        ConfigurableConversionService conversionService = (ConfigurableConversionService) field.get(null);
        field.setAccessible(false);
        return conversionService;
    }
}
