package springfox.documentation.spring.data.rest;

import java.util.Arrays;
import java.util.List;


public class ExtractorConfiguration8 implements RequestHandlerExtractorConfiguration {
    private final List<EntityOperationsExtractor> defaultEntityExtractors = Arrays.asList(
            new EntitySaveExtractor8(),
            new EntityDeleteExtractor8(),
            new EntityFindOneExtractor8(),
            new EntityFindAllExtractor8(),
            new EntitySearchExtractor8(),
            new EntityAssociationsExtractor8()
    );


    private final List<EntityAssociationOperationsExtractor> defaultAssociationExtractors = Arrays.asList(
            new EntityAssociationSaveExtractor(),
            new EntityAssociationDeleteExtractor(),
            new EntityAssociationGetExtractor(),
            new EntityAssociationItemGetExtractor(),
            new EntityAssociationItemDeleteExtractor()
    );

    @Override
    public List<EntityOperationsExtractor> getEntityExtractors() {
        return defaultEntityExtractors;
    }

    @Override
    public List<EntityAssociationOperationsExtractor> getAssociationExtractors() {
        return defaultAssociationExtractors;
    }
}
