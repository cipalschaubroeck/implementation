/*
 *
 *  Copyright 2017-2018 the original author or authors.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *
 */
package springfox.documentation.spring.data.rest;

import lombok.RequiredArgsConstructor;
import lombok.experimental.Delegate;
import org.springframework.data.mapping.Association;
import org.springframework.data.mapping.PersistentEntity;
import org.springframework.data.mapping.PersistentProperty;
import org.springframework.data.rest.webmvc.mapping.Associations;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.RequestHandler;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class EntityAssociationsExtractor8 implements EntityOperationsExtractor {


    @Override
    public List<RequestHandler> extract(final EntityContext context) {
        final List<RequestHandler> handlers = new ArrayList<>();
        final PersistentEntity<?, ?> entity = context.entity();
        final Associations associations = context.getAssociations();

        entity.doWithAssociations((Association<? extends PersistentProperty<?>> association) -> {
            PersistentProperty<?> property = association.getInverse();
            if (!associations.isLinkableAssociation(property)) {
                return;
            }
            final EntityAssociationContext associationContext = new EntityAssociationContext(context, association);
            Stream<RequestHandler> currentHandlers = context.getAssociationExtractors().stream()
                    .flatMap(input -> input.extract(associationContext).stream());
            if (!property.isCollectionLike()) {
                currentHandlers = currentHandlers.map(this::forSingleAssociation);
            }
            currentHandlers.forEach(handlers::add);
        });
        return handlers;
    }

    /**
     * removes POST and PATCH method from the allowed methods for the handler. This should be done only for single associations,
     * because https://docs.spring.io/spring-data/rest/docs/current/reference/html/#_supported_http_methods_3 states that
     * PATCH is not valid for associations and POST is valid only for collection associations
     *
     * @param handler a handler for a to-one association, possibly accepting POST
     * @return handler without POST
     */
    private RequestHandler forSingleAssociation(RequestHandler handler) {
        return new SingleAssociationHandlerWrapper(handler);
    }

    @RequiredArgsConstructor
    static class SingleAssociationHandlerWrapper implements RequestHandler {

        private interface SupportedMethods {
            // used by lombok to know what it does not have to delegate
            @SuppressWarnings("unused")
            Set<RequestMethod> supportedMethods();
        }

        private static final Collection<RequestMethod> FORBIDDEN = EnumSet.of(RequestMethod.PATCH, RequestMethod.POST);

        @Delegate(excludes = SupportedMethods.class)
        private final RequestHandler delegate;

        @Override
        public Set<RequestMethod> supportedMethods() {
            Set<RequestMethod> fromSuper = new HashSet<>(delegate.supportedMethods());
            fromSuper.removeAll(FORBIDDEN);
            return Collections.unmodifiableSet(fromSuper);
        }
    }
}
