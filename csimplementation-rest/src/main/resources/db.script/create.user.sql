CREATE USER 'user_csimpl'@'localhost' IDENTIFIED BY 'gvb3lg13';

-- single tenant
GRANT ALL PRIVILEGES ON csimpl.* TO 'user_csimpl'@'localhost';
GRANT INDEX ON csimpl.* TO 'user_csimpl'@'localhost';

-- multitenant: one csimpl_<tenantId> per tenant
GRANT ALL PRIVILEGES ON csimpl_anonymous.* TO 'user_csimpl'@'localhost';
GRANT ALL PRIVILEGES ON csimpl_csimplementation.* TO 'user_csimpl'@'localhost';
GRANT ALL PRIVILEGES ON csimpl_csimplementation2.* TO 'user_csimpl'@'localhost';
GRANT INDEX ON csimpl_csimplementation.* TO 'user_csimpl'@'localhost';
