-- single tenant
CREATE SCHEMA csimpl DEFAULT CHARACTER SET utf8;

-- multitenant, one csimpl_<tenantId> per tenant
CREATE SCHEMA csimpl_anonymous DEFAULT CHARACTER SET utf8;
CREATE SCHEMA csimpl_csimplementation DEFAULT CHARACTER SET utf8;
CREATE SCHEMA csimpl_csimplementation2 DEFAULT CHARACTER SET utf8;
