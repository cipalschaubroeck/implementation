package com.cipalschaubroeck.csimplementation.organization.controller;

import com.cipalschaubroeck.csimplementation.organization.domain.OrganizationType;
import com.cipalschaubroeck.csimplementation.organization.service.OrganizationBusinessService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class OrganizationControllerTest {
    @InjectMocks
    private OrganizationController controller;
    @Mock
    private OrganizationBusinessService service;

    @Test
    public void getOrganizations() {
        List<OrganizationInfo> organizations = new ArrayList<>();
        Mockito.when(service.findOrganizations()).thenReturn(organizations);
        Assert.assertEquals(organizations, controller.getOrganizations(null).getBody());
    }

    @Test
    public void getSingleOrganizations() {
        List<OrganizationInfo> organizations = new ArrayList<>();
        Mockito.when(service.findOrganizations(OrganizationType.SINGLE_ORGANIZATION)).thenReturn(organizations);
        Assert.assertEquals(organizations, controller.getOrganizations(OrganizationType.SINGLE_ORGANIZATION).getBody());
    }

    @Test
    public void getJointVentures() {
        List<OrganizationInfo> organizations = new ArrayList<>();
        Mockito.when(service.findOrganizations(OrganizationType.JOINT_VENTURE)).thenReturn(organizations);
        Assert.assertEquals(organizations, controller.getOrganizations(OrganizationType.JOINT_VENTURE).getBody());
    }

    @Test
    public void postOrganization() {
        JointVentureInfo body = new JointVentureInfo();
        controller.postOrganization(body);
        Mockito.verify(service).saveOrganization(body);
    }

    @Test
    public void patchOrganization() {
        JointVentureInfo body = new JointVentureInfo();
        body.setType(OrganizationType.SINGLE_ORGANIZATION);
        String id = "3";
        controller.patchOrganization(id, body);
        Mockito.verify(service).updateOrganization(body, id);

        body.setType(OrganizationType.JOINT_VENTURE);
        controller.patchOrganization(id, body);
        Mockito.verify(service).updateJointVenture(body, Integer.parseInt(id));
    }

    @Test
    public void getDetailOrganization() {
        Integer id = 3;
        controller.getDetailOrganization(id.toString(), OrganizationType.SINGLE_ORGANIZATION);
        Mockito.verify(service).detailById(id.toString());

        controller.getDetailOrganization(id.toString(), OrganizationType.JOINT_VENTURE);
        Mockito.verify(service).detailById(id);
    }

    @Test
    public void getContacts() {
        String id = "4";
        controller.getContacts(id);
        Mockito.verify(service).contactsOf(id);
    }

    @Test
    public void deleteOrganization() {
        Integer id = 4;
        controller.deleteOrganization(id.toString(), OrganizationType.JOINT_VENTURE);
        Mockito.verify(service).deleteOrganization(id);

        controller.deleteOrganization(id.toString(), OrganizationType.SINGLE_ORGANIZATION);
        Mockito.verify(service).deleteOrganization(id.toString());
    }
}
