package com.cipalschaubroeck.csimplementation.organization.config;

import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPersonId;
import org.junit.Assert;
import org.junit.Test;

public class ContactPersonIdConverterTest {
    private ContactPersonIdConverter converter = new ContactPersonIdConverter();

    @Test
    public void supports() {
        Assert.assertTrue(converter.supports(ContactPerson.class));
    }

    @Test
    public void convert() {
        ContactPersonId id = new ContactPersonId();
        id.setOrganization(1);
        id.setPerson(2);
        ContactPersonId answer = (ContactPersonId) converter.fromRequestId(
                converter.toRequestId(id, ContactPerson.class), ContactPerson.class);
        Assert.assertEquals(answer, id);
    }
}
