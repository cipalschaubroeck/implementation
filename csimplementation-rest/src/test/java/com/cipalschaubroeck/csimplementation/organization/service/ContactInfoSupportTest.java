package com.cipalschaubroeck.csimplementation.organization.service;

import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.person.domain.PostalAddress;
import com.cipalschaubroeck.csimplementation.person.service.EmailAddressDuplicator;
import com.cipalschaubroeck.csimplementation.person.service.PhoneNumberDuplicator;
import com.cipalschaubroeck.csimplementation.person.service.PostalAddressDuplicator;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
public class ContactInfoSupportTest {
    @InjectMocks
    private ContactInfoSupport support;

    @Mock
    // needed to initialize
    @SuppressWarnings("unused")
    private EmailAddressDuplicator emailAddressDuplicator;
    @Mock
    // needed to initialize
    @SuppressWarnings("unused")
    private PostalAddressDuplicator postalAddressDuplicator;
    @Mock
    private PhoneNumberDuplicator phoneNumberDuplicator;

    @Test
    public void copyWhenNull() {
        Assert.assertNull(support.copyIfNeeded((PostalAddress) null, null));
    }

    @Test
    public void copyWhenNotInList() {
        EmailAddress emailAddress = new EmailAddress();
        Assert.assertEquals(emailAddress, support.copyIfNeeded(emailAddress, Collections.emptyList()));
    }

    @Test
    public void copyWhenInList() {
        PhoneNumber source = new PhoneNumber();
        PhoneNumber copy = new PhoneNumber();
        Mockito.when(phoneNumberDuplicator.duplicate(source)).thenReturn(copy);
        Assert.assertEquals(copy, support.copyIfNeeded(source, Collections.singletonList(source)));

    }
}
