package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.ExternalAddressee;
import com.cipalschaubroeck.csimplementation.organization.domain.ContactPerson;
import com.cipalschaubroeck.csimplementation.organization.domain.PersonFromContacts;
import com.cipalschaubroeck.csimplementation.organization.domain.SingleOrganization;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import org.junit.Assert;
import org.junit.Test;

public class DefaultExternalAddresseeDuplicatorTest {
    private DefaultExternalAddresseeSupport service = new DefaultExternalAddresseeSupport();

    @Test
    public void duplicateDispatched() {
        PersonInOrganization externalPO = new PersonInOrganization();
        externalPO.setId("externalPO");
        externalPO.setFunction("function");
        externalPO.setOrganization(new Organization());
        externalPO.getOrganization().setId("externalOrg");
        externalPO.setPerson(new com.greenvalley.contacts.domain.contact.Person());
        externalPO.getPerson().setId("externalPerson");
        SingleOrganization internalOrganization = new SingleOrganization(externalPO.getOrganization());
        PersonFromContacts internalPerson = new PersonFromContacts(externalPO.getPerson());

        ContactPerson contact = new ContactPerson(externalPO, internalOrganization, internalPerson);

        ExternalAddressee original = new ExternalAddressee();
        original.setContact(contact);
        original.setId(1);
        original.setVersionNum(2);

        EmailAddress email = new EmailAddress();
        email.setEmail("some@where.com");
        original.setEmail(email);
        internalPerson.getEmails().add(email);

        PhoneNumber phone = new PhoneNumber();
        phone.setPhone("555-1234");
        original.setPhone(phone);

        ExternalAddressee copy = service.duplicate(original);

        Assert.assertSame(contact, copy.getContact());
        Assert.assertSame(original.getEffectivePerson(), copy.getEffectivePerson());
        Assert.assertNull(copy.getId());
        Assert.assertNull(copy.getVersionNum());
        // null in original
        Assert.assertNull(copy.getAddress());
        // not found in person
        Assert.assertNull(copy.getPhone());
        // referenced from person
        Assert.assertEquals(original.getEmail().getEmail(), copy.getEmail().getEmail());
        Assert.assertTrue(copy.getEffectivePerson().getEmails().contains(copy.getEmail()));
    }
}
