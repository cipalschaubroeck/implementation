package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class InternalAddresseeRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;
    
    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crudInternal() {
        String caUri = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"single authority\",\"nationalId\": \"1234567891021\"}")
                .read("$._links.self.href");
        String personUri = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        String collaborator = tester.create(URI + API_PATH + "/collaborator",
                String.format("{\"function\":\"president\",\"authority\":\"%s\",\"person\":\"%s\"}", caUri, personUri))
                .read("$._links.self.href");

        tester.crudTest(URI + API_PATH + "/internal-addressee",
                String.format("{\"collaborator\":\"%s\",\"salutation\":\"dear someone\"}", collaborator),
                parsed -> {
                },
                String.format("{\"collaborator\":\"%s\",\"salutation\":\"dear another\"}", collaborator),
                parsed -> {
                },
                "{\"salutation\":\"dear somebody\"}",
                parsed -> {
                });
    }
}
