package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.AdHocAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.ExternalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.InternalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.cipalschaubroeck.csimplementation.template.service.impl.HeadingDuplicator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class DefaultPostalAddresseeDuplicatorTest {

    @InjectMocks
    private DefaultPostalAddresseeService service;
    @Mock
    private DefaultAdHocAddresseeSupport adHocService;
    @Mock
    private DefaultInternalAddresseeSupport internalService;
    @Mock
    private DefaultExternalAddresseeSupport externalService;
    @Mock
    // unused in the test, but used in DefaultPostalAddresseeService
    @SuppressWarnings("unused")
    private HeadingDuplicator headingDuplicator;

    @Before
    public void prepare() {
        List<AddresseeDuplicator> addresseeDuplicators = new ArrayList<>();
        addresseeDuplicators.add(adHocService);
        addresseeDuplicators.add(internalService);
        addresseeDuplicators.add(externalService);

        for (AddresseeDuplicator as : addresseeDuplicators) {
            Mockito.when(as.getSupportedType()).thenCallRealMethod();
        }

        service.setAddresseeServices(addresseeDuplicators);
    }

    private PostalAddressee create(Addressee addressee) {
        PostalAddressee pa = new PostalAddressee();
        PostalDocumentProcess process = Mockito.mock(PostalDocumentProcess.class);
        pa.setProcess(process);
        pa.setAddressee(addressee);
        pa.setGeneratedDocument(Mockito.mock(GeneratedDocument.class));

        Mockito.when(process.getId()).thenReturn(1);
        pa.getId().setProcess(process.getId());
        pa.getId().setAddressee(addressee.getId());
        return pa;
    }

    public void testDuplicateWith(Addressee addressee, Addressee copyOfAddressee) {
        PostalAddressee pa = create(addressee);
        PostalAddressee copy = service.duplicate(pa);
        Assert.assertNull(copy.getProcess());
        Assert.assertNull(copy.getId().getAddressee());
        Assert.assertNull(copy.getId().getProcess());
        Assert.assertNull(copy.getGeneratedDocument());
        Assert.assertSame(copyOfAddressee, copy.getAddressee());
    }

    @Test
    public void duplicate() {
        AdHocAddressee adHocAddressee1 = new AdHocAddressee();
        AdHocAddressee adHocAddressee2 = new AdHocAddressee();
        Mockito.when(adHocService.duplicate(adHocAddressee1)).thenReturn(adHocAddressee2);
        testDuplicateWith(adHocAddressee1, adHocAddressee2);
        Mockito.verify(adHocService).duplicate(adHocAddressee1);

        InternalAddressee internalAddressee1 = new InternalAddressee();
        InternalAddressee internalAddressee2 = new InternalAddressee();
        Mockito.when(internalService.duplicate(internalAddressee1)).thenReturn(internalAddressee2);
        testDuplicateWith(internalAddressee1, internalAddressee2);
        Mockito.verify(internalService).duplicate(internalAddressee1);

        ExternalAddressee externalAddressee1 = new ExternalAddressee();
        ExternalAddressee externalAddressee2 = new ExternalAddressee();
        Mockito.when(externalService.duplicate(externalAddressee1)).thenReturn(externalAddressee2);
        testDuplicateWith(externalAddressee1, externalAddressee2);
        Mockito.verify(externalService).duplicate(externalAddressee1);

        // if this fails, we're not checking all possible types of addressee and should add the corresponding cases to the ones above
        Assert.assertEquals(3, QualifiedPersonType.values().length);
    }
}
