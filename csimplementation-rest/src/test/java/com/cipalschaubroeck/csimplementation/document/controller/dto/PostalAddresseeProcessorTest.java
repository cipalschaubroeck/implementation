package com.cipalschaubroeck.csimplementation.document.controller.dto;

import com.cipalschaubroeck.csimplementation.document.controller.processors.PostalAddresseeProcessor;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-it.properties")
public class PostalAddresseeProcessorTest {
    @Autowired
    private PostalAddresseeProcessor processor;

    @Test
    public void process() {
        @SuppressWarnings("unchecked")
        Resource<PostalAddressee> resource = Mockito.mock(Resource.class);
        PostalAddressee addressee = new PostalAddressee();
        addressee.getId().setAddressee(1);
        addressee.getId().setProcess(2);
        addressee.setHeading(new Heading());
        addressee.getHeading().setId(3);
        Mockito.when(resource.getContent()).thenReturn(addressee);
        PostalDocumentProcess process = new PostalDocumentProcess();
        addressee.setProcess(process);
        process.setBody(Mockito.mock(TemplateData.class));
        Mockito.when(process.getBody().getId()).thenReturn(4);
        process.setSigningOfficerData(new SigningOfficerData());
        process.getSigningOfficerData().setId(5);
        processor.process(resource);

        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-addressee/1-2/generate-document")
                        && link.getRel().equals("generate")));
        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-addressee/1-2/preview/3{?format}")
                        && link.getRel().equals("previewHeading")));
        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-addressee/1-2/preview{?format}")
                        && link.getRel().equals("preview")));
    }
}
