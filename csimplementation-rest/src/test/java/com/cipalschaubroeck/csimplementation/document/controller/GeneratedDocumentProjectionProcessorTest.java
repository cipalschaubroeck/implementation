package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.contentmanager.controller.ContentManagerFileProcessor;
import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.document.controller.processors.GeneratedDocumentDtoProcessor;
import com.cipalschaubroeck.csimplementation.document.controller.projections.GeneratedDocumentProjection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.hateoas.Resource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class GeneratedDocumentProjectionProcessorTest {
    @InjectMocks
    private GeneratedDocumentDtoProcessor processor;
    @Mock
    private ContentManagerFileProcessor cmProcessor;

    @Test
    public void process() {
        @SuppressWarnings("unchecked")
        Resource<GeneratedDocumentProjection> resource = Mockito.mock(Resource.class);
        GeneratedDocumentProjection generated = Mockito.mock(GeneratedDocumentProjection.class);
        Mockito.when(resource.getContent()).thenReturn(generated);
        Mockito.when(generated.getDispatched()).thenReturn(new ContentManagerFile());
        Mockito.when(generated.getGenerated()).thenReturn(new ContentManagerFile());
        processor.process(resource);
        Mockito.verify(cmProcessor).buildDownloadLinkFor(generated.getDispatched(), "downloadDispatched");
        Mockito.verify(cmProcessor).buildDownloadLinkFor(generated.getGenerated(), "downloadGenerated");
    }
}
