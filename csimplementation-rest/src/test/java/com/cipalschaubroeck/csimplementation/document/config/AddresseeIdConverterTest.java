package com.cipalschaubroeck.csimplementation.document.config;

import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.EmailAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.GeneratedDocument;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import org.junit.Assert;
import org.junit.Test;

public class AddresseeIdConverterTest {

    private AddresseeIdConverter converter = new AddresseeIdConverter();

    @Test
    public void supports() {
        Assert.assertTrue(converter.supports(EmailAddressee.class));
        Assert.assertTrue(converter.supports(PostalAddressee.class));
        Assert.assertTrue(converter.supports(GeneratedDocument.class));
    }

    @Test
    public void convert() {
        AddresseeId id = new AddresseeId();
        id.setProcess(1);
        id.setAddressee(2);
        AddresseeId answer = (AddresseeId) converter.fromRequestId(
                converter.toRequestId(id, EmailAddressee.class), EmailAddressee.class);
        Assert.assertEquals(answer, id);
    }
}
