package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.document.controller.specifications.DocumentProcessSpecification;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.DocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessService;
import com.cipalschaubroeck.csimplementation.shared.controller.ControllerSupport;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class DocumentProcessControllerTest {
    @InjectMocks
    private DocumentProcessController controller;
    @Mock
    private DocumentProcessRepository repository;
    @Mock
    private DocumentProcessService service;
    @Mock
    // used by findProcesses
    @SuppressWarnings("unused")
    private PagedResourcesAssembler<Object> pagedResourcesAssembler;

    //used by controller
    @SuppressWarnings("unused")
    @Mock
    private ControllerSupport support;

    @Test
    public void findProcesses() {
        DocumentProcessSpecification specification = new DocumentProcessSpecification();
        Integer idContract = 1;
        specification.setContractId(idContract);
        @NotNull Pageable pageable = PageRequest.of(1, 20);
        List<String> sorts = new ArrayList<>();
        sorts.add("type");
        sorts.add("asc");
        PersistentEntityResourceAssembler assembler = Mockito.mock(PersistentEntityResourceAssembler.class);
        Page<DocumentProcess> results = Page.empty();

        Mockito.when(repository.findAll(specification, ControllerSupport.getPageConfig(pageable, sorts, null)))
                .thenReturn(results);

        Mockito.when(service.findDocumentProcessWithStatus(specification, ControllerSupport.getPageConfig(pageable, sorts, null)))
                .thenReturn(results);
        controller.search(specification, pageable, sorts, 1, assembler);
        Mockito.verify(service)
                .findDocumentProcessWithStatus(specification, ControllerSupport.getPageConfig(pageable, sorts, null));
    }
}
