package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class PostalDocumentProcessControllerIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    private String attachmentsUri;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
        attachmentsUri = tester.create(URI + API_PATH + "/postal-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()))
                .read("$._links.attachments.href");
    }

    @Test
    public void crudAttachments() {
        // upload one
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("files", getTestFile());
        HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

        RestTemplate rest = new RestTemplate();
        ResponseEntity<String> response = rest.postForEntity(attachmentsUri, requestEntity, String.class);
        DocumentContext file1Data = JsonPath.parse(response.getBody());
        String file1 = file1Data.read("$._embedded.contentManagerFiles[0]._links.self.href");

        // link another
        HttpHeaders uriListHeaders = new HttpHeaders();
        uriListHeaders.add("Content-Type", "text/uri-list");
        DocumentContext filesData = JsonPath.parse(rest.exchange(attachmentsUri, HttpMethod.POST,
                new HttpEntity<>(file1, uriListHeaders), String.class).getBody());
        Assert.assertNotEquals(file1, filesData.read("$._embedded.contentManagerFiles[0]._links.self.href"));
        Assert.assertEquals((String) file1Data.read("$._embedded.contentManagerFiles[0].publicPath"),
                filesData.read("$._embedded.contentManagerFiles[0].publicPath"));
        Assert.assertEquals((String) file1Data.read("$._embedded.contentManagerFiles[0].contentManagerId"),
                filesData.read("$._embedded.contentManagerFiles[0].contentManagerId"));

        // check there's two
        Assert.assertEquals(2, ((JSONArray) tester.read(attachmentsUri).read("$._embedded.contentManagerFiles")).size());

        // replace
        filesData = JsonPath.parse(rest.exchange(attachmentsUri, HttpMethod.PUT,
                new HttpEntity<>(file1, uriListHeaders), String.class).getBody());
        Assert.assertEquals(1, ((JSONArray) filesData.read("$._embedded.contentManagerFiles")).size());
        Assert.assertEquals(file1, filesData.read("$._embedded.contentManagerFiles[0]._links.self.href"));

        // delete
        rest.delete(attachmentsUri);

        // check there is none
        filesData = tester.read(attachmentsUri);
        Assert.assertEquals(0, ((JSONArray) filesData.read("$._embedded.contentManagerFiles")).size());
    }

    private FileSystemResource getTestFile() {
        try {
            FileSystemResource fsr = Mockito.mock(FileSystemResource.class);
            Mockito.when(fsr.getFilename()).thenReturn("some file.txt");
            Mockito.when(fsr.getInputStream()).thenReturn(new ByteArrayInputStream("content of file".getBytes()));
            return fsr;
        } catch (IOException e) {
            // shouldn't get here, just mocking
            return null;
        }
    }
}
