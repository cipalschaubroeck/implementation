package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.domain.Addressee;
import com.cipalschaubroeck.csimplementation.document.domain.AddresseeId;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalAddresseeRepository;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.PostalAddresseeService;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import com.cipalschaubroeck.csimplementation.shared.service.EntityInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class DefaultPostalDocumentProcessServiceTest {
    @InjectMocks
    private DefaultPostalDocumentProcessService service;
    @Mock
    private PostalDocumentProcessRepository repository;
    @Mock
    private PostalAddresseeRepository postalAddresseeRepository;
    @Mock
    private PostalAddresseeService postalAddresseeService;
    @Mock
    private EntityEventPublisher publisher;
    @Mock
    private PostalAttachmentComponent attachmentComponent;

    @Test
    public void setAddressees() {
        PostalDocumentProcess process = new PostalDocumentProcess();
        PostalAddressee addressee1 = Mockito.mock(PostalAddressee.class);
        process.getAddresseeList().add(addressee1);
        process.setId(1);
        Mockito.when(repository.findById(process.getId())).thenReturn(Optional.of(process));

        EntityInfo infoAddressee = new EntityInfo();
        infoAddressee.setId(new AddresseeId());
        infoAddressee.setDomainType(PostalAddressee.class);
        PostalAddressee addressee2 = Mockito.mock(PostalAddressee.class);
        Mockito.when(addressee2.getId()).thenReturn((AddresseeId) infoAddressee.getId());
        Mockito.when(postalAddresseeRepository.findById(addressee2.getId())).thenReturn(Optional.of(addressee2));
        Mockito.when(postalAddresseeRepository.save(addressee2)).thenReturn(addressee2);

        EntityInfo infoNew = new EntityInfo();
        infoNew.setId(3);
        infoNew.setDomainType(Collaborator.class);
        Addressee baseAddressee = Mockito.mock(Addressee.class);
        Mockito.when(postalAddresseeService.build(infoNew)).thenReturn(baseAddressee);

        PostalAddressee addressee4 = Mockito.mock(PostalAddressee.class);
        Mockito.when(postalAddresseeRepository.save(ArgumentMatchers.argThat(argument ->
                baseAddressee.equals(argument.getAddressee()) && process.equals(argument.getProcess()))))
                .thenReturn(addressee4);

        service.setAddressees(process.getId(), Arrays.asList(infoAddressee, infoNew));
        Mockito.verify(postalAddresseeRepository).delete(addressee1);
        Mockito.verify(addressee2).setProcess(process);
        Mockito.verify(publisher).create(ArgumentMatchers.argThat(argument ->
                        baseAddressee.equals(argument.getAddressee()) && process.equals(argument.getProcess())),
                ArgumentMatchers.eq(postalAddresseeRepository));
    }

    @Test
    public void addAddressees() {
        PostalDocumentProcess process = new PostalDocumentProcess();
        PostalAddressee addressee1 = Mockito.mock(PostalAddressee.class);
        process.getAddresseeList().add(addressee1);
        process.setId(1);
        Mockito.when(repository.findById(process.getId())).thenReturn(Optional.of(process));

        EntityInfo infoAddressee = new EntityInfo();
        infoAddressee.setId(new AddresseeId());
        infoAddressee.setDomainType(PostalAddressee.class);
        PostalAddressee addressee2 = Mockito.mock(PostalAddressee.class);
        Mockito.when(addressee2.getId()).thenReturn((AddresseeId) infoAddressee.getId());
        Mockito.when(postalAddresseeRepository.findById(addressee2.getId())).thenReturn(Optional.of(addressee2));
        Mockito.when(postalAddresseeRepository.save(addressee2)).thenReturn(addressee2);

        EntityInfo infoNew = new EntityInfo();
        infoNew.setId(3);
        infoNew.setDomainType(Collaborator.class);
        Addressee baseAddressee = Mockito.mock(Addressee.class);
        Mockito.when(postalAddresseeService.build(infoNew)).thenReturn(baseAddressee);

        PostalAddressee addressee4 = Mockito.mock(PostalAddressee.class);
        Mockito.when(postalAddresseeRepository.save(ArgumentMatchers.argThat(argument ->
                baseAddressee.equals(argument.getAddressee()) && process.equals(argument.getProcess()))))
                .thenReturn(addressee4);

        service.addAddressees(process.getId(), Arrays.asList(infoAddressee, infoNew));
        Mockito.verify(postalAddresseeRepository, Mockito.never()).delete(addressee1);
        Mockito.verify(addressee2).setProcess(process);
        Mockito.verify(publisher).create(ArgumentMatchers.argThat(argument ->
                        baseAddressee.equals(argument.getAddressee()) && process.equals(argument.getProcess())),
                ArgumentMatchers.eq(postalAddresseeRepository));
    }

    @Test
    public void addAttachment() {
        PostalDocumentProcess process = new PostalDocumentProcess();
        process.setId(1);
        process.setName("processName");
        Mockito.when(repository.findById(process.getId())).thenReturn(Optional.of(process));

        String fileName = "fileName";
        byte[] data = "content".getBytes();
        String mimeType = "text/plain";

        service.addAttachment(process.getId(), fileName, data, mimeType);
        Mockito.verify(attachmentComponent).addAttachment(process.getId(), fileName, data, mimeType);
    }
}
