package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.domain.InternalAddressee;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import org.junit.Assert;
import org.junit.Test;

public class DefaultInternalAddresseeSupportTest {
    private DefaultInternalAddresseeSupport service = new DefaultInternalAddresseeSupport();

    @Test
    public void duplicateDispatched() {
        InternalAddressee original = new InternalAddressee();
        Collaborator collaborator = new Collaborator();
        Person person = new Person();
        collaborator.setPerson(person);
        original.setCollaborator(collaborator);
        original.setId(1);
        original.setVersionNum(2);

        EmailAddress email = new EmailAddress();
        email.setEmail("some@where.com");
        original.setEmail(email);
        person.getEmails().add(email);

        PhoneNumber phone = new PhoneNumber();
        phone.setPhone("555-1234");
        original.setPhone(phone);

        InternalAddressee copy = service.duplicate(original);

        Assert.assertSame(collaborator, copy.getCollaborator());
        Assert.assertSame(original.getEffectivePerson(), copy.getEffectivePerson());
        Assert.assertNull(copy.getId());
        Assert.assertNull(copy.getVersionNum());
        // null in original
        Assert.assertNull(copy.getAddress());
        // not found in person
        Assert.assertNull(copy.getPhone());
        // referenced from person
        Assert.assertEquals(original.getEmail().getEmail(), copy.getEmail().getEmail());
        Assert.assertTrue(copy.getEffectivePerson().getEmails().contains(copy.getEmail()));
    }
}
