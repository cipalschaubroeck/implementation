package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class UploadDocumentProcessRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crud() {
        tester.crudTest(URI + API_PATH + "/upload-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("name of the process", parsed.read("$.name")),
                String.format("{\"name\":\"new name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("new name of the process", parsed.read("$.name")),
                "{\"name\":\"name of the process\"}",
                parsed -> assertEquals("name of the process", parsed.read("$.name")));
    }
}
