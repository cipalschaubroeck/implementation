package com.cipalschaubroeck.csimplementation.document.listener;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import javax.validation.ValidationException;


public class DocumentProcessListenerTest {
    private DocumentProcessListener listener = new DocumentProcessListener();

    @Test
    public void deleteDocumentProcess() {
        DocumentProcess d = Mockito.mock(DocumentProcess.class);
        for (DocumentStatus status : DocumentStatus.values()) {
            Mockito.when(d.getStatus()).thenReturn(status);
            try {
                listener.failOnForbiddenDelete(d);
                if (status != DocumentStatus.IN_PROGRESS) {
                    Assert.fail();
                }
            } catch (ValidationException ex) {
                if (status == DocumentStatus.IN_PROGRESS) {
                    Assert.fail();
                }
            }
        }
    }
}
