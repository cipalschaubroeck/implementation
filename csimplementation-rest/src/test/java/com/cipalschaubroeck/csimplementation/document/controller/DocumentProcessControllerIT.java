package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class DocumentProcessControllerIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Test
    public void duplicate() {

        String originalUri = tester.create(URI + API_PATH + "/postal-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()))
                .read("$._links.self.href");

        RestTemplate rest = new RestTemplate();
        rest.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

        HttpHeaders textPlainHeaders = new HttpHeaders();
        textPlainHeaders.add("Content-Type", "text/plain");

        ResponseEntity<String> response = rest.exchange(originalUri,
                HttpMethod.POST, new HttpEntity<>("new name", textPlainHeaders), String.class);
        Assert.assertEquals(HttpStatus.CREATED, response.getStatusCode());
        DocumentContext parsed = JsonPath.parse(response.getBody());
        Assert.assertEquals("new name", parsed.read("$.name"));
        Assert.assertNotEquals(originalUri, parsed.read("$._links.self.href"));
    }

    @Test
    public void findAll() {
        String url = URI + API_PATH + "/contract/1/document-process?projection=documentProcessInfo";


        tester.create(URI + API_PATH + "/postal-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()))
                .read("$._links.self.href");

        DocumentContext read = tester.read(url);

        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.documentProcesses")).size());
    }
}
