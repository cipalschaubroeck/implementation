package com.cipalschaubroeck.csimplementation.document.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class DocumentProcessRepositoryIT {
    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crudEmail() {
        tester.crudTest(URI + API_PATH + "/document-process",
                String.format("{\"name\":\"name of the process\",\"type\":\"EMAIL\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("name of the process", parsed.read("$.name")),
                "{\"name\":\"new name of the process\"}",
                parsed -> assertEquals("new name of the process", parsed.read("$.name")),
                "{\"name\":\"name of the process\"}",
                parsed -> assertEquals("name of the process", parsed.read("$.name")));
    }

    @Test
    public void crudDocument() {
        tester.crudTest(URI + API_PATH + "/document-process",
                String.format("{\"name\":\"name of the process\",\"type\":\"DOCUMENT\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("name of the process", parsed.read("$.name")),
                String.format("{\"name\":\"new name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("new name of the process", parsed.read("$.name")),
                "{\"name\":\"name of the process\"}",
                parsed -> assertEquals("name of the process", parsed.read("$.name")));
    }

    @Test
    public void crudUpload() {
        tester.crudTest(URI + API_PATH + "/document-process",
                String.format("{\"name\":\"name of the process\",\"type\":\"UPLOAD\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("name of the process", parsed.read("$.name")),
                String.format("{\"name\":\"new name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()),
                parsed -> assertEquals("new name of the process", parsed.read("$.name")),
                "{\"name\":\"name of the process\"}",
                parsed -> assertEquals("name of the process", parsed.read("$.name")));
    }
}
