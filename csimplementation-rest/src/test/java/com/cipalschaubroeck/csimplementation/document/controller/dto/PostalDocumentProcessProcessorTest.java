package com.cipalschaubroeck.csimplementation.document.controller.dto;

import com.cipalschaubroeck.csimplementation.document.controller.processors.PostalDocumentProcessProcessor;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;

public class PostalDocumentProcessProcessorTest {
    private PostalDocumentProcessProcessor processor = new PostalDocumentProcessProcessor();

    @Test
    public void process() {
        PostalDocumentProcess process = new PostalDocumentProcess();
        process.setId(1);
        process.setSigningOfficerData(new SigningOfficerData());
        process.getSigningOfficerData().setId(2);
        process.setBody(Mockito.mock(TemplateData.class));
        Mockito.when(process.getBody().getId()).thenReturn(3);

        @SuppressWarnings("unchecked")
        Resource<PostalDocumentProcess> resource = Mockito.mock(Resource.class);
        Mockito.when(resource.getContent()).thenReturn(process);

        processor.process(resource);

        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-document-process/1/generate-documents")
                        && link.getRel().equals("generateAll")));
        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-document-process/1/preview/3{?format}")
                        && link.getRel().equals("previewBody")));
        Mockito.verify(resource).add(ArgumentMatchers.argThat((Link link) ->
                link.getHref().endsWith("postal-document-process/1/preview/2{?format}")
                        && link.getRel().equals("previewSigningOfficers")));
    }
}
