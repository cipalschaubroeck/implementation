package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.AdHocAddressee;
import com.cipalschaubroeck.csimplementation.person.domain.EmailAddress;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import org.junit.Assert;
import org.junit.Test;

public class DefaultAdHocAddresseeDuplicatorTest {
    private DefaultAdHocAddresseeSupport service = new DefaultAdHocAddresseeSupport();

    @Test
    public void duplicateDispatched() {
        AdHocAddressee original = new AdHocAddressee();
        AdHocQualifiedPerson qualifiedPerson = new AdHocQualifiedPerson();
        Person person = new Person();
        qualifiedPerson.setPerson(person);
        original.setPerson(qualifiedPerson);
        original.setId(1);
        original.setVersionNum(2);

        EmailAddress email = new EmailAddress();
        email.setEmail("some@where.com");
        original.setEmail(email);
        person.getEmails().add(email);

        PhoneNumber phone = new PhoneNumber();
        phone.setPhone("555-1234");
        original.setPhone(phone);

        AdHocAddressee copy = service.duplicate(original);

        Assert.assertSame(qualifiedPerson, copy.getPerson());
        Assert.assertSame(original.getEffectivePerson(), copy.getEffectivePerson());
        Assert.assertNull(copy.getId());
        Assert.assertNull(copy.getVersionNum());
        // null in original
        Assert.assertNull(copy.getAddress());
        // not found in person
        Assert.assertNull(copy.getPhone());
        // referenced from person
        Assert.assertEquals(original.getEmail().getEmail(), copy.getEmail().getEmail());
        Assert.assertTrue(copy.getEffectivePerson().getEmails().contains(copy.getEmail()));
    }
}
