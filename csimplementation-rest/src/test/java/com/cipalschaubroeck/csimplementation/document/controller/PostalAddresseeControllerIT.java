package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class PostalAddresseeControllerIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;
    
    @Test
    public void test() {
        String caUri = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"single authority\",\"nationalId\": \"1234567891014\"}")
                .read("$._links.self.href");
        String personUri = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        String phoneUri = tester.create(URI + API_PATH + "/phone", "{\"phone\":\"555-1234\"}")
                .read("$._links.self.href");
        tester.addAssociation(personUri + "/phoneNumbers", phoneUri);
        String collaborator = tester.create(URI + API_PATH + "/collaborator",
                String.format("{\"function\":\"president\",\"authority\":\"%s\",\"person\":\"%s\"}", caUri, personUri))
                .read("$._links.self.href");
        String addressee = tester.create(URI + API_PATH + "/internal-addressee",
                String.format("{\"collaborator\":\"%s\",\"salutation\":\"dear someone\"}", collaborator))
                .read("$._links.self.href");
        String process = tester.create(URI + API_PATH + "/postal-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()))
                .read("$._links.self.href");
        String postalAddressee = tester.create(URI + API_PATH + "/postal-addressee",
                String.format("{\"addressee\":\"%s\", \"process\":\"%s\", \"ourProperty\":\"our property\", \"yourProperty\":\"your property\"}", addressee, process))
                .read("$._links.self.href");

        DocumentContext result = tester.updatePut(postalAddressee + "?projection=postalAddresseeTableItem",
                String.format("{ \"addressee\":\"%s\", \"phone\":\"%s\"}",
                        collaborator, phoneUri));
        Assert.assertEquals("555-1234", result.read("$.phone"));
        Assert.assertEquals(postalAddressee, result.read("$._links.self.href"));
    }
}
