package com.cipalschaubroeck.csimplementation.document.service.impl;

import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.domain.EmailDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.domain.UploadDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.DocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import com.cipalschaubroeck.csimplementation.shared.service.EntityEventPublisher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
public class DefaultDocumentProcessServiceTest {
    @Mock
    private PostalDPDuplicator postalDuplicator;
    @Mock
    private EmailDPDuplicator emailDuplicator;
    @Mock
    private UploadDPDuplicator uploadDuplicator;
    @Mock
    private DocumentProcessRepository repository;
    @Mock
    private EntityEventPublisher publisher;

    @InjectMocks
    private DefaultDocumentProcessService service;

    @Before
    public void prepare() {
        List<DocumentProcessDuplicator> duplicators = Arrays.asList(postalDuplicator, emailDuplicator, uploadDuplicator);
        duplicators.forEach(d -> Mockito.when(d.getSupportedType()).thenCallRealMethod());
        service.setDuplicators(duplicators);
    }

    private void prepare(Class<? extends DocumentProcess> dpClass,
                         DocumentProcessDuplicator duplicator,
                         int id, List<DocumentProcess> entities, List<DocumentProcess> copies) {
        try {
            DocumentProcess entity = dpClass.newInstance();
            entity.setId(id);
            entity.setName("original name");
            entities.add(entity);
            Mockito.when(repository.findById(entity.getId())).thenReturn(Optional.of(entity));
            DocumentProcess copy = dpClass.newInstance();
            Mockito.when(duplicator.duplicate(entity)).thenReturn(copy);
            Mockito.when(repository.save(copy)).thenReturn(copy);
            copies.add(copy);
        } catch (IllegalAccessException | InstantiationException e) {
            // should never happen since they're jpa classes, so jpa forces them to have a no argument constructor
        }
    }

    @Test
    public void duplicate() {
        List<DocumentProcess> entities = new ArrayList<>();
        List<DocumentProcess> copies = new ArrayList<>();
        Map<Class<? extends DocumentProcess>, DocumentProcessDuplicator> config = new HashMap<>();
        config.put(EmailDocumentProcess.class, emailDuplicator);
        config.put(UploadDocumentProcess.class, uploadDuplicator);
        config.put(PostalDocumentProcess.class, postalDuplicator);

        List<Class<? extends DocumentProcess>> classes = new ArrayList<>(config.keySet());
        for (int i = 0; i < classes.size(); i++) {
            prepare(classes.get(i), config.get(classes.get(i)), i + 1, entities, copies);
            service.duplicate(i + 1, "new name");
            Mockito.verify(config.get(classes.get(i))).duplicate(entities.get(i));
            Assert.assertEquals("new name", copies.get(i).getName());
            Mockito.verify(publisher).create(copies.get(i), repository);
            Mockito.reset(publisher);
        }

        // this is to check we're testing all document types
        Assert.assertEquals(EnumSet.allOf(DocumentProcessType.class),
                entities.stream().map(DocumentProcess::getType)
                        .collect(Collectors.toSet()));
    }
}
