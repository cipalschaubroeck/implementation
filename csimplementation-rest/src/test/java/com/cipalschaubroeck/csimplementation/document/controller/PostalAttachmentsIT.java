package com.cipalschaubroeck.csimplementation.document.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class PostalAttachmentsIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;
    
    @Test
    public void removeOneFromList() {
        String process = tester.create(URI + API_PATH + "/postal-document-process",
                String.format("{\"name\":\"name of the process\", \"contract\":\"%s/api/contract/1\"}", tester.getBaseUri()))
                .read("$._links.self.href");
        String file1 = tester.create(URI + API_PATH + "/content-manager-file",
                "{\"contentManagerId\": \"id for content manager\", \"publicPath\" : \"path as seen by the user\"}")
                .read("$._links.self.href");
        String file2 = tester.create(URI + API_PATH + "/content-manager-file",
                "{\"contentManagerId\": \"id for content manager\", \"publicPath\" : \"path as seen by the user\"}")
                .read("$._links.self.href");
        String attachments = process + "/attachments";
        tester.addAssociation(attachments, file1, file2);

        DocumentContext attachmentsData = tester.read(attachments);
        Assert.assertEquals(2, ((JSONArray) attachmentsData.read("$._embedded.contentManagerFiles")).size());

        tester.delete(attachmentsData.read("$._embedded.contentManagerFiles[0]._links.itemInAssociation.href"));
        Assert.assertEquals(1, ((JSONArray) tester.read(attachments).read("$._embedded.contentManagerFiles")).size());
    }
}
