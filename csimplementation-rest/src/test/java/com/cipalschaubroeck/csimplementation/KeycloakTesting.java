package com.cipalschaubroeck.csimplementation;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

public class KeycloakTesting {

    @Getter
    @Setter
    private static class AuthData {
        private String accessToken;
        private String refreshToken;
        private long expiresIn;
        private long refreshExpiresIn;
    }

    public static void main(String[] args) {
        String auth = "http://localhost:8180/auth/realms/master/protocol/openid-connect/token";

        RestTemplate rest = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("client_id", "csimplementation");
        map.add("username", "user");
        map.add("password", "password");
        map.add("grant_type", "password");
        map.add("client_secret", "cad658e6-4a45-4721-9a9b-418aa84be0fc");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<String> response = rest.postForEntity(auth, request, String.class);
        System.out.println(response.getBody());
        DocumentContext parsed = JsonPath.parse(response.getBody());
        AuthData data = new AuthData();
        data.setAccessToken(parsed.read("$.access_token"));
        data.setExpiresIn(System.currentTimeMillis() + Long.parseLong(parsed.read("$.expires_in")));
        data.setRefreshToken(parsed.read("$.refresh_token"));
        data.setRefreshExpiresIn(System.currentTimeMillis() + Long.parseLong(parsed.read("$.refresh_expires_in")));
    }

}
