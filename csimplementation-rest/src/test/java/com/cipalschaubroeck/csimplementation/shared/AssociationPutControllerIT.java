package com.cipalschaubroeck.csimplementation.shared;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class AssociationPutControllerIT {
    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void createPropertyReference() {
        String phone = tester.create(URI + API_PATH + "/phone", "{\"phone\": \"555-1234\"}")
                .read("$._links.self.href");
        String person = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        DocumentContext result = tester.replaceAssociation(person + "/phoneNumbers", phone);
        Assert.assertEquals(1, ((JSONArray) result.read("$._embedded.phoneNumbers")).size());
        Assert.assertNotNull(result.read("$._embedded.phoneNumbers[0]._links.itemInAssociation.href"));

        result = tester.addAssociation(person + "/phoneNumbers", phone);
        Assert.assertEquals(1, ((JSONArray) result.read("$._embedded.phoneNumbers")).size());

        result = tester.read(person + "/phoneNumbers");
        Assert.assertEquals(2, ((JSONArray) result.read("$._embedded.phoneNumbers")).size());
    }
}
