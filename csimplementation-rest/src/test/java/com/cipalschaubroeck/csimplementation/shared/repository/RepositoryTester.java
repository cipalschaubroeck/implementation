package com.cipalschaubroeck.csimplementation.shared.repository;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import lombok.Getter;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Map;
import java.util.function.Consumer;

import static org.junit.Assert.assertEquals;

/**
 * class to ease testing of spring data rest repositories
 */
public class RepositoryTester {
    @Getter
    private final String baseUri;
    private final RestTemplate rest;
    private final HttpHeaders jsonHeaders;
    private final HttpHeaders uriListHeaders;

    public RepositoryTester(int port) {
        this.baseUri = "http://localhost:" + port;

        rest = new RestTemplate();
        rest.setRequestFactory(new HttpComponentsClientHttpRequestFactory());

        jsonHeaders = new HttpHeaders();
        jsonHeaders.setContentType(MediaType.APPLICATION_JSON);

        uriListHeaders = new HttpHeaders();
        uriListHeaders.add("Content-Type", "text/uri-list");
    }

    private String baseUri(String relativeUri) {
        String withoutOptions = relativeUri.replaceAll("\\{[^}]+}", "");
        if (relativeUri.contains("localhost")) {
            return withoutOptions;
        } else {
            return baseUri + withoutOptions;
        }
    }

    /**
     * Tests creation, read, update and delete of one element
     *
     * @param uri          uri of the elements
     * @param jsonForPost  json data to be POSTed to create the element
     * @param checkPost    checks on the data returned by the POST
     * @param jsonForPut   json data to be PUT to update the element
     * @param checkPut     checks on the data returned by the PUT
     * @param jsonForPatch json data to PATCH the element
     * @param checkPatch   checks on the data returned by the PATCH
     */
    public void crudTest(String uri, String jsonForPost, Consumer<DocumentContext> checkPost,
                         String jsonForPut, Consumer<DocumentContext> checkPut,
                         String jsonForPatch, Consumer<DocumentContext> checkPatch) {
        DocumentContext parsed = read(uri);
        int before = parsed.read("$.page.totalElements");

        parsed = create(uri, jsonForPost);
        checkPost.accept(parsed);
        String elementUri = parsed.read("$._links.self.href");
        Assert.assertEquals(before + 1, (int) read(uri).read("$.page.totalElements"));

        parsed = updatePut(elementUri, jsonForPut);
        checkPut.accept(parsed);

        parsed = updatePatch(elementUri, jsonForPatch);
        checkPatch.accept(parsed);

        delete(elementUri);
        Assert.assertEquals(before, (int) read(uri).read("$.page.totalElements"));
    }

    public void toManyTest(String associationUri, String element1Uri, String element2Uri,
                           Consumer<DocumentContext> afterAddBoth,
                           Consumer<DocumentContext> afterReplaceWithFirst) {
        int before = countFromResults(read(associationUri));

        addAssociation(associationUri, element1Uri, element2Uri);
        DocumentContext parsed = read(associationUri);
        assertEquals(2 + before, countFromResults(parsed));
        afterAddBoth.accept(parsed);

        replaceAssociation(associationUri, element1Uri);
        parsed = read(associationUri);
        assertEquals(1, countFromResults(parsed));
        afterReplaceWithFirst.accept(parsed);

        clearToManyAssociation(associationUri);
        parsed = read(associationUri);
        assertEquals(0, countFromResults(parsed));
    }

    /**
     * @param parsed a JsonPath parsed result of reading a to-Many association
     * @return total number of elements in the to-Many association
     */
    public int countFromResults(DocumentContext parsed) {
        Map<String, JSONArray> object = parsed.read("$._embedded");
        return object.values().stream().map(JSONArray::size).mapToInt(Integer::intValue).sum();
    }

    public DocumentContext read(String uri) {
        ResponseEntity<String> response = rest.getForEntity(baseUri(uri), String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        return JsonPath.parse(response.getBody());
    }

    /**
     * @param uri      uri relative to the base uri of the application
     * @param jsonData json with the object to post
     * @return jsonPath-parsed result for further checks
     */
    public DocumentContext create(String uri, String jsonData) {
        ResponseEntity<String> response = rest.exchange(baseUri(uri), HttpMethod.POST,
                new HttpEntity<>(jsonData, jsonHeaders), String.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        return JsonPath.parse(response.getBody());
    }

    /**
     * @param uri      uri relative to the base uri of the application
     * @param jsonData json with the object
     * @param method   HTTP Method type
     * @return jsonPath-parsed result for further checks
     */
    public DocumentContext createOrUpdate(String uri, String jsonData, HttpMethod method) {
        ResponseEntity<String> response = rest.exchange(baseUri(uri), method,
                new HttpEntity<>(jsonData, jsonHeaders), String.class);
        assertEquals(HttpStatus.CREATED, response.getStatusCode());
        return JsonPath.parse(response.getBody());
    }

    public void createOrUpdate(String uri, String jsonData, HttpMethod method, HttpStatus res) {
        ResponseEntity<String> response = rest.exchange(baseUri(uri), method,
                new HttpEntity<>(jsonData, jsonHeaders), String.class);
        assertEquals(res, response.getStatusCode());
    }

    private String uriList(String first, String... extra) {
        if (extra == null || extra.length < 1) {
            return baseUri(first);
        } else {
            StringBuilder sb = new StringBuilder(baseUri(first));
            for (String s : extra) {
                sb.append("\r\n").append(s);
            }
            return sb.toString();
        }
    }

    /**
     * Adds one resource to one association
     *
     * @param ownerUri the uri of the association
     * @param ownedUri the uri of the owned entity
     */
    public DocumentContext addAssociation(String ownerUri, String ownedUri, String... extraOwnedUri) {
        ResponseEntity<String> response = rest.exchange(baseUri(ownerUri), HttpMethod.POST,
                new HttpEntity<>(uriList(ownedUri, extraOwnedUri), uriListHeaders), String.class);
        return JsonPath.parse(response.getBody());
    }

    /**
     * Replaces the current content of the association with the ownedUri
     *
     * @param ownerUri the uri of the association
     * @param ownedUri the uri of the owned entity
     */
    public DocumentContext replaceAssociation(String ownerUri, String ownedUri, String... extraOwnedUri) {
        ResponseEntity<String> response = rest.exchange(baseUri(ownerUri), HttpMethod.PUT,
                new HttpEntity<>(uriList(ownedUri, extraOwnedUri), uriListHeaders), String.class);
        return JsonPath.parse(response.getBody());
    }

    /**
     * Clears the content of a X-to-Many association
     *
     * @param ownerUri uri of the association
     */
    private void clearToManyAssociation(String ownerUri) {
        rest.exchange(baseUri(ownerUri), HttpMethod.PUT, new HttpEntity<>(uriListHeaders), String.class);
    }

    /**
     * @param uri      uri relative to the base uri of the application
     * @param jsonData json with the object to post
     * @return jsonPath-parsed result for further checks
     */
    public DocumentContext updatePut(String uri, String jsonData) {
        ResponseEntity<String> response = rest.exchange(baseUri(uri), HttpMethod.PUT,
                new HttpEntity<>(jsonData, jsonHeaders),
                String.class);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        return JsonPath.parse(response.getBody());
    }

    /**
     * @param uri      uri relative to the base uri of the application
     * @param jsonData json with the object to post
     * @return jsonPath-parsed result for further checks
     */
    private DocumentContext updatePatch(String uri, String jsonData) {
        rest.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
        return JsonPath.parse(rest.patchForObject(baseUri(uri), new HttpEntity<>(jsonData, jsonHeaders), String.class));
    }

    public void delete(String uri) {
        rest.delete(baseUri(uri));
    }
}
