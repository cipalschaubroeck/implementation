package com.cipalschaubroeck.csimplementation;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentProcessType;
import com.cipalschaubroeck.csimplementation.document.service.AddresseeDuplicator;
import com.cipalschaubroeck.csimplementation.document.service.DocumentProcessDuplicator;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataDuplicator;
import com.cipalschaubroeck.csimplementation.xperido.TemplateDataXmlTransformer;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations = "classpath:application-it.properties")
public class CsImplementationApplicationTests {

    @Autowired
    private ApplicationContext context;

    @Test
    public void contextLoads() {
    }

    /**
     * there should be one template data duplicator for each kind of templateType
     */
    @Test
    public void allTemplateDataDuplicators() {
        Assert.assertEquals(EnumSet.allOf(TemplateType.class), context.getBeansOfType(TemplateDataDuplicator.class)
                .values().stream().map(TemplateDataDuplicator::getSupportedType).collect(Collectors.toSet()));
    }

    /**
     * there should be one duplicator for each kind of documentProcessType
     */
    @Test
    public void allDocumentProcessDuplicator() {
        Assert.assertEquals(EnumSet.allOf(DocumentProcessType.class), context.getBeansOfType(DocumentProcessDuplicator.class)
                .values().stream().map(DocumentProcessDuplicator::getSupportedType).collect(Collectors.toSet()));
    }

    /**
     * there should be one duplicator for each kind of address
     */
    @Test
    public void allAddresseeDuplicator() {
        Assert.assertEquals(EnumSet.allOf(QualifiedPersonType.class), context.getBeansOfType(AddresseeDuplicator.class)
                .values().stream().map(AddresseeDuplicator::getSupportedType).collect(Collectors.toSet()));
    }

    /**
     * there should be one adapter from data to xml for each template type
     */
    @Test
    public void allTemplateDataXml() {
        Map<TemplateType, Class<? extends TemplateData>> typeToClass = new EnumMap<>(TemplateType.class);
        typeToClass.put(TemplateType.SIGNING_OFFICERS, SigningOfficerData.class);
        typeToClass.put(TemplateType.HEADING, Heading.class);
        typeToClass.put(TemplateType.BAIL_REQUEST_GUARANTEE, BailRequestGuarantee.class);

        // if this fails, there's new template types
        Assert.assertEquals(TemplateType.values().length, typeToClass.size());
        Assert.assertTrue(typeToClass.keySet().containsAll(Arrays.asList(TemplateType.values())));

        Collection<TemplateDataXmlTransformer> transformers = context.getBeansOfType(TemplateDataXmlTransformer.class).values();
        Assert.assertTrue(typeToClass.values().stream()
                .allMatch(dataClass -> transformers.stream().filter(t -> t.supports(dataClass)).count() == 1));
    }
}

