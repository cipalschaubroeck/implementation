package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractingAuthorityRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Test
    public void crud() {
        String phone = tester.create(URI + API_PATH + "/phone", "{\"phone\": \"555-1234\"}")
                .read("$._links.self.href");
        String email = tester.create(URI + API_PATH + "/email", "{\"email\": \"somebody@somewhere.org\"}")
                .read("$._links.self.href");
        String address = tester.create(URI + API_PATH + "/postal-address", "{\"street\": \"calle\", \"number\":\"numero\", \"postalCode\":\"codigo postal\", \"municipality\":\"municipio\", \"country\":\"pais\"}")
                .read("$._links.self.href");

        tester.crudTest(URI + API_PATH + "/contracting-authority",
                String.format("{\"name\":\"contracting authority\",\"nationalId\":\"2254567891012\",\"phone\":\"%s\",\"email\":\"%s\",\"address\":\"%s\"}", phone, email, address),
                parsed -> {
                    assertEquals("contracting authority", parsed.read("$.name"));
                    assertEquals("555-1234", tester.read(parsed.read("$._links.phone.href")).read("$.phone"));
                    assertEquals("somebody@somewhere.org", tester.read(parsed.read("$._links.email.href")).read("$.email"));
                    assertEquals("calle", tester.read(parsed.read("$._links.address.href")).read("$.street"));
                },
                String.format("{\"nationalId\":\"2254567891012\",\"name\":\"new contracting authority\",\"phone\":\"%s\",\"email\":\"%s\",\"address\":\"%s\"}", phone, email, address),
                parsed -> {
                    assertEquals("new contracting authority", parsed.read("$.name"));
                    assertEquals("555-1234", tester.read(parsed.read("$._links.phone.href")).read("$.phone"));
                    assertEquals("somebody@somewhere.org", tester.read(parsed.read("$._links.email.href")).read("$.email"));
                    assertEquals("calle", tester.read(parsed.read("$._links.address.href")).read("$.street"));
                },
                "{\"name\":\"contracting authority\"}",
                parsed -> {
                    assertEquals("contracting authority", parsed.read("$.name"));
                    assertEquals("555-1234", tester.read(parsed.read("$._links.phone.href")).read("$.phone"));
                    assertEquals("somebody@somewhere.org", tester.read(parsed.read("$._links.email.href")).read("$.email"));
                    assertEquals("calle", tester.read(parsed.read("$._links.address.href")).read("$.street"));
                });
    }

}
