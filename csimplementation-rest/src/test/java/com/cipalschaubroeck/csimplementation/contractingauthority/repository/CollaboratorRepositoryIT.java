package com.cipalschaubroeck.csimplementation.contractingauthority.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class CollaboratorRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }


    @Test
    public void crud() {
        String caUri = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"single authority\", \"nationalId\": \"1234567891025\"}")
                .read("$._links.self.href");

        String ca2Uri = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"single authority2\", \"nationalId\": \"123456789454\"}")
                .read("$._links.self.href");

        String personUri = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");

        tester.crudTest(URI + API_PATH + "/collaborator",
                String.format("{\"authority\":\"%s\",\"person\":\"%s\"}", caUri, personUri),
                parsed -> {
                    assertEquals("single authority", tester.read(parsed.read("$._links.authority.href"))
                            .read("$.name"));
                    assertEquals("nombre", tester.read(parsed.read("$._links.person.href"))
                            .read("$.firstName"));
                },
                String.format("{\"authority\":\"%s\",\"person\":\"%s\"}", caUri, personUri),
                parsed -> {
                    assertEquals("single authority", tester.read(parsed.read("$._links.authority.href"))
                            .read("$.name"));
                    assertEquals("nombre", tester.read(parsed.read("$._links.person.href"))
                            .read("$.firstName"));
                },
                String.format("{\"authority\":\"%s\"}", ca2Uri),
                parsed -> assertEquals("single authority2", tester.read(parsed.read("$._links.authority.href"))
                        .read("$.name")));

        String person2 = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");

        String collaborator = tester.create(URI + API_PATH + "/collaborator", String.format("{\"function\":\"president\"," +
                "\"person\":\"%s\"}", person2))
                .read("$._links.self.href");

        String qualification = tester.create(URI + API_PATH + "/qualification",
                "{" +
                        "\"name\": \"qualification 2\"," +
                        "\"competence\":\"competence\"," +
                        "\"fixed\": true\n" +
                        "}")
                .read("$._links.self.href");

        tester.createOrUpdate(String.format("%s/assignment", collaborator), String.format("{" +
                "\"contractingAuthority\":\"%s\"," +
                "\"function\": \"function 2\"," +
                "\"qualification\": \"%s\"" +
                "}", caUri, qualification), HttpMethod.POST, HttpStatus.CREATED);

    }
}
