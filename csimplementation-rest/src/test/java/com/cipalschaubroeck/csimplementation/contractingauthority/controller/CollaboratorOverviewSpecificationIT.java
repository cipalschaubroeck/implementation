package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:ContractingAuthorityOverviewSpecificationIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class CollaboratorOverviewSpecificationIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    private String encode(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private String encode(String url, Map<String, String> parameters) {
        return parameters.keySet().stream()
                .map(key -> key + "=" + encode(parameters.get(key)))
                .collect(Collectors.joining("&", url + '?', ""));
    }

    private void checkOrder(String sortProperty, String jsonPath) {
        String url = URI + API_PATH + "/collaborator/overview?projection=collaboratorInfo&sort=" + sortProperty;
        DocumentContext read = tester.read(url + ",asc");
        check(read.read("$._embedded.collaborators[0]." + jsonPath), read.read("$._embedded.collaborators[1]." + jsonPath));

        read = tester.read(url + ",desc");
        check(read.read("$._embedded.collaborators[1]." + jsonPath), read.read("$._embedded.collaborators[0]." + jsonPath));
    }

    @SuppressWarnings("unchecked")
    private void check(Comparable lesser, Comparable greater) {
        if (lesser != null && greater != null) {
            Assert.assertTrue(lesser.compareTo(greater) <= 0);
        }
    }

    @Test
    public void filter() {

        String authority = tester.create(URI + API_PATH + "/contracting-authority",
                "{\"name\":\"CollaboratorOverviewSpecificationIT 1\", \"nationalId\": \"61.55.22-11.98\"}")
                .read("$._links.self.href");

        String postalAddress = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street\",\"number\":\"222\", \"postalCode\":\"28051\",\"municipality\":\"Madrid\",\"country\": \"Spain\"}")
                .read("$._links.self.href");

        String phone = tester.create(URI + API_PATH + "/phone",
                "{\"phone\": \"555-66666\"}")
                .read("$._links.self.href");

        String email = tester.create(URI + API_PATH + "/email",
                "{\"email\": \"someEmail@email.com\"}")
                .read("$._links.self.href");

        String person = tester.create(URI + API_PATH + "/person",
                String.format("{\"firstName\":\"person 1\",\"lastName\":\"person 1\"," +
                        " \"phone\": \"%s\", \"email\": \"%s\", \"address\": \"%s\"}", phone, email, postalAddress))
                .read("$._links.self.href");

        tester.create(URI + API_PATH + "/collaborator",
                String.format("{\"authority\": \"%s\", \"person\": \"%s\"}", authority, person))
                .read("$._links.self.href");

        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("projection", "collaboratorInfo");

        String url = URI + API_PATH + "/collaborator/overview";

        DocumentContext read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.collaborators")).size());

        requestParams.put("name", "a name that does not exist");
        read = tester.read(encode(url, requestParams));
        Assert.assertEquals(0, ((JSONArray) read.read("$._embedded.collaborators")).size());
        requestParams.remove("name");

        requestParams.put("email", "someEmail");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.collaborators")).size());

        requestParams.put("phone", "555-66666");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.collaborators")).size());
    }

    @Test
    public void sorting() {
        String postalAddress1 = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street\",\"number\":\"222\", \"postalCode\":\"28051\",\"municipality\":\"Madrid\",\"country\": \"Spain\"}")
                .read("$._links.self.href");
        String phone1 = tester.create(URI + API_PATH + "/phone",
                "{\"phone\": \"555-66666\"}")
                .read("$._links.self.href");
        String email1 = tester.create(URI + API_PATH + "/email",
                "{\"email\": \"someEmai1.com\"}")
                .read("$._links.self.href");
        String person1 = tester.create(URI + API_PATH + "/person",
                String.format("{\"firstName\":\"person 1\",\"lastName\":\"person 1\",\"phone\": \"%s\", \"email\": \"%s\", \"address\": \"%s\"}", phone1, email1, postalAddress1))
                .read("$._links.self.href");
        String authority1 = tester.create(URI + API_PATH + "/contracting-authority",
                "{\"name\":\"CollaboratorOverviewSpecificationIT 2\", \"nationalId\": \"61.55.22-11.99\"}")
                .read("$._links.self.href");

        String collaborator1 = tester.create(URI + API_PATH + "/collaborator",
                String.format("{\"person\": \"%s\", \"authority\": \"%s\"}", person1, authority1))
                .read("$._links.self.href");

        tester.read(collaborator1);

        String person2 = tester.create(URI + API_PATH + "/person",
                "{\"firstName\":\"person 2\",\"lastName\":\"person 2\"}")
                .read("$._links.self.href");
        String authority2 = tester.create(URI + API_PATH + "/contracting-authority",
                "{\"name\":\"CollaboratorOverviewSpecificationIT 3\", \"nationalId\": \"61.55.22-11.97\"}")
                .read("$._links.self.href");
        String postalAddress2 = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street 2\",\"number\":\"223\", \"postalCode\":\"28052\",\"municipality\":\"Valencia\",\"country\": \"Colombia\"}")
                .read("$._links.self.href");
        String phone2 = tester.create(URI + API_PATH + "/phone",
                "{\"phone\": \"555-66667\"}")
                .read("$._links.self.href");

        String email2 = tester.create(URI + API_PATH + "/email",
                "{\"email\": \"someEmail2.com\"}")
                .read("$._links.self.href");
        String collaborator2 = tester.create(URI + API_PATH + "/collaborator",
                String.format("{\"authority\": \"%s\", \"person\": \"%s\", " +
                        "\"phone\": \"%s\", \"email\": \"%s\", \"address\": \"%s\"}", authority2, person2, phone2, email2, postalAddress2))
                .read("$._links.self.href");
        tester.read(collaborator2);

        checkOrder("name", "name");
        checkOrder("email", "email");
        checkOrder("phone", "phone");
    }
}
