package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.contractingauthority.service.AuthorityImage;
import com.cipalschaubroeck.csimplementation.contractingauthority.service.ContractingAuthorityImageSettingsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RunWith(SpringRunner.class)
public class ContractingAuthorityImageSettingsControllerTest {
    @InjectMocks
    private ContractingAuthorityImageSettingsController controller;
    @Mock
    private ContractingAuthorityImageSettingsService service;

    @Test
    public void setLogo() throws IOException {
        Integer authorityId = 1;
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(file.getOriginalFilename()).thenReturn("fileName.pdf");
        byte[] data = "data".getBytes();
        Mockito.when(file.getBytes()).thenReturn(data);
        Mockito.when(file.getContentType()).thenReturn(MediaType.APPLICATION_PDF_VALUE);
        controller.setImage(authorityId, AuthorityImage.LOGO, file);
        Mockito.verify(service).setImage(AuthorityImage.LOGO, authorityId, file.getOriginalFilename(), file.getBytes(), MediaType.APPLICATION_PDF_VALUE);
    }

    @Test
    public void setRibbon() throws IOException {
        Integer authorityId = 1;
        MultipartFile file = Mockito.mock(MultipartFile.class);
        Mockito.when(file.getOriginalFilename()).thenReturn("fileName.pdf");
        byte[] data = "data".getBytes();
        Mockito.when(file.getBytes()).thenReturn(data);
        Mockito.when(file.getContentType()).thenReturn(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        controller.setImage(authorityId, AuthorityImage.RIBBON, file);
        Mockito.verify(service).setImage(AuthorityImage.RIBBON, authorityId, file.getOriginalFilename(), file.getBytes(), MediaType.APPLICATION_PDF_VALUE);
    }
}
