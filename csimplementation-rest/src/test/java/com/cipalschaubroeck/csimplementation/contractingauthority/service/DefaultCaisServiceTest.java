package com.cipalschaubroeck.csimplementation.contractingauthority.service;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityImageSettingsRepository;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import org.apache.chemistry.opencmis.client.api.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Objects;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class DefaultCaisServiceTest {
    @InjectMocks
    private DefaultCaisService service;
    @Mock
    private ContractingAuthorityRepository authorityRepository;
    @Mock
    private ContractingAuthorityImageSettingsRepository repository;
    @Mock
    private CMCommunicationService contentManager;

    @Test
    public void setLogo() {
        Integer authorityId = 1;
        String fileName = "fileName.pdf";
        byte[] data = "data".getBytes();
        String mime = MediaType.APPLICATION_PDF_VALUE;

        ContractingAuthorityImageSettings settings = Mockito.mock(ContractingAuthorityImageSettings.class);
        Mockito.when(repository.findById(authorityId)).thenReturn(Optional.of(settings));

        Document document = Mockito.mock(Document.class);
        Mockito.when(document.getId()).thenReturn("cmid");
        Mockito.when(contentManager.uploadFile(ArgumentMatchers.eq(fileName), ArgumentMatchers.eq(data),
                ArgumentMatchers.anyList(), ArgumentMatchers.eq(mime))).thenReturn(document);

        service.setImage(AuthorityImage.LOGO, authorityId, fileName, data, mime);
        Mockito.verify(settings).setLogoId(document.getId());
        Mockito.verify(repository).save(settings);
    }

    @Test
    public void setRibbon() {
        Integer authorityId = 1;
        String fileName = "fileName.pdf";
        byte[] data = "data".getBytes();
        String mime = MediaType.APPLICATION_PDF_VALUE;

        Mockito.when(repository.findById(authorityId)).thenReturn(Optional.empty());

        ContractingAuthority authority = new ContractingAuthority();
        Mockito.when(authorityRepository.findById(authorityId)).thenReturn(Optional.of(authority));

        Document document = Mockito.mock(Document.class);
        Mockito.when(document.getId()).thenReturn("cmid");
        Mockito.when(contentManager.uploadFile(ArgumentMatchers.eq(fileName), ArgumentMatchers.eq(data),
                ArgumentMatchers.anyList(), ArgumentMatchers.eq(mime))).thenReturn(document);

        service.setImage(AuthorityImage.RIBBON, authorityId, fileName, data, mime);
        ArgumentMatcher<ContractingAuthorityImageSettings> matcher = argument ->
                Objects.equals(document.getId(), argument.getRibbonId()) && authority.equals(argument.getAuthority());
        Mockito.verify(repository).save(ArgumentMatchers.argThat(matcher));
    }
}
