package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpServerErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractAuthorityCollaboratorQFControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    private void contractCollaboratorAssign() {
        String person = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        String collaborator = tester.create(URI + API_PATH + "/collaborator", String.format("{\"function\":\"president\",\"person\":\"%s\"}", person))
                .read("$._links.self.href");
        String authority = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"contracting authority\", \"nationalId\": \"12345651111\"}")
                .read("$._links.self.href");
        String department = tester.create(URI + API_PATH + "/department", String.format("{\"name\":\"name1\", \"authority\": \"%s\"}", authority))
                .read("$._links.self.href");
        String qualification = tester.create(URI + API_PATH + "/qualification",
                "{\"name\":\"name qualification\", \"competence\": \"competence\", \"fixed\": \"true\"}")
                .read("$._links.self.href");
        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);

        String contract = tester.create(URI + API_PATH + "/contract",
                "{\"name\":\"name of contract\"}")
                .read("$._links.self.href");

        tester.createOrUpdate(URI + API_PATH + "/contract-authority-relation",
                String.format("{\"contract\": \"%s\" ,\"contractingAuthority\": \"%s\"}", contract, authority), HttpMethod.PUT);

        String qualification1 = tester.read(URI + API_PATH + "/qualification").read("$._embedded.qualifications[0].name");

        String qf = tester.read(String.format("%s/collaborators/filtered?projection=qualificationFunctionInfo&qualification=%s", authority, qualification1))
                .read("$._embedded.qualificationFunctions[0]._links.qualificationFunction.href");
        qf = qf.replace("{?projection}", "");


        tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                String.format("{\"qualificationFunction\": \"%s\" ,\"competence\": \"competence\"}", qf), HttpMethod.POST, HttpStatus.CREATED);

        tester.read(String.format("%s/collaborators/filtered?projection=qualificationFunctionInfo&qualification=", authority))
                .read("$._embedded.qualificationFunctions[0]._links.qualificationFunction.href");


        String cqa = tester.read(String.format("%s/?projection=contractAuthorityCollaboratorsInfo", contract)).read("$.collaborators[0]._links.self.href");
        cqa = cqa.replace("{?projection}", "");

        String qf2 = tester.read(String.format("%s/?projection=contractAuthorityCollaboratorsInfo", contract))
                .read("$.collaborators[0]._links.qualification-function.href");
        qf2 = qf2.replace("{?projection}", "");

        tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                String.format("{\"qualificationFunction\": \"%s\" ,\"competence\": \"competence1\",\"contractCollaborator\":\"%s\"}", qf2, cqa),
                HttpMethod.POST, HttpStatus.CREATED);
        /*
         * non existing contract
         */
        try {
            tester.createOrUpdate(URI + API_PATH + "/contract/22/authority-collaborator-q-f/assignment",
                    String.format("{\"qualificationFunction\": \"%s\" ,\"competence\": \"competence1\",\"contractCollaborator\":\"%s\"}", qf2, cqa),
                    HttpMethod.POST, HttpStatus.CREATED);
        } catch (HttpServerErrorException.InternalServerError ex) {
            Assert.assertEquals("500 ", ex.getMessage());
        }
        /*
         * passing non existing qualification-function
         */
        try {
            tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                    String.format("{\"qualificationFunction\": \"%s\" ,\"competence\": \"competence1\",\"contractCollaborator\":\"%s\"}", URI + API_PATH + "/qualification-function/545", cqa),
                    HttpMethod.POST, HttpStatus.CREATED);
        } catch (HttpServerErrorException.InternalServerError ex) {
            Assert.assertEquals("500 ", ex.getMessage());
        }

        /*
         *passing null qualification-function
         */
        try {
            tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                    String.format("{\"competence\": \"competence1\",\"contractCollaborator\":\"%s\"}", cqa),
                    HttpMethod.POST, HttpStatus.CREATED);
        } catch (HttpServerErrorException.InternalServerError ex) {
            Assert.assertEquals("500 ", ex.getMessage());
        }

        String person1 = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        String collaborator2 = tester.create(URI + API_PATH + "/collaborator", String.format("{\"function\":\"president\",\"person\":\"%s\"}", person1))
                .read("$._links.self.href");
        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator2, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);

        tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                String.format("{\"qualificationFunction\": \"%s\" ,\"competence\": \"competence1\",\"contractCollaborator\":\"%s\",\"collaborator\":\"%s\"}", qf2, cqa, collaborator2),
                HttpMethod.POST, HttpStatus.CREATED);

        tester.createOrUpdate(String.format("%s", contract), String.format("{\"mainAuthorityCollaboratorContact\": \"%s\"}", cqa), HttpMethod.PATCH, HttpStatus.OK);

        tester.delete(cqa);

    }

    @Test()
    public void collaboratorsInContractingAuthority() {
        /*
         * Create Person
         */
        String person = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        /*
         * create collaborator from person
         */
        String collaborator = tester.create(URI + API_PATH + "/collaborator", String.format("{\"function\":\"president\",\"person\":\"%s\"}", person))
                .read("$._links.self.href");

        /*
         * create contracting authority
         */
        String authority = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"contracting authority\", \"nationalId\": \"1234567211111\"}")
                .read("$._links.self.href");
        /*
         *create department
         */
        String department = tester.create(URI + API_PATH + "/department", String.format("{\"name\":\"name\", \"authority\": \"%s\"}", authority))
                .read("$._links.self.href");
        /*
         * create Qualification resource
         */
        String qualification = tester.create(URI + API_PATH + "/qualification",
                "{\"name\":\"name qualification\", \"competence\": \"competence\", \"fixed\": \"true\"}")
                .read("$._links.self.href");

        /*
         * assigns the collaborator to the contracting authority function department and qualification
         */
        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);

        /*
         * get list of collaborators assigned to the contracting authority
         */
        String qualificationFunction = tester.read(String.format("%s?projection=initiateContractAuthorityInfo", authority))
                .read("$.collaborators[0].qualificationFunction[0]._links.self.href");
        qualificationFunction = qualificationFunction.replace("{?projection}", "");

        /*
         * create contract resource
         */
        String contract = tester.create(URI + API_PATH + "/contract", "{\"name\":\"contract\"}").read("$._links.self.href");


        try {
            /*
             * Link collaborator to contract without a contracting authority that's not assigned to the contract
             */
            tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                    String.format("{\"qualificationFunction\":\"%s\"}", qualificationFunction), HttpMethod.POST, HttpStatus.CREATED);
        } catch (HttpServerErrorException.InternalServerError ex) {
            Assert.assertEquals("500 ", ex.getMessage());
        }

        /*
         *link contracting authority and contract
         */
        tester.createOrUpdate(URI + API_PATH + "/contract-authority-relation",
                String.format("{\"contract\": \"%s\", \"contractingAuthority\": \"%s\"}", contract, authority), HttpMethod.PUT);

        /*
         *link collaborator to the contract
         */
        tester.createOrUpdate(String.format("%s/authority-collaborator-q-f/assignment", contract),
                String.format("{\"qualificationFunction\":\"%s\"}", qualificationFunction), HttpMethod.POST, HttpStatus.CREATED);
        contractCollaboratorAssign();
    }
}
