package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpServerErrorException;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class CollaboratorControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    private void uniqueDepartmentForAuthority(String authority, String collaborator, String department, String qualification) {
        try {
            tester.createOrUpdate(String.format("%s/assignment", collaborator),
                    String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                    "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                            authority, collaborator, department, qualification)
                    , HttpMethod.POST, HttpStatus.CREATED);
        } catch (HttpServerErrorException.InternalServerError ex) {
            Assert.assertEquals("500 ", ex.getMessage());
        }
    }

    private void assignDifferentQualificationToDepartmentInAuthority(String authority, String collaborator, String department) {
        String qualification = tester.create(URI + API_PATH + "/qualification",
                "{\"name\":\"name qualification\", \"competence\": \"competence\", \"fixed\": \"true\"}")
                .read("$._links.self.href");

        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);
    }

    private void modifyExistingAuthorityCollaborator(String authority, String collaborator, String department, String qualification) {
        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);
    }

    @Test()
    public void collaboratorsInContractingAuthority() {
        String person = tester.create(URI + API_PATH + "/person", "{\"firstName\":\"nombre\",\"lastName\":\"apellido\"}")
                .read("$._links.self.href");
        String collaborator = tester.create(URI + API_PATH + "/collaborator", String.format("{\"function\":\"president\",\"person\":\"%s\"}", person))
                .read("$._links.self.href");

        String authority = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"contracting authority\", \"nationalId\": \"1234567111111\"}")
                .read("$._links.self.href");
        String department = tester.create(URI + API_PATH + "/department", String.format("{\"name\":\"name\", \"authority\": \"%s\"}", authority))
                .read("$._links.self.href");

        String qualification = tester.create(URI + API_PATH + "/qualification",
                "{\"name\":\"name qualification\", \"competence\": \"competence\", \"fixed\": \"true\"}")
                .read("$._links.self.href");

        tester.createOrUpdate(String.format("%s/assignment", collaborator),
                String.format("{\"contractingAuthority\":\"%s\", \"collaborator\": \"%s\", " +
                                "\"department\": \"%s\", \"function\": \"function\", \"qualification\":\"%s\"}",
                        authority, collaborator, department, qualification)
                , HttpMethod.POST, HttpStatus.CREATED);

        String authority2 = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"contracting authority\", \"nationalId\": \"123456713211111\"}")
                .read("$._links.self.href");

        uniqueDepartmentForAuthority(authority2, collaborator, department, qualification);
        modifyExistingAuthorityCollaborator(authority, collaborator, department, qualification);
        assignDifferentQualificationToDepartmentInAuthority(authority, collaborator, department);
    }
}
