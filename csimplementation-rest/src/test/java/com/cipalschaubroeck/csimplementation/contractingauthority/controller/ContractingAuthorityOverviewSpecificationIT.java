package com.cipalschaubroeck.csimplementation.contractingauthority.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:ContractingAuthorityOverviewSpecificationIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractingAuthorityOverviewSpecificationIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    private String encode(String value) {
        try {
            return URLEncoder.encode(value, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    private String encode(String url, Map<String, String> parameters) {
        return parameters.keySet().stream()
                .map(key -> key + "=" + encode(parameters.get(key)))
                .collect(Collectors.joining("&", url + '?', ""));
    }

    private void checkOrder(String sortProperty, String jsonPath) {
        String url = URI + API_PATH + "/contracting-authority/overview?projection=contractingAuthorityInfo&sort=" + sortProperty;
        DocumentContext read = tester.read(url + ",asc");
        check(read.read("$._embedded.contractingAuthorities[0]." + jsonPath), read.read("$._embedded.contractingAuthorities[1]." + jsonPath));

        read = tester.read(url + ",desc");
        check(read.read("$._embedded.contractingAuthorities[1]." + jsonPath), read.read("$._embedded.contractingAuthorities[0]." + jsonPath));
    }

    @SuppressWarnings("unchecked")
    private void check(Comparable lesser, Comparable greater) {
        if (lesser != null && greater != null) {
            Assert.assertTrue(lesser.compareTo(greater) <= 0);
        }
    }

    @Test
    public void filter() {
        String postalAddress = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street\",\"number\":\"222\", \"postalCode\":\"28051\",\"municipality\":\"Madrid\",\"country\": \"Spain\"}")
                .read("$._links.self.href");

        tester.create(URI + API_PATH + "/contracting-authority",
                String.format("{\"name\":\"ContractingAuthorityOverviewSpecificationIT 1\", \"nationalId\": \"66.55.22-11.43\",\"address\":\"%s\"}", postalAddress))
                .read("$._links.self.href");

        Map<String, String> requestParams = new HashMap<>();
        requestParams.put("projection", "contractingAuthorityInfo");

        String url = URI + API_PATH + "/contracting-authority/overview";

        DocumentContext read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());

        requestParams.put("authority", "a name that does not exist");
        read = tester.read(encode(url, requestParams));
        Assert.assertEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());
        requestParams.remove("authority");

        requestParams.put("nationalId", "66.55.22-11.43");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());


        requestParams.put("address", "ome str");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());

        requestParams.put("postalCode", "05");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());

        requestParams.put("municipality", "dri");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());

        requestParams.put("procurementName", "of proc");
        read = tester.read(encode(url, requestParams));
        Assert.assertNotEquals(0, ((JSONArray) read.read("$._embedded.contractingAuthorities")).size());

    }

    @Test
    public void sorting() {
        String postalAddress = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street 2\",\"number\":\"333\", \"postalCode\":\"28052\",\"municipality\":\"Valencia\",\"country\": \"Belgium\"}")
                .read("$._links.self.href");

        String contractingAuthority = tester.create(URI + API_PATH + "/contracting-authority",
                String.format("{\"name\":\"ContractingAuthorityOverviewSpecificationIT 2\", \"nationalId\": \"66.55.22-11.44\",\"address\":\"%s\"}", postalAddress))
                .read("$._links.self.href");

        tester.read(contractingAuthority);

        String postalAddress2 = tester.create(URI + API_PATH + "/postal-address",
                "{\"street\":\"some street 3\",\"number\":\"444\", \"postalCode\":\"28053\",\"municipality\":\"Malaga\",\"country\": \"France\"}")
                .read("$._links.self.href");

        String contractingAuthority2 = tester.create(URI + API_PATH + "/contracting-authority",
                String.format("{\"name\":\"ContractingAuthorityOverviewSpecificationIT 2\", \"nationalId\": \"66.55.22-11.45\",\"address\":\"%s\"}", postalAddress2))
                .read("$._links.self.href");

        tester.read(contractingAuthority2);

        checkOrder("authority", "name");
        checkOrder("address", "address");
        checkOrder("number", "number");
        checkOrder("municipality", "municipality");
        checkOrder("nationalId", "nationalId");
    }
}
