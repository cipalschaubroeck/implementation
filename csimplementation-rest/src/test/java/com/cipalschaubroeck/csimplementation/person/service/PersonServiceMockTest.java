package com.cipalschaubroeck.csimplementation.person.service;


import com.cipalschaubroeck.csimplementation.organization.service.mock.PersonServiceMock;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.contact.Gender;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import com.greenvalley.contacts.domain.search.PersonSearchCriterion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class PersonServiceMockTest {
    @InjectMocks
    private PersonServiceMock service;
    private Person person;

    private Person createPerson(String firstName) {
        Person person = new Person();

        person.setFirstName(firstName);
        person.setGender(Gender.MALE);
        person.setFamilyName("family name");

        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        person.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");

        person.setContact(contact);
        return service.save(person);
    }

    @Before
    public void initFixture() {
        person = createPerson("firstName");
    }

    @Test
    public void findByDetail() {

        person = service.detailById(person.getId());

        assertThat(person, is(not(nullValue())));
        assertEquals("city", person.getBasicAddress().getCity());
        assertEquals("firstName", person.getFirstName());
    }

    @Test
    public void find() {
        List<Person> persons = service.find(new PersonSearchCriterion.Builder().withFirstName("firstName").build());
        assertEquals("firstName", persons.get(0).getFirstName());
        assertEquals(Gender.MALE, persons.get(0).getGender());
    }

    @Test
    public void findSingle() {
        Person person = service.findSingle(new PersonSearchCriterion.Builder().withFirstName("firstName").build());
        assertEquals("firstName", person.getFirstName());
    }

    @Test
    public void save() {
        Person person = new Person();

        person.setFirstName("first name");
        person.setGender(Gender.MALE);

        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        person.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");
        person.setContact(contact);

        Person per = service.save(person);

        assertEquals("first name", per.getFirstName());
        assertEquals("nick name", per.getContact().getNickname());
        assertNotEquals("nick1", per.getContact().getNickname());
        assertEquals("city", per.getBasicAddress().getCity());
    }

    @Test
    public void delete() {
        createPerson("Pepe");
        Person person = service.findSingle(new PersonSearchCriterion.Builder().withFirstName("Pepe").build());
        assertNotNull(person);
        service.delete(person.getId());
        person = service.findSingle(new PersonSearchCriterion.Builder().withFirstName("Pepe").build());
        assertNull(person);
    }
}
