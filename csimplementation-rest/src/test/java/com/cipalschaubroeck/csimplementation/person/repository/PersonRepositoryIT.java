package com.cipalschaubroeck.csimplementation.person.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class PersonRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crud() {
        String phone = tester.create(URI + API_PATH + "/phone", "{\"phone\": \"555-1234\"}")
                .read("$._links.self.href");
        String email = tester.create(URI + API_PATH + "/email", "{\"email\": \"somebody@somewhere.org\"}")
                .read("$._links.self.href");
        String address = tester.create(URI + API_PATH + "/postal-address", "{\"street\": \"calle\", \"number\":\"numero\", \"postalCode\":\"codigo postal\", \"municipality\":\"municipio\", \"country\":\"pais\"}")
                .read("$._links.self.href");

        tester.crudTest(URI + API_PATH + "/person",
                String.format("{\"firstName\":\"nombre\",\"lastName\":\"apellido\",\"phoneNumbers\":[\"%s\"],\"emails\":[\"%s\"],\"addresses\":[\"%s\"]}", phone, email, address),
                parsed -> {
                    assertEquals("nombre", parsed.read("$.firstName"));
                    assertEquals("apellido", parsed.read("$.lastName"));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.phoneNumbers.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.emails.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.addresses.href"))));
                },
                String.format("{\"firstName\":\"nuevo nombre\",\"lastName\":\"apellido\",\"phoneNumbers\":[\"%s\"],\"emails\":[\"%s\"],\"addresses\":[\"%s\"]}", phone, email, address),
                parsed -> {
                    assertEquals("nuevo nombre", parsed.read("$.firstName"));
                    assertEquals("apellido", parsed.read("$.lastName"));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.phoneNumbers.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.emails.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.addresses.href"))));
                },
                "{\"firstName\":\"nombre\"}",
                parsed -> {
                    assertEquals("nombre", parsed.read("$.firstName"));
                    assertEquals("apellido", parsed.read("$.lastName"));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.phoneNumbers.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.emails.href"))));
                    assertEquals(1, tester.countFromResults(tester.read(
                            parsed.read("$._links.addresses.href"))));
                });
    }
}
