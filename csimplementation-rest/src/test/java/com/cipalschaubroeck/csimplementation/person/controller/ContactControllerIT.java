package com.cipalschaubroeck.csimplementation.person.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import net.minidev.json.JSONArray;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class ContactControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }


    private DocumentContext createContact(String nickName, String streetName, String city, String firstName) {
        return tester.create(URI + API_PATH + "/contacts",
                String.format("{" +
                        "    \"contact\": {" +
                        "      \"nickname\": \"%s\"," +
                        "      \"email\": \"xxxxx@xxxxbelgium.be\"," +
                        "      \"phoneNumber\": \"+66666666\"," +
                        "      \"website\": \"www.xxxxxxx.be\"," +
                        "      \"fax\": \"+77777777\"" +
                        "    }," +
                        "    \"basicAddress\": {" +
                        "      \"streetName\": \"%s\"," +
                        "      \"houseNumber\": \"11125\"," +
                        "      \"locator\": \"2132\"," +
                        "      \"zipCode\": \"360023\"," +
                        "      \"country\": \"BE\"," +
                        "      \"city\": \"%s\"" +
                        "    }," +
                        "    \"identityNumber\": null," +
                        "    \"firstName\": \"%s\"," +
                        "    \"familyName\": \"XXXXX\"," +
                        "    \"gender\": \"FEMALE\"" +
                        "  }", nickName, streetName, city, firstName));
    }

    @Test
    public void create() {

        DocumentContext contact = createContact("xxxxxxxxxxxx", "street name", "GenkZZZZZ", "zzzzz");

        Assert.assertEquals("zzzzz", contact.read("$.firstName"));
        Assert.assertEquals("xxxxxxxxxxxx", contact.read("$.contact.nickname"));
        Assert.assertNotEquals("MALE", contact.read("$.basicAddress.streetName"));
        Assert.assertEquals("GenkZZZZZ", contact.read("$.basicAddress.city"));
    }

    @Test
    public void getList() {
        DocumentContext contact = tester.read(URI + API_PATH + "/contacts");
        Assert.assertNotEquals(0, ((JSONArray) contact.read("$")).size());
    }

    @Test
    public void createOrUpdate() {

        DocumentContext contact1 = createContact("nick name", "street name1 ", "Genk1", "fist name");
        String contactId = contact1.read("$.id");
        String body = "{\"firstName\": \"mock first name\",\"basicAddress\": {\"streetName\":\"street n2\"}}";
        DocumentContext contact2 = tester.createOrUpdate(String.format(URI + API_PATH + "/contacts/%s", contactId), body, HttpMethod.PATCH);
        Assert.assertEquals("mock first name", contact2.read("$.firstName"));
    }

    @Test
    public void getDetail() {
        DocumentContext contact1 = createContact("nick name", "street name1 ", "Genk2", "first name");
        String contactId = contact1.read("$.id");
        DocumentContext contact = tester.read(String.format(URI + API_PATH + "/contacts/%s", contactId));
        Assert.assertEquals("first name", contact.read("$.firstName"));
        Assert.assertEquals("XXXXX", contact.read("$.familyName"));
        Assert.assertEquals("Genk2", contact.read("$.basicAddress.city"));

    }

    @Test
    public void delete() {
        DocumentContext contact1 = createContact("nick name", "street name1 ", "Genk2", "first name");
        String contactId = contact1.read("$.id");

        DocumentContext contact = tester.read(URI + API_PATH + "/contacts");
        Integer count1 = ((JSONArray) contact.read("$")).size();
        tester.delete(String.format(URI + API_PATH + "/contacts/%s", contactId));
        contact = tester.read(URI + API_PATH + "/contacts");
        Integer count2 = ((JSONArray) contact.read("$")).size();
        Assert.assertNotEquals(count1, count2);

    }
}
