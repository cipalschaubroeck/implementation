package com.cipalschaubroeck.csimplementation.person.service;


import com.cipalschaubroeck.csimplementation.organization.service.mock.PersonInOrganizationServiceMock;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.contact.Gender;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.contact.Person;
import com.greenvalley.contacts.domain.contact.PersonInOrganization;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import com.greenvalley.contacts.domain.search.PersonInOrganizationSearchCriterion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class PersonInOrganizationServiceMockIT {

    @Autowired
    private PersonInOrganizationServiceMock service;

    private PersonInOrganization perInOrg;
    @Before
    public void initFixture() {

        PersonInOrganization personInOrganization = new PersonInOrganization();

        Person person = new Person();
        person.setFirstName("first name");
        person.setGender(Gender.MALE);
        personInOrganization.setPerson(person);

        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        personInOrganization.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");
        personInOrganization.setContact(contact);
        perInOrg = personInOrganization;
        service.save(perInOrg);
    }

    @Test
    public void findByDetail() {

        Organization org = new Organization();
        org.setId("1");

        final PersonInOrganization personInOrganization = service.detailById(perInOrg.getId());

        assertEquals("city", personInOrganization.getBasicAddress().getCity());
        assertEquals("first name", personInOrganization.getPerson().getFirstName());
    }

    @Test
    public void find() {
        List<PersonInOrganization> personsInOrg = service.find(new PersonInOrganizationSearchCriterion.Builder().withPersonFirstName("").build());
        assert (personsInOrg.size() > 0);
    }

    @Test
    public void findSingle() {
        PersonInOrganization personsInOrg = service.findSingle(new PersonInOrganizationSearchCriterion.Builder().withPersonFirstName("first name").build());
        assertNull(personsInOrg);
    }

    @Test
    public void save() {
        PersonInOrganization personInOrganization = new PersonInOrganization();


        Person person = new Person();
        person.setFirstName("first name");
        person.setGender(Gender.MALE);
        personInOrganization.setPerson(person);

        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        personInOrganization.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");
        personInOrganization.setContact(contact);

        PersonInOrganization perInOrg = service.save(personInOrganization);

        assertEquals("first name", perInOrg.getPerson().getFirstName());
        assertEquals("nick name", perInOrg.getContact().getNickname());
        assertNotEquals("nick1", perInOrg.getContact().getNickname());
        assertEquals("city", perInOrg.getBasicAddress().getCity());
    }

    @Test
    public void delete() {
        List<PersonInOrganization> personsInOrg1 = service.find(new PersonInOrganizationSearchCriterion.Builder().build());
        service.delete(personsInOrg1.get(0).getId());
        List<PersonInOrganization> personsInOrg2 = service.find(new PersonInOrganizationSearchCriterion.Builder().build());
        assertNotEquals(personsInOrg1.size(), personsInOrg2.size());
    }
}
