package com.cipalschaubroeck.csimplementation.person.service;


import com.cipalschaubroeck.csimplementation.organization.service.mock.OrganizationServiceMock;
import com.greenvalley.contacts.domain.contact.Contact;
import com.greenvalley.contacts.domain.contact.Organization;
import com.greenvalley.contacts.domain.localisation.BasicAddress;
import com.greenvalley.contacts.domain.search.OrganizationSearchCriterion;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
public class OrganizationServiceMockTest {

    @InjectMocks
    private OrganizationServiceMock organizationServiceMock;

    private Organization organization;

    private Organization createOrganization(String name) {
        organization = new Organization();
        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        organization.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");
        organization.setContact(contact);
        organization.setSme(false);
        organization.setKboNumber("1111111");
        organization.setLegalName(name);
        organization.setType("company");
        organization.setVatNumber("1111111");
        return organizationServiceMock.save(organization);
    }

    @Before
    public void initFixture() {
        organization = new Organization();
        organization = createOrganization("Legal name");


    }

    @Test
    public void findByDetail() {
        Organization org = new Organization();
        org.setId("1");

        organizationServiceMock.detailById(organization.getId());

        assertEquals("city", organization.getBasicAddress().getCity());
        assertEquals("Legal name", organization.getLegalName());
    }

    @Test
    public void findOrganization() {
        List<Organization> organizations = organizationServiceMock.find(new OrganizationSearchCriterion.Builder().withName("Legal name").build());
        assertEquals("Legal name", organizations.get(0).getLegalName());
        List<Organization> organizations2 = organizationServiceMock.find(new OrganizationSearchCriterion.Builder().withKboNumber("1111111").build());
        assertEquals("1111111", organizations2.get(0).getVatNumber());
    }

    @Test
    public void findSingle() {
        Organization organization = organizationServiceMock.findSingle(new OrganizationSearchCriterion.Builder().withName("Legal name").build());
        assertEquals("1111111", organization.getVatNumber());
    }

    @Test
    public void save() {
        Organization org = new Organization();

        org.setVatNumber("1111111");
        org.setType("company");
        org.setLegalName("legal name");
        org.setKboNumber("1111111");
        org.setSme(false);

        BasicAddress basicAddress = new BasicAddress();
        basicAddress.setStreetName("street name");
        basicAddress.setCountry("country");
        basicAddress.setCity("city");
        basicAddress.setZipCode("zipCode");
        basicAddress.setHouseNumber("22");
        org.setBasicAddress(basicAddress);

        Contact contact = new Contact();
        contact.setPhoneNumber("2222222");
        contact.setNickname("nick name");
        contact.setFax("2222222");
        contact.setEmail("email");
        contact.setWebsite("web site");
        org.setContact(contact);

        Organization organization = organizationServiceMock.save(org);

        assertEquals("1111111", organization.getVatNumber());
        assertEquals("nick name", organization.getContact().getNickname());
        assertNotEquals("nick1", organization.getContact().getNickname());
        assertEquals("city", organization.getBasicAddress().getCity());
    }

    @Test
    public void delete() {
        createOrganization("Org1");
        Organization organization = organizationServiceMock.findSingle(new OrganizationSearchCriterion.Builder().withName("Org1").build());
        assertNotNull(organization);
        organizationServiceMock.delete(organization.getId());
        organization = organizationServiceMock.findSingle(new OrganizationSearchCriterion.Builder().withName("Org1").build());
        assertNull(organization);
    }
}
