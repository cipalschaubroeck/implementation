package com.cipalschaubroeck.csimplementation.person.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class EmailRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;
    
    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crud() {
        tester.crudTest(URI + API_PATH + "/email",
                "{\"email\": \"somebody@somewhere.org\"}",
                parsed -> assertEquals("somebody@somewhere.org", parsed.read("$.email")),
                "{\"email\": \"somebody2@somewhere.org\"}",
                parsed -> assertEquals("somebody2@somewhere.org", parsed.read("$.email")),
                "{\"email\": \"somebody@somewhere.org\"}",
                parsed -> assertEquals("somebody@somewhere.org", parsed.read("$.email"))
        );
    }
}
