package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:ContractDefinitionControllerIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractDefinitionControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void validate() {
        String contract = tester.create(URI + API_PATH + "/contract",
                "{\"name\":\"name of contract\"}")
                .read("$._links.self.href");

        validateInitiateContractDataForm(contract);
        validateInitiateContractContractingAuthorityForm(contract);
        validateInitiateContractBailForm(contract);
        validateInitiateContractPricesReviewForm(contract);
        validateContractProcess(contract);
    }

    private void validateContractProcess(String contract) {
        tester.createOrUpdate(String.format("%s/validate/initiate/contract-process", contract), "{}", HttpMethod.POST, HttpStatus.OK);
        Assert.assertEquals("READ_ONLY", tester.read(String.format("%s/validate/initiate/contract-process", contract)).read("$"));
    }

    private void validateInitiateContractBailForm(String contractLink) {
        String response = tester.read(String.format("%s/validate/initiate/bail-form", contractLink)).read("$");
        Assert.assertEquals("COMPLETED", response);

        tester.createOrUpdate(String.format("%s/bail", contractLink), "{\"type\": \"BAIL\"}", HttpMethod.POST, HttpStatus.CREATED);
        response = tester.read(String.format("%s/validate/initiate/bail-form", contractLink)).read("$");
        Assert.assertEquals("NOT_COMPLETED", response);
        tester.createOrUpdate(String.format("%s/bail", contractLink), "{\"percentage\": \"10\", \"type\": \"BAIL\"}", HttpMethod.POST, HttpStatus.CREATED);
    }

    private void validateInitiateContractDataForm(String contractLink) {
        String response = tester.read(String.format("%s/validate/initiate/data-form", contractLink)).read("$");
        Assert.assertEquals("NOT_COMPLETED", response);

        String procurement = tester.create(URI + API_PATH + "/procurement", "{\"name\":\"procurement\"}")
                .read("$._links.self.href");

        tester.createOrUpdate(contractLink, String.format("{\"procurement\":\"%s\"," +
                "\"qualification\":\"WORKS\", \"procedureType\":\"PUBLIC\", \"decisionMakingBody\":\"decisionMakingBody\" ," +
                "\"amountVat\":\"2\", \"amountMinusVat\":\"2\", \"vat\":\"2\", \"valueIncVat\":\"2\"}", procurement), HttpMethod.PATCH, HttpStatus.OK);

        response = tester.read(String.format("%s/validate/initiate/data-form", contractLink)).read("$");

        Assert.assertEquals("COMPLETED", response);
    }

    private void validateInitiateContractContractingAuthorityForm(String contractLink) {

        String response = tester.read(String.format("%s/validate/initiate/authority-form", contractLink)).read("$");
        Assert.assertEquals("NOT_COMPLETED", response);
        String authority = tester.create(URI + API_PATH + "/contracting-authority",
                "{\"name\":\"ContractDefinitionControllerIT 1\", \"nationalId\": \"61.55.22-11.98\"}")
                .read("$._links.self.href");
        tester.createOrUpdate(URI + API_PATH + "/contract-authority-relation", String.format("{\"contract\":\"%s\",\"contractingAuthority\":\"%s\"}",
                contractLink, authority), HttpMethod.PUT);
        response = tester.read(String.format("%s/validate/initiate/authority-form", contractLink)).read("$");
        Assert.assertEquals("COMPLETED", response);
    }

    private void validateInitiateContractPricesReviewForm(String contractLink) {
        String response = tester.read(String.format("%s/validate/initiate/prices-review-form", contractLink)).read("$");
        Assert.assertEquals("COMPLETED", response);

        tester.createOrUpdate(String.format("%s/prices-review", contractLink), "{\"type\": \"FIXED\"}", HttpMethod.POST, HttpStatus.CREATED);
        response = tester.read(String.format("%s/validate/initiate/prices-review-form", contractLink)).read("$");
        Assert.assertEquals("NOT_COMPLETED", response);

        tester.createOrUpdate(String.format("%s/prices-review", contractLink), "{\"formula\": \"abc123\", \"type\": \"FIXED\"}", HttpMethod.POST, HttpStatus.CREATED);
        response = tester.read(String.format("%s/validate/initiate/prices-review-form", contractLink)).read("$");
        Assert.assertEquals("COMPLETED", response);
    }
}
