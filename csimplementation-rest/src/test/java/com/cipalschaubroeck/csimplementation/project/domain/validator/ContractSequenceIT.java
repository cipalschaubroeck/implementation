package com.cipalschaubroeck.csimplementation.project.domain.validator;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Calendar;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:contractIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ContractSequenceIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void validateSequenceContract() {
        String contract2 = tester.create(URI + API_PATH + "/contract",
                "{\"name\":\"ContractSequenceIT1\", \"contractType\":\"aaa\", \"amountMinusVat\":10, \"amountVat\":2}")
                .read("$._links.self.href");
        tester.read(contract2);

        String contract3 = tester.create(URI + API_PATH + "/contract",
                "{ \"name\":\"ContractSequenceIT2\", \"contractType\":\"bbb\", \"amountMinusVat\":100, \"amountVat\":20}")
                .read("$._links.self.href");
        DocumentContext contract = tester.read(contract3);
        Assert.assertEquals(contract.read("contractId"), Calendar.getInstance().get(Calendar.YEAR) + "-" + 2);
    }

}
