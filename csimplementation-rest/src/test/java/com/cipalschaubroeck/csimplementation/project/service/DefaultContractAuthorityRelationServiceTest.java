package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityRepository;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import com.cipalschaubroeck.csimplementation.project.repository.ContractAuthorityRelationRepository;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.impl.DefaultContractAuthorityRelationService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class DefaultContractAuthorityRelationServiceTest {
    @Mock
    private ContractAuthorityRelationRepository repository;

    @Mock
    private ContractRepository contractRepository;

    @Mock
    private ContractingAuthorityRepository contractingAuthorityRepository;

    @InjectMocks
    private DefaultContractAuthorityRelationService service;

    @Rule
    public final ExpectedException exception = ExpectedException.none();
    @Test
    public void test() {
        Integer contractId = 2;
        Contract contract = new Contract();
        contract.setId(contractId);

        Mockito.when(contractRepository.findById(contractId)).thenReturn(Optional.of(contract));

        ContractingAuthority contractingAuthority = new ContractingAuthority();
        Integer authorityId = 1;
        contractingAuthority.setId(authorityId);
        Mockito.when(contractingAuthorityRepository.findById(authorityId)).thenReturn(Optional.of(contractingAuthority));

        ContractAuthorityRelation contractAuthorityRelation = new ContractAuthorityRelation();
        contractAuthorityRelation.setContract(contract);
        contractAuthorityRelation.setContractingAuthority(contractingAuthority);
        Mockito.when(repository.findById(contractId)).thenReturn(Optional.of(contractAuthorityRelation));

        Mockito.when(service.setContractAuthorityRelation(contractId, authorityId)).thenReturn(contractAuthorityRelation);
        ContractAuthorityRelation contractAuthorityRelation1 = service.setContractAuthorityRelation(contractId, authorityId);

        Assert.assertSame(contractAuthorityRelation1.getContract().getId(), contractAuthorityRelation.getContract().getId());


        Mockito.when(repository.findById(contractId)).thenReturn(Optional.of(contractAuthorityRelation));
        Mockito.when(contractRepository.findById(contractId)).thenReturn(Optional.of(contract));
        service.setContractAuthorityRelation(contractId, null);

        Mockito.when(repository.findById(contractId)).thenReturn(Optional.empty());
        service.setContractAuthorityRelation(contractId, null);
        Mockito.when(contractRepository.findById(3)).thenThrow(new EntityNotFoundException());
        exception.expect(EntityNotFoundException.class);
        service.setContractAuthorityRelation(3, authorityId);

    }


}
