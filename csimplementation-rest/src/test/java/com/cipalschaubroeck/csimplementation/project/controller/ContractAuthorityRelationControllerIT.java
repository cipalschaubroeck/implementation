package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.DocumentContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:ContractAuthorityRelationControllerIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractAuthorityRelationControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }


    @Test
    public void test() {

        String authority = tester.create(URI + API_PATH + "/contracting-authority", "{\"name\":\"contracting authority\", \"nationalId\": \"1234567891012\"}")
                .read("$._links.self.href");
        DocumentContext authorityDocument = tester.read(authority);

        String contract = tester.create(URI + API_PATH + "/contract",
                "{\"name\":\"name of contract\"}")
                .read("$._links.self.href");
        DocumentContext contractDocument = tester.read(contract);
        DocumentContext response = tester.createOrUpdate(URI + API_PATH + "/contract-authority-relation",
                "{\"contract\": \"" + contractDocument.read("$._links.self.href") + "\" ,\"contractingAuthority\": \""
                        + authorityDocument.read("$._links.self.href") + "\"}", HttpMethod.PUT);
        Assert.assertEquals("NO_CONTENT", response.read("$"));
    }
}
