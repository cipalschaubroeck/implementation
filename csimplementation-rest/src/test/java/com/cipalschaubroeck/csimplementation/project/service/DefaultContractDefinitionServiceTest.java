package com.cipalschaubroeck.csimplementation.project.service;

import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.project.domain.Bail;
import com.cipalschaubroeck.csimplementation.project.domain.BailType;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import com.cipalschaubroeck.csimplementation.project.domain.ContractStatus;
import com.cipalschaubroeck.csimplementation.project.domain.Formula;
import com.cipalschaubroeck.csimplementation.project.domain.FormulaType;
import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import com.cipalschaubroeck.csimplementation.project.domain.ProcurementProcedureType;
import com.cipalschaubroeck.csimplementation.project.domain.ProcurementQualification;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import com.cipalschaubroeck.csimplementation.project.service.impl.DefaultContractDefinitionService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Optional;

@RunWith(SpringRunner.class)
public class DefaultContractDefinitionServiceTest {
    @Mock
    private ContractRepository repository;
    @InjectMocks
    private DefaultContractDefinitionService service;

    private Contract contract;

    @Before
    public void before() {
        contract = new Contract();
        contract.setId(4);
        contract.setContractId("2019-1");
        contract.setName("contract 1");

        Procurement procurement = new Procurement();
        procurement.setId(1);
        procurement.setName("procurement 1");

        contract.setProcurement(procurement);
        contract.setAmountMinusVat(new BigDecimal(1));
        contract.setDecisionMakingBody("decision making");
        contract.setAmountVat(new BigDecimal(1));
        contract.setParcel("Parcel 1");
        contract.setProcedureType(ProcurementProcedureType.ACCEPTED_INVOICE);
        contract.setQualification(ProcurementQualification.DELIVERIES);
        contract.setVat(new BigDecimal(1));
        contract.setProject("project 1");
        contract.setStatus(ContractStatus.IN_PREPARATION);
        ContractAuthorityRelation relation = new ContractAuthorityRelation();
        relation.setId(contract.getId());
        relation.setContract(contract);
        ContractingAuthority authority = new ContractingAuthority();
        authority.setId(1);
        authority.setName("name1");
        relation.setContractingAuthority(authority);
        contract.setContractAuthorityRelation(relation);
    }

    @Test
    public void isValidateInitiateContractProcessComplete() {

        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        // TODO CSPROC-2164 add validations as they are developed
        contract.setStatus(ContractStatus.IN_PROGRESS);
        Assert.assertEquals(FormStatus.READ_ONLY, service.getValidateInitiateContractProcessComplete(contract.getId()));
        contract.setName(null);
        contract.setStatus(ContractStatus.IN_PREPARATION);
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.getValidateInitiateContractProcessComplete(contract.getId()));
    }

    @Test
    public void validateInitiateContractData() {
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        Assert.assertEquals(FormStatus.COMPLETED, service.validateInitiateContractData(contract.getId()));
        contract.setName("");
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.validateInitiateContractData(contract.getId()));
    }

    @Test
    public void validateInitiateContractProcessComplete() {
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        contract.setStatus(ContractStatus.IN_PREPARATION);
        Assert.assertEquals(FormStatus.READ_ONLY, service.validateInitiateContractProcessComplete(contract.getId()));
        contract.setStatus(ContractStatus.IN_PROGRESS);
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.validateInitiateContractProcessComplete(contract.getId()));
    }

    @Test
    public void validateInitiateContractAuthority() {
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        Assert.assertEquals(FormStatus.COMPLETED, service.validateInitiateContractAuthority(contract.getId()));
        contract.setContractAuthorityRelation(null);
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.validateInitiateContractAuthority(contract.getId()));
    }

    @Test
    public void validateBail() {
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        Assert.assertEquals(FormStatus.COMPLETED, service.validateInitiateContractBail(contract.getId()));
        Bail bail = new Bail();
        bail.setType(BailType.SPECIAL_BAIL);
        contract.getProcurement().setBail(bail);
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.validateInitiateContractBail(contract.getId()));
    }

    @Test
    public void validatePricesReview() {
        Mockito.when(repository.findById(contract.getId())).thenReturn(Optional.of(contract));
        Assert.assertEquals(FormStatus.COMPLETED, service.validateInitiateContractPricesReview(contract.getId()));
        Formula formula = new Formula();
        formula.setType(FormulaType.FIXED);
        contract.setFormula(formula);
        Assert.assertEquals(FormStatus.NOT_COMPLETED, service.validateInitiateContractPricesReview(contract.getId()));
        contract.getFormula().setFormula("FIXED formula");
        Assert.assertEquals(FormStatus.COMPLETED, service.validateInitiateContractPricesReview(contract.getId()));
    }
}
