package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:BailControllerIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractBailControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }


    @Test
    public void test() {
        String contract = tester.create(URI + API_PATH + "/contract", "{\"name\":\"contract bail\"}")
                .read("$._links.self.href");
        tester.createOrUpdate(contract + "/bail", "{\"type\":\"BAIL\"}", HttpMethod.POST, HttpStatus.CREATED);
    }
}
