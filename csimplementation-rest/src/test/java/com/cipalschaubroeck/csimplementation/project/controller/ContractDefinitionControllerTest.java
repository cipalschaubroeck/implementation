package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.project.service.ContractDefinitionService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class ContractDefinitionControllerTest {
    @InjectMocks
    private ContractDefinitionController controller;
    @Mock
    private ContractDefinitionService service;

    @Test
    public void passToAdministration() {
        Integer contractId = 4;
        controller.setInProgress(contractId);
        Mockito.verify(service).validateInitiateContractProcessComplete(contractId);
    }

    @Test
    public void findContract() {
        Mockito.when(service.validateContractName("contract1", null)).thenReturn(true, false);
        Assert.assertEquals(Boolean.TRUE, controller.validateContractName("contract1").getBody());
        Assert.assertEquals(Boolean.FALSE, controller.validateContractName("contract2").getBody());

        Mockito.when(service.validateContractName("contract1", 1)).thenReturn(true, false);

        Assert.assertEquals(Boolean.FALSE, controller.validateContractName(1, "contract2").getBody());
        Assert.assertEquals(Boolean.TRUE, controller.validateContractName(1, "contract1").getBody());

    }
}
