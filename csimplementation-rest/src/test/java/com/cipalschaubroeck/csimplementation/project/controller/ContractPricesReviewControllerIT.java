package com.cipalschaubroeck.csimplementation.project.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = {"spring.datasource.url=jdbc:h2:mem:ContractPricesReviewControllerIT"})
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContractPricesReviewControllerIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }


    @Test
    public void test() {
        String contract = tester.create(URI + API_PATH + "/contract", "{\"name\":\"contract prices-review\"}")
                .read("$._links.self.href");
        tester.createOrUpdate(contract + "/prices-review", "{\"type\":\"CUSTOM\"}", HttpMethod.POST, HttpStatus.CREATED);
        String response = tester.read(contract + "/validate/initiate/prices-review-form").read("$");
        Assert.assertEquals("NOT_COMPLETED", response);

        tester.createOrUpdate(contract + "/prices-review", "{\"formula\": \"abc123\", \"type\": \"CUSTOM\"}", HttpMethod.POST, HttpStatus.CREATED);
        response = tester.read((contract + "/validate/initiate/prices-review-form")).read("$");
        Assert.assertEquals("COMPLETED", response);
    }
}
