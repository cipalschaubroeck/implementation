package com.cipalschaubroeck.csimplementation.project.domain.validator;

import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ExternalQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.InternalQualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPersonType;
import com.cipalschaubroeck.csimplementation.project.repository.QualifiedPersonRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
public class QualifiedPersonValidatorTest {
    @InjectMocks
    private QualifiedPersonValidator validator;
    @Mock
    private QualifiedPersonRepository repository;

    @Test
    public void supports() {
        Set<QualifiedPersonType> typesToTest = EnumSet.allOf(QualifiedPersonType.class);
        Collection<QualifiedPerson> instances = Arrays.asList(new AdHocQualifiedPerson(),
                new ExternalQualifiedPerson(), new InternalQualifiedPerson());
        instances.forEach(qp -> {
            Assert.assertTrue(validator.supports(qp.getClass()));
            typesToTest.remove(qp.getType());
        });
        Assert.assertEquals(0, typesToTest.size());
    }

    @Test
    public void validate() {
        Contract contract = Mockito.mock(Contract.class);
        List<QualifiedPerson> alreadyInDb = new ArrayList<>();
        Mockito.when(repository.findByContract(contract)).thenReturn(alreadyInDb);
        Errors errors = Mockito.mock(Errors.class);

        // first qualified never fails
        validator.validate(mockWithIdAndPersonId(contract, 1, 1), errors);
        validator.validate(mockWithIdAndPersonId(contract, null, 1), errors);
        Mockito.verifyZeroInteractions(errors);

        // qualified with no match, or same, never fails
        alreadyInDb.add(mockWithIdAndPersonId(contract, 2, 2));
        validator.validate(mockWithIdAndPersonId(contract, 1, 1), errors);
        validator.validate(mockWithIdAndPersonId(contract, null, 1), errors);
        validator.validate(alreadyInDb.get(0), errors);
        Mockito.verifyZeroInteractions(errors);

        // qualified with match fails
        Person person = Mockito.mock(Person.class);
        Mockito.when(person.getId()).thenReturn(2);
        AdHocQualifiedPerson qp = Mockito.mock(AdHocQualifiedPerson.class);
        Mockito.when(qp.getType()).thenCallRealMethod();
        Mockito.when(qp.getEffectivePerson()).thenReturn(person);
        Mockito.when(qp.getId()).thenReturn(1);
        Mockito.when(qp.getContract()).thenReturn(contract);
        validator.validate(qp, errors);
        Mockito.verify(errors).rejectValue(ArgumentMatchers.eq("person"), ArgumentMatchers.anyString());
    }

    private QualifiedPerson mockWithIdAndPersonId(Contract contract, Integer id, Integer personId) {
        Person person = Mockito.mock(Person.class);
        Mockito.when(person.getId()).thenReturn(personId);
        QualifiedPerson qp = Mockito.mock(QualifiedPerson.class);
        Mockito.when(qp.getEffectivePerson()).thenReturn(person);
        Mockito.when(qp.getId()).thenReturn(id);
        Mockito.when(qp.getContract()).thenReturn(contract);
        return qp;
    }

}
