package com.cipalschaubroeck.csimplementation.xperido.xml;

import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.project.domain.AdHocQualifiedPerson;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.xperido.generated.SigningOfficers;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.util.Collections;

public class SigningOfficersTransformerTest {
    private SigningOfficersTransformer transformer = new SigningOfficersTransformer();

    @Test
    public void supports() {
        Assert.assertTrue(transformer.supports(SigningOfficerData.class));
    }

    @Test
    public void transform() throws JAXBException, SAXException {
        SigningOfficerData data = new SigningOfficerData();
        SigningOfficer officer = new SigningOfficer();
        data.setSigningOfficers(Collections.singletonList(officer));
        AdHocQualifiedPerson person = new AdHocQualifiedPerson();
        person.setCompetence("competence");
        person.setQualification("qualification");
        Person actualPerson = new Person();
        person.setPerson(actualPerson);
        actualPerson.setLastName("lastname");
        actualPerson.setFirstName("firstname");
        officer.setPerson(person);
        officer.setData(data);

        SigningOfficers transformed = (SigningOfficers) transformer.transform(new PostalDocumentProcess(), data);
        Assert.assertEquals(transformed.getSigningOfficer().size(), data.getSigningOfficers().size());
        for (int i = 0; i < transformed.getSigningOfficer().size(); i++) {
            officer = data.getSigningOfficers().get(i);
            SigningOfficers.SigningOfficer tOfficer = transformed.getSigningOfficer().get(i);
            Assert.assertEquals(officer.getPerson().getCompetence(), tOfficer.getCompetence());
            Assert.assertEquals(officer.getPerson().getEffectivePerson().getFirstName(), tOfficer.getFirstName());
            Assert.assertEquals(officer.getPerson().getEffectivePerson().getLastName(), tOfficer.getLastName());
            Assert.assertEquals(officer.getPerson().getQualification(), tOfficer.getQualification());
        }

        XmlTransformHelp.checkWithSchema(transformed, data.getType());
    }
}
