package com.cipalschaubroeck.csimplementation.xperido.xml.bail;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.xperido.xml.XmlTransformHelp;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.math.BigDecimal;
import java.time.LocalDate;

public class BailRequestGuaranteeTransformerTest {

    private BailRequestGuaranteeTransformer transformer = new BailRequestGuaranteeTransformer();

    @Test
    public void supports() {
        Assert.assertTrue(transformer.supports(BailRequestGuarantee.class));
    }

    @Test
    public void transform() throws JAXBException, SAXException {
        PostalDocumentProcess process = new PostalDocumentProcess();
        BailRequestGuarantee brg = new BailRequestGuarantee();

        brg.setPhone(new PhoneNumber());
        brg.setCollaborator(new Collaborator());
        brg.getCollaborator().setPerson(new Person());
        brg.getCollaborator().getPerson().setFirstName("firstname");
        Department department = new Department();
        department.setName("department");
        brg.setDepartment(department);
        ContractingAuthority authority = new ContractingAuthority();
        authority.setName("authority");
        brg.getCollaborator().setAuthority(authority);
        brg.setReference("reference");
        brg.setBailAmount(BigDecimal.valueOf(320.23));
        brg.getPhone().setPhone("phone");
        process.setContract(new Contract());
        process.getContract().setName("name");
        process.getContract().setAwardingDate(LocalDate.now());
        process.getContract().setAmountMinusVat(BigDecimal.valueOf(32000));
        process.getContract().setAmountVat(BigDecimal.valueOf(6400));
        process.getContract().setDecisionMakingBody("decision making body");

        PostalAddressee addressee = new PostalAddressee();
        addressee.setProcess(process);
        com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee transformed =
                (com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee) transformer.transform(addressee, brg);
        Assert.assertEquals(transformed.getProject().getAmountMinusVat(),
                process.getContract().getAmountMinusVat());
        Assert.assertEquals(transformed.getProject().getAmountVat(),
                process.getContract().getAmountVat());
        Assert.assertEquals(transformed.getProject().getAmountBail(),
                brg.getBailAmount());
        Assert.assertEquals(transformed.getProject().getAwardingDate().toGregorianCalendar().toZonedDateTime().toLocalDate(),
                process.getContract().getAwardingDate());
        Assert.assertEquals(transformed.getProject().getDecisionMakingBody(),
                process.getContract().getDecisionMakingBody());
        Assert.assertEquals(transformed.getProject().getName(),
                process.getContract().getName());
        Assert.assertEquals(transformed.getProject().getReference(),
                brg.getReference());
        Assert.assertEquals(transformed.getClient().getContactDepartment(),
                brg.getDepartment().getName());
        Assert.assertEquals(transformed.getClient().getContactName(),
                brg.getCollaborator().getEffectivePerson().getFullName());
        Assert.assertEquals(transformed.getClient().getContactPhone(),
                brg.getPhone().getPhone());
        Assert.assertEquals(transformed.getClient().getContractingAuthority(),
                brg.getCollaborator().getAuthority().getName());

        XmlTransformHelp.checkWithSchema(transformed, brg.getType());
    }
}
