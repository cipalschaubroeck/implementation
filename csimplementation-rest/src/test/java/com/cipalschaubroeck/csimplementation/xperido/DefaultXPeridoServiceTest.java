package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthorityImageSettings;
import com.cipalschaubroeck.csimplementation.contractingauthority.repository.ContractingAuthorityImageSettingsRepository;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateConfig;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateFormat;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.xperido.generated.Heading;
import com.cipalschaubroeck.csimplementation.xperido.generated.SigningOfficers;
import com.greenvalley.docgen.domain.Document;
import com.greenvalley.docgen.domain.OutputType;
import com.greenvalley.docgen.domain.Template;
import com.greenvalley.docgen.domain.expression.Query;
import com.greenvalley.docgen.service.api.DocumentService;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.util.ReflectionTestUtils;

import javax.xml.bind.JAXBException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Locale;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class DefaultXPeridoServiceTest {

    @InjectMocks
    private DefaultXPeridoService service;
    @Mock
    private DocumentService documentService;
    @Mock
    private TemplateDataXmlTransformer transformer;
    @Mock
    private ContractingAuthorityImageSettingsRepository caisRepository;
    @Mock
    private CMCommunicationService contentManager;

    @Before
    public void prepare() {
        ReflectionTestUtils.setField(service, "xmlTransformers", Collections.singletonList(transformer));
    }

    @Test
    public void create() throws JAXBException {
        TemplateData data = Mockito.mock(TemplateData.class);
        TemplateConfig config = Mockito.mock(TemplateConfig.class);
        Mockito.when(data.getConfig()).thenReturn(config);
        String templateName = "templateName";
        Mockito.when(config.getTemplateName()).thenReturn(templateName);
        Mockito.when(data.getType()).thenReturn(TemplateType.HEADING);
        Template template = Mockito.mock(Template.class);
        Mockito.when(documentService.getTemplates(ArgumentMatchers.any(Query.class))).thenReturn(Collections.singletonList(template));
        Mockito.when(template.getName()).thenReturn(templateName);
        Mockito.when(transformer.supports(data.getClass())).thenReturn(true);
        PostalAddressee addressee = new PostalAddressee();
        Mockito.when(transformer.transform(addressee, data)).thenReturn(new Heading());

        Locale locale = Locale.getDefault();
        OutputType filetype = OutputType.HTML;
        String filename = "filename";

        service.createDocument(addressee, data, locale, filetype, filename);
        Mockito.verify(documentService).getTemplates(ArgumentMatchers.any(Query.class));
        Mockito.verify(transformer).supports(data.getClass());
        Mockito.verify(transformer).transform(addressee, data);
        Mockito.verify(documentService).createDocument(ArgumentMatchers.eq(template),
                ArgumentMatchers.eq(filename),
                ArgumentMatchers.eq(locale.toString().toUpperCase()),
                ArgumentMatchers.anyString(),
                ArgumentMatchers.anyList(),
                ArgumentMatchers.eq(filetype));
    }

    @Test
    public void getTemplates() {
        Locale locale = Locale.getDefault();
        TemplateType fileType = TemplateType.HEADING;
        boolean includeThumbs = true;
        service.getTemplates(locale, fileType, includeThumbs);
        Mockito.verify(documentService).getTemplates(ArgumentMatchers.any(Query.class), ArgumentMatchers.eq(includeThumbs));
        includeThumbs = false;
        service.getTemplates(locale, fileType, includeThumbs);
        Mockito.verify(documentService).getTemplates(ArgumentMatchers.any(Query.class), ArgumentMatchers.eq(includeThumbs));
    }

    @Test
    public void createDocument() throws IOException {
        PostalDocumentProcess process = new PostalDocumentProcess();
        Locale locale = Locale.getDefault();
        OutputType fileType = OutputType.HTML;
        String fileName = "fileName";
        Template combinedTemplate = Mockito.mock(Template.class);
        Template bodyTemplate = Mockito.mock(Template.class);
        Template officersTemplate = Mockito.mock(Template.class);
        Mockito.when(documentService.getTemplates(ArgumentMatchers.any(Query.class)))
                .thenReturn(Arrays.asList(combinedTemplate, bodyTemplate, officersTemplate));

        PostalAddressee addressee = new PostalAddressee();
        addressee.setProcess(process);
        addressee.setHeading(new com.cipalschaubroeck.csimplementation.template.domain.Heading());
        addressee.getHeading().getConfig().setText("html text");
        addressee.getHeading().getConfig().setTemplateFormat(TemplateFormat.TEXT);
        process.setBody(new BailRequestGuarantee());
        Mockito.when(bodyTemplate.getName()).thenReturn("bodyTemplate");
        process.getBody().getConfig().setTemplateName(bodyTemplate.getName());
        process.setSigningOfficerData(new SigningOfficerData());
        Mockito.when(officersTemplate.getName()).thenReturn("officersTemplate");
        process.getSigningOfficerData().getConfig().setTemplateName(officersTemplate.getName());

        Document docHeading = Mockito.mock(Document.class);
        Mockito.when(documentService.createDocument(ArgumentMatchers.eq(bodyTemplate), ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString(),
                ArgumentMatchers.anyList(), ArgumentMatchers.any(OutputType.class))).thenReturn(docHeading);
        Mockito.when(docHeading.getData()).thenReturn("heading data".getBytes());
        Document docOfficers = Mockito.mock(Document.class);
        Mockito.when(documentService.createDocument(ArgumentMatchers.eq(officersTemplate), ArgumentMatchers.anyString(),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyString(),
                ArgumentMatchers.anyList(), ArgumentMatchers.any(OutputType.class))).thenReturn(docOfficers);
        Mockito.when(docOfficers.getData()).thenReturn("officers data".getBytes());

        process.setContract(new Contract());
        process.getContract().setProcurement(new Procurement());
        process.getContract().setContractAuthorityRelation(new ContractAuthorityRelation());
        process.getContract().getContractAuthorityRelation().setContractingAuthority(new ContractingAuthority());
        process.getContract().getContractAuthorityRelation().getContractingAuthority().setId(1);
        ContractingAuthorityImageSettings imageSettings = new ContractingAuthorityImageSettings();
        Mockito.when(caisRepository.findById(process.getContract().getContractAuthorityRelation().getContractingAuthority().getId())).thenReturn(Optional.of(imageSettings));
        imageSettings.setRibbonId("ribbonId");
        org.apache.chemistry.opencmis.client.api.Document imageDoc = Mockito.mock(org.apache.chemistry.opencmis.client.api.Document.class);
        Mockito.when(contentManager.getDocumentById(imageSettings.getRibbonId())).thenReturn(imageDoc);
        Mockito.when(imageDoc.getContentStream()).thenReturn(Mockito.mock(ContentStream.class));
        Mockito.when(imageDoc.getContentStream().getStream()).thenReturn(new ByteArrayInputStream("image content".getBytes()));
        Mockito.when(imageDoc.getContentStream().getFileName()).thenReturn("image.jpg");
        Mockito.when(imageDoc.getContentStream().getMimeType()).thenReturn("image/jpg");

        Mockito.when(transformer.supports(process.getBody().getClass())).thenReturn(true);
        Mockito.when(transformer.transform(addressee, process.getBody())).thenReturn(new com.cipalschaubroeck.csimplementation.xperido.generated.BailRequestGuarantee());
        Mockito.when(transformer.supports(process.getSigningOfficerData().getClass())).thenReturn(true);
        Mockito.when(transformer.transform(addressee, process.getSigningOfficerData())).thenReturn(new SigningOfficers());

        service.createDocument(addressee, locale, fileType, fileName);
        Mockito.verify(documentService).createDocument(ArgumentMatchers.eq(combinedTemplate), ArgumentMatchers.eq(fileName),
                ArgumentMatchers.eq(locale.getLanguage().toUpperCase()),
                ArgumentMatchers.anyString(), ArgumentMatchers.anyList(), ArgumentMatchers.eq(fileType));
    }
}
