package com.cipalschaubroeck.csimplementation.xperido.xml;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.xperido.PrefixNamespaceMapper;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

public class XmlTransformHelp {
    private static final String DOC_URL_FORMAT = "http://www.greenvalley.com/document/%1$s %1$s.xsd";

    private XmlTransformHelp() {
        // utility class
    }

    /**
     * @param transformed a TemplateData object transformed and ready to be converted to xml
     * @param type        the type of the original TemplateData object
     * @throws JAXBException if there are problems with normal jaxb initialization
     * @throws SAXException  if there are validation errors
     */
    public static void checkWithSchema(Object transformed, TemplateType type) throws JAXBException, SAXException {
        JAXBContext jaxbContext = JAXBContext.newInstance(transformed.getClass());
        Marshaller marshaller = jaxbContext.createMarshaller();
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema schema = schemaFactory.newSchema(new StreamSource(XmlTransformHelp.class.getResourceAsStream("/template/schema/" + type.getSchemaName() + ".xsd")));
        marshaller.setSchema(schema);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_SCHEMA_LOCATION, String.format(DOC_URL_FORMAT, type.getSchemaName()));
        marshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new PrefixNamespaceMapper());
        marshaller.marshal(transformed, new DefaultHandler());
    }
}
