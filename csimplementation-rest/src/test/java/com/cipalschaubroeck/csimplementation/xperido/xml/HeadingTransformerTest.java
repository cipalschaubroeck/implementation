package com.cipalschaubroeck.csimplementation.xperido.xml;

import com.cipalschaubroeck.csimplementation.document.domain.AdHocAddressee;
import com.cipalschaubroeck.csimplementation.document.domain.PostalAddressee;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;

public class HeadingTransformerTest {
    private HeadingTransformer transformer = new HeadingTransformer();

    @Test
    public void supports() {
        Assert.assertTrue(transformer.supports(Heading.class));
    }

    @Test
    public void transform() throws JAXBException, SAXException {
        Heading data = new Heading();
        data.setSubject("subject");
        data.setRegisteredLetter(true);
        data.setNumberAttachments(3);
        data.setFrom("from");

        PostalAddressee addressee = new PostalAddressee();
        addressee.setAddressee(new AdHocAddressee());

        com.cipalschaubroeck.csimplementation.xperido.generated.Heading transformed =
                (com.cipalschaubroeck.csimplementation.xperido.generated.Heading) transformer.transform(addressee, data);
        Assert.assertEquals(data.getSubject(), transformed.getSubject());
        Assert.assertEquals(data.isRegisteredLetter(), transformed.isRegisteredLetter());
        Assert.assertEquals(data.getNumberAttachments(), transformed.getNumberAttachments());
        Assert.assertEquals(data.getFrom(), transformed.getFrom());
        // if this one fails, fields have been added or removed and the transformer is possibly not updated
        Assert.assertEquals(7, com.cipalschaubroeck.csimplementation.xperido.generated.Heading.class.getDeclaredFields().length);

        XmlTransformHelp.checkWithSchema(transformed, data.getType());
    }
}
