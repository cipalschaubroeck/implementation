package com.cipalschaubroeck.csimplementation.xperido;

import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.xperido.xml.HeadingTransformer;
import org.junit.Assert;
import org.junit.Test;

public class TemplateDataXmlTransformerTest {
    private HeadingTransformer transformer = new HeadingTransformer();

    @Test
    public void whenAddresseeNeededFailIfNotProvided() {
        PostalDocumentProcess process = new PostalDocumentProcess();
        Heading data = new Heading();
        try {
            transformer.transform(process, data);
            Assert.fail("Should throw exception");
        } catch (UnsupportedOperationException e) {
            // expected result
        }
    }
}
