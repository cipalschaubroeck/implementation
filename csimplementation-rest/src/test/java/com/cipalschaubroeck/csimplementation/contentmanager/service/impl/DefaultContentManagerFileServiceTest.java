package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import org.junit.Assert;
import org.junit.Test;

public class DefaultContentManagerFileServiceTest {
    private DefaultContentManagerFileService service = new DefaultContentManagerFileService();

    @Test
    public void duplicate() {
        ContentManagerFile original = new ContentManagerFile();
        original.setContentManagerId("cmid");
        original.setPublicPath("path");
        original.setId(1);
        original.setVersionNum(2);
        ContentManagerFile copy = service.duplicate(original);
        Assert.assertEquals(original.getContentManagerId(), copy.getContentManagerId());
        Assert.assertEquals(original.getPublicPath(), copy.getPublicPath());
        Assert.assertNull(copy.getId());
        Assert.assertNull(copy.getVersionNum());
    }
}
