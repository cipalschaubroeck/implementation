package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import org.apache.chemistry.opencmis.client.api.Document;
import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;

public class MockCMCommunicationsServiceTest {
    private MockCMCommunicationService service = new MockCMCommunicationService();

    @Test
    public void createReadDelete() {
        Document document = service.uploadFile("filename", "content".getBytes(),
                Arrays.asList("part1", "part2"), "text/plain");
        Assert.assertSame(document, service.getDocumentById(document.getId()));
        service.removeDocument(document.getId());
        Assert.assertNull(service.getDocumentById(document.getId()));
        Assert.assertEquals("filename", document.getName());
        Assert.assertEquals("filename", document.getContentStreamFileName());
        Assert.assertEquals("text/plain", document.getContentStreamMimeType());
    }
}
