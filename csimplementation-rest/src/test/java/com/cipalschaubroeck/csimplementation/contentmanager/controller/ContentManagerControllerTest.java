package com.cipalschaubroeck.csimplementation.contentmanager.controller;

import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.ByteArrayInputStream;
import java.io.IOException;

@RunWith(SpringRunner.class)
public class ContentManagerControllerTest {
    @InjectMocks
    private ContentManagerController controller;
    @Mock
    private CMCommunicationService service;

    @Test
    public void download() throws IOException {
        String id = "id";
        Document document = Mockito.mock(Document.class);
        Mockito.when(document.getContentStream()).thenReturn(Mockito.mock(ContentStream.class));
        Mockito.when(document.getContentStream().getStream()).thenReturn(new ByteArrayInputStream("content".getBytes()));
        Mockito.when(document.getContentStream().getFileName()).thenReturn("filename");
        Mockito.when(service.getDocumentById(id)).thenReturn(document);
        controller.download(id);
    }
}
