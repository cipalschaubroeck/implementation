package com.cipalschaubroeck.csimplementation.contentmanager.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.jayway.jsonpath.JsonPath;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class ContentManagerRepositoryIT {

    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Test
    public void crud() {
        tester.crudTest(URI + API_PATH + "/content-manager-file",
                "{\"contentManagerId\": \"id for content manager\", \"publicPath\" : \"path as seen by the user\"}",
                parsed -> {
                    assertEquals("id for content manager", parsed.read("$.contentManagerId"));
                    assertEquals("path as seen by the user", parsed.read("$.publicPath"));
                },
                "{\"contentManagerId\": \"changed id for content manager\", \"publicPath\" : \"path as seen by the user\"}",
                parsed -> {
                    assertEquals("changed id for content manager", parsed.read("$.contentManagerId"));
                    assertEquals("path as seen by the user", parsed.read("$.publicPath"));
                },
                "{\"publicPath\" : \"path/as/seen/by/the/user\"}",
                parsed -> {
                    assertEquals("changed id for content manager", parsed.read("$.contentManagerId"));
                    assertEquals("path/as/seen/by/the/user", parsed.read("$.publicPath"));
                });
    }

    @Test
    public void cmIdNotNull() {
        try {
            tester.create(URI + API_PATH + "/content-manager-file", "{\"publicPath\" : \"path as seen by the user\"}");
            Assert.fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.SC_BAD_REQUEST, ex.getRawStatusCode());
            assertNotNull(JsonPath.parse(ex.getResponseBodyAsString()).read("$.errors[0].message"));
        }
    }

    @Test
    public void pathNotNull() {
        try {
            tester.create(URI + API_PATH + "/content-manager-file", "{\"publicPath\" : \"path as seen by the user\"}");
            Assert.fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(HttpStatus.SC_BAD_REQUEST, ex.getRawStatusCode());
            assertNotNull(JsonPath.parse(ex.getResponseBodyAsString()).read("$.errors[0].message"));
        }
    }
}
