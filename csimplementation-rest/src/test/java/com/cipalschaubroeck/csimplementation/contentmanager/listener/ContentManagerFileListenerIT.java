package com.cipalschaubroeck.csimplementation.contentmanager.listener;

import com.cipalschaubroeck.csimplementation.contentmanager.domain.ContentManagerFile;
import com.cipalschaubroeck.csimplementation.contentmanager.repository.ContentManagerRepository;
import com.cipalschaubroeck.csimplementation.contentmanager.service.CMCommunicationService;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.repository.ContractRepository;
import org.apache.chemistry.opencmis.client.api.Document;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties",
        properties = {"spring.liquibase.change-log=classpath:db/data/db.changelog-test.xml",
                "spring.datasource.url=jdbc:h2:mem:test_with_data;MODE=MYSQL"})
public class ContentManagerFileListenerIT {

    @Autowired
    private ContentManagerRepository fileRepository;
    @Autowired
    private CMCommunicationService service;
    @Autowired
    private PostalDocumentProcessRepository processRepository;
    @Autowired
    private ContractRepository contractRepository;

    @Test
    public void deletesWhenExplicitlyRemoved() {
        ContentManagerFile cmf = new ContentManagerFile();
        cmf.setPublicPath("a/path");
        Document document = service.uploadFile("filename.txt", "content".getBytes(),
                Collections.singletonList("testing"), "text/plain");
        cmf.setContentManagerId(document.getId());
        fileRepository.save(cmf);
        Assert.assertNotNull(service.getDocumentById(cmf.getContentManagerId()));

        fileRepository.delete(cmf);
        Assert.assertNull(service.getDocumentById(cmf.getContentManagerId()));
    }

    @Test
    public void deletesWhenOrphan() {
        Contract contract = contractRepository.findById(1).orElseThrow(() -> new IllegalStateException("There should be a contract with id 1"));

        PostalDocumentProcess process = new PostalDocumentProcess();
        process.setName("process");
        process.setContract(contract);

        ContentManagerFile cmf = new ContentManagerFile();
        cmf.setPublicPath("a/path");
        Document document = service.uploadFile("filename.txt", "content".getBytes(),
                Collections.singletonList("testing"), "text/plain");
        cmf.setContentManagerId(document.getId());
        process.getAttachments().add(cmf);
        process = processRepository.save(process);

        Assert.assertNotNull(service.getDocumentById(cmf.getContentManagerId()));
        process.getAttachments().clear();
        processRepository.save(process);
        Assert.assertNull(service.getDocumentById(cmf.getContentManagerId()));
    }
}
