package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.ObjectFactory;
import org.apache.chemistry.opencmis.client.api.Repository;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.runtime.SessionFactoryImpl;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SessionFactoryImpl.class)
public class AlfrescoCMCommunicationServiceTest {
    private AlfrescoCMCommunicationService service = new AlfrescoCMCommunicationService();

    @Mock
    private SessionFactoryImpl factory;
    @Mock
    private Repository repository;
    @Mock
    private Session session;

    @Before
    public void prepare() {
        ReflectionTestUtils.setField(service, AlfrescoCMCommunicationService.class, "cmisUser", "user", String.class);
        ReflectionTestUtils.setField(service, AlfrescoCMCommunicationService.class, "cmisPass", "pass", String.class);
        ReflectionTestUtils.setField(service, AlfrescoCMCommunicationService.class, "cmisUrl", "url", String.class);
        ReflectionTestUtils.setField(service, AlfrescoCMCommunicationService.class, "rootFolder", "/folder", String.class);

        PowerMockito.mockStatic(SessionFactoryImpl.class);
        PowerMockito.when(SessionFactoryImpl.newInstance()).thenReturn(factory);
        PowerMockito.when(factory.getRepositories(ArgumentMatchers.anyMap())).thenReturn(Collections.singletonList(repository));
        PowerMockito.when(repository.createSession()).thenReturn(session);
    }

    @Test
    public void getDocumentById() {
        String id = "id";
        Document document = Mockito.mock(Document.class);
        Mockito.when(session.getObject(id)).thenReturn(document);
        Mockito.when(document.getObjectOfLatestVersion(ArgumentMatchers.anyBoolean())).thenReturn(document);
        Assert.assertSame(document, service.getDocumentById(id));
    }

    @Test
    public void removeDocument() {
        String id = "id";
        Document document = Mockito.mock(Document.class);
        Mockito.when(session.getObject(id)).thenReturn(document);
        service.removeDocument(id);
        Mockito.verify(document).delete();
    }

    @Test
    public void upload() {
        String filename = "filename";
        byte[] content = "content".getBytes();
        List<String> path = Arrays.asList("part1", "part2");
        String mime = "text/plain";

        Mockito.when(session.getObjectByPath("/folder/part1/part2")).thenThrow(new CmisObjectNotFoundException());
        Folder part1 = Mockito.mock(Folder.class);
        Mockito.when(session.getObjectByPath("/folder/part1")).thenReturn(part1);
        Folder part2 = Mockito.mock(Folder.class);
        Mockito.when(part1.createFolder(ArgumentMatchers.anyMap())).thenReturn(part2);
        ObjectFactory objectFactory = Mockito.mock(ObjectFactory.class);
        Mockito.when(session.getObjectFactory()).thenReturn(objectFactory);
        ContentStream contentStream = Mockito.mock(ContentStream.class);
        Mockito.when(objectFactory.createContentStream(ArgumentMatchers.any(),
                ArgumentMatchers.anyInt(), ArgumentMatchers.any(), ArgumentMatchers.any()))
                .thenReturn(contentStream);

        service.uploadFile(filename, content, path, mime);
        Mockito.verify(part2).createDocument(ArgumentMatchers.anyMap(), ArgumentMatchers.any(), ArgumentMatchers.any(VersioningState.class));
    }
}
