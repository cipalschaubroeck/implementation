package com.cipalschaubroeck.csimplementation.contentmanager.service.impl;

import org.apache.chemistry.opencmis.client.api.OperationContext;
import org.apache.chemistry.opencmis.commons.enums.AclPropagation;
import org.apache.chemistry.opencmis.commons.enums.ExtensionLevel;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.math.BigInteger;
import java.util.Collections;

/**
 * this test class is to make sure MockCMCommunicationsServiceTest's errors are not from DocumentMock
 * and to avoid artificially lowering the coverage due to not testing
 */
public class DocumentMockTest {
    private DocumentMock document = new DocumentMock("id", "name", "filename",
            "content".getBytes(), "text/plain");

    @Test
    public void inheritedFields() {
        Assert.assertNotNull(document.getDocumentType());
        Assert.assertNotNull(document.getContentStream());
        Assert.assertNotNull(document.getContentStream(BigInteger.ONE, BigInteger.TEN));
        Assert.assertNotNull(document.getContentStream("id"));
        Assert.assertNotNull(document.getContentStream("id", BigInteger.ONE, BigInteger.TEN));
        Assert.assertNotNull(document.getAllVersions());
        Assert.assertNotNull(document.getContentStreamHashes());
        Assert.assertNotNull(document.getParents());
        Assert.assertNotNull(document.getPaths());
        Assert.assertNotNull(document.getRelationships());
        Assert.assertNotNull(document.getPermissionsForPrincipal(null));
        Assert.assertNotNull(document.getParents(null));
        Assert.assertNotNull(document.getAllVersions(Mockito.mock(OperationContext.class)));
        Assert.assertNotNull(document.getRenditions());
        Assert.assertNotNull(document.getPolicies());
        Assert.assertNotNull(document.getPolicyIds());
        Assert.assertNotNull(document.getExtensions(ExtensionLevel.ACL));
        Assert.assertNotNull(document.getProperties());
        Assert.assertNotNull(document.getSecondaryTypes());
        Assert.assertNotNull(document.findObjectType("id"));

        Assert.assertFalse(document.isVersionable());
        Assert.assertFalse(document.isVersionSeriesPrivateWorkingCopy());
        Assert.assertFalse(document.isImmutable());
        Assert.assertFalse(document.isLatestMajorVersion());
        Assert.assertFalse(document.isLatestVersion());
        Assert.assertFalse(document.isVersionSeriesCheckedOut());
        Assert.assertFalse(document.isMajorVersion());
        Assert.assertFalse(document.isPrivateWorkingCopy());
        Assert.assertFalse(document.hasAllowableAction(null));

        Assert.assertNull(document.getContentUrl());
        Assert.assertNull(document.getContentUrl("id"));
        Assert.assertNull(document.getVersionLabel());
        Assert.assertNull(document.getVersionSeriesId());
        Assert.assertNull(document.getVersionSeriesCheckedOutBy());
        Assert.assertNull(document.getVersionSeriesCheckedOutId());
        Assert.assertNull(document.getCheckinComment());
        Assert.assertNull(document.setContentStream(null, false));
        Assert.assertNull(document.setContentStream(null, false, false));
        Assert.assertNull(document.appendContentStream(null, false));
        Assert.assertNull(document.appendContentStream(null, false, false));
        Assert.assertNull(document.deleteContentStream());
        Assert.assertNull(document.deleteContentStream(false));
        Assert.assertNull(document.createOverwriteOutputStream("name", "mime"));
        Assert.assertNull(document.createOverwriteOutputStream("name", "mime", 100));
        Assert.assertNull(document.createAppendOutputStream());
        Assert.assertNull(document.createAppendOutputStream(20));
        Assert.assertNull(document.checkOut());
        Assert.assertNull(document.checkIn(false, Collections.emptyMap(), null, "comment",
                Collections.emptyList(), Collections.emptyList(), Collections.emptyList()));
        Assert.assertNull(document.checkIn(false, Collections.emptyMap(), null, "comment"));
        Assert.assertNull(document.getObjectOfLatestVersion(false));
        Assert.assertNull(document.getObjectOfLatestVersion(false, null));
        Assert.assertNull(document.copy(null));
        Assert.assertNull(document.copy(null, Collections.emptyMap(), VersioningState.NONE,
                Collections.emptyList(), Collections.emptyList(), Collections.emptyList(), null));
        Assert.assertNull(document.getContentStreamId());
        Assert.assertNull(document.getAllowableActions());
        Assert.assertNull(document.getAcl());
        Assert.assertNull(document.move(null, null));
        Assert.assertNull(document.move(null, null, null));
        Assert.assertNull(document.updateProperties(Collections.emptyMap()));
        Assert.assertNull(document.updateProperties(Collections.emptyMap(), Collections.emptyList(),
                Collections.emptyList()));
        Assert.assertNull(document.updateProperties(Collections.emptyMap(), Collections.emptyList(),
                Collections.emptyList(), false));
        Assert.assertNull(document.updateProperties(Collections.emptyMap(), false));
        Assert.assertNull(document.rename("newName"));
        Assert.assertNull(document.rename("newName", false));
        Assert.assertNull(document.getLatestAccessibleStateId());
        Assert.assertNull(document.applyAcl(Collections.emptyList(), Collections.emptyList(), AclPropagation.PROPAGATE));
        Assert.assertNull(document.addAcl(Collections.emptyList(), AclPropagation.PROPAGATE));
        Assert.assertNull(document.removeAcl(Collections.emptyList(), AclPropagation.PROPAGATE));
        Assert.assertNull(document.setAcl(Collections.emptyList()));
        Assert.assertNull(document.getAdapter(Object.class));
        Assert.assertNull(document.getProperty("id"));
        Assert.assertNull(document.getPropertyValue("id"));
        Assert.assertNull(document.getDescription());
        Assert.assertNull(document.getCreatedBy());
        Assert.assertNull(document.getCreationDate());
        Assert.assertNull(document.getLastModifiedBy());
        Assert.assertNull(document.getLastModificationDate());
        Assert.assertNull(document.getBaseTypeId());
        Assert.assertNull(document.getBaseType());
        Assert.assertNull(document.getType());
        Assert.assertNull(document.getChangeToken());

        Assert.assertEquals(0, document.getContentStreamLength());
        Assert.assertEquals(0, document.getRefreshTimestamp());
    }

    @Test
    public void unimplemented() {
        document.deleteAllVersions();
        document.cancelCheckOut();
        document.addToFolder(null, false);
        document.removeFromFolder(null);
        document.delete();
        document.deleteAllVersions();
        document.applyPolicy();
        document.applyPolicy(null, false);
        document.removePolicy();
        document.removePolicy(null, false);
        document.refresh();
        document.refreshIfOld(20);
    }
}
