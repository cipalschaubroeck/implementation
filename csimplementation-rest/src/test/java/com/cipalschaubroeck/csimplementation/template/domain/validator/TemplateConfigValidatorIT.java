package com.cipalschaubroeck.csimplementation.template.domain.validator;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.cipalschaubroeck.csimplementation.template.domain.TemplateType;
import com.cipalschaubroeck.csimplementation.xperido.XPeridoService;
import com.jayway.jsonpath.JsonPath;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Locale;
import java.util.Optional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class TemplateConfigValidatorIT implements MessageSourceAware {

    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    private RepositoryTester tester;

    private MessageSource messages;

    @MockBean
    private XPeridoService service;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Override
    public void setMessageSource(MessageSource messageSource) {
        this.messages = messageSource;
    }

    @Test
    public void validatorFails() {
        Mockito.when(service.getTemplate("name", TemplateType.SIGNING_OFFICERS)).thenReturn(Optional.empty());

        try {
            tester.create(URI + API_PATH + "/signing-officer-data", "{ \"type\" : \"SIGNING_OFFICERS\", \"config\":{ \"templateName\":\"name\"}}")
                    .read("$._links.self.href");
            Assert.fail();
        } catch (HttpClientErrorException ex) {
            assertEquals(400, ex.getRawStatusCode());
            assertEquals(messages.getMessage("template-config.type.data-equals-template", null, Locale.getDefault()),
                    JsonPath.parse(ex.getResponseBodyAsString()).read("$.errors[0].message"));
        }
    }
}
