package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class SigningOfficerDataRepositoryIT {
    @LocalServerPort
    private int port;

    private RepositoryTester tester;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crud() {
        // empty json because only possible changes are the collection of signing officers, and then by changing that signing officer
        tester.crudTest(URI + API_PATH + "/signing-officer-data",
                "{}",
                parsed -> assertEquals("SIGNING_OFFICERS", parsed.read("$.type")),
                "{}",
                parsed -> assertEquals("SIGNING_OFFICERS", parsed.read("$.type")),
                "{}",
                parsed -> assertEquals("SIGNING_OFFICERS", parsed.read("$.type")));
    }
}
