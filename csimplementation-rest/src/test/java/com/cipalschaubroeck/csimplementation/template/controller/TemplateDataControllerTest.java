package com.cipalschaubroeck.csimplementation.template.controller;

import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import com.greenvalley.docgen.domain.Template;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
public class TemplateDataControllerTest {
    @InjectMocks
    private TemplateDataController controller;
    @Mock
    private TemplateDataService service;

    @Test
    public void availableTemplates() {
        Integer id = 4;
        boolean thumbs = true;
        List<Template> templates = new ArrayList<>();
        Mockito.when(service.getAvailableTemplates(id, thumbs)).thenReturn(templates);

        Assert.assertEquals(templates, controller.getAvailableTemplates(id, thumbs).getBody());
        Mockito.verify(service).getAvailableTemplates(id, thumbs);
    }
}
