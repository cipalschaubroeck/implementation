package com.cipalschaubroeck.csimplementation.template.domain.validator;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.ContractingAuthority;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Department;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import com.cipalschaubroeck.csimplementation.project.domain.Contract;
import com.cipalschaubroeck.csimplementation.project.domain.ContractAuthorityRelation;
import com.cipalschaubroeck.csimplementation.project.domain.Procurement;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.validation.Errors;

import java.util.Collections;

@RunWith(SpringRunner.class)
public class BailRequestGuaranteeValidatorTest {
    @InjectMocks
    private BailRequestGuaranteeValidator validator;
    @Mock
    private PostalDocumentProcessRepository repository;

    private PostalDocumentProcess configureProcess(BailRequestGuarantee data) {
        if (data.getId() == null) {
            data.setId(2);
        }
        PostalDocumentProcess process = Mockito.mock(PostalDocumentProcess.class);
        Mockito.when(process.getStatus()).thenReturn(DocumentStatus.IN_PROGRESS);
        Mockito.when(process.getContract()).thenReturn(new Contract());
        process.getContract().setProcurement(new Procurement());
        process.getContract().setContractAuthorityRelation(new ContractAuthorityRelation());
        process.getContract().getContractAuthorityRelation().setContractingAuthority(new ContractingAuthority());
        process.getContract().getContractAuthorityRelation().getContractingAuthority().setId(1);
        Mockito.when(repository.findAllByBodyId(data.getId())).thenReturn(Collections.singletonList(process));
        return process;
    }

    @Test
    public void checkCollaborator() {
        BailRequestGuarantee data = new BailRequestGuarantee();
        configureCollaborator(data);
        configureProcess(data);

        Errors errors = Mockito.mock(Errors.class);
        validator.validate(data, errors);
        Mockito.verifyZeroInteractions(errors);

        configureDepartment(data);
        validator.validate(data, errors);
        Mockito.verifyZeroInteractions(errors);

    }

    @Test
    public void checkPhone() {
        BailRequestGuarantee data = new BailRequestGuarantee();
        configureCollaborator(data);
        configurePhoneInProgress(data);
        PostalDocumentProcess process = configureProcess(data);

        Errors errors = Mockito.mock(Errors.class);
        validator.validate(data, errors);
        Mockito.verifyZeroInteractions(errors);

        Mockito.when(process.getStatus()).thenReturn(DocumentStatus.FOR_SIGNATURE);
        validator.validate(data, errors);
        Mockito.verify(errors).rejectValue(ArgumentMatchers.eq("phone"), ArgumentMatchers.anyString());

        Mockito.reset(errors);
        data.setPhone(new PhoneNumber());
        data.getPhone().setId(2);
        validator.validate(data, errors);
        Mockito.verifyZeroInteractions(errors);
    }

    private void configureCollaborator(BailRequestGuarantee data) {
        data.setCollaborator(new Collaborator());
        data.getCollaborator().setAuthority(new ContractingAuthority());
        data.getCollaborator().getAuthority().setId(1);
    }

    private void configureDepartment(BailRequestGuarantee data) {
        data.setDepartment(new Department());
        data.getDepartment().setId(1);
    }

    private void configurePhoneInProgress(BailRequestGuarantee data) {
        PostalDocumentProcess process = Mockito.mock(PostalDocumentProcess.class);
        Mockito.when(repository.findAllByBodyId(data.getId())).thenReturn(Collections.singletonList(process));
        Mockito.when(process.getStatus()).thenReturn(DocumentStatus.IN_PROGRESS);
        PhoneNumber phone = new PhoneNumber();
        phone.setId(1);
        data.setPhone(phone);
        data.getCollaborator().setPerson(new Person());
        data.getCollaborator().getPerson().getPhoneNumbers().add(phone);
    }
}
