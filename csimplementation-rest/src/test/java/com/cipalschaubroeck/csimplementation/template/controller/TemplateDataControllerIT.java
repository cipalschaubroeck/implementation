package com.cipalschaubroeck.csimplementation.template.controller;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDataService;
import com.jayway.jsonpath.DocumentContext;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class TemplateDataControllerIT {
    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    private RepositoryTester tester;

    @SpyBean
    private TemplateDataService service;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    /**
     * if this test fails, probably TemplateDataController.getAvailableTemplates has changed
     * its request mapping
     */
    @Test
    public void availableTemplates() {
        DocumentContext data = tester.create(URI + API_PATH + "/signing-officer-data", "{}");
        String uri = data.read("$._links.self.href");
        Integer id = Integer.parseInt(uri.substring(1 + uri.lastIndexOf('/')));
        String availableUri = data.read("$._links.available-templates.href");
        availableUri = availableUri.replaceAll("\\{[^}]+}", "");

        tester.read(availableUri);
        Mockito.verify(service)
                .getAvailableTemplates(ArgumentMatchers.eq(id),
                        ArgumentMatchers.eq(false));

        tester.read(availableUri + "?thumbnails=true");
        Mockito.verify(service)
                .getAvailableTemplates(ArgumentMatchers.eq(id),
                        ArgumentMatchers.eq(true));

        // if this fails, then generate method changed its mapping
        String generateUri = data.read("$._links.generate.href");
        generateUri = generateUri.replaceAll("\\{[^}]+}", "");
        Assert.assertEquals(uri.replace("signing-officer-data", "template-data") + "/generate", generateUri);
    }
}
