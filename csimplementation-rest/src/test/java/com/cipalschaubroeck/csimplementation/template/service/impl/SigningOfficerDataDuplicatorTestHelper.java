package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.project.domain.QualifiedPerson;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDuplicatorTestHelper;
import org.junit.Assert;
import org.mockito.Mockito;

import java.util.Collections;

public class SigningOfficerDataDuplicatorTestHelper extends TemplateDuplicatorTestHelper<SigningOfficerData> {
    @Override
    protected SigningOfficerData getInstance() {
        return new SigningOfficerData();
    }

    @Override
    public SigningOfficerData create() {
        SigningOfficerData data = super.create();
        SigningOfficer officer = new SigningOfficer();
        officer.setData(data);
        officer.setPerson(Mockito.mock(QualifiedPerson.class));
        data.setSigningOfficers(Collections.singletonList(officer));
        return data;
    }

    @Override
    public void checkDuplicate(SigningOfficerData realOne, SigningOfficerData newOne) {
        super.checkDuplicate(realOne, newOne);
        int realOneSize = realOne.getSigningOfficers().size();
        Assert.assertEquals(realOneSize, newOne.getSigningOfficers().size());
        for (int i = 0; i < realOneSize; i++) {
            Assert.assertEquals(realOne.getSigningOfficers().get(i).getPerson(),
                    newOne.getSigningOfficers().get(i).getPerson());
            Assert.assertNotEquals(realOne.getSigningOfficers().get(i).getData(),
                    newOne.getSigningOfficers().get(i).getData());
        }
    }
}
