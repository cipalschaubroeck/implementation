package com.cipalschaubroeck.csimplementation.template.listener;

import com.cipalschaubroeck.csimplementation.bail.domain.BailRequestGuarantee;
import com.cipalschaubroeck.csimplementation.contractingauthority.domain.Collaborator;
import com.cipalschaubroeck.csimplementation.document.domain.DocumentStatus;
import com.cipalschaubroeck.csimplementation.document.domain.PostalDocumentProcess;
import com.cipalschaubroeck.csimplementation.document.repository.PostalDocumentProcessRepository;
import com.cipalschaubroeck.csimplementation.organization.service.ContactInfoSupport;
import com.cipalschaubroeck.csimplementation.person.domain.Person;
import com.cipalschaubroeck.csimplementation.person.domain.PhoneNumber;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;

@RunWith(SpringRunner.class)
public class BailRequestGuaranteeListenerTest {
    @InjectMocks
    private BailRequestGuaranteeListener listener;
    @Mock
    private ContactInfoSupport support;
    @Mock
    private PostalDocumentProcessRepository repository;

    @Test
    public void checkPhone() {
        BailRequestGuarantee data = new BailRequestGuarantee();
        listener.checkPhone(data);

        data.setId(1);
        Mockito.when(repository.findAllByBodyId(data.getId())).thenReturn(Collections.emptyList());
        listener.checkPhone(data);

        PostalDocumentProcess process = Mockito.mock(PostalDocumentProcess.class);
        Mockito.when(process.getStatus()).thenReturn(DocumentStatus.IN_PROGRESS);
        Mockito.when(repository.findAllByBodyId(data.getId())).thenReturn(Collections.singletonList(process));
        listener.checkPhone(data);

        PhoneNumber phone = new PhoneNumber();
        data.setPhone(phone);
        listener.checkPhone(data);

        // there should be zero interactions yet
        Mockito.verifyZeroInteractions(support);

        Mockito.when(process.getStatus()).thenReturn(DocumentStatus.FOR_SIGNATURE);
        PhoneNumber newOne = new PhoneNumber();
        data.setCollaborator(new Collaborator());
        data.getCollaborator().setPerson(new Person());
        Mockito.when(support.copyIfNeeded(phone, data.getCollaborator().getEffectivePerson().getPhoneNumbers()))
                .thenReturn(newOne);
        listener.checkPhone(data);
        Mockito.verify(support).copyIfNeeded(phone, data.getCollaborator().getEffectivePerson().getPhoneNumbers());
    }
}
