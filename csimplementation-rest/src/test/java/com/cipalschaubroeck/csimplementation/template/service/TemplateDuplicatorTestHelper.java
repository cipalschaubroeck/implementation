package com.cipalschaubroeck.csimplementation.template.service;

import com.cipalschaubroeck.csimplementation.template.domain.TemplateData;
import org.junit.Assert;

import java.util.Random;

public abstract class TemplateDuplicatorTestHelper<T extends TemplateData> {
    private static final Random RANDOM = new Random();

    /**
     * @return instance, just to avoid class instancing stuff
     */
    protected abstract T getInstance();

    /**
     * Designed to override and use from super
     *
     * @return an instance with all fields filled
     */
    public T create() {
        T t = getInstance();
        t.setId(RANDOM.nextInt());
        t.setVersionNum(RANDOM.nextInt());
        return t;
    }

    /**
     * designed to be override and use from super
     *
     * @param realOne the instance with all the fields filled
     * @param newOne  the instance that was duplicated
     */
    public void checkDuplicate(T realOne, T newOne) {
        Assert.assertNotNull(realOne.getId());
        Assert.assertNull(newOne.getId());
        Assert.assertNotNull(realOne.getVersionNum());
        Assert.assertNull(newOne.getVersionNum());
    }
}
