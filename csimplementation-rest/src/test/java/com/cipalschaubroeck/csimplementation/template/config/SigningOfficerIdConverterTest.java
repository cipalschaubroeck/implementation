package com.cipalschaubroeck.csimplementation.template.config;

import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficer;
import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerId;
import org.junit.Assert;
import org.junit.Test;

public class SigningOfficerIdConverterTest {
    private SigningOfficerIdConverter converter = new SigningOfficerIdConverter();

    @Test
    public void supports() {
        Assert.assertTrue(converter.supports(SigningOfficer.class));
    }

    @Test
    public void convert() {
        SigningOfficerId id = new SigningOfficerId();
        id.setData(1);
        id.setPerson(2);
        SigningOfficerId answer = (SigningOfficerId) converter.fromRequestId(
                converter.toRequestId(id, SigningOfficer.class), SigningOfficer.class);
        Assert.assertEquals(answer, id);
    }
}
