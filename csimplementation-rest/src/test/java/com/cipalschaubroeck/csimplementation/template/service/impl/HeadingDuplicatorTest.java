package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import org.junit.Test;

public class HeadingDuplicatorTest {
    private HeadingDuplicatorTestHelper helper = new HeadingDuplicatorTestHelper();
    private HeadingDuplicator duplicator = new HeadingDuplicator();

    @Test
    public void duplicate() {
        Heading original = helper.create();
        Heading copy = (Heading) duplicator.duplicate(original);
        helper.checkDuplicate(original, copy);
    }
}
