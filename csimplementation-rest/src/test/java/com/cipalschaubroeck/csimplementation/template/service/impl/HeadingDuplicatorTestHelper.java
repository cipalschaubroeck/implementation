package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.template.domain.Heading;
import com.cipalschaubroeck.csimplementation.template.service.TemplateDuplicatorTestHelper;
import org.junit.Assert;

public class HeadingDuplicatorTestHelper extends TemplateDuplicatorTestHelper<Heading> {
    @Override
    protected Heading getInstance() {
        return new Heading();
    }

    @Override
    public Heading create() {
        Heading data = super.create();
        data.setSubject("subject");
        data.setRegisteredLetter(true);
        data.setNumberAttachments(5);
        data.setFrom("from");
        return data;
    }

    @Override
    public void checkDuplicate(Heading realOne, Heading newOne) {
        super.checkDuplicate(realOne, newOne);
        Assert.assertEquals(realOne.getFrom(), newOne.getFrom());
        Assert.assertEquals(realOne.getSubject(), newOne.getSubject());
        Assert.assertEquals(realOne.getNumberAttachments(), newOne.getNumberAttachments());
        Assert.assertEquals(realOne.isRegisteredLetter(), newOne.isRegisteredLetter());
    }
}
