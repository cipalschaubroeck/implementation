package com.cipalschaubroeck.csimplementation.template.repository;

import com.cipalschaubroeck.csimplementation.shared.repository.RepositoryTester;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestPropertySource(locations = "classpath:application-it.properties")
public class HeadingConfigRepositoryIT {
    @LocalServerPort
    private int port;

    @Value("${server.servlet.context-path}")
    private String URI;

    @Value("${spring.data.rest.base-path}")
    private String API_PATH;

    private RepositoryTester tester;

    @Before
    public void prepare() {
        tester = new RepositoryTester(port);
    }

    @Test
    public void crud() {
        tester.crudTest(URI + API_PATH + "/heading",
                "{\"registeredLetter\": true, \"from\":\"origin of the text\", \"subject\":\"topic\", \"numberAttachments\":3}",
                parsed -> {
                    assertTrue(parsed.read("$.registeredLetter"));
                    assertEquals("origin of the text", parsed.read("$.from"));
                    assertEquals("topic", parsed.read("$.subject"));
                    assertEquals(3, (int) parsed.read("$.numberAttachments"));
                },
                "{\"registeredLetter\": false, \"from\":\"origin of the text\", \"subject\":\"topic\", \"numberAttachments\":3}",
                parsed -> {
                    assertFalse(parsed.read("$.registeredLetter"));
                    assertEquals("origin of the text", parsed.read("$.from"));
                    assertEquals("topic", parsed.read("$.subject"));
                    assertEquals(3, (int) parsed.read("$.numberAttachments"));
                },
                "{\"registeredLetter\": true}",
                parsed -> {
                    assertTrue(parsed.read("$.registeredLetter"));
                    assertEquals("origin of the text", parsed.read("$.from"));
                    assertEquals("topic", parsed.read("$.subject"));
                    assertEquals(3, (int) parsed.read("$.numberAttachments"));
                });
    }
}
