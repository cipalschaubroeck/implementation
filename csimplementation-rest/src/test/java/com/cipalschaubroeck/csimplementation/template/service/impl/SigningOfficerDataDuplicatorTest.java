package com.cipalschaubroeck.csimplementation.template.service.impl;

import com.cipalschaubroeck.csimplementation.template.domain.SigningOfficerData;
import org.junit.Test;

public class SigningOfficerDataDuplicatorTest {
    private SigningOfficerDataDuplicatorTestHelper helper = new SigningOfficerDataDuplicatorTestHelper();
    private SigningOfficerDataDuplicator duplicator = new SigningOfficerDataDuplicator();

    @Test
    public void duplicate() {
        SigningOfficerData original = helper.create();
        SigningOfficerData copy = (SigningOfficerData) duplicator.duplicate(original);
        helper.checkDuplicate(original, copy);
    }
}
