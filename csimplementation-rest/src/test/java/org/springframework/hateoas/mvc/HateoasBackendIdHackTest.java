package org.springframework.hateoas.mvc;

import org.junit.Test;
import org.springframework.core.convert.support.ConfigurableConversionService;

import java.util.Collection;
import java.util.Collections;

public class HateoasBackendIdHackTest {

    /**
     * This tests works as warning in case we change the version of hateoas and implementation has changed
     *
     * @throws NoSuchFieldException   the field has changed its name
     * @throws IllegalAccessException shouldn't happen since we're making the field accessible before returning it
     */
    @Test
    public void addConverter() throws NoSuchFieldException, IllegalAccessException {
        ConfigurableConversionService service = HateoasBackendIdHack.getControllerLinkBuilderConversionService();
        service.addConverter(String.class, Collection.class, Collections::singletonList);
    }
}
